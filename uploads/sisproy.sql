-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 27-12-2012 a las 09:13:02
-- Versión del servidor: 5.1.41
-- Versión de PHP: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `sisproy`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `authassignment`
--

CREATE TABLE IF NOT EXISTS `authassignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcar la base de datos para la tabla `authassignment`
--

INSERT INTO `authassignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('admin', '1', NULL, 'N;');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `authitem`
--

CREATE TABLE IF NOT EXISTS `authitem` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcar la base de datos para la tabla `authitem`
--

INSERT INTO `authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('admin', 2, NULL, NULL, 'N;'),
('CentroCosto.*', 1, NULL, NULL, 'N;'),
('CentroCosto.Actualizar', 0, NULL, NULL, 'N;'),
('CentroCosto.Admin', 0, NULL, NULL, 'N;'),
('CentroCosto.Crear', 0, NULL, NULL, 'N;'),
('CentroCosto.Create', 0, NULL, NULL, 'N;'),
('CentroCosto.Delete', 0, NULL, NULL, 'N;'),
('CentroCosto.GenerateExcel', 0, NULL, NULL, 'N;'),
('CentroCosto.GeneratePdf', 0, NULL, NULL, 'N;'),
('CentroCosto.Index', 0, NULL, NULL, 'N;'),
('CentroCosto.Update', 0, NULL, NULL, 'N;'),
('CentroCosto.Ver', 0, NULL, NULL, 'N;'),
('CentroCosto.VerTodos', 0, NULL, NULL, 'N;'),
('CentroCosto.View', 0, NULL, NULL, 'N;'),
('Contrato.*', 1, NULL, NULL, 'N;'),
('Contrato.Actualizar', 0, NULL, NULL, 'N;'),
('Contrato.Admin', 0, NULL, NULL, 'N;'),
('Contrato.Crear', 0, NULL, NULL, 'N;'),
('Contrato.Create', 0, NULL, NULL, 'N;'),
('Contrato.Delete', 0, NULL, NULL, 'N;'),
('Contrato.Finalizar', 0, NULL, NULL, 'N;'),
('Contrato.GenerateExcel', 0, NULL, NULL, 'N;'),
('Contrato.GeneratePdf', 0, NULL, NULL, 'N;'),
('Contrato.Index', 0, NULL, NULL, 'N;'),
('Contrato.Update', 0, NULL, NULL, 'N;'),
('Contrato.Ver', 0, NULL, NULL, 'N;'),
('Contrato.VerRrhh', 0, NULL, NULL, 'N;'),
('Contrato.VerTodos', 0, NULL, NULL, 'N;'),
('Contrato.View', 0, NULL, NULL, 'N;'),
('CuentaContable.*', 1, NULL, NULL, 'N;'),
('CuentaContable.Admin', 0, NULL, NULL, 'N;'),
('CuentaContable.Crear', 0, NULL, NULL, 'N;'),
('CuentaContable.Create', 0, NULL, NULL, 'N;'),
('CuentaContable.Delete', 0, NULL, NULL, 'N;'),
('CuentaContable.GenerateExcel', 0, NULL, NULL, 'N;'),
('CuentaContable.GeneratePdf', 0, NULL, NULL, 'N;'),
('CuentaContable.Index', 0, NULL, NULL, 'N;'),
('CuentaContable.Update', 0, NULL, NULL, 'N;'),
('CuentaContable.Ver', 0, NULL, NULL, 'N;'),
('CuentaContable.VerTodos', 0, NULL, NULL, 'N;'),
('CuentaContable.View', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.*', 1, NULL, NULL, 'N;'),
('CuentaEspecifica.Actualizar', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.Admin', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.Crear', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.Create', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.Delete', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.GenerateExcel', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.GeneratePdf', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.Index', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.Update', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.Ver', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.VerTodos', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.View', 0, NULL, NULL, 'N;'),
('Director Ejecutivo', 2, 'Director Ejecutivo', NULL, 'N;'),
('Donante.*', 1, NULL, NULL, 'N;'),
('Donante.Admin', 0, NULL, NULL, 'N;'),
('Donante.Create', 0, NULL, NULL, 'N;'),
('Donante.Delete', 0, NULL, NULL, 'N;'),
('Donante.GenerateExcel', 0, NULL, NULL, 'N;'),
('Donante.GeneratePdf', 0, NULL, NULL, 'N;'),
('Donante.Index', 0, NULL, NULL, 'N;'),
('Donante.Update', 0, NULL, NULL, 'N;'),
('Donante.View', 0, NULL, NULL, 'N;'),
('Emision.*', 1, NULL, NULL, 'N;'),
('Emision.Actualizar', 0, NULL, NULL, 'N;'),
('Emision.Admin', 0, NULL, NULL, 'N;'),
('Emision.Anular', 0, NULL, NULL, 'N;'),
('Emision.Crear', 0, NULL, NULL, 'N;'),
('Emision.Create', 0, NULL, NULL, 'N;'),
('Emision.Delete', 0, NULL, NULL, 'N;'),
('Emision.Finalizadas', 0, NULL, NULL, 'N;'),
('Emision.Finalizar', 0, NULL, NULL, 'N;'),
('Emision.GenerateExcel', 0, NULL, NULL, 'N;'),
('Emision.GeneratePdf', 0, NULL, NULL, 'N;'),
('Emision.Index', 0, NULL, NULL, 'N;'),
('Emision.Update', 0, NULL, NULL, 'N;'),
('Emision.Ver', 0, NULL, NULL, 'N;'),
('Emision.VerEmisionRendicion', 0, NULL, NULL, 'N;'),
('Emision.VerTodos', 0, NULL, NULL, 'N;'),
('Emision.View', 0, NULL, NULL, 'N;'),
('Financiamiento.*', 1, NULL, NULL, 'N;'),
('Financiamiento.Actualizar', 0, NULL, NULL, 'N;'),
('Financiamiento.Admin', 0, NULL, NULL, 'N;'),
('Financiamiento.Crear', 0, NULL, NULL, 'N;'),
('Financiamiento.Create', 0, NULL, NULL, 'N;'),
('Financiamiento.Delete', 0, NULL, NULL, 'N;'),
('Financiamiento.GenerateExcel', 0, NULL, NULL, 'N;'),
('Financiamiento.GeneratePdf', 0, NULL, NULL, 'N;'),
('Financiamiento.Index', 0, NULL, NULL, 'N;'),
('Financiamiento.Update', 0, NULL, NULL, 'N;'),
('Financiamiento.Ver', 0, NULL, NULL, 'N;'),
('Financiamiento.VerTodos', 0, NULL, NULL, 'N;'),
('Financiamiento.View', 0, NULL, NULL, 'N;'),
('Informe.*', 1, NULL, NULL, 'N;'),
('Informe.Crear', 0, NULL, NULL, 'N;'),
('Informe.Test', 0, NULL, NULL, 'N;'),
('Ingreso.*', 1, NULL, NULL, 'N;'),
('Ingreso.Admin', 0, NULL, NULL, 'N;'),
('Ingreso.Create', 0, NULL, NULL, 'N;'),
('Ingreso.Delete', 0, NULL, NULL, 'N;'),
('Ingreso.GenerateExcel', 0, NULL, NULL, 'N;'),
('Ingreso.GeneratePdf', 0, NULL, NULL, 'N;'),
('Ingreso.Index', 0, NULL, NULL, 'N;'),
('Ingreso.Update', 0, NULL, NULL, 'N;'),
('Ingreso.View', 0, NULL, NULL, 'N;'),
('IngresoProyecto.*', 1, NULL, NULL, 'N;'),
('IngresoProyecto.Admin', 0, NULL, NULL, 'N;'),
('IngresoProyecto.Create', 0, NULL, NULL, 'N;'),
('IngresoProyecto.Delete', 0, NULL, NULL, 'N;'),
('IngresoProyecto.GenerateExcel', 0, NULL, NULL, 'N;'),
('IngresoProyecto.GeneratePdf', 0, NULL, NULL, 'N;'),
('IngresoProyecto.Index', 0, NULL, NULL, 'N;'),
('IngresoProyecto.Update', 0, NULL, NULL, 'N;'),
('IngresoProyecto.View', 0, NULL, NULL, 'N;'),
('Inventario.*', 1, NULL, NULL, 'N;'),
('Inventario.Admin', 0, NULL, NULL, 'N;'),
('Inventario.Create', 0, NULL, NULL, 'N;'),
('Inventario.Delete', 0, NULL, NULL, 'N;'),
('Inventario.GenerateExcel', 0, NULL, NULL, 'N;'),
('Inventario.GeneratePdf', 0, NULL, NULL, 'N;'),
('Inventario.Index', 0, NULL, NULL, 'N;'),
('Inventario.Update', 0, NULL, NULL, 'N;'),
('Inventario.View', 0, NULL, NULL, 'N;'),
('ItemEmision.*', 1, NULL, NULL, 'N;'),
('ItemEmision.Admin', 0, NULL, NULL, 'N;'),
('ItemEmision.Agregar', 0, NULL, NULL, 'N;'),
('ItemEmision.Create', 0, NULL, NULL, 'N;'),
('ItemEmision.Delete', 0, NULL, NULL, 'N;'),
('ItemEmision.GenerateExcel', 0, NULL, NULL, 'N;'),
('ItemEmision.GeneratePdf', 0, NULL, NULL, 'N;'),
('ItemEmision.Index', 0, NULL, NULL, 'N;'),
('ItemEmision.Update', 0, NULL, NULL, 'N;'),
('ItemEmision.View', 0, NULL, NULL, 'N;'),
('ItemRendicion.*', 1, NULL, NULL, 'N;'),
('ItemRendicion.Actualizar', 0, NULL, NULL, 'N;'),
('ItemRendicion.Admin', 0, NULL, NULL, 'N;'),
('ItemRendicion.Agregar', 0, NULL, NULL, 'N;'),
('ItemRendicion.Create', 0, NULL, NULL, 'N;'),
('ItemRendicion.Delete', 0, NULL, NULL, 'N;'),
('ItemRendicion.GenerateExcel', 0, NULL, NULL, 'N;'),
('ItemRendicion.GeneratePdf', 0, NULL, NULL, 'N;'),
('ItemRendicion.GetCuenta', 0, NULL, NULL, 'N;'),
('ItemRendicion.Index', 0, NULL, NULL, 'N;'),
('ItemRendicion.Update', 0, NULL, NULL, 'N;'),
('ItemRendicion.Ver', 0, NULL, NULL, 'N;'),
('ItemRendicion.View', 0, NULL, NULL, 'N;'),
('ItemSolicitud.*', 1, NULL, NULL, 'N;'),
('ItemSolicitud.Actualizar', 0, NULL, NULL, 'N;'),
('ItemSolicitud.Admin', 0, NULL, NULL, 'N;'),
('ItemSolicitud.Agregar', 0, NULL, NULL, 'N;'),
('ItemSolicitud.Create', 0, NULL, NULL, 'N;'),
('ItemSolicitud.Delete', 0, NULL, NULL, 'N;'),
('ItemSolicitud.GenerateExcel', 0, NULL, NULL, 'N;'),
('ItemSolicitud.GeneratePdf', 0, NULL, NULL, 'N;'),
('ItemSolicitud.Index', 0, NULL, NULL, 'N;'),
('ItemSolicitud.Update', 0, NULL, NULL, 'N;'),
('ItemSolicitud.Ver', 0, NULL, NULL, 'N;'),
('ItemSolicitud.View', 0, NULL, NULL, 'N;'),
('Jefe de Proyecto', 2, 'Jefe de Proyecto', NULL, 'N;'),
('Jefe de Taller', 2, 'Jefe de Taller', NULL, 'N;'),
('Presupuesto.*', 1, NULL, NULL, 'N;'),
('Presupuesto.Actualizar', 0, NULL, NULL, 'N;'),
('Presupuesto.Admin', 0, NULL, NULL, 'N;'),
('Presupuesto.Crear', 0, NULL, NULL, 'N;'),
('Presupuesto.Create', 0, NULL, NULL, 'N;'),
('Presupuesto.Delete', 0, NULL, NULL, 'N;'),
('Presupuesto.GenerateExcel', 0, NULL, NULL, 'N;'),
('Presupuesto.GeneratePdf', 0, NULL, NULL, 'N;'),
('Presupuesto.Index', 0, NULL, NULL, 'N;'),
('Presupuesto.Update', 0, NULL, NULL, 'N;'),
('Presupuesto.Ver', 0, NULL, NULL, 'N;'),
('Presupuesto.VerTodos', 0, NULL, NULL, 'N;'),
('Presupuesto.View', 0, NULL, NULL, 'N;'),
('Proveedor.*', 1, NULL, NULL, 'N;'),
('Proveedor.Actualizar', 0, NULL, NULL, 'N;'),
('Proveedor.Admin', 0, NULL, NULL, 'N;'),
('Proveedor.Agregar', 0, NULL, NULL, 'N;'),
('Proveedor.Crear', 0, NULL, NULL, 'N;'),
('Proveedor.Create', 0, NULL, NULL, 'N;'),
('Proveedor.Delete', 0, NULL, NULL, 'N;'),
('Proveedor.GenerateExcel', 0, NULL, NULL, 'N;'),
('Proveedor.GeneratePdf', 0, NULL, NULL, 'N;'),
('Proveedor.GetTodos', 0, NULL, NULL, 'N;'),
('Proveedor.Index', 0, NULL, NULL, 'N;'),
('Proveedor.Update', 0, NULL, NULL, 'N;'),
('Proveedor.Ver', 0, NULL, NULL, 'N;'),
('Proveedor.VerTodos', 0, NULL, NULL, 'N;'),
('Proveedor.View', 0, NULL, NULL, 'N;'),
('Proyecto.*', 1, NULL, NULL, 'N;'),
('Proyecto.Actualizar', 0, NULL, NULL, 'N;'),
('Proyecto.Admin', 0, NULL, NULL, 'N;'),
('Proyecto.Crear', 0, NULL, NULL, 'N;'),
('Proyecto.Create', 0, NULL, NULL, 'N;'),
('Proyecto.Delete', 0, NULL, NULL, 'N;'),
('Proyecto.GenerateExcel', 0, NULL, NULL, 'N;'),
('Proyecto.GeneratePdf', 0, NULL, NULL, 'N;'),
('Proyecto.GetComuna', 0, NULL, NULL, 'N;'),
('Proyecto.Index', 0, NULL, NULL, 'N;'),
('Proyecto.Update', 0, NULL, NULL, 'N;'),
('Proyecto.Ver', 0, NULL, NULL, 'N;'),
('Proyecto.VerPresupuesto', 0, NULL, NULL, 'N;'),
('Proyecto.VerTodos', 0, NULL, NULL, 'N;'),
('Proyecto.View', 0, NULL, NULL, 'N;'),
('Rendicion.*', 1, NULL, NULL, 'N;'),
('Rendicion.Actualizar', 0, NULL, NULL, 'N;'),
('Rendicion.Admin', 0, NULL, NULL, 'N;'),
('Rendicion.Anular', 0, NULL, NULL, 'N;'),
('Rendicion.Crear', 0, NULL, NULL, 'N;'),
('Rendicion.Create', 0, NULL, NULL, 'N;'),
('Rendicion.Delete', 0, NULL, NULL, 'N;'),
('Rendicion.Finalizar', 0, NULL, NULL, 'N;'),
('Rendicion.GenerateExcel', 0, NULL, NULL, 'N;'),
('Rendicion.GeneratePdf', 0, NULL, NULL, 'N;'),
('Rendicion.Index', 0, NULL, NULL, 'N;'),
('Rendicion.Update', 0, NULL, NULL, 'N;'),
('Rendicion.Ver', 0, NULL, NULL, 'N;'),
('Rendicion.VerTodos', 0, NULL, NULL, 'N;'),
('Rendicion.View', 0, NULL, NULL, 'N;'),
('Rrhh.*', 1, NULL, NULL, 'N;'),
('Rrhh.Actualizar', 0, NULL, NULL, 'N;'),
('Rrhh.Admin', 0, NULL, NULL, 'N;'),
('Rrhh.Crear', 0, NULL, NULL, 'N;'),
('Rrhh.Create', 0, NULL, NULL, 'N;'),
('Rrhh.Delete', 0, NULL, NULL, 'N;'),
('Rrhh.GenerateExcel', 0, NULL, NULL, 'N;'),
('Rrhh.GeneratePdf', 0, NULL, NULL, 'N;'),
('Rrhh.Index', 0, NULL, NULL, 'N;'),
('Rrhh.Ver', 0, NULL, NULL, 'N;'),
('Rrhh.VerContrato', 0, NULL, NULL, 'N;'),
('Rrhh.VerTodos', 0, NULL, NULL, 'N;'),
('Rrhh.View', 0, NULL, NULL, 'N;'),
('Site.*', 1, NULL, NULL, 'N;'),
('Site.Contact', 0, NULL, NULL, 'N;'),
('Site.Error', 0, NULL, NULL, 'N;'),
('Site.Index', 0, NULL, NULL, 'N;'),
('Site.Login', 0, NULL, NULL, 'N;'),
('Site.Logout', 0, NULL, NULL, 'N;'),
('Solicitud.*', 1, NULL, NULL, 'N;'),
('Solicitud.Actualizar', 0, NULL, NULL, 'N;'),
('Solicitud.Admin', 0, NULL, NULL, 'N;'),
('Solicitud.Anular', 0, NULL, NULL, 'N;'),
('Solicitud.Crear', 0, NULL, NULL, 'N;'),
('Solicitud.CrearSolicitud', 0, NULL, NULL, 'N;'),
('Solicitud.Create', 0, NULL, NULL, 'N;'),
('Solicitud.Delete', 0, NULL, NULL, 'N;'),
('Solicitud.Finalizadas', 0, NULL, NULL, 'N;'),
('Solicitud.Finalizar', 0, NULL, NULL, 'N;'),
('Solicitud.GenerateExcel', 0, NULL, NULL, 'N;'),
('Solicitud.GeneratePdf', 0, NULL, NULL, 'N;'),
('Solicitud.Index', 0, NULL, NULL, 'N;'),
('Solicitud.Update', 0, NULL, NULL, 'N;'),
('Solicitud.Ver', 0, NULL, NULL, 'N;'),
('Solicitud.VerSolicitudEmision', 0, NULL, NULL, 'N;'),
('Solicitud.VerTodos', 0, NULL, NULL, 'N;'),
('Solicitud.View', 0, NULL, NULL, 'N;'),
('TipoContrato.*', 1, NULL, NULL, 'N;'),
('TipoContrato.Actualizar', 0, NULL, NULL, 'N;'),
('TipoContrato.Admin', 0, NULL, NULL, 'N;'),
('TipoContrato.Crear', 0, NULL, NULL, 'N;'),
('TipoContrato.Create', 0, NULL, NULL, 'N;'),
('TipoContrato.Delete', 0, NULL, NULL, 'N;'),
('TipoContrato.GenerateExcel', 0, NULL, NULL, 'N;'),
('TipoContrato.GeneratePdf', 0, NULL, NULL, 'N;'),
('TipoContrato.Index', 0, NULL, NULL, 'N;'),
('TipoContrato.Update', 0, NULL, NULL, 'N;'),
('TipoContrato.Ver', 0, NULL, NULL, 'N;'),
('TipoContrato.VerTodos', 0, NULL, NULL, 'N;'),
('TipoContrato.View', 0, NULL, NULL, 'N;'),
('TipoIngreso.*', 1, NULL, NULL, 'N;'),
('TipoIngreso.Actualizar', 0, NULL, NULL, 'N;'),
('TipoIngreso.Admin', 0, NULL, NULL, 'N;'),
('TipoIngreso.Crear', 0, NULL, NULL, 'N;'),
('TipoIngreso.Create', 0, NULL, NULL, 'N;'),
('TipoIngreso.Delete', 0, NULL, NULL, 'N;'),
('TipoIngreso.GenerateExcel', 0, NULL, NULL, 'N;'),
('TipoIngreso.GeneratePdf', 0, NULL, NULL, 'N;'),
('TipoIngreso.Index', 0, NULL, NULL, 'N;'),
('TipoIngreso.Update', 0, NULL, NULL, 'N;'),
('TipoIngreso.Ver', 0, NULL, NULL, 'N;'),
('TipoIngreso.VerTodos', 0, NULL, NULL, 'N;'),
('TipoIngreso.View', 0, NULL, NULL, 'N;'),
('User.*', 1, NULL, NULL, 'N;'),
('User.Admin', 0, NULL, NULL, 'N;'),
('User.Create', 0, NULL, NULL, 'N;'),
('User.Delete', 0, NULL, NULL, 'N;'),
('User.GenerateExcel', 0, NULL, NULL, 'N;'),
('User.GeneratePdf', 0, NULL, NULL, 'N;'),
('User.Index', 0, NULL, NULL, 'N;'),
('User.Update', 0, NULL, NULL, 'N;'),
('User.View', 0, NULL, NULL, 'N;');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `authitemchild`
--

CREATE TABLE IF NOT EXISTS `authitemchild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcar la base de datos para la tabla `authitemchild`
--

INSERT INTO `authitemchild` (`parent`, `child`) VALUES
('Director Ejecutivo', 'CentroCosto.Actualizar'),
('Director Ejecutivo', 'CentroCosto.Crear'),
('Director Ejecutivo', 'CentroCosto.GenerateExcel'),
('Director Ejecutivo', 'CentroCosto.GeneratePdf'),
('Director Ejecutivo', 'CentroCosto.Ver'),
('Director Ejecutivo', 'CentroCosto.VerTodos'),
('Director Ejecutivo', 'CuentaContable.Crear'),
('Jefe de Proyecto', 'CuentaContable.Crear'),
('Jefe de Taller', 'CuentaContable.Crear'),
('Director Ejecutivo', 'CuentaContable.GenerateExcel'),
('Jefe de Proyecto', 'CuentaContable.GenerateExcel'),
('Jefe de Taller', 'CuentaContable.GenerateExcel'),
('Director Ejecutivo', 'CuentaContable.GeneratePdf'),
('Jefe de Proyecto', 'CuentaContable.GeneratePdf'),
('Jefe de Taller', 'CuentaContable.GeneratePdf'),
('Director Ejecutivo', 'CuentaContable.Ver'),
('Jefe de Proyecto', 'CuentaContable.Ver'),
('Jefe de Taller', 'CuentaContable.Ver'),
('Director Ejecutivo', 'CuentaContable.VerTodos'),
('Jefe de Proyecto', 'CuentaContable.VerTodos'),
('Jefe de Taller', 'CuentaContable.VerTodos'),
('Director Ejecutivo', 'CuentaEspecifica.Actualizar'),
('Director Ejecutivo', 'CuentaEspecifica.Crear'),
('Director Ejecutivo', 'CuentaEspecifica.GenerateExcel'),
('Jefe de Proyecto', 'CuentaEspecifica.GenerateExcel'),
('Jefe de Taller', 'CuentaEspecifica.GenerateExcel'),
('Director Ejecutivo', 'CuentaEspecifica.GeneratePdf'),
('Jefe de Proyecto', 'CuentaEspecifica.GeneratePdf'),
('Jefe de Taller', 'CuentaEspecifica.GeneratePdf'),
('Director Ejecutivo', 'CuentaEspecifica.Ver'),
('Jefe de Proyecto', 'CuentaEspecifica.Ver'),
('Jefe de Taller', 'CuentaEspecifica.Ver'),
('Director Ejecutivo', 'Emision.Actualizar'),
('Jefe de Proyecto', 'Emision.Actualizar'),
('Jefe de Taller', 'Emision.Actualizar'),
('Director Ejecutivo', 'Emision.Anular'),
('Jefe de Proyecto', 'Emision.Anular'),
('Jefe de Taller', 'Emision.Anular'),
('Director Ejecutivo', 'Emision.Crear'),
('Jefe de Proyecto', 'Emision.Crear'),
('Jefe de Taller', 'Emision.Crear'),
('Director Ejecutivo', 'Emision.Finalizadas'),
('Jefe de Proyecto', 'Emision.Finalizadas'),
('Jefe de Taller', 'Emision.Finalizadas'),
('Director Ejecutivo', 'Emision.Finalizar'),
('Jefe de Proyecto', 'Emision.Finalizar'),
('Jefe de Taller', 'Emision.Finalizar'),
('Director Ejecutivo', 'Emision.GenerateExcel'),
('Jefe de Proyecto', 'Emision.GenerateExcel'),
('Jefe de Taller', 'Emision.GenerateExcel'),
('Director Ejecutivo', 'Emision.GeneratePdf'),
('Jefe de Proyecto', 'Emision.GeneratePdf'),
('Jefe de Taller', 'Emision.GeneratePdf'),
('Director Ejecutivo', 'Emision.Ver'),
('Jefe de Proyecto', 'Emision.Ver'),
('Jefe de Taller', 'Emision.Ver'),
('Director Ejecutivo', 'Emision.VerEmisionRendicion'),
('Jefe de Proyecto', 'Emision.VerEmisionRendicion'),
('Jefe de Taller', 'Emision.VerEmisionRendicion'),
('Director Ejecutivo', 'Emision.VerTodos'),
('Jefe de Proyecto', 'Emision.VerTodos'),
('Jefe de Taller', 'Emision.VerTodos'),
('Director Ejecutivo', 'ItemRendicion.Actualizar'),
('Jefe de Proyecto', 'ItemRendicion.Actualizar'),
('Jefe de Taller', 'ItemRendicion.Actualizar'),
('Director Ejecutivo', 'ItemRendicion.Agregar'),
('Jefe de Proyecto', 'ItemRendicion.Agregar'),
('Jefe de Taller', 'ItemRendicion.Agregar'),
('Director Ejecutivo', 'ItemRendicion.GenerateExcel'),
('Jefe de Proyecto', 'ItemRendicion.GenerateExcel'),
('Jefe de Taller', 'ItemRendicion.GenerateExcel'),
('Director Ejecutivo', 'ItemRendicion.GeneratePdf'),
('Jefe de Proyecto', 'ItemRendicion.GeneratePdf'),
('Jefe de Taller', 'ItemRendicion.GeneratePdf'),
('Director Ejecutivo', 'ItemRendicion.GetCuenta'),
('Jefe de Proyecto', 'ItemRendicion.GetCuenta'),
('Jefe de Taller', 'ItemRendicion.GetCuenta'),
('Director Ejecutivo', 'ItemRendicion.Ver'),
('Jefe de Proyecto', 'ItemRendicion.Ver'),
('Jefe de Taller', 'ItemRendicion.Ver'),
('Director Ejecutivo', 'ItemSolicitud.Actualizar'),
('Jefe de Proyecto', 'ItemSolicitud.Actualizar'),
('Jefe de Taller', 'ItemSolicitud.Actualizar'),
('Director Ejecutivo', 'ItemSolicitud.Agregar'),
('Jefe de Proyecto', 'ItemSolicitud.Agregar'),
('Jefe de Taller', 'ItemSolicitud.Agregar'),
('Director Ejecutivo', 'ItemSolicitud.GenerateExcel'),
('Jefe de Proyecto', 'ItemSolicitud.GenerateExcel'),
('Jefe de Taller', 'ItemSolicitud.GenerateExcel'),
('Director Ejecutivo', 'ItemSolicitud.GeneratePdf'),
('Jefe de Proyecto', 'ItemSolicitud.GeneratePdf'),
('Jefe de Taller', 'ItemSolicitud.GeneratePdf'),
('Director Ejecutivo', 'ItemSolicitud.Ver'),
('Jefe de Proyecto', 'ItemSolicitud.Ver'),
('Jefe de Taller', 'ItemSolicitud.Ver'),
('Director Ejecutivo', 'Proveedor.Actualizar'),
('Jefe de Proyecto', 'Proveedor.Actualizar'),
('Jefe de Taller', 'Proveedor.Actualizar'),
('Director Ejecutivo', 'Proveedor.Agregar'),
('Jefe de Proyecto', 'Proveedor.Agregar'),
('Jefe de Taller', 'Proveedor.Agregar'),
('Director Ejecutivo', 'Proveedor.Crear'),
('Jefe de Proyecto', 'Proveedor.Crear'),
('Jefe de Taller', 'Proveedor.Crear'),
('Director Ejecutivo', 'Proveedor.GenerateExcel'),
('Jefe de Proyecto', 'Proveedor.GenerateExcel'),
('Jefe de Taller', 'Proveedor.GenerateExcel'),
('Director Ejecutivo', 'Proveedor.GeneratePdf'),
('Jefe de Proyecto', 'Proveedor.GeneratePdf'),
('Jefe de Taller', 'Proveedor.GeneratePdf'),
('Director Ejecutivo', 'Proveedor.GetTodos'),
('Jefe de Proyecto', 'Proveedor.GetTodos'),
('Jefe de Taller', 'Proveedor.GetTodos'),
('Director Ejecutivo', 'Proveedor.Ver'),
('Jefe de Proyecto', 'Proveedor.Ver'),
('Jefe de Taller', 'Proveedor.Ver'),
('Director Ejecutivo', 'Proveedor.VerTodos'),
('Jefe de Proyecto', 'Proveedor.VerTodos'),
('Jefe de Taller', 'Proveedor.VerTodos'),
('Director Ejecutivo', 'Proyecto.Actualizar'),
('Jefe de Proyecto', 'Proyecto.Actualizar'),
('Jefe de Taller', 'Proyecto.Actualizar'),
('Director Ejecutivo', 'Proyecto.Crear'),
('Jefe de Proyecto', 'Proyecto.Crear'),
('Jefe de Taller', 'Proyecto.Crear'),
('Director Ejecutivo', 'Proyecto.GenerateExcel'),
('Jefe de Proyecto', 'Proyecto.GenerateExcel'),
('Jefe de Taller', 'Proyecto.GenerateExcel'),
('Director Ejecutivo', 'Proyecto.GeneratePdf'),
('Jefe de Proyecto', 'Proyecto.GeneratePdf'),
('Jefe de Taller', 'Proyecto.GeneratePdf'),
('Director Ejecutivo', 'Proyecto.GetComuna'),
('Jefe de Proyecto', 'Proyecto.GetComuna'),
('Jefe de Taller', 'Proyecto.GetComuna'),
('Director Ejecutivo', 'Proyecto.Ver'),
('Jefe de Proyecto', 'Proyecto.Ver'),
('Jefe de Taller', 'Proyecto.Ver'),
('Director Ejecutivo', 'Proyecto.VerTodos'),
('Jefe de Proyecto', 'Proyecto.VerTodos'),
('Jefe de Taller', 'Proyecto.VerTodos'),
('Director Ejecutivo', 'Rendicion.Actualizar'),
('Jefe de Proyecto', 'Rendicion.Actualizar'),
('Jefe de Taller', 'Rendicion.Actualizar'),
('Director Ejecutivo', 'Rendicion.Anular'),
('Jefe de Proyecto', 'Rendicion.Anular'),
('Jefe de Taller', 'Rendicion.Anular'),
('Director Ejecutivo', 'Rendicion.Crear'),
('Jefe de Proyecto', 'Rendicion.Crear'),
('Jefe de Taller', 'Rendicion.Crear'),
('Director Ejecutivo', 'Rendicion.Finalizar'),
('Jefe de Proyecto', 'Rendicion.Finalizar'),
('Jefe de Taller', 'Rendicion.Finalizar'),
('Director Ejecutivo', 'Rendicion.GenerateExcel'),
('Jefe de Proyecto', 'Rendicion.GenerateExcel'),
('Jefe de Taller', 'Rendicion.GenerateExcel'),
('Director Ejecutivo', 'Rendicion.GeneratePdf'),
('Jefe de Proyecto', 'Rendicion.GeneratePdf'),
('Jefe de Taller', 'Rendicion.GeneratePdf'),
('Director Ejecutivo', 'Rendicion.Ver'),
('Jefe de Proyecto', 'Rendicion.Ver'),
('Jefe de Taller', 'Rendicion.Ver'),
('Director Ejecutivo', 'Rendicion.VerTodos'),
('Jefe de Proyecto', 'Rendicion.VerTodos'),
('Jefe de Taller', 'Rendicion.VerTodos'),
('Director Ejecutivo', 'Rrhh.Actualizar'),
('Jefe de Proyecto', 'Rrhh.Actualizar'),
('Jefe de Taller', 'Rrhh.Actualizar'),
('Director Ejecutivo', 'Rrhh.Crear'),
('Jefe de Proyecto', 'Rrhh.Crear'),
('Jefe de Taller', 'Rrhh.Crear'),
('Director Ejecutivo', 'Rrhh.GenerateExcel'),
('Jefe de Proyecto', 'Rrhh.GenerateExcel'),
('Jefe de Taller', 'Rrhh.GenerateExcel'),
('Director Ejecutivo', 'Rrhh.GeneratePdf'),
('Jefe de Proyecto', 'Rrhh.GeneratePdf'),
('Jefe de Taller', 'Rrhh.GeneratePdf'),
('Director Ejecutivo', 'Rrhh.Ver'),
('Jefe de Proyecto', 'Rrhh.Ver'),
('Jefe de Taller', 'Rrhh.Ver'),
('Director Ejecutivo', 'Rrhh.VerContrato'),
('Jefe de Proyecto', 'Rrhh.VerContrato'),
('Jefe de Taller', 'Rrhh.VerContrato'),
('Director Ejecutivo', 'Rrhh.VerTodos'),
('Jefe de Proyecto', 'Rrhh.VerTodos'),
('Jefe de Taller', 'Rrhh.VerTodos'),
('Director Ejecutivo', 'Solicitud.Actualizar'),
('Jefe de Proyecto', 'Solicitud.Actualizar'),
('Jefe de Taller', 'Solicitud.Actualizar'),
('Director Ejecutivo', 'Solicitud.Anular'),
('Jefe de Proyecto', 'Solicitud.Anular'),
('Jefe de Taller', 'Solicitud.Anular'),
('Director Ejecutivo', 'Solicitud.Crear'),
('Jefe de Proyecto', 'Solicitud.Crear'),
('Jefe de Taller', 'Solicitud.Crear'),
('Director Ejecutivo', 'Solicitud.CrearSolicitud'),
('Jefe de Proyecto', 'Solicitud.CrearSolicitud'),
('Jefe de Taller', 'Solicitud.CrearSolicitud'),
('Director Ejecutivo', 'Solicitud.Finalizadas'),
('Jefe de Proyecto', 'Solicitud.Finalizadas'),
('Jefe de Taller', 'Solicitud.Finalizadas'),
('Director Ejecutivo', 'Solicitud.Finalizar'),
('Jefe de Proyecto', 'Solicitud.Finalizar'),
('Jefe de Taller', 'Solicitud.Finalizar'),
('Director Ejecutivo', 'Solicitud.GenerateExcel'),
('Jefe de Proyecto', 'Solicitud.GenerateExcel'),
('Jefe de Taller', 'Solicitud.GenerateExcel'),
('Director Ejecutivo', 'Solicitud.GeneratePdf'),
('Jefe de Proyecto', 'Solicitud.GeneratePdf'),
('Jefe de Taller', 'Solicitud.GeneratePdf'),
('Director Ejecutivo', 'Solicitud.Ver'),
('Jefe de Proyecto', 'Solicitud.Ver'),
('Jefe de Taller', 'Solicitud.Ver'),
('Director Ejecutivo', 'Solicitud.VerSolicitudEmision'),
('Jefe de Proyecto', 'Solicitud.VerSolicitudEmision'),
('Jefe de Taller', 'Solicitud.VerSolicitudEmision'),
('Director Ejecutivo', 'Solicitud.VerTodos'),
('Jefe de Taller', 'Solicitud.VerTodos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `centro_costo`
--

CREATE TABLE IF NOT EXISTS `centro_costo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcar la base de datos para la tabla `centro_costo`
--

INSERT INTO `centro_costo` (`id`, `nombre`) VALUES
(1, 'Taller Arquitectura'),
(2, 'Taller de Difusion'),
(3, 'Administracion'),
(4, 'Taller Historia del Arte'),
(5, 'Taller Investigacion y Desarrollo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comuna`
--

CREATE TABLE IF NOT EXISTS `comuna` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `region_id` int(11) DEFAULT NULL,
  `nombre` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_comuna_region_id` (`region_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Volcar la base de datos para la tabla `comuna`
--

INSERT INTO `comuna` (`id`, `region_id`, `nombre`) VALUES
(1, 1, 'Arica'),
(2, 1, 'Putre');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contrato`
--

CREATE TABLE IF NOT EXISTS `contrato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rrhh_id` int(11) NOT NULL,
  `proyecto_id` int(11) NOT NULL,
  `tipo_contrato_id` int(11) NOT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_termino` date DEFAULT NULL,
  `remuneracion` int(11) DEFAULT NULL,
  `estado` varchar(45) DEFAULT 'VIGENTE',
  PRIMARY KEY (`id`),
  KEY `fk_contrato_proyecto1_idx` (`proyecto_id`),
  KEY `fk_contrato_tipo_contrato1_idx` (`tipo_contrato_id`),
  KEY `fk_contrato_rrhh1_idx` (`rrhh_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcar la base de datos para la tabla `contrato`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuenta_contable`
--

CREATE TABLE IF NOT EXISTS `cuenta_contable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcar la base de datos para la tabla `cuenta_contable`
--

INSERT INTO `cuenta_contable` (`id`, `nombre`) VALUES
(1, 'Recursos Humano'),
(2, 'Operacion'),
(3, 'Inversion'),
(4, 'Difusion'),
(5, 'Gastos Generales');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuenta_especifica`
--

CREATE TABLE IF NOT EXISTS `cuenta_especifica` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cuenta_contable_id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cuenta_especifica_cuenta_contable1_idx` (`cuenta_contable_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=50 ;

--
-- Volcar la base de datos para la tabla `cuenta_especifica`
--

INSERT INTO `cuenta_especifica` (`id`, `cuenta_contable_id`, `nombre`) VALUES
(1, 1, 'Bonos'),
(2, 1, 'Boleta de Honorario'),
(3, 3, 'Maquinas de Construccion'),
(4, 1, 'Capacitacion'),
(5, 1, 'Finiquitos'),
(6, 1, 'Imposiciones'),
(7, 1, 'IVA'),
(8, 1, 'Remuneraciones'),
(9, 1, 'Retenciones'),
(10, 1, 'Viaticos'),
(11, 2, 'Agua Potable'),
(12, 2, 'Alimentacion'),
(13, 2, 'Alojamiento'),
(14, 2, 'Arriendo Vehiculo'),
(15, 2, 'Combustible/Peajes'),
(16, 2, 'Celular'),
(17, 2, 'Fletes'),
(18, 2, 'Fotocopias, Impresiones, Ploteo'),
(19, 2, 'Gastos de Representacion'),
(20, 2, 'Gastos Notariales'),
(21, 2, 'Impl. de Seguridad'),
(22, 2, 'Luz'),
(23, 2, 'Mantencion de Vehiculos'),
(24, 2, 'Materiales de Construccion'),
(25, 2, 'Materiales de Oficina'),
(26, 2, 'Partes'),
(27, 2, 'Pasajes'),
(28, 2, 'Ropa de Trabajo'),
(29, 2, 'Telecomunicacion'),
(30, 3, 'Tecnologia, Computadores, Impresiones, Maq. Fotografica, Etc.'),
(31, 3, 'Otros Activos'),
(32, 3, 'Vehiculos'),
(33, 4, 'Difusion/Publicidad'),
(34, 4, 'Honorario Evento'),
(35, 4, 'Webhosting Internet'),
(36, 5, 'Abarrotes, Manaje y Otros'),
(37, 5, 'Arreglos Oficina'),
(38, 5, 'Arriendo Oficina'),
(39, 5, 'Correspondencia'),
(40, 5, 'Donaciones'),
(41, 5, 'Fotografia y Ediciones'),
(42, 5, 'Gastos Comunes'),
(43, 5, 'Gastos Financieros'),
(44, 5, 'Mantencion de Equipos'),
(45, 5, 'Otros Gastos Generales'),
(46, 5, 'Patente'),
(47, 5, 'Seguros'),
(48, 5, 'Servicios Legales'),
(49, 5, 'Taxis Colectivos/Estacionamiento');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `emision`
--

CREATE TABLE IF NOT EXISTS `emision` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `proveedor_id` int(11) NOT NULL,
  `proyecto_id` int(11) NOT NULL,
  `solicitud_id` int(11) NOT NULL,
  `ciudad` varchar(100) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `concepto` varchar(255) DEFAULT NULL,
  `total_debe` int(11) NOT NULL DEFAULT '0',
  `total_haber` int(11) NOT NULL DEFAULT '0',
  `cuenta` varchar(100) DEFAULT NULL,
  `tipo_solicitud` varchar(50) DEFAULT NULL,
  `numero_cheque` varchar(100) DEFAULT NULL,
  `observaciones` blob,
  `estado` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_emision_solicitud1_idx` (`solicitud_id`),
  KEY `fk_emision_proyecto1_idx` (`proyecto_id`),
  KEY `fk_emision_proveedor1_idx` (`proveedor_id`),
  KEY `fk_emision_user1` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- Volcar la base de datos para la tabla `emision`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `financiamiento`
--

CREATE TABLE IF NOT EXISTS `financiamiento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcar la base de datos para la tabla `financiamiento`
--

INSERT INTO `financiamiento` (`id`, `nombre`) VALUES
(1, 'Conadi'),
(2, 'Municipalidad'),
(3, 'Corfo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ingreso`
--

CREATE TABLE IF NOT EXISTS `ingreso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proyecto_id` int(11) NOT NULL,
  `tipo_ingreso_id` int(11) NOT NULL,
  `financiamiento_id` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `monto` int(11) DEFAULT '0',
  `observaciones` blob,
  PRIMARY KEY (`id`),
  KEY `fk_presupuesto_financiamiento1_idx` (`financiamiento_id`),
  KEY `fk_presupuesto_tipo_ingreso1_idx` (`tipo_ingreso_id`),
  KEY `fk_presupuesto_proyecto1_idx` (`proyecto_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcar la base de datos para la tabla `ingreso`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `item_presupuesto`
--

CREATE TABLE IF NOT EXISTS `item_presupuesto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `presupuesto_id` int(11) NOT NULL,
  `cuenta_contable_id` int(11) NOT NULL,
  `cuenta_especifica_id` int(11) NOT NULL,
  `monto` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_item_presupuesto_presupuesto1_idx` (`presupuesto_id`),
  KEY `fk_item_presupuesto_cuenta_especifica1_idx` (`cuenta_especifica_id`),
  KEY `fk_item_presupuesto_cuenta_contable1_idx` (`cuenta_contable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Volcar la base de datos para la tabla `item_presupuesto`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `item_rendicion`
--

CREATE TABLE IF NOT EXISTS `item_rendicion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proveedor_id` int(11) NOT NULL,
  `cuenta_contable_id` int(11) NOT NULL,
  `cuenta_especifica_id` int(11) NOT NULL,
  `rendicion_id` int(11) NOT NULL,
  `proyecto_id` int(11) NOT NULL,
  `tipo_documento` varchar(100) DEFAULT NULL,
  `numero_documento` varchar(100) DEFAULT NULL,
  `monto` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_item_rendicion_rendicion1_idx` (`rendicion_id`),
  KEY `fk_item_rendicion_proyecto1_idx` (`proyecto_id`),
  KEY `fk_item_rendicion_proveedor1_idx` (`proveedor_id`),
  KEY `fk_item_rendicion_cuenta_especifica1_idx` (`cuenta_especifica_id`),
  KEY `fk_item_rendicion_cuenta_contable1` (`cuenta_contable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Volcar la base de datos para la tabla `item_rendicion`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `item_solicitud`
--

CREATE TABLE IF NOT EXISTS `item_solicitud` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `solicitud_id` int(11) NOT NULL,
  `descripcion` blob,
  `valor_unitario` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_item_solicitud_solicitud1_idx` (`solicitud_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Volcar la base de datos para la tabla `item_solicitud`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `presupuesto`
--

CREATE TABLE IF NOT EXISTS `presupuesto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proyecto_id` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `monto` int(11) DEFAULT '0',
  `observaciones` blob,
  PRIMARY KEY (`id`),
  KEY `fk_presupuesto_proyecto2_idx` (`proyecto_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Volcar la base de datos para la tabla `presupuesto`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE IF NOT EXISTS `proveedor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rut` varchar(13) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcar la base de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`id`, `rut`, `nombre`) VALUES
(1, '15.430.567.8', 'SODIMAC'),
(2, '55876675-9', 'Comercial Basurto'),
(3, '99786756-3', 'Comercial Benni');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyecto`
--

CREATE TABLE IF NOT EXISTS `proyecto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rrhh_id` int(11) NOT NULL,
  `centro_costo_id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `descripcion` blob,
  `archivo` blob,
  `contrato` blob,
  `comuna` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_proyecto_centro_costo1_idx` (`centro_costo_id`),
  KEY `fk_proyecto_rrhh1_idx` (`rrhh_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcar la base de datos para la tabla `proyecto`
--

INSERT INTO `proyecto` (`id`, `rrhh_id`, `centro_costo_id`, `nombre`, `descripcion`, `archivo`, `contrato`, `comuna`, `region`) VALUES
(1, 1, 1, 'Socoroma', 0x50726f796563746f207061726120656c20706f626c61646f20646520736f636f726f6d61, '', '', 'ARICA', '15'),
(2, 1, 2, 'Festival de Jazz', NULL, NULL, NULL, 'Arica', 'XV');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `region`
--

CREATE TABLE IF NOT EXISTS `region` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcar la base de datos para la tabla `region`
--

INSERT INTO `region` (`id`, `nombre`) VALUES
(1, 'XV Arica y Parinacota');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rendicion`
--

CREATE TABLE IF NOT EXISTS `rendicion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rrhh_id` int(11) NOT NULL,
  `emision_id` int(11) NOT NULL,
  `asignacion` varchar(100) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `comuna` varchar(100) DEFAULT NULL,
  `region` varchar(100) DEFAULT NULL,
  `tipo_pago` varchar(100) DEFAULT NULL,
  `total_entrada` int(11) DEFAULT NULL,
  `total_salida` int(11) NOT NULL DEFAULT '0',
  `total_saldo` int(11) NOT NULL DEFAULT '0',
  `observacion` blob,
  `estado` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_rendicion_rrhh1_idx` (`rrhh_id`),
  KEY `fk_rendicion_emision1_idx` (`emision_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Volcar la base de datos para la tabla `rendicion`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rights`
--

CREATE TABLE IF NOT EXISTS `rights` (
  `itemname` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  PRIMARY KEY (`itemname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcar la base de datos para la tabla `rights`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rrhh`
--

CREATE TABLE IF NOT EXISTS `rrhh` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rut` varchar(13) DEFAULT NULL,
  `nombres` varchar(255) DEFAULT NULL,
  `apellidos` varchar(255) DEFAULT NULL,
  `sexo` varchar(15) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `telefono` varchar(50) DEFAULT NULL,
  `celular` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcar la base de datos para la tabla `rrhh`
--

INSERT INTO `rrhh` (`id`, `rut`, `nombres`, `apellidos`, `sexo`, `fecha_nacimiento`, `direccion`, `telefono`, `celular`, `email`) VALUES
(1, '16469470-4', 'Francisco Javier', 'Abalan Osorio', 'Masculino', '1987-04-01', 'Buin 1825', '325071', '90931785', 'rapbore@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitud`
--

CREATE TABLE IF NOT EXISTS `solicitud` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proyecto_id` int(11) NOT NULL,
  `proveedor_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `tipo_solicitud` varchar(50) NOT NULL,
  `total` int(11) NOT NULL DEFAULT '0',
  `estado` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_solicitud_proyecto1_idx` (`proyecto_id`),
  KEY `fk_solicitud_proveedor1_idx` (`proveedor_id`),
  KEY `fk_solicitud_user1_idx` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Volcar la base de datos para la tabla `solicitud`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_contrato`
--

CREATE TABLE IF NOT EXISTS `tipo_contrato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(45) NOT NULL,
  `caracteristicas` blob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcar la base de datos para la tabla `tipo_contrato`
--

INSERT INTO `tipo_contrato` (`id`, `tipo`, `caracteristicas`) VALUES
(2, 'Honorario', NULL),
(3, 'Planta', NULL),
(4, 'Indefinido', 0x436f6e747261746f206465207469656d706f20696e646566696e69646f);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_ingreso`
--

CREATE TABLE IF NOT EXISTS `tipo_ingreso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcar la base de datos para la tabla `tipo_ingreso`
--

INSERT INTO `tipo_ingreso` (`id`, `nombre`) VALUES
(1, 'Convenio'),
(2, 'Donaciones'),
(3, 'Venta de Servicios'),
(4, 'Otros');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcar la base de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `email`) VALUES
(1, 'admin', 'c63b447bcee0c5064cac9d264e62114e2d50516e', NULL);

--
-- Filtros para las tablas descargadas (dump)
--

--
-- Filtros para la tabla `authassignment`
--
ALTER TABLE `authassignment`
  ADD CONSTRAINT `authassignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `authitemchild`
--
ALTER TABLE `authitemchild`
  ADD CONSTRAINT `authitemchild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `authitemchild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `comuna`
--
ALTER TABLE `comuna`
  ADD CONSTRAINT `fk_comuna_region_id` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `contrato`
--
ALTER TABLE `contrato`
  ADD CONSTRAINT `fk_contrato_proyecto1` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_contrato_rrhh1` FOREIGN KEY (`rrhh_id`) REFERENCES `rrhh` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_contrato_tipo_contrato1` FOREIGN KEY (`tipo_contrato_id`) REFERENCES `tipo_contrato` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `cuenta_especifica`
--
ALTER TABLE `cuenta_especifica`
  ADD CONSTRAINT `fk_cuenta_especifica_cuenta_contable1` FOREIGN KEY (`cuenta_contable_id`) REFERENCES `cuenta_contable` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `emision`
--
ALTER TABLE `emision`
  ADD CONSTRAINT `fk_emision_proveedor1` FOREIGN KEY (`proveedor_id`) REFERENCES `proveedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_emision_proyecto1` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_emision_solicitud1` FOREIGN KEY (`solicitud_id`) REFERENCES `solicitud` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_emision_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ingreso`
--
ALTER TABLE `ingreso`
  ADD CONSTRAINT `fk_presupuesto_financiamiento1` FOREIGN KEY (`financiamiento_id`) REFERENCES `financiamiento` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_presupuesto_proyecto1` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_presupuesto_tipo_ingreso1` FOREIGN KEY (`tipo_ingreso_id`) REFERENCES `tipo_ingreso` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `item_presupuesto`
--
ALTER TABLE `item_presupuesto`
  ADD CONSTRAINT `fk_item_presupuesto_presupuesto1` FOREIGN KEY (`presupuesto_id`) REFERENCES `presupuesto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_presupuesto_cuenta_especifica1` FOREIGN KEY (`cuenta_especifica_id`) REFERENCES `cuenta_especifica` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_presupuesto_cuenta_contable1` FOREIGN KEY (`cuenta_contable_id`) REFERENCES `cuenta_contable` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `item_rendicion`
--
ALTER TABLE `item_rendicion`
  ADD CONSTRAINT `fk_item_rendicion_cuenta_contable1` FOREIGN KEY (`cuenta_contable_id`) REFERENCES `cuenta_contable` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_rendicion_cuenta_especifica1` FOREIGN KEY (`cuenta_especifica_id`) REFERENCES `cuenta_especifica` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_rendicion_proveedor1` FOREIGN KEY (`proveedor_id`) REFERENCES `proveedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_rendicion_proyecto1` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_rendicion_rendicion1` FOREIGN KEY (`rendicion_id`) REFERENCES `rendicion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `item_solicitud`
--
ALTER TABLE `item_solicitud`
  ADD CONSTRAINT `fk_item_solicitud_solicitud1` FOREIGN KEY (`solicitud_id`) REFERENCES `solicitud` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `presupuesto`
--
ALTER TABLE `presupuesto`
  ADD CONSTRAINT `fk_presupuesto_proyecto2` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `proyecto`
--
ALTER TABLE `proyecto`
  ADD CONSTRAINT `fk_proyecto_centro_costo1` FOREIGN KEY (`centro_costo_id`) REFERENCES `centro_costo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_proyecto_rrhh1` FOREIGN KEY (`rrhh_id`) REFERENCES `rrhh` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `rendicion`
--
ALTER TABLE `rendicion`
  ADD CONSTRAINT `fk_rendicion_emision1` FOREIGN KEY (`emision_id`) REFERENCES `emision` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_rendicion_rrhh1` FOREIGN KEY (`rrhh_id`) REFERENCES `rrhh` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `rights`
--
ALTER TABLE `rights`
  ADD CONSTRAINT `rights_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `solicitud`
--
ALTER TABLE `solicitud`
  ADD CONSTRAINT `fk_solicitud_proveedor1` FOREIGN KEY (`proveedor_id`) REFERENCES `proveedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_solicitud_proyecto1` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_solicitud_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
