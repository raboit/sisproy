/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50525
 Source Host           : localhost
 Source Database       : scabd

 Target Server Type    : MySQL
 Target Server Version : 50525
 File Encoding         : utf-8

 Date: 01/03/2013 17:18:51 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `adc_clases`
-- ----------------------------
DROP TABLE IF EXISTS `adc_clases`;
CREATE TABLE `adc_clases` (
  `adc_cur_cod` int(12) NOT NULL,
  `cla_corr` int(3) NOT NULL,
  `cla_fecha` date DEFAULT NULL,
  `cla_fecha_reg` date DEFAULT NULL,
  `cla_nombre` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cla_desc` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`adc_cur_cod`,`cla_corr`),
  KEY `fk_Clases_Curso1_idx` (`adc_cur_cod`),
  CONSTRAINT `fk_Clases_Curso1` FOREIGN KEY (`adc_cur_cod`) REFERENCES `adc_curso` (`adc_cur_cod`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `adc_clases`
-- ----------------------------
BEGIN;
INSERT INTO `adc_clases` VALUES ('111', '1', '2012-12-02', null, 'Tema 4', 'Descripcion 2'), ('111', '2', '2012-02-14', null, 'Actividad 5', 'Descripcion 6'), ('111', '3', '2013-01-03', '2013-01-03', 'Presentacion', 'Rmi'), ('111', '4', '2013-01-03', '2013-01-03', 'Presentacion', 'Rmi'), ('111', '5', '2013-01-03', '2013-01-03', 'Presentacion', 'Rmi'), ('111', '6', '2013-01-03', '2013-01-03', 'Presentacion', 'Rmi'), ('222', '1', '2012-10-15', null, 'Presentacion 1', 'Descripcion 1'), ('222', '2', '2012-09-11', null, 'Trabajo 3', 'Descripcion 4'), ('333', '1', '2012-09-21', null, 'Prueba 2', 'Descripcion 3'), ('333', '2', '2012-04-15', null, 'Revision Prueba 1', 'Descripcion 5'), ('333', '3', '2013-01-03', '2013-01-03', 'Presentacion', 'Rmi'), ('444', '1', '2012-05-10', null, 'Ensayo', 'descripcion 9'), ('555', '1', '2012-08-18', null, 'Trabajo 2', 'Descripcion 7'), ('555', '2', '2012-07-13', null, 'Presentacion 2', 'Descripcion 8');
COMMIT;

-- ----------------------------
--  Table structure for `adc_curso`
-- ----------------------------
DROP TABLE IF EXISTS `adc_curso`;
CREATE TABLE `adc_curso` (
  `adc_cur_cod` int(12) NOT NULL,
  `adc_cur_nombre` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `adc_cur_hora` int(3) DEFAULT NULL,
  `adc_cur_tipo` int(1) NOT NULL,
  `adc_cur_cupo` int(4) DEFAULT NULL,
  `adc_cur_grupo` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adc_cur_estado` int(1) DEFAULT NULL,
  `LectorRFID_idLectorRFID` int(11) NOT NULL,
  PRIMARY KEY (`adc_cur_cod`),
  UNIQUE KEY `adc_cur_cod_UNIQUE` (`adc_cur_cod`),
  KEY `fk_Curso_LectorRFID1_idx` (`LectorRFID_idLectorRFID`),
  CONSTRAINT `fk_Curso_LectorRFID1` FOREIGN KEY (`LectorRFID_idLectorRFID`) REFERENCES `lectorrfid` (`idLectorRFID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `adc_curso`
-- ----------------------------
BEGIN;
INSERT INTO `adc_curso` VALUES ('111', 'Aplicaciones Distribuidas Avanzadas', null, '0', '20', 'A', '1', '1'), ('222', 'Ingenieria de Software Avanzada', null, '0', '30', 'A', '1', '2'), ('333', 'Comunicacion de Datos y Redes de Computadores', null, '1', '30', 'A', '0', '1'), ('444', 'Fundamentos de Lenguaje de Programacion', null, '0', '35', 'A', '1', '3'), ('555', 'Base de Datos', null, '2', '35', 'A', '1', '3'), ('666', 'Tecnologia de Objetos', null, '0', '35', 'A', '0', '2');
COMMIT;

-- ----------------------------
--  Table structure for `adc_usuario`
-- ----------------------------
DROP TABLE IF EXISTS `adc_usuario`;
CREATE TABLE `adc_usuario` (
  `usr_rut` int(10) NOT NULL,
  `Carrera_Cod` int(4) DEFAULT NULL,
  `Tipo_usr` int(1) NOT NULL,
  `Nombre` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `Apellido` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `ID_tag` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`usr_rut`),
  UNIQUE KEY `ID_tag_UNIQUE` (`ID_tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `adc_usuario`
-- ----------------------------
BEGIN;
INSERT INTO `adc_usuario` VALUES ('73390028', '0', '1', 'Diego', 'Aracena', '4e00353446'), ('157346201', '542', '0', 'Judith', 'Sanchez', '4d003c346d'), ('161072459', '580', '0', 'Sebastian', 'Piñera', null), ('161469872', '541', '0', 'Shirley', 'Quelquezana', '4d003c4f6a'), ('165493680', '580', '0', 'Jesus', 'Navarro', '001'), ('173922305', '541', '0', 'Michael', 'Jackson', '0231');
COMMIT;

-- ----------------------------
--  Table structure for `bloque`
-- ----------------------------
DROP TABLE IF EXISTS `bloque`;
CREATE TABLE `bloque` (
  `idBloque` int(11) NOT NULL,
  `Bloque` tinyint(4) DEFAULT NULL,
  `HoraInicio` time DEFAULT NULL,
  `HoraFin` time DEFAULT NULL,
  PRIMARY KEY (`idBloque`),
  UNIQUE KEY `index2` (`HoraInicio`,`HoraFin`),
  UNIQUE KEY `Bloque_UNIQUE` (`Bloque`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `bloque`
-- ----------------------------
BEGIN;
INSERT INTO `bloque` VALUES ('1', '1', '08:00:00', '09:30:00'), ('2', '2', '09:40:00', '11:10:00'), ('3', '3', '11:20:00', '12:50:00'), ('4', '4', '14:45:00', '16:15:00'), ('5', '5', '16:20:00', '17:50:00'), ('6', '6', '17:55:00', '19:25:00'), ('7', '7', '19:30:00', '21:00:00');
COMMIT;

-- ----------------------------
--  Table structure for `horario`
-- ----------------------------
DROP TABLE IF EXISTS `horario`;
CREATE TABLE `horario` (
  `IDhorario` int(11) NOT NULL,
  `adc_cur_cod` int(12) NOT NULL,
  `Fecha` date DEFAULT NULL,
  `Estado` int(1) DEFAULT NULL,
  `IDSala` int(11) NOT NULL,
  PRIMARY KEY (`IDhorario`),
  UNIQUE KEY `index3` (`adc_cur_cod`,`Fecha`,`IDSala`),
  KEY `fk_Horario_Curso1_idx` (`adc_cur_cod`),
  KEY `fk_Salas_IDSala_idx` (`IDSala`),
  CONSTRAINT `fk_Horario_Curso` FOREIGN KEY (`adc_cur_cod`) REFERENCES `adc_curso` (`adc_cur_cod`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Salas_IDSala` FOREIGN KEY (`IDSala`) REFERENCES `salas` (`IDSala`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `horario`
-- ----------------------------
BEGIN;
INSERT INTO `horario` VALUES ('1', '111', '2012-12-10', null, '3'), ('2', '222', '2012-12-11', null, '4'), ('3', '111', '2012-12-12', null, '3'), ('4', '222', '2012-12-13', null, '4'), ('5', '222', '2012-12-14', null, '2'), ('6', '555', '2012-12-15', null, '7');
COMMIT;

-- ----------------------------
--  Table structure for `horario_has_bloque`
-- ----------------------------
DROP TABLE IF EXISTS `horario_has_bloque`;
CREATE TABLE `horario_has_bloque` (
  `horario_IDhorario` int(11) NOT NULL,
  `Bloque_idBloque` int(11) NOT NULL,
  PRIMARY KEY (`horario_IDhorario`,`Bloque_idBloque`),
  KEY `fk_Horario_has_Bloque_Bloque1_idx` (`Bloque_idBloque`),
  KEY `fk_orario_has_Bloque_horario1_idx` (`horario_IDhorario`),
  CONSTRAINT `fk_Horario_has_Bloque_Bloque1` FOREIGN KEY (`Bloque_idBloque`) REFERENCES `bloque` (`idBloque`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_horario_has_Bloque_horario` FOREIGN KEY (`horario_IDhorario`) REFERENCES `horario` (`IDhorario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `horario_has_bloque`
-- ----------------------------
BEGIN;
INSERT INTO `horario_has_bloque` VALUES ('2', '1'), ('4', '1'), ('5', '1'), ('6', '2'), ('1', '3'), ('3', '3');
COMMIT;

-- ----------------------------
--  Table structure for `lectorrfid`
-- ----------------------------
DROP TABLE IF EXISTS `lectorrfid`;
CREATE TABLE `lectorrfid` (
  `idLectorRFID` int(11) NOT NULL,
  `desc_rfid` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idLectorRFID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `lectorrfid`
-- ----------------------------
BEGIN;
INSERT INTO `lectorrfid` VALUES ('1', 'lector1'), ('2', 'lector2'), ('3', 'lector3'), ('4', 'lector4'), ('5', 'lector5');
COMMIT;

-- ----------------------------
--  Table structure for `salas`
-- ----------------------------
DROP TABLE IF EXISTS `salas`;
CREATE TABLE `salas` (
  `IDSala` int(11) NOT NULL,
  `Nombre` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Lugar` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`IDSala`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `salas`
-- ----------------------------
BEGIN;
INSERT INTO `salas` VALUES ('1', 'Licancabur', 'Edificio Computacion'), ('2', 'Socompa', 'Edificio Computacion'), ('3', 'Azufre', 'Edificio Computacion'), ('4', 'Guallatire', 'Edificio Computacion'), ('5', 'Parinacota', 'Edificio Computacion'), ('6', 'S301', 'Aulario C'), ('7', 'S201', 'Aulario C'), ('8', 'N2', 'Colina');
COMMIT;

-- ----------------------------
--  Table structure for `usuario_has_clases`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_has_clases`;
CREATE TABLE `usuario_has_clases` (
  `adc_usuario_usr_rut` int(10) NOT NULL,
  `adc_clases_adc_cur_cod` int(12) NOT NULL,
  `adc_clases_cla_corr` int(3) NOT NULL,
  `Estado` int(1) DEFAULT '0',
  PRIMARY KEY (`adc_usuario_usr_rut`,`adc_clases_adc_cur_cod`,`adc_clases_cla_corr`),
  KEY `fk_adc_usuario_has_adc_clases_adc_clases1_idx` (`adc_clases_adc_cur_cod`,`adc_clases_cla_corr`),
  KEY `fk_adc_usuario_has_adc_clases_adc_usuario1_idx` (`adc_usuario_usr_rut`),
  CONSTRAINT `fk_adc_usuario_has_adc_clases_adc_clases1` FOREIGN KEY (`adc_clases_adc_cur_cod`, `adc_clases_cla_corr`) REFERENCES `adc_clases` (`adc_cur_cod`, `cla_corr`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_adc_usuario_has_adc_clases_adc_usuario1` FOREIGN KEY (`adc_usuario_usr_rut`) REFERENCES `adc_usuario` (`usr_rut`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `usuario_has_clases`
-- ----------------------------
BEGIN;
INSERT INTO `usuario_has_clases` VALUES ('157346201', '111', '2', '1'), ('157346201', '111', '3', '1'), ('157346201', '111', '4', '0'), ('157346201', '111', '5', '1'), ('157346201', '111', '6', '0'), ('157346201', '333', '1', '0'), ('157346201', '333', '3', '1'), ('161469872', '111', '3', '0'), ('161469872', '111', '4', '0'), ('161469872', '111', '5', '0'), ('161469872', '111', '6', '1'), ('161469872', '222', '2', '0'), ('161469872', '333', '3', '1'), ('165493680', '111', '1', '1'), ('165493680', '111', '3', '0'), ('165493680', '111', '4', '0'), ('165493680', '111', '5', '0'), ('165493680', '111', '6', '0'), ('165493680', '222', '1', '0'), ('165493680', '333', '3', '0'), ('173922305', '111', '1', '0');
COMMIT;

-- ----------------------------
--  Table structure for `usuario_has_curso`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_has_curso`;
CREATE TABLE `usuario_has_curso` (
  `adc_cur_cod` int(12) NOT NULL,
  `usr_rut` int(10) NOT NULL,
  PRIMARY KEY (`adc_cur_cod`,`usr_rut`),
  KEY `fk_adc_cur_cod_idx` (`adc_cur_cod`),
  KEY `fk_usr_rut_idx` (`usr_rut`),
  CONSTRAINT `fk_adc_cur_cod` FOREIGN KEY (`adc_cur_cod`) REFERENCES `adc_curso` (`adc_cur_cod`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usr_rut` FOREIGN KEY (`usr_rut`) REFERENCES `adc_usuario` (`usr_rut`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `usuario_has_curso`
-- ----------------------------
BEGIN;
INSERT INTO `usuario_has_curso` VALUES ('111', '73390028'), ('111', '157346201'), ('111', '161469872'), ('111', '165493680'), ('222', '161469872'), ('222', '165493680'), ('333', '73390028'), ('333', '157346201'), ('333', '161469872'), ('333', '165493680'), ('444', '157346201'), ('444', '161469872'), ('555', '157346201'), ('555', '161469872'), ('555', '165493680');
COMMIT;

-- ----------------------------
--  View structure for `alumnoscursos`
-- ----------------------------
DROP VIEW IF EXISTS `alumnoscursos`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `alumnoscursos` AS select `adc_curso`.`adc_cur_cod` AS `adc_cur_cod`,`adc_curso`.`adc_cur_nombre` AS `adc_cur_nombre`,`adc_usuario`.`usr_rut` AS `usr_rut`,`adc_usuario`.`Nombre` AS `Nombre`,`adc_usuario`.`Apellido` AS `Apellido`,`adc_usuario`.`Carrera_Cod` AS `Carrera_Cod`,`adc_usuario`.`ID_tag` AS `ID_tag` from ((`adc_curso` join `usuario_has_curso` on((`usuario_has_curso`.`adc_cur_cod` = `adc_curso`.`adc_cur_cod`))) join `adc_usuario` on((`usuario_has_curso`.`usr_rut` = `adc_usuario`.`usr_rut`))) where (`adc_usuario`.`Tipo_usr` <> 1) order by `usuario_has_curso`.`adc_cur_cod`,`adc_usuario`.`Apellido`;

-- ----------------------------
--  View structure for `asistenciaclases`
-- ----------------------------
DROP VIEW IF EXISTS `asistenciaclases`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `asistenciaclases` AS select `adc_usuario`.`usr_rut` AS `usr_rut`,`adc_usuario`.`Nombre` AS `Nombre`,`adc_usuario`.`Apellido` AS `Apellido`,`adc_usuario`.`ID_tag` AS `ID_tag`,`usuario_has_clases`.`Estado` AS `Estado`,`usuario_has_clases`.`adc_clases_adc_cur_cod` AS `adc_clases_adc_cur_cod`,`usuario_has_clases`.`adc_clases_cla_corr` AS `adc_clases_cla_corr` from (`adc_usuario` join `usuario_has_clases` on((`usuario_has_clases`.`adc_usuario_usr_rut` = `adc_usuario`.`usr_rut`)));

-- ----------------------------
--  View structure for `clasescursosprofesores`
-- ----------------------------
DROP VIEW IF EXISTS `clasescursosprofesores`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `clasescursosprofesores` AS select `cursosprofesores`.`usr_rut` AS `usr_rut`,`cursosprofesores`.`Nombre` AS `Nombre`,`cursosprofesores`.`Apellido` AS `Apellido`,`cursosprofesores`.`ID_tag` AS `ID_tag`,`cursosprofesores`.`adc_cur_nombre` AS `adc_cur_nombre`,`cursosprofesores`.`adc_cur_cod` AS `adc_cur_cod`,`adc_clases`.`cla_corr` AS `cla_corr`,`adc_clases`.`cla_fecha` AS `cla_fecha` from (`cursosprofesores` join `adc_clases` on((`adc_clases`.`adc_cur_cod` = `cursosprofesores`.`adc_cur_cod`)));

-- ----------------------------
--  View structure for `cursoshorarios`
-- ----------------------------
DROP VIEW IF EXISTS `cursoshorarios`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `cursoshorarios` AS select `adc_curso`.`adc_cur_cod` AS `adc_cur_cod`,`adc_curso`.`adc_cur_nombre` AS `adc_cur_nombre`,`horario`.`IDhorario` AS `IDhorario`,`horario`.`Fecha` AS `Fecha`,`horario`.`IDSala` AS `IDSala`,`bloque`.`idBloque` AS `idBloque`,`bloque`.`Bloque` AS `Bloque`,`bloque`.`HoraInicio` AS `HoraInicio`,`bloque`.`HoraFin` AS `HoraFin` from (((`adc_curso` join `horario` on((`horario`.`adc_cur_cod` = `adc_curso`.`adc_cur_cod`))) join `horario_has_bloque` on((`horario_has_bloque`.`horario_IDhorario` = `horario`.`IDhorario`))) join `bloque` on((`horario_has_bloque`.`Bloque_idBloque` = `bloque`.`idBloque`)));

-- ----------------------------
--  View structure for `cursosprofesores`
-- ----------------------------
DROP VIEW IF EXISTS `cursosprofesores`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `cursosprofesores` AS select `adc_usuario`.`usr_rut` AS `usr_rut`,`adc_usuario`.`Nombre` AS `Nombre`,`adc_usuario`.`Apellido` AS `Apellido`,`adc_usuario`.`ID_tag` AS `ID_tag`,`adc_curso`.`adc_cur_cod` AS `adc_cur_cod`,`adc_curso`.`adc_cur_nombre` AS `adc_cur_nombre` from ((`adc_usuario` join `usuario_has_curso` on((`usuario_has_curso`.`usr_rut` = `adc_usuario`.`usr_rut`))) join `adc_curso` on((`usuario_has_curso`.`adc_cur_cod` = `adc_curso`.`adc_cur_cod`))) where (`adc_usuario`.`Tipo_usr` = 1);

-- ----------------------------
--  View structure for `profesorcursohorario`
-- ----------------------------
DROP VIEW IF EXISTS `profesorcursohorario`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `profesorcursohorario` AS select `cursosprofesores`.`usr_rut` AS `usr_rut`,`cursosprofesores`.`Nombre` AS `Nombre`,`cursosprofesores`.`ID_tag` AS `ID_tag`,`cursosprofesores`.`adc_cur_cod` AS `adc_cur_cod`,`cursosprofesores`.`adc_cur_nombre` AS `adc_cur_nombre`,`cursoshorarios`.`IDhorario` AS `IDhorario`,`cursoshorarios`.`Fecha` AS `Fecha`,`cursoshorarios`.`IDSala` AS `IDSala`,`cursoshorarios`.`idBloque` AS `idBloque`,`cursoshorarios`.`Bloque` AS `Bloque`,`cursoshorarios`.`HoraInicio` AS `HoraInicio`,`cursoshorarios`.`HoraFin` AS `HoraFin` from (`cursosprofesores` join `cursoshorarios` on((`cursoshorarios`.`adc_cur_cod` = `cursosprofesores`.`adc_cur_cod`)));

SET FOREIGN_KEY_CHECKS = 1;
