<?php

class RecargaController extends Controller
{
	
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			//'accessControl', // perform access control for CRUD operations
			'rights',
		);
	}
	public function allowedActions()
	{
	 	return 'index, create, update, delete, view, mias, ultimas, realizadas, creaphone, adminBB, admin, supervisor';
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','index','list','view','mias','realizadas','creaphone','ultimas','supervisor'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','adminbb'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	/**
	FUNCION QUE VALIDA SI ES SUPERVISOR EL USUARIO
	*/
	
	public function validaSupervisor(){
		
		$model=User::model()->findByPk(Yii::app()->user->getId());
		$nivelUser=$model->nivel;
		
		if($nivelUser=="supervisor" || $nivelUser=="admin")
		return true;
		else
		return false;
	
	}
	
	/**
	FUNCION QUE VALIDA SI ES CLIENTE PUEDE COMPANIA
	*/
	
	public function validaCompania($compania){
		
		$model=User::model()->findByPk(Yii::app()->user->getId());
		$nivelUser=$model->nivel;
		if($compania=="Entel"){
		$permisoUser=$model->entel;
		}else{
		$permisoUser=$model->movistar;
		}
		if($permisoUser=="SI" || $nivelUser=="admin")
		return true;
		else
		return false;
	
	}
	

	/**
	FUNCION QUE VALIDA SI ES CLIENTE EL USUARIO
	*/
	
	public function validaCliente(){
		
		$model=User::model()->findByPk(Yii::app()->user->getId());
		$nivelUser=$model->nivel;
		
		if($nivelUser=="cliente" || $nivelUser=="admin")
		return true;
		else
		return false;
	
	}
	/**
	FUNCION QUE VALIDA SI ES IPHONE EL USUARIO
	*/
	
	public function validaIphone(){
		
		$model=User::model()->findByPk(Yii::app()->user->getId());
		$nivelUser=$model->nivel;
		
		if($nivelUser=="iphone" || $nivelUser=="admin")
		return true;
		else
		return false;
	
	}
	
	
	
	public function validaOperador(){
	$model=User::model()->findByPk(Yii::app()->user->getId());
	$nivelUser=$model->nivel;
	
	if($nivelUser=="operador" || $nivelUser=="admin")
	return true;
	else
	return false;
	
	}
	
	public function validaSistema(){
	$status_model=Status::model()->ultimo()->find();
	
	if($status_model->estado=='Cerrado')
	return true;
	else
	return false;
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		if($this->validaSistema())
			throw new CHttpException(' /  FUERA DE HORARIO - Horario de Lunes a Sabado de 09.00 hrs a 23.00 hrs - Domingos y Festivos de 09.00 hrs a 21.00 hrs');
					
		if(!$this->validaCliente()){
		$this->accessDenied();
		}
		$model=new Recarga;
		if(isset($_POST['Recarga']))
		{
			$model->attributes=$_POST['Recarga'];
			$model->fechaIN=date("Y-m-d H:i:s",time());
			if($model->save())
				$this->redirect(array('ultimas'));
		}
		$usuario_recarga=User::model()->findByPk(Yii::app()->user->getId());
		
		$this->render('create',array(
			'model'=>$model,
			'usuario_recarga'=>$usuario_recarga,

		));
	}
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreaphone()
	{
		if($this->validaSistema())
			throw new CHttpException(' /  FUERA DE HORARIO - Horario de Lunes a Sabado de 09.00 hrs a 23.00 hrs - Domingos y Festivos de 09.00 hrs a 21.00 hrs');
			
			
		$this->layout='//layouts/column1';
		if(!$this->validaIphone()){
		$this->accessDenied();
		}
		
		$model=new Recarga;
		if(isset($_POST['Recarga']))
		{
			$model->attributes=$_POST['Recarga'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('creaphone',array(
			'model'=>$model,
		));
	}
	
	


	private function calcularTiempo($id,$tiempo){
	
		$sql = "SELECT TIMEDIFF(:tiempo,fechaIN) as tiempo FROM rc_recarga WHERE id=:id_recarga";
		$parameters = array(":id_recarga"=>$id,
							":tiempo"=>$tiempo,
							);
		$dataReader=Yii::app()->db->createCommand($sql)->query($parameters);
		$dataReader->bindColumn(1,$tiempo);
		$total_tiempo = $dataReader->read(1);
		return $total_tiempo["tiempo"];
	}
	
	private function calcularTotal($id){
	
		$sql = "SELECT SUM(monto) as total FROM rc_recarga WHERE idCliente=:id_cliente AND estado='LISTA' AND fechaIN>='".date("Y-m-d",time())."'";
		$parameters = array(":id_cliente"=>$id,
							);
		$dataReader=Yii::app()->db->createCommand($sql)->query($parameters);
		$dataReader->bindColumn(1,$total);
		$total_monto = $dataReader->read(1);
		if(!$total_monto["total"]){
		$total_monto["total"]=0;
		}
		return $total_monto["total"];
	}
	
	/*
	FUNCI�N QUE VERIFICA QUE NO TENGA RECARGAS PENDIENTES
	
	*/
	private function validaPendientes($id){
	
		$sql = "SELECT SUM(monto) as total FROM rc_recarga WHERE idCliente=:id_cliente AND estado='LISTA' AND fechaIN>='".date("Y-m-d",time())."'";
		$parameters = array(":id_cliente"=>$id,
							);
		$dataReader=Yii::app()->db->createCommand($sql)->query($parameters);
		$dataReader->bindColumn(1,$total);
		$total_monto = $dataReader->read(1);
		if(!$total_monto["total"]){
		$total_monto["total"]=0;
		}
		return $total_monto["total"];
	}
	
	
	/**
	 * Cambia el estado de en Espera a PROCESANDO.
	 * Para mantener la bandera de que una recarga es atendida.
	 */	
	private function atenderRecarga($model){		
		
		if($model->estado=="ESPERA"){		
		try{
			$trans = Yii::app()->db->beginTransaction();
			$model->estado="PROCESANDO";
			$model->idOperador=Yii::app()->user->getName();			
			$model->save(false);
			$trans->commit();								
		}catch(Exception $e)
		{				
		 $trans->rollBack();							
		}
		}
	
		
		}
	
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
			 CASO 1 LA SE TOMA LA RECARGA POR PRIMERA VEZ . RESULTADO: 
	 */
	public function actionUpdate($id)
	{
		if(!$this->validaOperador()){
		$this->accessDenied();
		}
		
		$model=$this->loadModel($id);
		$this->atenderRecarga($model);
		if(isset($_POST['Recarga']))
		{
			$model->attributes=$_POST['Recarga'];
			$model->fechaED=date("Y-m-d H:i:s",time());
			$model->tiempo=$this->calcularTiempo($id,$model->fechaED);
			if($model->save()){
			if(Yii::app()->user->getName()=="admin"){
				$this->redirect(array('admin'));
			}else{
				$this->redirect(array('index'));
			}
		}
		}
		
		
		if($model->estado=="PROCESANDO"){
			if($model->idOperador!=Yii::app()->user->getName()){
			if(Yii::app()->user->getName()!="admin"){
			$this->redirect(array('index'));
				}
			}				
		}
		
		
		$this->render('update',array(
			'model'=>$model,
			'estado'=>$model->estado,
		));
		
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest())
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}
	
	/**
	* Muestra las recargas atendidas por un operador
	* mostrando una grilla
	* ademas debe mostrar un calculo de total recaudado diario
	*/
	public function actionRealizadas()
	{
		if(Yii::app()->user->isGuest)
			$this->redirect(Yii::app()->urlManager->createUrl('/site/login'));
		
		if(!$this->validaOperador())
			$this->redirect(Yii::app()->urlManager->createUrl('/recarga/create'));
			
		$model=new Recarga('search');
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Recarga']))
			$model->attributes=$_GET['Recarga'];	
			
		
		
		$this->render('aprobadas',array(
			'model'=>$model,
		));
	}
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		if(Yii::app()->user->isGuest)
			$this->redirect(Yii::app()->urlManager->createUrl('/site/login'));
		
		if(!$this->validaOperador()){
		$this->accessDenied();
		}
		$criteria=new CDbCriteria(array(
			'condition'=>'estado="ESPERA"',
			'order'=>'fechaIN DESC',
		));
		$criteriaAlter=new CDbCriteria(array(
			'condition'=>'estado="PROCESANDO" and idOperador="'.Yii::app()->user->getName().'"',
			'order'=>'fechaIN DESC',
		));
		
		$dataAlter=new CActiveDataProvider('Recarga',array(
			'criteria'=>$criteriaAlter,
			'pagination' => array(
                    'pageSize'=>50
                ),
		));
		
		$dataProvider=new CActiveDataProvider('Recarga',array(
			'criteria'=>$criteria,
			'pagination' => array(
                    'pageSize'=>50
                ),
		));
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
			'dataProviderAlter'=>$dataAlter,
			'menuUser'=>array(
				array('label'=>'Create Recarga', 'url'=>array('create')),
				array('label'=>'Manage Recarga', 'url'=>array('admin')),
			),

		));
	}
	public function actionMias()
	{
		if(Yii::app()->user->isGuest)
			$this->redirect(Yii::app()->urlManager->createUrl('/site/login'));
		
		$result=$this->validaCliente();
		if(!$result){
		$this->accessDenied();
		}
		

			
		$criteria=new CDbCriteria(array(
			'condition'=>'idCliente="'.Yii::app()->user->getName().'"',
			'order'=>'fechaIN DESC',
			'limit'=>500,
		));
		
		$dataProvider=new CActiveDataProvider('Recarga',array(
			'criteria'=>$criteria,
		));
		$dataProvider->setPagination(false);
		//CONSULTA DE TOTAL DE MONTOS RECARGADOS
		$dataSUM = $this->calcularTotal(Yii::app()->user->getName());
		//FIN CONSULTA
		$this->render('mias',array(
			'dataProvider'=>$dataProvider,
			'dataSUM'=>$dataSUM,
		));
	}
	
	public function actionUltimas()
	{
		if(Yii::app()->user->isGuest)
			$this->redirect(Yii::app()->urlManager->createUrl('/site/login'));
		
		$result=$this->validaCliente();
		if(!$result){
		$this->accessDenied();
		}
		

			
		$criteria=new CDbCriteria(array(
			'condition'=>'idCliente="'.Yii::app()->user->getName().'"',
			'order'=>'fechaIN DESC',
			'limit'=>20,
		));
		
		$dataProvider=new CActiveDataProvider('Recarga',array(
			'criteria'=>$criteria,

		));
		$dataProvider->setPagination(false);
		//CONSULTA DE TOTAL DE MONTOS RECARGADOS
		$dataSUM = $this->calcularTotal(Yii::app()->user->getName());
		//FIN CONSULTA
		$this->render('ultimas',array(
			'dataProvider'=>$dataProvider,
			'dataSUM'=>$dataSUM,
		));
	}

	/**
	 * ADMIN SUPERVISOR
	 */
	public function actionSupervisor()
	{
	
		if(Yii::app()->user->isGuest)
			$this->redirect(Yii::app()->urlManager->createUrl('/site/login'));
		
		$result=$this->validaSupervisor();
		if(!$result){
		$this->accessDenied();
		}
		
		$model=new Recarga('seach');
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Recarga']))
			$model->attributes=$_GET['Recarga'];

		$this->render('supervisor',array(
			'model'=>$model,
		));
	}
	
	
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Recarga('seach');
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Recarga']))
			$model->attributes=$_GET['Recarga'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function actionAdminbb()
	{
		$model=new Recarga('seach');
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Recarga']))
			$model->attributes=$_GET['Recarga'];

		$this->render('adminBB',array(
			'model'=>$model,
		));
	}
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Recarga::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='recarga-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
