
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'venta-ss-form',
        'layout'=>TbHtml::FORM_LAYOUT_HORIZONTAL,
        'enableAjaxValidation' => true,
));
?>

	<p class="help-block">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<?php echo $form->textFieldControlGroup($model,'codigo', array('span'=>3, 'maxlength'=>45)); ?>
		<div class='row-fluid'><div class='span7'><?php echo $form->dropDownListControlGroup($model,'color',  array('Blanco' => 'Blanco', 'Negro' => 'Negro', 'Beige' => 'Beige', 'Rosa' => 'Rosa', 'Gris' => 'Gris', 'Azul' => 'Azul', 'Rojo' => 'Rojo', 'Marfil' => 'Marfil', 'Navy' => 'Navy'), array('class'=>'span12 selectpicker', 'empty'=>'', 'placeholder'=> Yii::t('app', 'Select') ));?></div></div>
                <?php echo $form->textFieldControlGroup($model,'cantidad', array('span'=>3, 'maxlength'=>11)); ?>
                <?php echo $form->textFieldControlGroup($model,'precio', array('span'=>3, 'maxlength'=>11)); ?>
		<div class='row-fluid'><div class='span7'><?php echo $form->dropDownListControlGroup($model,'talla',  array('24' => '24','25' => '25','26' => '26','27' => '27','28' => '28','29' => '29','30' => '30','31' => '31','32' => '32','33' => '33','34' => '34','35' => '35', '36' => '36', '37' => '37', '38' => '38', '39' => '39', '40' => '40', '41' => '41', '42' => '42', '43' => '43', '44' => '44'), array('class'=>'span12 selectpicker', 'empty'=>'', 'placeholder'=> Yii::t('app', 'Select') ));?></div></div>
                
<?php echo TbHtml::formActions(array(
    TbHtml::submitButton($model->isNewRecord ? Yii::t('app', 'Guardar') : Yii::t('app', 'Save'), array('color' => TbHtml::BUTTON_COLOR_PRIMARY, 'id'=>'buttonStateful', 'data-loading-text'=>  Yii::t('app', 'Loading...'))),
    //TbHtml::linkButton(Yii::t('app','Pagar'),array('url'=>array('crearPago', 'id'=>$model->venta_id),'color'=>TbHtml::BUTTON_COLOR_DANGER)),
));
?>
<?php $this->endWidget(); ?>        
 <?php $this->widget('bootstrap.widgets.TbModal', array(
                            'id' => 'modal',
                            'header' => Yii::t('app','Create'),
                            'content' => '<div id="contenido"></div>',
                            'footer' => array(
                                TbHtml::button('Close', array('data-dismiss' => 'modal')),
                             ),
                        )); ?>                                                                                                      <?php 
Yii::app()->clientScript->registerScript('venta', "function venta_crear(){ jQuery.ajax({'url':'/zsmo/venta/json.html','data':$(this).serialize(),'type':'post','dataType':'json','success':function(data)
                {
                if (data.estado == 'failure')
                {
                    $('#contenido').html(data.contenido);
                    $('#contenido form').submit(venta_crear);
                }
                else
                {
                    $('#contenido').html(data.contenido);
                    
		 }                   	
                },'cache':false}); 
               return false;}", CClientScript::POS_END)?><?php 
Yii::app()->clientScript->registerScript('venta_click', "$('#venta_boton').click(function(event){event.preventDefault(); venta_crear(); });", CClientScript::POS_READY)?><?php $this->widget('ext.select2.ESelect2'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/buttonReset.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/buttonStateful.js', CClientScript::POS_END); ?>
