<?php

class Variables {
    
/**
 * Variable que devuelve arreglo de meses
 * donde Indice y valor son iguales
 * y contienen los valores de los meses
 * @property $meses = array('key'=>'value);
 */    
    
    public static $nombreMeses = array(
        'Enero' => 'Enero',
        'Febrero' => 'Febrero',
        'Marzo' => 'Marzo',
        'Abril' => 'Abril',
        'Mayo' => 'Mayo',
        'Junio' => 'Junio',
        'Julio' => 'Julio',
        'Agosto' => 'Agosto',
        'Septiembre' => 'Septiembre',
        'Octubre' => 'Octubre',
        'Noviembre' => 'Noviembre',
        'Diciembre' => 'Diciembre',
    );  
    
}

?>
