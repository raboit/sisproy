<?php


Yii::import('application.models._base.BaseProyecto');

class Proyecto extends BaseProyecto
{
        
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
	public function rules() {
		return array_merge(parent::rules(), array(
                        //array('archivo, contrato', 'file', 'on' => 'crear'),
                        array('archivo, contrato', 'file', 'on'=>'crear, actualizar', 'allowEmpty'=>true), 
		));
	} 
        
	public static function representingColumn() {
		return 'sigla';
	}
        
	public function relations() {
		return array_merge(parent::relations(), array(
			'totalPresupuestos' => array(self::STAT, 'Presupuesto', 'proyecto_id',
                            'select'=> 'SUM(monto)',
                            ),
                        'totalSolicitudesEmitidas' => array(self::STAT, 'Solicitud', 'proyecto_id',
                            'select' => 'SUM(total)',
                            'condition' => 'estado=\'EMITIDA\'',
                            ),
			'totalIngresos' => array(self::STAT, 'Ingreso', 'proyecto_id',
                            'select'=> 'SUM(monto)',
                            ),
			'totalEgresos' => array(self::STAT, 'Egreso', 'proyecto_id',
                            'select'=> 'SUM(monto)',
                            ),
		));
	}
        public function all() {
		return new CActiveDataProvider($this);
	}
        
        public function getSaldo(){
            if(isset($this->presupuesto->monto) && isset($this->totalIngresos))
                return $this->presupuesto->monto - $this->totalIngresos;
            return null;
        }
}
