<?php


Yii::import('application.models._base.BaseCuenta');

class Cuenta extends BaseCuenta
{
        
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
        public static function representingColumn() {
        return array('numero_cuenta', 'banco');
        }
        
        public function __toString() {
                $columnSeparator = ' ';
                $representingColumn = $this->representingColumn();

                if (($representingColumn === null) || ($representingColumn === array()))
                    if ($this->getTableSchema()->primaryKey !== null) {
                        $representingColumn = $this->getTableSchema()->primaryKey;
                    } else {
                        $columnNames = $this->getTableSchema()->getColumnNames();
                        $representingColumn = $columnNames[0];
                    }

                if (is_array($representingColumn)) {
                    $part = '';
                    foreach ($representingColumn as $representingColumn_item) {
                        $part .= ( $this->$representingColumn_item === null ? '' : $this->$representingColumn_item) . $columnSeparator;
                    }
                    return substr($part, 0, -1);
                } else {
                    return $this->$representingColumn === null ? '' : (string) $this->$representingColumn;
                }
        }
}