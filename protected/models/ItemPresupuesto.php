<?php


Yii::import('application.models._base.BaseItemPresupuesto');

class ItemPresupuesto extends BaseItemPresupuesto
{
        public $fecha_inicio;
        public $fecha_termino;
        
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function rules() {
		return array_merge(parent::rules(), array(
                        array('fecha_inicio, fecha_termino', 'safe', 'on'=>'search'),
		));
	}
	public function attributeLabels() {
		return array_merge(parent::attributeLabels(), array(
    			'fecha_inicio' => Yii::t('app', 'fecha_inicio'),
    			'fecha_termino' => Yii::t('app', 'fecha_termino'),
    		));
	}  

	public function getComprometido(){
            $model_disponible = ItemSolicitud::model()->with(array(
                                            'cuentaContable.itemPresupuestos'=>array(
                                                    'condition' => 'itemPresupuestos.id = :id_cc AND solicitud.proyecto_id = itemPresupuestos.proyecto_id AND (solicitud.estado = \'EMITIDA\' AND emisions.solicitud_id = solicitud.id AND emisions.estado = \'PENDIENTE\')  AND YEAR(solicitud.fecha) = YEAR(itemPresupuestos.fecha) AND MONTH(solicitud.fecha) = MONTH(itemPresupuestos.fecha)',
                                                    'params'=>array(':id_cc'=>$this->id),
                                                ),
                                            'solicitud.emisions'))->findAll();
            return Funciones::Suma($model_disponible, 'total');
            
        }
        public function getEmitido(){
            $model_disponible = ItemSolicitud::model()->with(array(
                                            'cuentaContable.itemPresupuestos'=>array(
                                                    'condition' => 'itemPresupuestos.id = :id_cc AND solicitud.proyecto_id = itemPresupuestos.proyecto_id AND (solicitud.estado = \'EMITIDA\' AND emisions.solicitud_id = solicitud.id AND emisions.estado = \'FINALIZADA\')  AND YEAR(solicitud.fecha) = YEAR(itemPresupuestos.fecha) AND MONTH(solicitud.fecha) = MONTH(itemPresupuestos.fecha)',
                                                    'params'=>array(':id_cc'=>$this->id),
                                                ),
                                            'solicitud.emisions'))->findAll();
            return Funciones::Suma($model_disponible, 'total');
            
        }
        
	public function search() {
		$criteria = parent::search()->getCriteria();

                if((isset($this->fecha_inicio) && trim($this->fecha_inicio) != "") && (isset($this->fecha_termino) && trim($this->fecha_termino) != ""))                
                	$criteria->addBetweenCondition('fecha', $this->fecha_inicio, $this->fecha_termino);
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
                        ));
	}
        
        public function misItems($id) {
		$criteria = new CDbCriteria;
		$criteria->compare('presupuesto_id', $id);
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
                        'pagination'=>false,
		));
	}
        
        public function behaviors() {
            return array('datetimeI18NBehavior' => array('class' => 'ext.DateTimeI18NBehavior'));
        }
}