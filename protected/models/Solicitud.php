<?php


Yii::import('application.models._base.BaseSolicitud');

class Solicitud extends BaseSolicitud
{
        public $fecha_inicio;
        public $fecha_termino;
        
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
	public static function representingColumn() {
		return 'id';
	}
        
	public function rules() {
		return array_merge(parent::rules(), array(
                        array('fecha_inicio, fecha_termino', 'safe', 'on'=>'search'),
		));
	}
	public function attributeLabels() {
		return array_merge(parent::attributeLabels(), array(
    			'fecha_inicio' => Yii::t('app', 'fecha_inicio'),
    			'fecha_termino' => Yii::t('app', 'fecha_termino'),
    		));
	}
        public function relations() {
		return array_merge(parent::relations(),array(
                            'sumaMontos'=>array(self::STAT, 'ItemSolicitud','solicitud_id', 'select' => 'SUM(t.total)')
                        ));                  
	}
        
        
	public function search() {
		$criteria = parent::search()->getCriteria();
//                $this->beforeSave();
//                    if(!empty($this->fecha))
//                        $criteria->compare('fecha', Yii::app()->dateFormatter->format('yyyy-MM-dd',$this->fecha), true);
                        //throw new CHttpException(400, $this->fecha);
                        //throw new CHttpException(400, $this->fecha_inicio);
                        //throw new CHttpException(400, Yii::app()->dateFormatter->format('yyyy-dd-MM',$this->fecha));
                if((isset($this->fecha_inicio) && trim($this->fecha_inicio) != "") && (isset($this->fecha_termino) && trim($this->fecha_termino) != ""))                
                	$criteria->addBetweenCondition('fecha', $this->fecha_inicio, $this->fecha_termino);
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
                        ));
	}
        
        public function finalizadas() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('proyecto_id', $this->proyecto_id);
		$criteria->compare('proveedor_id', $this->proveedor_id);
		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('fecha', $this->fecha, true);
		$criteria->compare('tipo_solicitud', $this->tipo_solicitud, true);
		$criteria->compare('total', $this->total);
		$criteria->compare('estado', 'FINALIZADA', true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
        public function all() {
		return new CActiveDataProvider($this);
	}
        public function behaviors() {
            return array('datetimeI18NBehavior' => array('class' => 'ext.DateTimeI18NBehavior'));
        }
}
