<?php


Yii::import('application.models._base.BaseCentroCosto');

class CentroCosto extends BaseCentroCosto
{
        
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        public function all() {
		return new CActiveDataProvider($this);
	}
}