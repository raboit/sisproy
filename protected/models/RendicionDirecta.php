<?php


Yii::import('application.models._base.BaseRendicionDirecta');

class RendicionDirecta extends BaseRendicionDirecta
{
        
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
        public static function label($n = 1) {
		return Yii::t('app', 'Rendicion Directa|Rendiciones Directas', $n);
	}
}