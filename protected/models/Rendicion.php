<?php


Yii::import('application.models._base.BaseRendicion');

class Rendicion extends BaseRendicion
{
        public $fecha_inicio;
        public $fecha_termino;
        
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function rules() {
		return array_merge(parent::rules(), array(
                        array('fecha_inicio, fecha_termino', 'safe', 'on'=>'search'),
		));
	}
        
        public static function representingColumn() {
		return 'id';
	}
        
	public function attributeLabels() {
		return array_merge(parent::attributeLabels(), array(
    			'fecha_inicio' => Yii::t('app', 'fecha_inicio'),
    			'fecha_termino' => Yii::t('app', 'fecha_termino'),
    		));
	}
        
	public function search() {
		$criteria = parent::search()->getCriteria();

                if((isset($this->fecha_inicio) && trim($this->fecha_inicio) != "") && (isset($this->fecha_termino) && trim($this->fecha_termino) != ""))                
                	$criteria->addBetweenCondition('fecha', $this->fecha_inicio, $this->fecha_termino);
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
                        ));
	}
        
        public function finalizadas() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('emision_id', $this->emision_id);
		$criteria->compare('asignacion', $this->asignacion, true);
		$criteria->compare('fecha', $this->fecha, true);
		$criteria->compare('comuna', $this->comuna, true);
		$criteria->compare('region', $this->region, true);
		$criteria->compare('tipo_pago', $this->tipo_pago, true);
		$criteria->compare('total_entrada', $this->total_entrada);
		$criteria->compare('total_salida', $this->total_salida);
		$criteria->compare('total_saldo', $this->total_saldo);
		$criteria->compare('observacion', $this->observacion, true);
		$criteria->compare('estado', 'FINALIZADA', true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
        public function all() {
		return new CActiveDataProvider($this);
	}

        public function behaviors() {
            return array('datetimeI18NBehavior' => array('class' => 'ext.DateTimeI18NBehavior'));
        }
}