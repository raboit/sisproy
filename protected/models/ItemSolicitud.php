<?php


Yii::import('application.models._base.BaseItemSolicitud');

class ItemSolicitud extends BaseItemSolicitud
{
        
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
        public function misItems($solicitud_id) {
		$criteria = new CDbCriteria;
		$criteria->compare('solicitud_id', $solicitud_id);
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
                        'pagination'=>false,
		));
	}
        public function Cuenta($id_cuenta="",$fecha="")
        {
                $this->getDbCriteria()->mergeWith(array(
//                        'select'=>'SUM(t.total)',
//                        'group'=>'t.cuenta_contable_id',                        
                        'condition' => 'cuenta_contable_id = :cuenta_id AND MONTH(solicitud.fecha) = MONTH(:fecha) AND YEAR(solicitud.fecha) = YEAR(:fecha) ', 
                        'params' => array(':cuenta_id'=>(int)$id_cuenta, ':fecha'=> $fecha),
            ));
            return $this;
        }
        
        
        
        public function attributeLabels() {
		return array(
			'descripcion' => Yii::t('app', 'Observacion'),
			
		);
	}
}