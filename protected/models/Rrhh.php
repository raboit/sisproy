<?php


Yii::import('application.models._base.BaseRrhh');

class Rrhh extends BaseRrhh
{
        public $fecha_nacimiento_inicio;
        public $fecha_nacimiento_termino;
        
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function rules() {
		return array_merge(parent::rules(), array(
                        array('fecha_nacimiento_inicio, fecha_nacimiento_termino', 'safe', 'on'=>'search'),
                        array('rut', 'unique'),
                        array('rut', 'rutValido'),
		));
	}
	public function attributeLabels() {
		return array_merge(parent::attributeLabels(), array(
    			'fecha_nacimiento_inicio' => Yii::t('app', 'fecha_nacimiento_inicio'),
    			'fecha_nacimiento_termino' => Yii::t('app', 'fecha_nacimiento_termino'),
                        'rut' => Yii::t('app', 'N° Documento'),
    		));
	}
        
	public function search() {
		$criteria = parent::search()->getCriteria();

                if((isset($this->fecha_nacimiento_inicio) && trim($this->fecha_nacimiento_inicio) != "") && (isset($this->fecha_nacimiento_termino) && trim($this->fecha_nacimiento_termino) != ""))                
                	$criteria->addBetweenCondition('fecha_nacimiento', $this->fecha_nacimiento_inicio, $this->fecha_nacimiento_termino);
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
                        ));
	}
        public function all() {
		return new CActiveDataProvider($this);
	}
        
        public static function representingColumn() {
        return array('nombres', 'apellidos');
        }
        
        public function __toString() {
                $columnSeparator = ' ';
                $representingColumn = $this->representingColumn();

                if (($representingColumn === null) || ($representingColumn === array()))
                    if ($this->getTableSchema()->primaryKey !== null) {
                        $representingColumn = $this->getTableSchema()->primaryKey;
                    } else {
                        $columnNames = $this->getTableSchema()->getColumnNames();
                        $representingColumn = $columnNames[0];
                    }

                if (is_array($representingColumn)) {
                    $part = '';
                    foreach ($representingColumn as $representingColumn_item) {
                        $part .= ( $this->$representingColumn_item === null ? '' : $this->$representingColumn_item) . $columnSeparator;
                    }
                    return substr($part, 0, -1);
                } else {
                    return $this->$representingColumn === null ? '' : (string) $this->$representingColumn;
                }
        }
            
        public function behaviors() {
            return array('datetimeI18NBehavior' => array('class' => 'ext.DateTimeI18NBehavior'));
        }

        public function rutValido($rut) {
            if(empty($this->$rut)) return;
            $validador = new Run;
            if (!$validador->isRut($this->$rut)) {
                $this->addError($rut, Yii::t('app', 'RUT Invalido'));
            }
        }
        
}
