<?php


Yii::import('application.models._base.BaseEmision');

class Emision extends BaseEmision
{
        public $fecha_inicio;
        public $fecha_termino;
        
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
	public static function representingColumn() {
		return 'id';
	}
        
	public function rules() {
		return array_merge(parent::rules(), array(
                        array('fecha_inicio, fecha_termino', 'safe', 'on'=>'search'),
		));
	}
	public function attributeLabels() {
		return array_merge(parent::attributeLabels(), array(
    			'fecha_inicio' => Yii::t('app', 'fecha_inicio'),
    			'fecha_termino' => Yii::t('app', 'fecha_termino'),
    		));
	}
        
	public function search() {
		$criteria = parent::search()->getCriteria();

                if((isset($this->fecha_inicio) && trim($this->fecha_inicio) != "") && (isset($this->fecha_termino) && trim($this->fecha_termino) != ""))                
                	$criteria->addBetweenCondition('fecha', $this->fecha_inicio, $this->fecha_termino);
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
                        ));
	}
        
        public function finalizadas() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('proveedor_id', $this->proveedor_id);
		$criteria->compare('proyecto_id', $this->proyecto_id);
		$criteria->compare('solicitud_id', $this->solicitud_id);
		$criteria->compare('ciudad', $this->ciudad, true);
		$criteria->compare('fecha', $this->fecha, true);
		$criteria->compare('concepto', $this->concepto, true);
		$criteria->compare('total_debe', $this->total_debe);
		$criteria->compare('total_haber', $this->total_haber);
		$criteria->compare('cuenta', $this->cuenta, true);
		$criteria->compare('tipo_solicitud', $this->tipo_solicitud, true);
		$criteria->compare('numero_cheque', $this->numero_cheque, true);
		$criteria->compare('observaciones', $this->observaciones, true);
                $criteria->compare('estado', 'FINALIZADA', true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
        public function all() {
		return new CActiveDataProvider($this);
	}
        
        public function behaviors() {
            return array('datetimeI18NBehavior' => array('class' => 'ext.DateTimeI18NBehavior'));
        }
}