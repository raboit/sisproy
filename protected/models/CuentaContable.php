<?php


Yii::import('application.models._base.BaseCuentaContable');

class CuentaContable extends BaseCuentaContable
{
        
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        public function all() {
		return new CActiveDataProvider($this);
	}
}