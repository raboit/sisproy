<?php


Yii::import('application.models._base.BaseProveedor');

class Proveedor extends BaseProveedor
{
 
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
	public static function representingColumn() {
		return 'nombre';
	}
        public function all() {
		return new CActiveDataProvider($this);
	}
        
}