<?php

Yii::import('application.models._base.BaseEgresosReales');

class EgresosReales extends BaseEgresosReales
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
        public function primaryKey(){
            	return 'rendicion';
        }
        
        public static function label($n = 1) {
		return Yii::t('app', 'Egreso Real|Egresos Reales', $n);
	}
}