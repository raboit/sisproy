<?php


Yii::import('application.models._base.BaseContrato');

class Contrato extends BaseContrato
{
        public $fecha_inicio_inicio;
        public $fecha_inicio_termino;
        public $fecha_termino_inicio;
        public $fecha_termino_termino;
        
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
	public static function representingColumn() {
		return 'id';
	}
        
	public function rules() {
		return array_merge(parent::rules(), array(
                        array('fecha_inicio_inicio, fecha_inicio_termino, fecha_termino_inicio, fecha_termino_termino', 'safe', 'on'=>'search'),
		));
	}
	public function attributeLabels() {
		return array_merge(parent::attributeLabels(), array(
    			'fecha_inicio_inicio' => Yii::t('app', 'fecha_inicio_inicio'),
    			'fecha_inicio_termino' => Yii::t('app', 'fecha_inicio_termino'),
    			'fecha_termino_inicio' => Yii::t('app', 'fecha_termino_inicio'),
    			'fecha_termino_termino' => Yii::t('app', 'fecha_termino_termino'),
                        'remuneracion' => Yii::t('app', 'Remuneracion Mensual'),
    		));
	}
        
	public function search() {
		$criteria = parent::search()->getCriteria();

                if((isset($this->fecha_inicio_inicio) && trim($this->fecha_inicio_inicio) != "") && (isset($this->fecha_inicio_termino) && trim($this->fecha_inicio_termino) != ""))                
                	$criteria->addBetweenCondition('fecha_inicio', $this->fecha_inicio_inicio, $this->fecha_inicio_termino);                if((isset($this->fecha_termino_inicio) && trim($this->fecha_termino_inicio) != "") && (isset($this->fecha_termino_termino) && trim($this->fecha_termino_termino) != ""))                
                	$criteria->addBetweenCondition('fecha_termino', $this->fecha_termino_inicio, $this->fecha_termino_termino);
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
                        ));
	}
        
        public function misContratos($id) {
		$criteria = new CDbCriteria;
		$criteria->compare('rrhh_id', $id);
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
        
        public function behaviors() {
            return array('datetimeI18NBehavior' => array('class' => 'ext.DateTimeI18NBehavior'));
        }
}