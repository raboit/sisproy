<?php


Yii::import('application.models._base.BaseCuentaEspecifica');

class CuentaEspecifica extends BaseCuentaEspecifica
{
        
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        public function all() {
		return new CActiveDataProvider($this);
	}
}