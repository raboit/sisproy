<?php


Yii::import('application.models._base.BaseItemRendicion');

class ItemRendicion extends BaseItemRendicion
{
        public $fecha_inicio;
        public $fecha_termino;
        
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function rules() {
		return array_merge(parent::rules(), array(
                        array('rendicion_id', 'required', 'except'=>'sinRendicion'),
                        array('fecha_inicio, fecha_termino', 'safe', 'on'=>'search'),
                        array('monto','montoValido','on'=>'crear'),
		));
	}
        
                  
        public function getEntradaRendicion(){
                return $this->rendicion->total_entrada;
        }
        
        public function getSaldoRendicion(){
                return $this->rendicion->total_saldo;
        }
        
        public function montoValido($monto) { 
            $saldo = $this->getSaldoRendicion();
                if ($this->monto > $saldo) {
                    $resta = $this->monto - $saldo;
                    $this->addError($monto, Yii::t('app', $this->tipo_documento.' Excedida en $ '.Yii::app()->format->formatNumber($resta).' CLP'));
                }
            }
            
        
	public function attributeLabels() {
		return array_merge(parent::attributeLabels(), array(
    			'fecha_inicio' => Yii::t('app', 'fecha_inicio'),
    			'fecha_termino' => Yii::t('app', 'fecha_termino'),
    		));
	}
        
	public function search() {
		$criteria = parent::search()->getCriteria();

                if((isset($this->fecha_inicio) && trim($this->fecha_inicio) != "") && (isset($this->fecha_termino) && trim($this->fecha_termino) != ""))                
                	$criteria->addBetweenCondition('fecha', $this->fecha_inicio, $this->fecha_termino);
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
                        ));
	}

        public function misItems($rendicion_id = null, $cuenta_contable_id = null, $proyecto_id = null, $fecha = null) {
                if($rendicion_id!=null) $this->rendicion_id = $rendicion_id;
                if($cuenta_contable_id!=null) $this->cuenta_contable_id = $rendicion_id;
                if($proyecto_id!=null) $this->proyecto_id = $proyecto_id;
                if($fecha!=null) $this->fecha = date('Y-m-d', CDateTimeParser::parse($fecha, Yii::app()->locale->getDateFormat('medium')));;
                
                
		$criteria = new CDbCriteria;
//		$criteria->compare('id', $this->id);
//		$criteria->compare('proveedor_id', $this->proveedor_id);
		$criteria->compare('cuenta_contable_id', $this->cuenta_contable_id);
//		$criteria->compare('cuenta_especifica_id', $this->cuenta_especifica_id);
		$criteria->compare('rendicion_id', $this->rendicion_id);
		$criteria->compare('proyecto_id', $this->proyecto_id);
//		$criteria->compare('tipo_documento', $this->tipo_documento, true);
//		$criteria->compare('numero_documento', $this->numero_documento, true);
//		$criteria->compare('monto', $this->monto);
                $criteria->compare('fecha', $this->fecha);
                
                if($fecha!=null){
                    $criteria->compare('fecha', date('Y-m-d', CDateTimeParser::parse($fecha, Yii::app()->locale->getDateFormat('medium'))));
                }
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
                        'pagination'=>false,
		));
	}
        
//        public static function label($n = 1) {
//		return Yii::t('app', 'Egreso Reale|Egresos Reales', $n);
//	}

        
        public function behaviors() {
            return array('datetimeI18NBehavior' => array('class' => 'ext.DateTimeI18NBehavior'));
        }
}
