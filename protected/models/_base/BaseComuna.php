<?php

/**
 * This is the model base class for the table "comuna".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Comuna".
 *
 * Columns in table "comuna" available as properties of the model,
 * followed by relations of table "comuna" available as properties of the model.
 *
 * @property integer $id
 * @property integer $region_id
 * @property string $nombre
 *
 * @property Region $region
 */
abstract class BaseComuna extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'comuna';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'Comuna|Comunas', $n);
	}

	public static function representingColumn() {
		return 'nombre';
	}

	public function rules() {
		return array(
			array('region_id', 'numerical', 'integerOnly'=>true),
			array('nombre', 'length', 'max'=>45),
			array('region_id, nombre', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, region_id, nombre', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'region' => array(self::BELONGS_TO, 'Region', 'region_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'region_id' => null,
			'nombre' => Yii::t('app', 'Nombre'),
			'region' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('region_id', $this->region_id);
		$criteria->compare('nombre', $this->nombre, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}