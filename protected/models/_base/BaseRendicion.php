<?php

/**
 * This is the model base class for the table "rendicion".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Rendicion".
 *
 * Columns in table "rendicion" available as properties of the model,
 * followed by relations of table "rendicion" available as properties of the model.
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $emision_id
 * @property integer $cuenta_id
 * @property string $asignacion
 * @property string $fecha
 * @property string $comuna
 * @property string $region
 * @property string $tipo_solicitud
 * @property string $numero_cheque
 * @property string $estado
 * @property integer $total_entrada
 * @property integer $total_salida
 * @property integer $total_saldo
 * @property string $observacion
 *
 * @property ItemRendicion[] $itemRendicions
 * @property Cuenta $cuenta
 * @property Emision $emision
 * @property User $user
 */
abstract class BaseRendicion extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'rendicion';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'Rendicion|Rendicions', $n);
	}

	public static function representingColumn() {
		return 'asignacion';
	}

	public function rules() {
		return array(
			array('user_id, emision_id, cuenta_id', 'required'),
			array('user_id, emision_id, cuenta_id, total_entrada, total_salida, total_saldo', 'numerical', 'integerOnly'=>true),
			array('asignacion, comuna, region', 'length', 'max'=>100),
			array('tipo_solicitud, numero_cheque, estado', 'length', 'max'=>45),
			array('fecha, observacion', 'safe'),
			array('asignacion, fecha, comuna, region, tipo_solicitud, numero_cheque, estado, total_entrada, total_salida, total_saldo, observacion', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, user_id, emision_id, cuenta_id, asignacion, fecha, comuna, region, tipo_solicitud, numero_cheque, estado, total_entrada, total_salida, total_saldo, observacion', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'itemRendicions' => array(self::HAS_MANY, 'ItemRendicion', 'rendicion_id'),
			'cuenta' => array(self::BELONGS_TO, 'Cuenta', 'cuenta_id'),
			'emision' => array(self::BELONGS_TO, 'Emision', 'emision_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'user_id' => null,
			'emision_id' => null,
			'cuenta_id' => null,
			'asignacion' => Yii::t('app', 'Asignacion'),
			'fecha' => Yii::t('app', 'Fecha'),
			'comuna' => Yii::t('app', 'Comuna'),
			'region' => Yii::t('app', 'Region'),
			'tipo_solicitud' => Yii::t('app', 'Tipo Solicitud'),
			'numero_cheque' => Yii::t('app', 'Numero Cheque'),
			'estado' => Yii::t('app', 'Estado'),
			'total_entrada' => Yii::t('app', 'Total Entrada'),
			'total_salida' => Yii::t('app', 'Total Salida'),
			'total_saldo' => Yii::t('app', 'Total Saldo'),
			'observacion' => Yii::t('app', 'Observacion'),
			'itemRendicions' => null,
			'cuenta' => null,
			'emision' => null,
			'user' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('emision_id', $this->emision_id);
		$criteria->compare('cuenta_id', $this->cuenta_id);
		$criteria->compare('asignacion', $this->asignacion, true);
		$criteria->compare('fecha', $this->fecha, true);
		$criteria->compare('comuna', $this->comuna, true);
		$criteria->compare('region', $this->region, true);
		$criteria->compare('tipo_solicitud', $this->tipo_solicitud, true);
		$criteria->compare('numero_cheque', $this->numero_cheque, true);
		$criteria->compare('estado', $this->estado, true);
		$criteria->compare('total_entrada', $this->total_entrada);
		$criteria->compare('total_salida', $this->total_salida);
		$criteria->compare('total_saldo', $this->total_saldo);
		$criteria->compare('observacion', $this->observacion, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}