<?php

/**
 * This is the model base class for the table "user".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "User".
 *
 * Columns in table "user" available as properties of the model,
 * followed by relations of table "user" available as properties of the model.
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $tipo
 * @property integer $rrhh_id
 *
 * @property Emision[] $emisions
 * @property Rendicion[] $rendicions
 * @property Solicitud[] $solicituds
 * @property Rrhh $rrhh
 */
abstract class BaseUser extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'user';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'User|Users', $n);
	}

	public static function representingColumn() {
		return 'username';
	}

	public function rules() {
		return array(
			array('username, password', 'required'),
			array('rrhh_id', 'numerical', 'integerOnly'=>true),
			array('username, password, email', 'length', 'max'=>128),
                        array('tipo', 'length', 'max'=>45),
			array('email, rrhh_id, tipo', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, username, password, email, rrhh_id, tipo', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'emisions' => array(self::HAS_MANY, 'Emision', 'user_id'),
			'rendicions' => array(self::HAS_MANY, 'Rendicion', 'user_id'),
			'solicituds' => array(self::HAS_MANY, 'Solicitud', 'user_id'),
			'rrhh' => array(self::BELONGS_TO, 'Rrhh', 'rrhh_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'username' => Yii::t('app', 'Username'),
			'password' => Yii::t('app', 'Password'),
			'email' => Yii::t('app', 'Email'),
                        'tipo' => Yii::t('app', 'Tipo'),
			'rrhh_id' => null,
			'emisions' => null,
			'rendicions' => null,
			'solicituds' => null,
			'rrhh' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('username', $this->username, true);
		$criteria->compare('password', $this->password, true);
		$criteria->compare('email', $this->email, true);
                $criteria->compare('tipo', $this->email, true);
		$criteria->compare('rrhh_id', $this->rrhh_id);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}
