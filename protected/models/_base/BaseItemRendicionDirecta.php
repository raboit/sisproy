<?php

/**
 * This is the model base class for the table "item_rendicion_directa".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "ItemRendicionDirecta".
 *
 * Columns in table "item_rendicion_directa" available as properties of the model,
 * followed by relations of table "item_rendicion_directa" available as properties of the model.
 *
 * @property integer $id
 * @property integer $rendicion_directa_id
 * @property integer $proveedor_id
 * @property integer $cuenta_contable_id
 * @property integer $cuenta_especifica_id
 * @property integer $proyecto_id
 * @property string $tipo_documento
 * @property string $numero_documento
 * @property string $fecha
 * @property string $mes
 * @property string $agno
 * @property integer $monto
 *
 * @property RendicionDirecta $rendicionDirecta
 * @property Proveedor $proveedor
 * @property CuentaContable $cuentaContable
 * @property CuentaEspecifica $cuentaEspecifica
 * @property Proyecto $proyecto
 */
abstract class BaseItemRendicionDirecta extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'item_rendicion_directa';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'ItemRendicionDirecta|ItemRendicionDirectas', $n);
	}

	public static function representingColumn() {
		return 'tipo_documento';
	}

	public function rules() {
		return array(
			array('rendicion_directa_id, proveedor_id, cuenta_contable_id, cuenta_especifica_id, proyecto_id', 'required'),
			array('rendicion_directa_id, proveedor_id, cuenta_contable_id, cuenta_especifica_id, proyecto_id, monto', 'numerical', 'integerOnly'=>true),
			array('tipo_documento, numero_documento, mes, agno', 'length', 'max'=>45),
			array('fecha', 'safe'),
			array('tipo_documento, numero_documento, fecha, mes, agno, monto', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, rendicion_directa_id, proveedor_id, cuenta_contable_id, cuenta_especifica_id, proyecto_id, tipo_documento, numero_documento, fecha, mes, agno, monto', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'rendicionDirecta' => array(self::BELONGS_TO, 'RendicionDirecta', 'rendicion_directa_id'),
			'proveedor' => array(self::BELONGS_TO, 'Proveedor', 'proveedor_id'),
			'cuentaContable' => array(self::BELONGS_TO, 'CuentaContable', 'cuenta_contable_id'),
			'cuentaEspecifica' => array(self::BELONGS_TO, 'CuentaEspecifica', 'cuenta_especifica_id'),
			'proyecto' => array(self::BELONGS_TO, 'Proyecto', 'proyecto_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'rendicion_directa_id' => null,
			'proveedor_id' => null,
			'cuenta_contable_id' => null,
			'cuenta_especifica_id' => null,
			'proyecto_id' => null,
			'tipo_documento' => Yii::t('app', 'Tipo Documento'),
			'numero_documento' => Yii::t('app', 'Numero Documento'),
			'fecha' => Yii::t('app', 'Fecha'),
			'mes' => Yii::t('app', 'Mes'),
			'agno' => Yii::t('app', 'Agno'),
			'monto' => Yii::t('app', 'Monto'),
			'rendicionDirecta' => null,
			'proveedor' => null,
			'cuentaContable' => null,
			'cuentaEspecifica' => null,
			'proyecto' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('rendicion_directa_id', $this->rendicion_directa_id);
		$criteria->compare('proveedor_id', $this->proveedor_id);
		$criteria->compare('cuenta_contable_id', $this->cuenta_contable_id);
		$criteria->compare('cuenta_especifica_id', $this->cuenta_especifica_id);
		$criteria->compare('proyecto_id', $this->proyecto_id);
		$criteria->compare('tipo_documento', $this->tipo_documento, true);
		$criteria->compare('numero_documento', $this->numero_documento, true);
		$criteria->compare('fecha', $this->fecha, true);
		$criteria->compare('mes', $this->mes, true);
		$criteria->compare('agno', $this->agno, true);
		$criteria->compare('monto', $this->monto);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}