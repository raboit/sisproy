<?php

/**
 * This is the model base class for the table "cuenta_contable".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "CuentaContable".
 *
 * Columns in table "cuenta_contable" available as properties of the model,
 * followed by relations of table "cuenta_contable" available as properties of the model.
 *
 * @property integer $id
 * @property string $nombre
 *
 * @property CuentaEspecifica[] $cuentaEspecificas
 * @property ItemPresupuesto[] $itemPresupuestos
 * @property ItemRendicion[] $itemRendicions
 * @property ItemRendicionDirecta[] $itemRendicionDirectas
 * @property ItemSolicitud[] $itemSolicituds
 */
abstract class BaseCuentaContable extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'cuenta_contable';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'CuentaContable|CuentaContables', $n);
	}

	public static function representingColumn() {
		return 'nombre';
	}

	public function rules() {
		return array(
			array('nombre', 'required'),
			array('nombre', 'length', 'max'=>100),
			array('id, nombre', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'cuentaEspecificas' => array(self::HAS_MANY, 'CuentaEspecifica', 'cuenta_contable_id'),
			'itemPresupuestos' => array(self::HAS_MANY, 'ItemPresupuesto', 'cuenta_contable_id'),
			'itemRendicions' => array(self::HAS_MANY, 'ItemRendicion', 'cuenta_contable_id'),
			'itemRendicionDirectas' => array(self::HAS_MANY, 'ItemRendicionDirecta', 'cuenta_contable_id'),
			'itemSolicituds' => array(self::HAS_MANY, 'ItemSolicitud', 'cuenta_contable_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'nombre' => Yii::t('app', 'Nombre'),
			'cuentaEspecificas' => null,
			'itemPresupuestos' => null,
			'itemRendicions' => null,
			'itemRendicionDirectas' => null,
			'itemSolicituds' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('nombre', $this->nombre, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}