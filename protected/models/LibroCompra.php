<?php


Yii::import('application.models._base.BaseLibroCompra');

class LibroCompra extends BaseLibroCompra
{
        public $fecha_emision_inicio;
        public $fecha_emision_termino;
        public $fecha_item_rendicion_inicio;
        public $fecha_item_rendicion_termino;
        
        
        
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
        public function primaryKey(){
            return 'item_rendicion_id';
        }  
                
                
	public function rules() {
		return array_merge(parent::rules(), array(
                        array('fecha_emision_inicio, fecha_emision_termino, fecha_item_rendicion_inicio, fecha_item_rendicion_termino', 'safe', 'on'=>'search'),
		));
	}
	public function attributeLabels() {
		return array_merge(parent::attributeLabels(), array(
    			'fecha_emision_inicio' => Yii::t('app', 'fecha_emision_inicio'),
    			'fecha_emision_termino' => Yii::t('app', 'fecha_emision_termino'),
    			'fecha_item_rendicion_inicio' => Yii::t('app', 'fecha_item_rendicion_inicio'),
    			'fecha_item_rendicion_termino' => Yii::t('app', 'fecha_item_rendicion_termino'),
                        'ano_emision' => Yii::t('app', 'Año Emisión'),
			'dia_item_rendicion' => Yii::t('app', 'Dia'),
			'mes_item_rendicion' => Yii::t('app', 'Mes'),
			'ano_item_rendicion' => Yii::t('app', 'Año'),
                        'item_rendicion_id' => Yii::t('app', 'Nº comprobante'),
                        'monto' => Yii::t('app', 'Total'),
                        
    		));
	}
        
	public function search() {
		$criteria = parent::search()->getCriteria();

                if((isset($this->fecha_emision_inicio) && trim($this->fecha_emision_inicio) != "") && (isset($this->fecha_emision_termino) && trim($this->fecha_emision_termino) != ""))                
                	$criteria->addBetweenCondition('fecha_emision', $this->fecha_emision_inicio, $this->fecha_emision_termino);                if((isset($this->fecha_item_rendicion_inicio) && trim($this->fecha_item_rendicion_inicio) != "") && (isset($this->fecha_item_rendicion_termino) && trim($this->fecha_item_rendicion_termino) != ""))                
                	$criteria->addBetweenCondition('fecha_item_rendicion', $this->fecha_item_rendicion_inicio, $this->fecha_item_rendicion_termino);
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
                        ));
	}
}