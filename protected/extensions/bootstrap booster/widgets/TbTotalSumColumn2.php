<?php
/**
 * TbTotalSumColumn2 widget class
 *
 * @author: antonio ramirez <antonio@clevertech.biz>
 * @copyright Copyright &copy; Clevertech 2012-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package YiiBooster bootstrap.widgets
 */
Yii::import('bootstrap.widgets.TbDataColumn');

class TbTotalSumColumn2 extends TbDataColumn
{
	public $totalExpression;

	public $totalValue;

	protected $total;

	public function init()
	{
		parent::init();

		if (!is_null($this->totalExpression))
		{
			$this->total = is_numeric(Yii::app()->format->unformatNumber($this->totalExpression)) ? Yii::app()->format->unformatNumber($this->totalExpression) : Yii::app()->format->unformatNumber($this->evaluateExpression(Yii::app()->format->unformatNumber($this->totalExpression)));
		}
		$this->footer = true;
	}

	protected function renderDataCellContent($row, $data)
	{
		ob_start();
		parent::renderDataCellContent($row, $data);
		$value = ob_get_clean();

		if(is_numeric(Yii::app()->format->unformatNumber($value)))
		{
			$this->total += Yii::app()->format->unformatNumber($value);
		}
		echo $value;
	}

	protected function renderFooterCellContent()
	{
		if(is_null($this->total))
			return parent::renderFooterCellContent();

		echo $this->totalValue? $this->evaluateExpression($this->totalValue, array('total'=>Yii::app()->format->formatNumber($this->total))) : $this->grid->getFormatter()->format(Yii::app()->format->formatNumber($this->total), $this->type);
	}
}