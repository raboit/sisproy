-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-07-2013 a las 17:13:31
-- Versión del servidor: 5.1.41
-- Versión de PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `rabo_sisproy`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `afp`
--

CREATE TABLE IF NOT EXISTS `afp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `afp`
--

INSERT INTO `afp` (`id`, `nombre`) VALUES
(1, 'Habitat'),
(2, 'Cuprum'),
(3, 'Modelo'),
(4, 'Capital'),
(5, 'Provida'),
(6, 'Plan Vital'),
(7, 'Inp'),
(8, 'Jubilado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `authassignment`
--

CREATE TABLE IF NOT EXISTS `authassignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `authassignment`
--

INSERT INTO `authassignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('admin', '1', NULL, 'N;'),
('admin', '2', NULL, 'N;'),
('admin', '3', NULL, 'N;'),
('admin', '4', NULL, 'N;'),
('Jefe de Taller', '11', NULL, 'N;'),
('Jefe de Taller', '12', NULL, 'N;'),
('Jefe de Taller', '14', NULL, 'N;'),
('Jefe de Taller', '16', NULL, 'N;'),
('Jefe de Taller', '20', NULL, 'N;'),
('Jefe de Taller', '5', NULL, 'N;'),
('Usuario Comun', '10', NULL, 'N;'),
('Usuario Comun', '13', NULL, 'N;'),
('Usuario Comun', '15', NULL, 'N;'),
('Usuario Comun', '17', NULL, 'N;'),
('Usuario Comun', '18', NULL, 'N;'),
('Usuario Comun', '19', NULL, 'N;'),
('Usuario Comun', '21', NULL, 'N;'),
('Usuario Comun', '22', NULL, 'N;'),
('Usuario Comun', '23', NULL, 'N;'),
('Usuario Comun', '24', NULL, 'N;'),
('Usuario Comun', '25', NULL, 'N;'),
('Usuario Comun', '6', NULL, 'N;'),
('Usuario Comun', '7', NULL, 'N;'),
('Usuario Comun', '8', NULL, 'N;'),
('Usuario Comun', '9', NULL, 'N;');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `authitem`
--

CREATE TABLE IF NOT EXISTS `authitem` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `authitem`
--

INSERT INTO `authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('admin', 2, 'Administrador', NULL, 'N;'),
('Admin.Admin.*', 1, NULL, NULL, 'N;'),
('Admin.Admin.Index', 0, NULL, NULL, 'N;'),
('Admin.CentroCosto.*', 1, NULL, NULL, 'N;'),
('Admin.CentroCosto.Admin', 0, NULL, NULL, 'N;'),
('Admin.CentroCosto.Create', 0, NULL, NULL, 'N;'),
('Admin.CentroCosto.Delete', 0, NULL, NULL, 'N;'),
('Admin.CentroCosto.Editable', 0, NULL, NULL, 'N;'),
('Admin.CentroCosto.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.CentroCosto.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.CentroCosto.Index', 0, NULL, NULL, 'N;'),
('Admin.CentroCosto.Update', 0, NULL, NULL, 'N;'),
('Admin.CentroCosto.View', 0, NULL, NULL, 'N;'),
('Admin.Contrato.*', 1, NULL, NULL, 'N;'),
('Admin.Contrato.Admin', 0, NULL, NULL, 'N;'),
('Admin.Contrato.Create', 0, NULL, NULL, 'N;'),
('Admin.Contrato.Delete', 0, NULL, NULL, 'N;'),
('Admin.Contrato.Editable', 0, NULL, NULL, 'N;'),
('Admin.Contrato.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.Contrato.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.Contrato.Index', 0, NULL, NULL, 'N;'),
('Admin.Contrato.Update', 0, NULL, NULL, 'N;'),
('Admin.Contrato.View', 0, NULL, NULL, 'N;'),
('Admin.CuentaContable.*', 1, NULL, NULL, 'N;'),
('Admin.CuentaContable.Admin', 0, NULL, NULL, 'N;'),
('Admin.CuentaContable.Create', 0, NULL, NULL, 'N;'),
('Admin.CuentaContable.Delete', 0, NULL, NULL, 'N;'),
('Admin.CuentaContable.Editable', 0, NULL, NULL, 'N;'),
('Admin.CuentaContable.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.CuentaContable.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.CuentaContable.Index', 0, NULL, NULL, 'N;'),
('Admin.CuentaContable.Update', 0, NULL, NULL, 'N;'),
('Admin.CuentaContable.View', 0, NULL, NULL, 'N;'),
('Admin.CuentaEspecifica.*', 1, NULL, NULL, 'N;'),
('Admin.CuentaEspecifica.Admin', 0, NULL, NULL, 'N;'),
('Admin.CuentaEspecifica.Create', 0, NULL, NULL, 'N;'),
('Admin.CuentaEspecifica.Delete', 0, NULL, NULL, 'N;'),
('Admin.CuentaEspecifica.Editable', 0, NULL, NULL, 'N;'),
('Admin.CuentaEspecifica.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.CuentaEspecifica.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.CuentaEspecifica.Index', 0, NULL, NULL, 'N;'),
('Admin.CuentaEspecifica.Update', 0, NULL, NULL, 'N;'),
('Admin.CuentaEspecifica.View', 0, NULL, NULL, 'N;'),
('Admin.Default.*', 1, NULL, NULL, 'N;'),
('Admin.Default.Index', 0, NULL, NULL, 'N;'),
('Admin.Default.Solicitud', 0, NULL, NULL, 'N;'),
('Admin.Emision.*', 1, NULL, NULL, 'N;'),
('Admin.Emision.Admin', 0, NULL, NULL, 'N;'),
('Admin.Emision.Create', 0, NULL, NULL, 'N;'),
('Admin.Emision.Delete', 0, NULL, NULL, 'N;'),
('Admin.Emision.Editable', 0, NULL, NULL, 'N;'),
('Admin.Emision.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.Emision.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.Emision.Index', 0, NULL, NULL, 'N;'),
('Admin.Emision.Update', 0, NULL, NULL, 'N;'),
('Admin.Emision.View', 0, NULL, NULL, 'N;'),
('Admin.Financiamiento.*', 1, NULL, NULL, 'N;'),
('Admin.Financiamiento.Admin', 0, NULL, NULL, 'N;'),
('Admin.Financiamiento.Create', 0, NULL, NULL, 'N;'),
('Admin.Financiamiento.Delete', 0, NULL, NULL, 'N;'),
('Admin.Financiamiento.Editable', 0, NULL, NULL, 'N;'),
('Admin.Financiamiento.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.Financiamiento.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.Financiamiento.Index', 0, NULL, NULL, 'N;'),
('Admin.Financiamiento.Update', 0, NULL, NULL, 'N;'),
('Admin.Financiamiento.View', 0, NULL, NULL, 'N;'),
('Admin.Ingreso.*', 1, NULL, NULL, 'N;'),
('Admin.Ingreso.Admin', 0, NULL, NULL, 'N;'),
('Admin.Ingreso.Create', 0, NULL, NULL, 'N;'),
('Admin.Ingreso.Delete', 0, NULL, NULL, 'N;'),
('Admin.Ingreso.Editable', 0, NULL, NULL, 'N;'),
('Admin.Ingreso.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.Ingreso.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.Ingreso.Index', 0, NULL, NULL, 'N;'),
('Admin.Ingreso.Update', 0, NULL, NULL, 'N;'),
('Admin.Ingreso.View', 0, NULL, NULL, 'N;'),
('Admin.ItemPresupuesto.*', 1, NULL, NULL, 'N;'),
('Admin.ItemPresupuesto.Admin', 0, NULL, NULL, 'N;'),
('Admin.ItemPresupuesto.Create', 0, NULL, NULL, 'N;'),
('Admin.ItemPresupuesto.Delete', 0, NULL, NULL, 'N;'),
('Admin.ItemPresupuesto.Editable', 0, NULL, NULL, 'N;'),
('Admin.ItemPresupuesto.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.ItemPresupuesto.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.ItemPresupuesto.Index', 0, NULL, NULL, 'N;'),
('Admin.ItemPresupuesto.Update', 0, NULL, NULL, 'N;'),
('Admin.ItemPresupuesto.View', 0, NULL, NULL, 'N;'),
('Admin.ItemRendicion.*', 1, NULL, NULL, 'N;'),
('Admin.ItemRendicion.Admin', 0, NULL, NULL, 'N;'),
('Admin.ItemRendicion.Create', 0, NULL, NULL, 'N;'),
('Admin.ItemRendicion.Delete', 0, NULL, NULL, 'N;'),
('Admin.ItemRendicion.Editable', 0, NULL, NULL, 'N;'),
('Admin.ItemRendicion.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.ItemRendicion.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.ItemRendicion.Index', 0, NULL, NULL, 'N;'),
('Admin.ItemRendicion.Update', 0, NULL, NULL, 'N;'),
('Admin.ItemRendicion.View', 0, NULL, NULL, 'N;'),
('Admin.ItemSolicitud.*', 1, NULL, NULL, 'N;'),
('Admin.ItemSolicitud.Admin', 0, NULL, NULL, 'N;'),
('Admin.ItemSolicitud.Create', 0, NULL, NULL, 'N;'),
('Admin.ItemSolicitud.Delete', 0, NULL, NULL, 'N;'),
('Admin.ItemSolicitud.Editable', 0, NULL, NULL, 'N;'),
('Admin.ItemSolicitud.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.ItemSolicitud.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.ItemSolicitud.Index', 0, NULL, NULL, 'N;'),
('Admin.ItemSolicitud.Update', 0, NULL, NULL, 'N;'),
('Admin.ItemSolicitud.View', 0, NULL, NULL, 'N;'),
('Admin.Presupuesto.*', 1, NULL, NULL, 'N;'),
('Admin.Presupuesto.Admin', 0, NULL, NULL, 'N;'),
('Admin.Presupuesto.Create', 0, NULL, NULL, 'N;'),
('Admin.Presupuesto.Delete', 0, NULL, NULL, 'N;'),
('Admin.Presupuesto.Editable', 0, NULL, NULL, 'N;'),
('Admin.Presupuesto.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.Presupuesto.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.Presupuesto.Index', 0, NULL, NULL, 'N;'),
('Admin.Presupuesto.Update', 0, NULL, NULL, 'N;'),
('Admin.Presupuesto.View', 0, NULL, NULL, 'N;'),
('Admin.Proveedor.*', 1, NULL, NULL, 'N;'),
('Admin.Proveedor.Actualizar', 0, NULL, NULL, 'N;'),
('Admin.Proveedor.Admin', 0, NULL, NULL, 'N;'),
('Admin.Proveedor.Agregar', 0, NULL, NULL, 'N;'),
('Admin.Proveedor.Crear', 0, NULL, NULL, 'N;'),
('Admin.Proveedor.Create', 0, NULL, NULL, 'N;'),
('Admin.Proveedor.Delete', 0, NULL, NULL, 'N;'),
('Admin.Proveedor.Editable', 0, NULL, NULL, 'N;'),
('Admin.Proveedor.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.Proveedor.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.Proveedor.GetTodos', 0, NULL, NULL, 'N;'),
('Admin.Proveedor.Index', 0, NULL, NULL, 'N;'),
('Admin.Proveedor.Update', 0, NULL, NULL, 'N;'),
('Admin.Proveedor.Ver', 0, NULL, NULL, 'N;'),
('Admin.Proveedor.VerTodos', 0, NULL, NULL, 'N;'),
('Admin.Proveedor.View', 0, NULL, NULL, 'N;'),
('Admin.Proyecto.*', 1, NULL, NULL, 'N;'),
('Admin.Proyecto.Admin', 0, NULL, NULL, 'N;'),
('Admin.Proyecto.Create', 0, NULL, NULL, 'N;'),
('Admin.Proyecto.Delete', 0, NULL, NULL, 'N;'),
('Admin.Proyecto.Editable', 0, NULL, NULL, 'N;'),
('Admin.Proyecto.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.Proyecto.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.Proyecto.Index', 0, NULL, NULL, 'N;'),
('Admin.Proyecto.Update', 0, NULL, NULL, 'N;'),
('Admin.Proyecto.View', 0, NULL, NULL, 'N;'),
('Admin.Rendicion.*', 1, NULL, NULL, 'N;'),
('Admin.Rendicion.Admin', 0, NULL, NULL, 'N;'),
('Admin.Rendicion.Create', 0, NULL, NULL, 'N;'),
('Admin.Rendicion.Delete', 0, NULL, NULL, 'N;'),
('Admin.Rendicion.Editable', 0, NULL, NULL, 'N;'),
('Admin.Rendicion.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.Rendicion.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.Rendicion.Index', 0, NULL, NULL, 'N;'),
('Admin.Rendicion.Update', 0, NULL, NULL, 'N;'),
('Admin.Rendicion.View', 0, NULL, NULL, 'N;'),
('Admin.Rrhh.*', 1, NULL, NULL, 'N;'),
('Admin.Rrhh.Admin', 0, NULL, NULL, 'N;'),
('Admin.Rrhh.Create', 0, NULL, NULL, 'N;'),
('Admin.Rrhh.Delete', 0, NULL, NULL, 'N;'),
('Admin.Rrhh.Editable', 0, NULL, NULL, 'N;'),
('Admin.Rrhh.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.Rrhh.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.Rrhh.Index', 0, NULL, NULL, 'N;'),
('Admin.Rrhh.Update', 0, NULL, NULL, 'N;'),
('Admin.Rrhh.View', 0, NULL, NULL, 'N;'),
('Admin.Solicitud.*', 1, NULL, NULL, 'N;'),
('Admin.Solicitud.Admin', 0, NULL, NULL, 'N;'),
('Admin.Solicitud.Create', 0, NULL, NULL, 'N;'),
('Admin.Solicitud.Delete', 0, NULL, NULL, 'N;'),
('Admin.Solicitud.Editable', 0, NULL, NULL, 'N;'),
('Admin.Solicitud.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.Solicitud.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.Solicitud.Index', 0, NULL, NULL, 'N;'),
('Admin.Solicitud.Update', 0, NULL, NULL, 'N;'),
('Admin.Solicitud.View', 0, NULL, NULL, 'N;'),
('Admin.TipoContrato.*', 1, NULL, NULL, 'N;'),
('Admin.TipoContrato.Admin', 0, NULL, NULL, 'N;'),
('Admin.TipoContrato.Create', 0, NULL, NULL, 'N;'),
('Admin.TipoContrato.Delete', 0, NULL, NULL, 'N;'),
('Admin.TipoContrato.Editable', 0, NULL, NULL, 'N;'),
('Admin.TipoContrato.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.TipoContrato.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.TipoContrato.Index', 0, NULL, NULL, 'N;'),
('Admin.TipoContrato.Update', 0, NULL, NULL, 'N;'),
('Admin.TipoContrato.View', 0, NULL, NULL, 'N;'),
('Admin.TipoIngreso.*', 1, NULL, NULL, 'N;'),
('Admin.TipoIngreso.Admin', 0, NULL, NULL, 'N;'),
('Admin.TipoIngreso.Create', 0, NULL, NULL, 'N;'),
('Admin.TipoIngreso.Delete', 0, NULL, NULL, 'N;'),
('Admin.TipoIngreso.Editable', 0, NULL, NULL, 'N;'),
('Admin.TipoIngreso.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.TipoIngreso.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.TipoIngreso.Index', 0, NULL, NULL, 'N;'),
('Admin.TipoIngreso.Update', 0, NULL, NULL, 'N;'),
('Admin.TipoIngreso.View', 0, NULL, NULL, 'N;'),
('Admin.User.*', 1, NULL, NULL, 'N;'),
('Admin.User.Admin', 0, NULL, NULL, 'N;'),
('Admin.User.ChangePassword', 0, NULL, NULL, 'N;'),
('Admin.User.Create', 0, NULL, NULL, 'N;'),
('Admin.User.Delete', 0, NULL, NULL, 'N;'),
('Admin.User.Editable', 0, NULL, NULL, 'N;'),
('Admin.User.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.User.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.User.Index', 0, NULL, NULL, 'N;'),
('Admin.User.Update', 0, NULL, NULL, 'N;'),
('Admin.User.View', 0, NULL, NULL, 'N;'),
('Afp.*', 1, NULL, NULL, 'N;'),
('Afp.Actualizar', 0, NULL, NULL, 'N;'),
('Afp.Admin', 0, NULL, NULL, 'N;'),
('Afp.Crear', 0, NULL, NULL, 'N;'),
('Afp.Create', 0, NULL, NULL, 'N;'),
('Afp.Delete', 0, NULL, NULL, 'N;'),
('Afp.GenerateExcel', 0, NULL, NULL, 'N;'),
('Afp.GeneratePdf', 0, NULL, NULL, 'N;'),
('Afp.Index', 0, NULL, NULL, 'N;'),
('Afp.Update', 0, NULL, NULL, 'N;'),
('Afp.Ver', 0, NULL, NULL, 'N;'),
('Afp.VerTodos', 0, NULL, NULL, 'N;'),
('Afp.View', 0, NULL, NULL, 'N;'),
('CentroCosto.*', 1, NULL, NULL, 'N;'),
('CentroCosto.Actualizar', 0, NULL, NULL, 'N;'),
('CentroCosto.Admin', 0, NULL, NULL, 'N;'),
('CentroCosto.Crear', 0, NULL, NULL, 'N;'),
('CentroCosto.Create', 0, NULL, NULL, 'N;'),
('CentroCosto.Delete', 0, NULL, NULL, 'N;'),
('CentroCosto.GenerateExcel', 0, NULL, NULL, 'N;'),
('CentroCosto.GeneratePdf', 0, NULL, NULL, 'N;'),
('CentroCosto.Index', 0, NULL, NULL, 'N;'),
('CentroCosto.Update', 0, NULL, NULL, 'N;'),
('CentroCosto.Ver', 0, NULL, NULL, 'N;'),
('CentroCosto.VerTodos', 0, NULL, NULL, 'N;'),
('CentroCosto.View', 0, NULL, NULL, 'N;'),
('Contrato.*', 1, NULL, NULL, 'N;'),
('Contrato.Actualizar', 0, NULL, NULL, 'N;'),
('Contrato.Admin', 0, NULL, NULL, 'N;'),
('Contrato.Crear', 0, NULL, NULL, 'N;'),
('Contrato.Create', 0, NULL, NULL, 'N;'),
('Contrato.Delete', 0, NULL, NULL, 'N;'),
('Contrato.Finalizar', 0, NULL, NULL, 'N;'),
('Contrato.GenerateExcel', 0, NULL, NULL, 'N;'),
('Contrato.GeneratePdf', 0, NULL, NULL, 'N;'),
('Contrato.Index', 0, NULL, NULL, 'N;'),
('Contrato.Update', 0, NULL, NULL, 'N;'),
('Contrato.Ver', 0, NULL, NULL, 'N;'),
('Contrato.VerRrhh', 0, NULL, NULL, 'N;'),
('Contrato.VerTodos', 0, NULL, NULL, 'N;'),
('Contrato.View', 0, NULL, NULL, 'N;'),
('CuentaContable.*', 1, NULL, NULL, 'N;'),
('CuentaContable.Actualizar', 0, NULL, NULL, 'N;'),
('CuentaContable.Admin', 0, NULL, NULL, 'N;'),
('CuentaContable.Crear', 0, NULL, NULL, 'N;'),
('CuentaContable.Create', 0, NULL, NULL, 'N;'),
('CuentaContable.Delete', 0, NULL, NULL, 'N;'),
('CuentaContable.GenerateExcel', 0, NULL, NULL, 'N;'),
('CuentaContable.GeneratePdf', 0, NULL, NULL, 'N;'),
('CuentaContable.Index', 0, NULL, NULL, 'N;'),
('CuentaContable.Update', 0, NULL, NULL, 'N;'),
('CuentaContable.Ver', 0, NULL, NULL, 'N;'),
('CuentaContable.VerTodos', 0, NULL, NULL, 'N;'),
('CuentaContable.View', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.*', 1, NULL, NULL, 'N;'),
('CuentaEspecifica.Actualizar', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.Admin', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.Crear', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.Create', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.Delete', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.GenerateExcel', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.GeneratePdf', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.Index', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.Update', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.Ver', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.VerTodos', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.View', 0, NULL, NULL, 'N;'),
('Director Ejecutivo', 2, 'Director Ejecutivo', NULL, 'N;'),
('Donante.*', 1, NULL, NULL, 'N;'),
('Donante.Admin', 0, NULL, NULL, 'N;'),
('Donante.Create', 0, NULL, NULL, 'N;'),
('Donante.Delete', 0, NULL, NULL, 'N;'),
('Donante.GenerateExcel', 0, NULL, NULL, 'N;'),
('Donante.GeneratePdf', 0, NULL, NULL, 'N;'),
('Donante.Index', 0, NULL, NULL, 'N;'),
('Donante.Update', 0, NULL, NULL, 'N;'),
('Donante.View', 0, NULL, NULL, 'N;'),
('Emision.*', 1, NULL, NULL, 'N;'),
('Emision.Actualizar', 0, NULL, NULL, 'N;'),
('Emision.Admin', 0, NULL, NULL, 'N;'),
('Emision.Anular', 0, NULL, NULL, 'N;'),
('Emision.Crear', 0, NULL, NULL, 'N;'),
('Emision.Create', 0, NULL, NULL, 'N;'),
('Emision.Delete', 0, NULL, NULL, 'N;'),
('Emision.Eliminar', 0, NULL, NULL, 'N;'),
('Emision.Finalizadas', 0, NULL, NULL, 'N;'),
('Emision.Finalizar', 0, NULL, NULL, 'N;'),
('Emision.GenerateExcel', 0, NULL, NULL, 'N;'),
('Emision.GeneratePdf', 0, NULL, NULL, 'N;'),
('Emision.Index', 0, NULL, NULL, 'N;'),
('Emision.Update', 0, NULL, NULL, 'N;'),
('Emision.Ver', 0, NULL, NULL, 'N;'),
('Emision.VerEmisionRendicion', 0, NULL, NULL, 'N;'),
('Emision.VerTodos', 0, NULL, NULL, 'N;'),
('Emision.View', 0, NULL, NULL, 'N;'),
('Financiamiento.*', 1, NULL, NULL, 'N;'),
('Financiamiento.Actualizar', 0, NULL, NULL, 'N;'),
('Financiamiento.Admin', 0, NULL, NULL, 'N;'),
('Financiamiento.Crear', 0, NULL, NULL, 'N;'),
('Financiamiento.Create', 0, NULL, NULL, 'N;'),
('Financiamiento.Delete', 0, NULL, NULL, 'N;'),
('Financiamiento.GenerateExcel', 0, NULL, NULL, 'N;'),
('Financiamiento.GeneratePdf', 0, NULL, NULL, 'N;'),
('Financiamiento.Index', 0, NULL, NULL, 'N;'),
('Financiamiento.Update', 0, NULL, NULL, 'N;'),
('Financiamiento.Ver', 0, NULL, NULL, 'N;'),
('Financiamiento.VerTodos', 0, NULL, NULL, 'N;'),
('Financiamiento.View', 0, NULL, NULL, 'N;'),
('Informe.*', 1, NULL, NULL, 'N;'),
('Informe.Crear', 0, NULL, NULL, 'N;'),
('Informe.Test', 0, NULL, NULL, 'N;'),
('Ingreso.*', 1, NULL, NULL, 'N;'),
('Ingreso.Actualizar', 0, NULL, NULL, 'N;'),
('Ingreso.Admin', 0, NULL, NULL, 'N;'),
('Ingreso.Crear', 0, NULL, NULL, 'N;'),
('Ingreso.Create', 0, NULL, NULL, 'N;'),
('Ingreso.Delete', 0, NULL, NULL, 'N;'),
('Ingreso.GenerateExcel', 0, NULL, NULL, 'N;'),
('Ingreso.GeneratePdf', 0, NULL, NULL, 'N;'),
('Ingreso.Index', 0, NULL, NULL, 'N;'),
('Ingreso.Update', 0, NULL, NULL, 'N;'),
('Ingreso.Ver', 0, NULL, NULL, 'N;'),
('Ingreso.VerTodos', 0, NULL, NULL, 'N;'),
('Ingreso.View', 0, NULL, NULL, 'N;'),
('IngresoProyecto.*', 1, NULL, NULL, 'N;'),
('IngresoProyecto.Admin', 0, NULL, NULL, 'N;'),
('IngresoProyecto.Create', 0, NULL, NULL, 'N;'),
('IngresoProyecto.Delete', 0, NULL, NULL, 'N;'),
('IngresoProyecto.GenerateExcel', 0, NULL, NULL, 'N;'),
('IngresoProyecto.GeneratePdf', 0, NULL, NULL, 'N;'),
('IngresoProyecto.Index', 0, NULL, NULL, 'N;'),
('IngresoProyecto.Update', 0, NULL, NULL, 'N;'),
('IngresoProyecto.View', 0, NULL, NULL, 'N;'),
('Inventario.*', 1, NULL, NULL, 'N;'),
('Inventario.Admin', 0, NULL, NULL, 'N;'),
('Inventario.Create', 0, NULL, NULL, 'N;'),
('Inventario.Delete', 0, NULL, NULL, 'N;'),
('Inventario.GenerateExcel', 0, NULL, NULL, 'N;'),
('Inventario.GeneratePdf', 0, NULL, NULL, 'N;'),
('Inventario.Index', 0, NULL, NULL, 'N;'),
('Inventario.Update', 0, NULL, NULL, 'N;'),
('Inventario.View', 0, NULL, NULL, 'N;'),
('ItemEmision.*', 1, NULL, NULL, 'N;'),
('ItemEmision.Admin', 0, NULL, NULL, 'N;'),
('ItemEmision.Agregar', 0, NULL, NULL, 'N;'),
('ItemEmision.Create', 0, NULL, NULL, 'N;'),
('ItemEmision.Delete', 0, NULL, NULL, 'N;'),
('ItemEmision.GenerateExcel', 0, NULL, NULL, 'N;'),
('ItemEmision.GeneratePdf', 0, NULL, NULL, 'N;'),
('ItemEmision.Index', 0, NULL, NULL, 'N;'),
('ItemEmision.Update', 0, NULL, NULL, 'N;'),
('ItemEmision.View', 0, NULL, NULL, 'N;'),
('ItemPresupuesto.*', 1, NULL, NULL, 'N;'),
('ItemPresupuesto.Actualizar', 0, NULL, NULL, 'N;'),
('ItemPresupuesto.ActualizarProyecto', 0, NULL, NULL, 'N;'),
('ItemPresupuesto.Admin', 0, NULL, NULL, 'N;'),
('ItemPresupuesto.Crear', 0, NULL, NULL, 'N;'),
('ItemPresupuesto.CrearProyecto', 0, NULL, NULL, 'N;'),
('ItemPresupuesto.Delete', 0, NULL, NULL, 'N;'),
('ItemPresupuesto.GenerateExcel', 0, NULL, NULL, 'N;'),
('ItemPresupuesto.GeneratePdf', 0, NULL, NULL, 'N;'),
('ItemPresupuesto.Index', 0, NULL, NULL, 'N;'),
('ItemPresupuesto.ItemPresupuestoVsItemRendicion', 0, NULL, NULL, 'N;'),
('ItemPresupuesto.Update', 0, NULL, NULL, 'N;'),
('ItemPresupuesto.Ver', 0, NULL, NULL, 'N;'),
('ItemPresupuesto.VerProyecto', 0, NULL, NULL, 'N;'),
('ItemPresupuesto.VerTodos', 0, NULL, NULL, 'N;'),
('ItemPresupuesto.View', 0, NULL, NULL, 'N;'),
('ItemRendicion.*', 1, NULL, NULL, 'N;'),
('ItemRendicion.Actualizar', 0, NULL, NULL, 'N;'),
('ItemRendicion.Admin', 0, NULL, NULL, 'N;'),
('ItemRendicion.Agregar', 0, NULL, NULL, 'N;'),
('ItemRendicion.Crear', 0, NULL, NULL, 'N;'),
('ItemRendicion.Create', 0, NULL, NULL, 'N;'),
('ItemRendicion.Delete', 0, NULL, NULL, 'N;'),
('ItemRendicion.GenerateExcel', 0, NULL, NULL, 'N;'),
('ItemRendicion.GeneratePdf', 0, NULL, NULL, 'N;'),
('ItemRendicion.GetCuenta', 0, NULL, NULL, 'N;'),
('ItemRendicion.Index', 0, NULL, NULL, 'N;'),
('ItemRendicion.Update', 0, NULL, NULL, 'N;'),
('ItemRendicion.Ver', 0, NULL, NULL, 'N;'),
('ItemRendicion.VerTodos', 0, NULL, NULL, 'N;'),
('ItemRendicion.View', 0, NULL, NULL, 'N;'),
('ItemSolicitud.*', 1, NULL, NULL, 'N;'),
('ItemSolicitud.Actualizar', 0, NULL, NULL, 'N;'),
('ItemSolicitud.Admin', 0, NULL, NULL, 'N;'),
('ItemSolicitud.Agregar', 0, NULL, NULL, 'N;'),
('ItemSolicitud.Create', 0, NULL, NULL, 'N;'),
('ItemSolicitud.Delete', 0, NULL, NULL, 'N;'),
('ItemSolicitud.GenerateExcel', 0, NULL, NULL, 'N;'),
('ItemSolicitud.GeneratePdf', 0, NULL, NULL, 'N;'),
('ItemSolicitud.Index', 0, NULL, NULL, 'N;'),
('ItemSolicitud.Update', 0, NULL, NULL, 'N;'),
('ItemSolicitud.Ver', 0, NULL, NULL, 'N;'),
('ItemSolicitud.View', 0, NULL, NULL, 'N;'),
('Jefe de Taller', 2, 'Jefe de Taller', NULL, 'N;'),
('LibroCompra.*', 1, NULL, NULL, 'N;'),
('LibroCompra.Admin', 0, NULL, NULL, 'N;'),
('LibroCompra.Create', 0, NULL, NULL, 'N;'),
('LibroCompra.Delete', 0, NULL, NULL, 'N;'),
('LibroCompra.GenerateExcel', 0, NULL, NULL, 'N;'),
('LibroCompra.GeneratePdf', 0, NULL, NULL, 'N;'),
('LibroCompra.Index', 0, NULL, NULL, 'N;'),
('LibroCompra.Update', 0, NULL, NULL, 'N;'),
('LibroCompra.VerTodos', 0, NULL, NULL, 'N;'),
('LibroCompra.View', 0, NULL, NULL, 'N;'),
('Presupuesto.*', 1, NULL, NULL, 'N;'),
('Presupuesto.Actualizar', 0, NULL, NULL, 'N;'),
('Presupuesto.Admin', 0, NULL, NULL, 'N;'),
('Presupuesto.Crear', 0, NULL, NULL, 'N;'),
('Presupuesto.Create', 0, NULL, NULL, 'N;'),
('Presupuesto.Delete', 0, NULL, NULL, 'N;'),
('Presupuesto.GenerateExcel', 0, NULL, NULL, 'N;'),
('Presupuesto.GeneratePdf', 0, NULL, NULL, 'N;'),
('Presupuesto.Index', 0, NULL, NULL, 'N;'),
('Presupuesto.Update', 0, NULL, NULL, 'N;'),
('Presupuesto.Ver', 0, NULL, NULL, 'N;'),
('Presupuesto.VerProyecto', 0, NULL, NULL, 'N;'),
('Presupuesto.VerTodos', 0, NULL, NULL, 'N;'),
('Presupuesto.View', 0, NULL, NULL, 'N;'),
('Proveedor.*', 1, NULL, NULL, 'N;'),
('Proveedor.Actualizar', 0, NULL, NULL, 'N;'),
('Proveedor.Admin', 0, NULL, NULL, 'N;'),
('Proveedor.Agregar', 0, NULL, NULL, 'N;'),
('Proveedor.Crear', 0, NULL, NULL, 'N;'),
('Proveedor.Create', 0, NULL, NULL, 'N;'),
('Proveedor.Delete', 0, NULL, NULL, 'N;'),
('Proveedor.GenerateExcel', 0, NULL, NULL, 'N;'),
('Proveedor.GeneratePdf', 0, NULL, NULL, 'N;'),
('Proveedor.GetTodos', 0, NULL, NULL, 'N;'),
('Proveedor.Index', 0, NULL, NULL, 'N;'),
('Proveedor.Update', 0, NULL, NULL, 'N;'),
('Proveedor.Ver', 0, NULL, NULL, 'N;'),
('Proveedor.VerTodos', 0, NULL, NULL, 'N;'),
('Proveedor.View', 0, NULL, NULL, 'N;'),
('Proyecto.*', 1, NULL, NULL, 'N;'),
('Proyecto.Actualizar', 0, NULL, NULL, 'N;'),
('Proyecto.Admin', 0, NULL, NULL, 'N;'),
('Proyecto.Crear', 0, NULL, NULL, 'N;'),
('Proyecto.Create', 0, NULL, NULL, 'N;'),
('Proyecto.Delete', 0, NULL, NULL, 'N;'),
('Proyecto.GenerateExcel', 0, NULL, NULL, 'N;'),
('Proyecto.GeneratePdf', 0, NULL, NULL, 'N;'),
('Proyecto.GetComuna', 0, NULL, NULL, 'N;'),
('Proyecto.Index', 0, NULL, NULL, 'N;'),
('Proyecto.PresupuestoVsIngreso', 0, NULL, NULL, 'N;'),
('Proyecto.Update', 0, NULL, NULL, 'N;'),
('Proyecto.Ver', 0, NULL, NULL, 'N;'),
('Proyecto.VerPresupuesto', 0, NULL, NULL, 'N;'),
('Proyecto.VerTodos', 0, NULL, NULL, 'N;'),
('Proyecto.View', 0, NULL, NULL, 'N;'),
('Rendicion.*', 1, NULL, NULL, 'N;'),
('Rendicion.Actualizar', 0, NULL, NULL, 'N;'),
('Rendicion.Admin', 0, NULL, NULL, 'N;'),
('Rendicion.Anular', 0, NULL, NULL, 'N;'),
('Rendicion.Crear', 0, NULL, NULL, 'N;'),
('Rendicion.Create', 0, NULL, NULL, 'N;'),
('Rendicion.Delete', 0, NULL, NULL, 'N;'),
('Rendicion.Finalizar', 0, NULL, NULL, 'N;'),
('Rendicion.GenerateExcel', 0, NULL, NULL, 'N;'),
('Rendicion.GeneratePdf', 0, NULL, NULL, 'N;'),
('Rendicion.Index', 0, NULL, NULL, 'N;'),
('Rendicion.Update', 0, NULL, NULL, 'N;'),
('Rendicion.Ver', 0, NULL, NULL, 'N;'),
('Rendicion.VerTodos', 0, NULL, NULL, 'N;'),
('Rendicion.View', 0, NULL, NULL, 'N;'),
('Rrhh.*', 1, NULL, NULL, 'N;'),
('Rrhh.Actualizar', 0, NULL, NULL, 'N;'),
('Rrhh.Admin', 0, NULL, NULL, 'N;'),
('Rrhh.Crear', 0, NULL, NULL, 'N;'),
('Rrhh.Create', 0, NULL, NULL, 'N;'),
('Rrhh.Delete', 0, NULL, NULL, 'N;'),
('Rrhh.GenerateExcel', 0, NULL, NULL, 'N;'),
('Rrhh.GeneratePdf', 0, NULL, NULL, 'N;'),
('Rrhh.Index', 0, NULL, NULL, 'N;'),
('Rrhh.Ver', 0, NULL, NULL, 'N;'),
('Rrhh.VerContrato', 0, NULL, NULL, 'N;'),
('Rrhh.VerTodos', 0, NULL, NULL, 'N;'),
('Rrhh.View', 0, NULL, NULL, 'N;'),
('Site.*', 1, NULL, NULL, 'N;'),
('Site.Contact', 0, NULL, NULL, 'N;'),
('Site.Error', 0, NULL, NULL, 'N;'),
('Site.Index', 0, NULL, NULL, 'N;'),
('Site.Login', 0, NULL, NULL, 'N;'),
('Site.Logout', 0, NULL, NULL, 'N;'),
('Solicitud.*', 1, NULL, NULL, 'N;'),
('Solicitud.Actualizar', 0, NULL, NULL, 'N;'),
('Solicitud.Admin', 0, NULL, NULL, 'N;'),
('Solicitud.Anular', 0, NULL, NULL, 'N;'),
('Solicitud.Crear', 0, NULL, NULL, 'N;'),
('Solicitud.CrearSolicitud', 0, NULL, NULL, 'N;'),
('Solicitud.Create', 0, NULL, NULL, 'N;'),
('Solicitud.Delete', 0, NULL, NULL, 'N;'),
('Solicitud.Finalizadas', 0, NULL, NULL, 'N;'),
('Solicitud.Finalizar', 0, NULL, NULL, 'N;'),
('Solicitud.GenerateExcel', 0, NULL, NULL, 'N;'),
('Solicitud.GeneratePdf', 0, NULL, NULL, 'N;'),
('Solicitud.Index', 0, NULL, NULL, 'N;'),
('Solicitud.Update', 0, NULL, NULL, 'N;'),
('Solicitud.Ver', 0, NULL, NULL, 'N;'),
('Solicitud.VerSolicitudEmision', 0, NULL, NULL, 'N;'),
('Solicitud.VerTodos', 0, NULL, NULL, 'N;'),
('Solicitud.View', 0, NULL, NULL, 'N;'),
('TipoContrato.*', 1, NULL, NULL, 'N;'),
('TipoContrato.Actualizar', 0, NULL, NULL, 'N;'),
('TipoContrato.Admin', 0, NULL, NULL, 'N;'),
('TipoContrato.Crear', 0, NULL, NULL, 'N;'),
('TipoContrato.Create', 0, NULL, NULL, 'N;'),
('TipoContrato.Delete', 0, NULL, NULL, 'N;'),
('TipoContrato.GenerateExcel', 0, NULL, NULL, 'N;'),
('TipoContrato.GeneratePdf', 0, NULL, NULL, 'N;'),
('TipoContrato.Index', 0, NULL, NULL, 'N;'),
('TipoContrato.Update', 0, NULL, NULL, 'N;'),
('TipoContrato.Ver', 0, NULL, NULL, 'N;'),
('TipoContrato.VerTodos', 0, NULL, NULL, 'N;'),
('TipoContrato.View', 0, NULL, NULL, 'N;'),
('TipoIngreso.*', 1, NULL, NULL, 'N;'),
('TipoIngreso.Actualizar', 0, NULL, NULL, 'N;'),
('TipoIngreso.Admin', 0, NULL, NULL, 'N;'),
('TipoIngreso.Crear', 0, NULL, NULL, 'N;'),
('TipoIngreso.Create', 0, NULL, NULL, 'N;'),
('TipoIngreso.Delete', 0, NULL, NULL, 'N;'),
('TipoIngreso.GenerateExcel', 0, NULL, NULL, 'N;'),
('TipoIngreso.GeneratePdf', 0, NULL, NULL, 'N;'),
('TipoIngreso.Index', 0, NULL, NULL, 'N;'),
('TipoIngreso.Update', 0, NULL, NULL, 'N;'),
('TipoIngreso.Ver', 0, NULL, NULL, 'N;'),
('TipoIngreso.VerTodos', 0, NULL, NULL, 'N;'),
('TipoIngreso.View', 0, NULL, NULL, 'N;'),
('User.*', 1, NULL, NULL, 'N;'),
('User.Admin', 0, NULL, NULL, 'N;'),
('User.ChangePassword', 0, NULL, NULL, 'N;'),
('User.Create', 0, NULL, NULL, 'N;'),
('User.Delete', 0, NULL, NULL, 'N;'),
('User.GenerateExcel', 0, NULL, NULL, 'N;'),
('User.GeneratePdf', 0, NULL, NULL, 'N;'),
('User.Index', 0, NULL, NULL, 'N;'),
('User.Update', 0, NULL, NULL, 'N;'),
('User.View', 0, NULL, NULL, 'N;'),
('Usuario Comun', 2, 'Usuario Común', NULL, 'N;');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `authitemchild`
--

CREATE TABLE IF NOT EXISTS `authitemchild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `authitemchild`
--

INSERT INTO `authitemchild` (`parent`, `child`) VALUES
('Jefe de Taller', 'Admin.User.ChangePassword'),
('Director Ejecutivo', 'CentroCosto.Actualizar'),
('Director Ejecutivo', 'CentroCosto.Crear'),
('Director Ejecutivo', 'CentroCosto.GenerateExcel'),
('Director Ejecutivo', 'CentroCosto.GeneratePdf'),
('Director Ejecutivo', 'CentroCosto.Ver'),
('Director Ejecutivo', 'CentroCosto.VerTodos'),
('Director Ejecutivo', 'CuentaContable.Crear'),
('Jefe de Taller', 'CuentaContable.Crear'),
('Usuario Comun', 'CuentaContable.Crear'),
('Director Ejecutivo', 'CuentaContable.GenerateExcel'),
('Jefe de Taller', 'CuentaContable.GenerateExcel'),
('Usuario Comun', 'CuentaContable.GenerateExcel'),
('Director Ejecutivo', 'CuentaContable.GeneratePdf'),
('Jefe de Taller', 'CuentaContable.GeneratePdf'),
('Usuario Comun', 'CuentaContable.GeneratePdf'),
('Director Ejecutivo', 'CuentaContable.Ver'),
('Jefe de Taller', 'CuentaContable.Ver'),
('Usuario Comun', 'CuentaContable.Ver'),
('Director Ejecutivo', 'CuentaContable.VerTodos'),
('Jefe de Taller', 'CuentaContable.VerTodos'),
('Usuario Comun', 'CuentaContable.VerTodos'),
('Director Ejecutivo', 'CuentaEspecifica.Actualizar'),
('Director Ejecutivo', 'CuentaEspecifica.Crear'),
('Director Ejecutivo', 'CuentaEspecifica.GenerateExcel'),
('Jefe de Taller', 'CuentaEspecifica.GenerateExcel'),
('Usuario Comun', 'CuentaEspecifica.GenerateExcel'),
('Director Ejecutivo', 'CuentaEspecifica.GeneratePdf'),
('Jefe de Taller', 'CuentaEspecifica.GeneratePdf'),
('Usuario Comun', 'CuentaEspecifica.GeneratePdf'),
('Director Ejecutivo', 'CuentaEspecifica.Ver'),
('Jefe de Taller', 'CuentaEspecifica.Ver'),
('Usuario Comun', 'CuentaEspecifica.Ver'),
('Director Ejecutivo', 'Emision.Actualizar'),
('Jefe de Taller', 'Emision.Actualizar'),
('Usuario Comun', 'Emision.Actualizar'),
('Director Ejecutivo', 'Emision.Anular'),
('Jefe de Taller', 'Emision.Anular'),
('Usuario Comun', 'Emision.Anular'),
('Director Ejecutivo', 'Emision.Crear'),
('Jefe de Taller', 'Emision.Crear'),
('Usuario Comun', 'Emision.Crear'),
('Director Ejecutivo', 'Emision.Finalizadas'),
('Jefe de Taller', 'Emision.Finalizadas'),
('Usuario Comun', 'Emision.Finalizadas'),
('Director Ejecutivo', 'Emision.Finalizar'),
('Jefe de Taller', 'Emision.Finalizar'),
('Usuario Comun', 'Emision.Finalizar'),
('Director Ejecutivo', 'Emision.GenerateExcel'),
('Jefe de Taller', 'Emision.GenerateExcel'),
('Usuario Comun', 'Emision.GenerateExcel'),
('Director Ejecutivo', 'Emision.GeneratePdf'),
('Jefe de Taller', 'Emision.GeneratePdf'),
('Usuario Comun', 'Emision.GeneratePdf'),
('Director Ejecutivo', 'Emision.Ver'),
('Jefe de Taller', 'Emision.Ver'),
('Usuario Comun', 'Emision.Ver'),
('Director Ejecutivo', 'Emision.VerEmisionRendicion'),
('Jefe de Taller', 'Emision.VerEmisionRendicion'),
('Usuario Comun', 'Emision.VerEmisionRendicion'),
('Director Ejecutivo', 'Emision.VerTodos'),
('Jefe de Taller', 'Emision.VerTodos'),
('Usuario Comun', 'Emision.VerTodos'),
('Director Ejecutivo', 'ItemRendicion.Actualizar'),
('Jefe de Taller', 'ItemRendicion.Actualizar'),
('Usuario Comun', 'ItemRendicion.Actualizar'),
('Director Ejecutivo', 'ItemRendicion.Agregar'),
('Jefe de Taller', 'ItemRendicion.Agregar'),
('Usuario Comun', 'ItemRendicion.Agregar'),
('Director Ejecutivo', 'ItemRendicion.GenerateExcel'),
('Jefe de Taller', 'ItemRendicion.GenerateExcel'),
('Usuario Comun', 'ItemRendicion.GenerateExcel'),
('Director Ejecutivo', 'ItemRendicion.GeneratePdf'),
('Jefe de Taller', 'ItemRendicion.GeneratePdf'),
('Usuario Comun', 'ItemRendicion.GeneratePdf'),
('Director Ejecutivo', 'ItemRendicion.GetCuenta'),
('Jefe de Taller', 'ItemRendicion.GetCuenta'),
('Usuario Comun', 'ItemRendicion.GetCuenta'),
('Director Ejecutivo', 'ItemRendicion.Ver'),
('Jefe de Taller', 'ItemRendicion.Ver'),
('Usuario Comun', 'ItemRendicion.Ver'),
('Director Ejecutivo', 'ItemSolicitud.Actualizar'),
('Jefe de Taller', 'ItemSolicitud.Actualizar'),
('Usuario Comun', 'ItemSolicitud.Actualizar'),
('Director Ejecutivo', 'ItemSolicitud.Agregar'),
('Jefe de Taller', 'ItemSolicitud.Agregar'),
('Usuario Comun', 'ItemSolicitud.Agregar'),
('Director Ejecutivo', 'ItemSolicitud.GenerateExcel'),
('Jefe de Taller', 'ItemSolicitud.GenerateExcel'),
('Usuario Comun', 'ItemSolicitud.GenerateExcel'),
('Director Ejecutivo', 'ItemSolicitud.GeneratePdf'),
('Jefe de Taller', 'ItemSolicitud.GeneratePdf'),
('Usuario Comun', 'ItemSolicitud.GeneratePdf'),
('Director Ejecutivo', 'ItemSolicitud.Ver'),
('Jefe de Taller', 'ItemSolicitud.Ver'),
('Usuario Comun', 'ItemSolicitud.Ver'),
('Director Ejecutivo', 'Proveedor.Actualizar'),
('Jefe de Taller', 'Proveedor.Actualizar'),
('Usuario Comun', 'Proveedor.Actualizar'),
('Director Ejecutivo', 'Proveedor.Agregar'),
('Jefe de Taller', 'Proveedor.Agregar'),
('Usuario Comun', 'Proveedor.Agregar'),
('Director Ejecutivo', 'Proveedor.Crear'),
('Jefe de Taller', 'Proveedor.Crear'),
('Usuario Comun', 'Proveedor.Crear'),
('Director Ejecutivo', 'Proveedor.GenerateExcel'),
('Jefe de Taller', 'Proveedor.GenerateExcel'),
('Usuario Comun', 'Proveedor.GenerateExcel'),
('Director Ejecutivo', 'Proveedor.GeneratePdf'),
('Jefe de Taller', 'Proveedor.GeneratePdf'),
('Usuario Comun', 'Proveedor.GeneratePdf'),
('Director Ejecutivo', 'Proveedor.GetTodos'),
('Jefe de Taller', 'Proveedor.GetTodos'),
('Usuario Comun', 'Proveedor.GetTodos'),
('Director Ejecutivo', 'Proveedor.Ver'),
('Jefe de Taller', 'Proveedor.Ver'),
('Usuario Comun', 'Proveedor.Ver'),
('Director Ejecutivo', 'Proveedor.VerTodos'),
('Jefe de Taller', 'Proveedor.VerTodos'),
('Usuario Comun', 'Proveedor.VerTodos'),
('Director Ejecutivo', 'Proyecto.Actualizar'),
('Jefe de Taller', 'Proyecto.Actualizar'),
('Usuario Comun', 'Proyecto.Actualizar'),
('Director Ejecutivo', 'Proyecto.Crear'),
('Jefe de Taller', 'Proyecto.Crear'),
('Usuario Comun', 'Proyecto.Crear'),
('Director Ejecutivo', 'Proyecto.GenerateExcel'),
('Jefe de Taller', 'Proyecto.GenerateExcel'),
('Usuario Comun', 'Proyecto.GenerateExcel'),
('Director Ejecutivo', 'Proyecto.GeneratePdf'),
('Jefe de Taller', 'Proyecto.GeneratePdf'),
('Usuario Comun', 'Proyecto.GeneratePdf'),
('Director Ejecutivo', 'Proyecto.GetComuna'),
('Jefe de Taller', 'Proyecto.GetComuna'),
('Usuario Comun', 'Proyecto.GetComuna'),
('Director Ejecutivo', 'Proyecto.Ver'),
('Jefe de Taller', 'Proyecto.Ver'),
('Usuario Comun', 'Proyecto.Ver'),
('Director Ejecutivo', 'Proyecto.VerTodos'),
('Jefe de Taller', 'Proyecto.VerTodos'),
('Usuario Comun', 'Proyecto.VerTodos'),
('Director Ejecutivo', 'Rendicion.Actualizar'),
('Jefe de Taller', 'Rendicion.Actualizar'),
('Usuario Comun', 'Rendicion.Actualizar'),
('Director Ejecutivo', 'Rendicion.Anular'),
('Jefe de Taller', 'Rendicion.Anular'),
('Usuario Comun', 'Rendicion.Anular'),
('Director Ejecutivo', 'Rendicion.Crear'),
('Jefe de Taller', 'Rendicion.Crear'),
('Usuario Comun', 'Rendicion.Crear'),
('Director Ejecutivo', 'Rendicion.Finalizar'),
('Jefe de Taller', 'Rendicion.Finalizar'),
('Usuario Comun', 'Rendicion.Finalizar'),
('Director Ejecutivo', 'Rendicion.GenerateExcel'),
('Jefe de Taller', 'Rendicion.GenerateExcel'),
('Usuario Comun', 'Rendicion.GenerateExcel'),
('Director Ejecutivo', 'Rendicion.GeneratePdf'),
('Jefe de Taller', 'Rendicion.GeneratePdf'),
('Usuario Comun', 'Rendicion.GeneratePdf'),
('Director Ejecutivo', 'Rendicion.Ver'),
('Jefe de Taller', 'Rendicion.Ver'),
('Usuario Comun', 'Rendicion.Ver'),
('Director Ejecutivo', 'Rendicion.VerTodos'),
('Jefe de Taller', 'Rendicion.VerTodos'),
('Usuario Comun', 'Rendicion.VerTodos'),
('Director Ejecutivo', 'Rrhh.Actualizar'),
('Jefe de Taller', 'Rrhh.Actualizar'),
('Usuario Comun', 'Rrhh.Actualizar'),
('Director Ejecutivo', 'Rrhh.Crear'),
('Jefe de Taller', 'Rrhh.Crear'),
('Usuario Comun', 'Rrhh.Crear'),
('Director Ejecutivo', 'Rrhh.GenerateExcel'),
('Jefe de Taller', 'Rrhh.GenerateExcel'),
('Usuario Comun', 'Rrhh.GenerateExcel'),
('Director Ejecutivo', 'Rrhh.GeneratePdf'),
('Jefe de Taller', 'Rrhh.GeneratePdf'),
('Usuario Comun', 'Rrhh.GeneratePdf'),
('Director Ejecutivo', 'Rrhh.Ver'),
('Jefe de Taller', 'Rrhh.Ver'),
('Usuario Comun', 'Rrhh.Ver'),
('Director Ejecutivo', 'Rrhh.VerContrato'),
('Jefe de Taller', 'Rrhh.VerContrato'),
('Usuario Comun', 'Rrhh.VerContrato'),
('Director Ejecutivo', 'Rrhh.VerTodos'),
('Jefe de Taller', 'Rrhh.VerTodos'),
('Usuario Comun', 'Rrhh.VerTodos'),
('Director Ejecutivo', 'Solicitud.Actualizar'),
('Jefe de Taller', 'Solicitud.Actualizar'),
('Usuario Comun', 'Solicitud.Actualizar'),
('Director Ejecutivo', 'Solicitud.Anular'),
('Jefe de Taller', 'Solicitud.Anular'),
('Usuario Comun', 'Solicitud.Anular'),
('Director Ejecutivo', 'Solicitud.Crear'),
('Jefe de Taller', 'Solicitud.Crear'),
('Usuario Comun', 'Solicitud.Crear'),
('Director Ejecutivo', 'Solicitud.CrearSolicitud'),
('Jefe de Taller', 'Solicitud.CrearSolicitud'),
('Usuario Comun', 'Solicitud.CrearSolicitud'),
('Director Ejecutivo', 'Solicitud.Finalizadas'),
('Jefe de Taller', 'Solicitud.Finalizadas'),
('Usuario Comun', 'Solicitud.Finalizadas'),
('Director Ejecutivo', 'Solicitud.Finalizar'),
('Jefe de Taller', 'Solicitud.Finalizar'),
('Usuario Comun', 'Solicitud.Finalizar'),
('Director Ejecutivo', 'Solicitud.GenerateExcel'),
('Jefe de Taller', 'Solicitud.GenerateExcel'),
('Usuario Comun', 'Solicitud.GenerateExcel'),
('Director Ejecutivo', 'Solicitud.GeneratePdf'),
('Jefe de Taller', 'Solicitud.GeneratePdf'),
('Usuario Comun', 'Solicitud.GeneratePdf'),
('Director Ejecutivo', 'Solicitud.Ver'),
('Jefe de Taller', 'Solicitud.Ver'),
('Usuario Comun', 'Solicitud.Ver'),
('Director Ejecutivo', 'Solicitud.VerSolicitudEmision'),
('Jefe de Taller', 'Solicitud.VerSolicitudEmision'),
('Usuario Comun', 'Solicitud.VerSolicitudEmision'),
('Director Ejecutivo', 'Solicitud.VerTodos'),
('Jefe de Taller', 'Solicitud.VerTodos'),
('Usuario Comun', 'Solicitud.VerTodos'),
('Director Ejecutivo', 'User.ChangePassword'),
('Jefe de Taller', 'User.ChangePassword'),
('Usuario Comun', 'User.ChangePassword');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `a_egresos_reales_sr`
--
CREATE TABLE IF NOT EXISTS `a_egresos_reales_sr` (
`rendicion` varchar(10)
,`item_rendicion_id` int(11)
,`rendicion_directa_id` int(11)
,`item_rendicion_directa_id` int(11)
,`proveedor_id` int(11)
,`cuenta_contable_id` int(11)
,`cuenta_especifica_id` int(11)
,`proyecto_id` int(11)
,`cuenta_id` int(11)
,`tipo_documento` varchar(100)
,`numero_documento` varchar(100)
,`tipo_solicitud` varchar(45)
,`tipo_cheque` varchar(45)
,`numero_cheque` varchar(45)
,`fecha` date
,`mes` varchar(45)
,`agno` varchar(45)
,`monto` int(11)
,`rendicion_directa.estado` varchar(45)
);
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `centro_costo`
--

CREATE TABLE IF NOT EXISTS `centro_costo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `centro_costo`
--

INSERT INTO `centro_costo` (`id`, `nombre`) VALUES
(1, 'Taller Arquitectura'),
(2, 'Taller de Difusion'),
(3, 'Administracion'),
(4, 'Taller Historia del Arte'),
(5, 'Taller Investigacion y Desarrollo'),
(6, 'Otro centro de costos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comuna`
--

CREATE TABLE IF NOT EXISTS `comuna` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `region_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_comuna_region_idx` (`region_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=347 ;

--
-- Volcado de datos para la tabla `comuna`
--

INSERT INTO `comuna` (`id`, `nombre`, `region_id`) VALUES
(1, 'Iquique', 1),
(2, 'Alto Hospicio', 1),
(3, 'Pozo Almonte', 1),
(4, 'Camiña', 1),
(5, 'Colchane', 1),
(6, 'Huara', 1),
(7, 'Pica', 1),
(8, 'Antofagasta', 2),
(9, 'Mejillones', 2),
(10, 'Sierra Gorda', 2),
(11, 'Taltal', 2),
(12, 'Calama', 2),
(13, 'Ollague', 2),
(14, 'San Pedro de Atacama', 2),
(15, 'Tocopilla', 2),
(16, 'María Elena', 2),
(17, 'Copiapó', 3),
(18, 'Caldera', 3),
(19, 'Tierra Amarilla', 3),
(20, 'Chañaral', 3),
(21, 'Diego de Almagro', 3),
(22, 'Vallenar', 3),
(23, 'Alto del Carmen', 3),
(24, 'Freirina', 3),
(25, 'Huasco', 3),
(26, 'La Serena', 4),
(27, 'Coquimbo', 4),
(28, 'Andacollo', 4),
(29, 'La Higuera', 4),
(30, 'Paihuano', 4),
(31, 'Vicuña', 4),
(32, 'Illapel', 4),
(33, 'Canela', 4),
(34, 'Los Vilos', 4),
(35, 'Salamanca', 4),
(36, 'Ovalle', 4),
(37, 'Combarbalá', 4),
(38, 'Monte Patria', 4),
(39, 'Punitaqui', 4),
(40, 'Río Hurtado', 4),
(41, 'Valparaíso', 5),
(42, 'Casablanca', 5),
(43, 'Concón', 5),
(44, 'Juan Fernández', 5),
(45, 'Puchuncaví', 5),
(46, 'Quilpué', 5),
(47, 'Quintero', 5),
(48, 'Villa Alemana', 5),
(49, 'Viña del Mar', 5),
(50, 'Isla de Pascua', 5),
(51, 'Los Andes', 5),
(52, 'Calle Larga', 5),
(53, 'Rinconada', 5),
(54, 'San Esteban', 5),
(55, 'La Ligua', 5),
(56, 'Cabildo', 5),
(57, 'Papudo', 5),
(58, 'Petorca', 5),
(59, 'Zapallar', 5),
(60, 'Quillota', 5),
(61, 'Calera', 5),
(62, 'Hijuelas', 5),
(63, 'La Cruz', 5),
(64, 'Limache', 5),
(65, 'Nogales', 5),
(66, 'Olmué', 5),
(67, 'San Antonio', 5),
(68, 'Algarrobo', 5),
(69, 'Cartagena', 5),
(70, 'El Quisco', 5),
(71, 'El Tabo', 5),
(72, 'Santo Domingo', 5),
(73, 'San Felipe', 5),
(74, 'Catemu', 5),
(75, 'Llay Llay', 5),
(76, 'Panquehue', 5),
(77, 'Putaendo', 5),
(78, 'Santa María', 5),
(79, 'Rancagua', 6),
(80, 'Codegua', 6),
(81, 'Coinco', 6),
(82, 'Coltauco', 6),
(83, 'Doñihue', 6),
(84, 'Graneros', 6),
(85, 'Las Cabras', 6),
(86, 'Machalí', 6),
(87, 'Malloa', 6),
(88, 'Mostazal', 6),
(89, 'Olivar', 6),
(90, 'Peumo', 6),
(91, 'Pichidegua', 6),
(92, 'Quinta de Tilcoco', 6),
(93, 'Rengo', 6),
(94, 'Requinoa', 6),
(95, 'San Vicente', 6),
(96, 'Pichilemu', 6),
(97, 'La Estrella', 6),
(98, 'Litueche', 6),
(99, 'Marchihue', 6),
(100, 'Navidad', 6),
(101, 'Paredones', 6),
(102, 'San Fernando', 6),
(103, 'Chépica', 6),
(104, 'Chimbarongo', 6),
(105, 'Lolol', 6),
(106, 'Nancagua', 6),
(107, 'Palmilla', 6),
(108, 'Peralillo', 6),
(109, 'Placilla', 6),
(110, 'Pumanque', 6),
(111, 'Santa Cruz', 6),
(112, 'Talca', 7),
(113, 'Constitución', 7),
(114, 'Curepto', 7),
(115, 'Empedrado', 7),
(116, 'Maule', 7),
(117, 'Pelarco', 7),
(118, 'Pencahue', 7),
(119, 'Río Claro', 7),
(120, 'San Clemente', 7),
(121, 'San Rafael', 7),
(122, 'Cauquenes', 7),
(123, 'Chanco', 7),
(124, 'Pelluhue', 7),
(125, 'Curicó', 7),
(126, 'Hualañé', 7),
(127, 'Licantén', 7),
(128, 'Molina', 7),
(129, 'Rauco', 7),
(130, 'Romeral', 7),
(131, 'Sagrada Familia', 7),
(132, 'Teno', 7),
(133, 'Vichuquén', 7),
(134, 'Linares', 7),
(135, 'Colbún', 7),
(136, 'Longaví', 7),
(137, 'Parral', 7),
(138, 'Retiro', 7),
(139, 'San Javier', 7),
(140, 'Villa Alegre', 7),
(141, 'Yerbas Buenas', 7),
(142, 'Concepción', 8),
(143, 'Coronel', 8),
(144, 'Chiguayante', 8),
(145, 'Florida', 8),
(146, 'Hualqui', 8),
(147, 'Lota', 8),
(148, 'Penco', 8),
(149, 'San Pedro De La Paz', 8),
(150, 'Santa Juana', 8),
(151, 'Talcahuano', 8),
(152, 'Tomé', 8),
(153, 'Hualpén', 8),
(154, 'Lebu', 8),
(155, 'Arauco', 8),
(156, 'Cañete', 8),
(157, 'Contulmo', 8),
(158, 'Curanilahue', 8),
(159, 'Los Alamos', 8),
(160, 'Tirua', 8),
(161, 'Los Angeles', 8),
(162, 'Antuco', 8),
(163, 'Cabrero', 8),
(164, 'Laja', 8),
(165, 'Mulchén', 8),
(166, 'Nacimiento', 8),
(167, 'Negrete', 8),
(168, 'Quilaco', 8),
(169, 'Quilleco', 8),
(170, 'San Rosendo', 8),
(171, 'Santa Bárbara', 8),
(172, 'Tucapel', 8),
(173, 'Yumbel', 8),
(174, 'Alto Biobío', 8),
(175, 'Chillán', 8),
(176, 'Bulnes', 8),
(177, 'Cobquecura', 8),
(178, 'Coelemu', 8),
(179, 'Coihueco', 8),
(180, 'Chillán Viejo', 8),
(181, 'El Carmen', 8),
(182, 'Ninhue', 8),
(183, 'Ñiquén', 8),
(184, 'Pemuco', 8),
(185, 'Pinto', 8),
(186, 'Portezuelo', 8),
(187, 'Quillón', 8),
(188, 'Quirihue', 8),
(189, 'Ranquil', 8),
(190, 'San Carlos', 8),
(191, 'San Fabián', 8),
(192, 'San Ignacio', 8),
(193, 'San Nicolás', 8),
(194, 'Trehuaco', 8),
(195, 'Yungay', 8),
(196, 'Temuco', 9),
(197, 'Carahue', 9),
(198, 'Cunco', 9),
(199, 'Curarrehue', 9),
(200, 'Freire', 9),
(201, 'Galvarino', 9),
(202, 'Gorbea', 9),
(203, 'Lautaro', 9),
(204, 'Loncoche', 9),
(205, 'Melipeuco', 9),
(206, 'Nueva Imperial', 9),
(207, 'Padre Las Casas', 9),
(208, 'Perquenco', 9),
(209, 'Pitrufquén', 9),
(210, 'Pucón', 9),
(211, 'Saavedra', 9),
(212, 'Teodoro Schmidt', 9),
(213, 'Toltén', 9),
(214, 'Vilcún', 9),
(215, 'Villarrica', 9),
(216, 'Cholchol', 9),
(217, 'Angol', 9),
(218, 'Collipulli', 9),
(219, 'Curacautín', 9),
(220, 'Ercilla', 9),
(221, 'Lonquimay', 9),
(222, 'Los Sauces', 9),
(223, 'Lumaco', 9),
(224, 'Purén', 9),
(225, 'Renaico', 9),
(226, 'Traiguén', 9),
(227, 'Victoria', 9),
(228, 'Puerto Montt', 10),
(229, 'Calbuco', 10),
(230, 'Cochamó', 10),
(231, 'Fresia', 10),
(232, 'Frutillar', 10),
(233, 'Los Muermos', 10),
(234, 'Llanquihue', 10),
(235, 'Maullín', 10),
(236, 'Puerto Varas', 10),
(237, 'Castro', 10),
(238, 'Ancud', 10),
(239, 'Chonchi', 10),
(240, 'Curaco de Vélez', 10),
(241, 'Dalcahue', 10),
(242, 'Puqueldón', 10),
(243, 'Queilén', 10),
(244, 'Quellón', 10),
(245, 'Quemchi', 10),
(246, 'Quinchao', 10),
(247, 'Osorno', 10),
(248, 'Puerto Octay', 10),
(249, 'Purranque', 10),
(250, 'Puyehue', 10),
(251, 'Río Negro', 10),
(252, 'San Juan de la Costa', 10),
(253, 'San Pablo', 10),
(254, 'Chaitén', 10),
(255, 'Futaleufú', 10),
(256, 'Hualaihue', 10),
(257, 'Palena', 10),
(258, 'Coihaique', 11),
(259, 'Lago Verde', 11),
(260, 'Aisén', 11),
(261, 'Cisnes', 11),
(262, 'Guaitecas', 11),
(263, 'Cochrane', 11),
(264, 'Ohiggins', 11),
(265, 'Tortel', 11),
(266, 'Chile Chico', 11),
(267, 'Río Ibáñez', 11),
(268, 'Punta Arenas', 12),
(269, 'Laguna Blanca', 12),
(270, 'Río Verde', 12),
(271, 'San Gregorio', 12),
(272, 'Cabo de Hornos', 12),
(273, 'ANTÁRTICA', 12),
(274, 'Porvenir', 12),
(275, 'Primavera', 12),
(276, 'Timaukel', 12),
(277, 'Natales', 12),
(278, 'Torres del Paine', 12),
(279, 'Santiago', 13),
(280, 'Cerrillos', 13),
(281, 'Cerro Navia', 13),
(282, 'Conchalí', 13),
(283, 'El Bosque', 13),
(284, 'Estación Central', 13),
(285, 'Huechuraba', 13),
(286, 'Independencia', 13),
(287, 'La Cisterna', 13),
(288, 'La Florida', 13),
(289, 'La Granja', 13),
(290, 'La Pintana', 13),
(291, 'La Reina', 13),
(292, 'Las Condes', 13),
(293, 'Lo Barnechea', 13),
(294, 'Lo Espejo', 13),
(295, 'Lo Prado', 13),
(296, 'Macul', 13),
(297, 'Maipú', 13),
(298, 'Ñuñoa', 13),
(299, 'Pedro Aguirre Cerda', 13),
(300, 'Peñalolén', 13),
(301, 'Providencia', 13),
(302, 'Pudahuel', 13),
(303, 'Quilicura', 13),
(304, 'Quinta Normal', 13),
(305, 'Recoleta', 13),
(306, 'Renca', 13),
(307, 'San Joaquín', 13),
(308, 'San Miguel', 13),
(309, 'San Ramón', 13),
(310, 'Vitacura', 13),
(311, 'Puente Alto', 13),
(312, 'Pirque', 13),
(313, 'San José de Maipo', 13),
(314, 'Colina', 13),
(315, 'Lampa', 13),
(316, 'Til til', 13),
(317, 'San Bernardo', 13),
(318, 'Buin', 13),
(319, 'Calera de Tango', 13),
(320, 'Paine', 13),
(321, 'Melipilla', 13),
(322, 'Alhué', 13),
(323, 'Curacaví', 13),
(324, 'María Pinto', 13),
(325, 'San Pedro', 13),
(326, 'Talagante', 13),
(327, 'El Monte', 13),
(328, 'Isla de Maipo', 13),
(329, 'Padre Hurtado', 13),
(330, 'Peñaflor', 13),
(331, 'Valdivia', 14),
(332, 'Corral', 14),
(333, 'Lanco', 14),
(334, 'Los Lagos', 14),
(335, 'Máfil', 14),
(336, 'Mariquina', 14),
(337, 'Paillaco', 14),
(338, 'Panguipulli', 14),
(339, 'La Unión', 14),
(340, 'Futrono', 14),
(341, 'Lago Ranco', 14),
(342, 'Río Bueno', 14),
(343, 'Arica', 15),
(344, 'Camarones', 15),
(345, 'Putre', 15),
(346, 'General Lagos', 15);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contrato`
--

CREATE TABLE IF NOT EXISTS `contrato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rrhh_id` int(11) NOT NULL,
  `proyecto_id` int(11) NOT NULL,
  `tipo_contrato_id` int(11) NOT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_termino` date DEFAULT NULL,
  `remuneracion` int(11) DEFAULT NULL,
  `estado` varchar(45) DEFAULT 'VIGENTE',
  PRIMARY KEY (`id`),
  KEY `fk_contrato_proyecto1_idx` (`proyecto_id`),
  KEY `fk_contrato_tipo_contrato1_idx` (`tipo_contrato_id`),
  KEY `fk_contrato_rrhh1_idx` (`rrhh_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuenta`
--

CREATE TABLE IF NOT EXISTS `cuenta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero_cuenta` varchar(100) DEFAULT NULL,
  `banco` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `cuenta`
--

INSERT INTO `cuenta` (`id`, `numero_cuenta`, `banco`) VALUES
(1, '01', 'Banco de Chile'),
(2, '02', 'Banco de Chile');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuenta_contable`
--

CREATE TABLE IF NOT EXISTS `cuenta_contable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `cuenta_contable`
--

INSERT INTO `cuenta_contable` (`id`, `nombre`) VALUES
(1, 'Recursos Humano'),
(2, 'Operacion'),
(3, 'Inversion'),
(4, 'Difusion'),
(5, 'Gastos Generales');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuenta_especifica`
--

CREATE TABLE IF NOT EXISTS `cuenta_especifica` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cuenta_contable_id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cuenta_especifica_cuenta_contable1_idx` (`cuenta_contable_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=50 ;

--
-- Volcado de datos para la tabla `cuenta_especifica`
--

INSERT INTO `cuenta_especifica` (`id`, `cuenta_contable_id`, `nombre`) VALUES
(1, 1, 'Bonos'),
(2, 1, 'Boleta de Honorario'),
(3, 3, 'Maquinas de Construccion'),
(4, 1, 'Capacitacion'),
(5, 1, 'Finiquitos'),
(6, 1, 'Imposiciones'),
(7, 1, 'IVA'),
(8, 1, 'Remuneraciones'),
(9, 1, 'Retenciones'),
(10, 1, 'Viaticos'),
(11, 2, 'Agua Potable'),
(12, 2, 'Alimentacion'),
(13, 2, 'Alojamiento'),
(14, 2, 'Arriendo Vehiculo'),
(15, 2, 'Combustible/Peajes'),
(16, 2, 'Celular'),
(17, 2, 'Fletes'),
(18, 2, 'Fotocopias, Impresiones, Ploteo'),
(19, 2, 'Gastos de Representacion'),
(20, 2, 'Gastos Notariales'),
(21, 2, 'Impl. de Seguridad'),
(22, 2, 'Luz'),
(23, 2, 'Mantencion de Vehiculos'),
(24, 2, 'Materiales de Construccion'),
(25, 2, 'Materiales de Oficina'),
(26, 2, 'Partes'),
(27, 2, 'Pasajes'),
(28, 2, 'Ropa de Trabajo'),
(29, 2, 'Telecomunicacion'),
(30, 3, 'Tecnologia, Computadores, Impresiones, Maq. Fotografica, Etc.'),
(31, 3, 'Otros Activos'),
(32, 3, 'Vehiculos'),
(33, 4, 'Difusion/Publicidad'),
(34, 4, 'Honorario Evento'),
(35, 4, 'Webhosting Internet'),
(36, 5, 'Abarrotes, Manaje y Otros'),
(37, 5, 'Arreglos Oficina'),
(38, 5, 'Arriendo Oficina'),
(39, 5, 'Correspondencia'),
(40, 5, 'Donaciones'),
(41, 5, 'Fotografia y Ediciones'),
(42, 5, 'Gastos Comunes'),
(43, 5, 'Gastos Financieros'),
(44, 5, 'Mantencion de Equipos'),
(45, 5, 'Otros Gastos Generales'),
(46, 5, 'Patente'),
(47, 5, 'Seguros'),
(48, 5, 'Servicios Legales'),
(49, 5, 'Taxis Colectivos/Estacionamiento');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `egresos_reales`
--
CREATE TABLE IF NOT EXISTS `egresos_reales` (
`rendicion` varchar(10)
,`item_rendicion_id` int(11)
,`rendicion_directa_id` int(11)
,`item_rendicion_directa_id` int(11)
,`proveedor_id` int(11)
,`rut` varchar(13)
,`proveedor` varchar(255)
,`cuenta_contable_id` int(11)
,`cuenta_contable` varchar(100)
,`cuenta_especifica_id` int(11)
,`cuenta_especifica` varchar(255)
,`proyecto_id` int(11)
,`sigla` varchar(100)
,`proyecto` varchar(255)
,`cuenta_id` int(11)
,`numero_cuenta` varchar(100)
,`banco` varchar(100)
,`tipo_documento` varchar(100)
,`numero_documento` varchar(100)
,`tipo_solicitud` varchar(45)
,`tipo_cheque` varchar(45)
,`numero_cheque` varchar(45)
,`fecha` date
,`mes` varchar(45)
,`agno` varchar(45)
,`monto` int(11)
,`estado` varchar(45)
);
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `emision`
--

CREATE TABLE IF NOT EXISTS `emision` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `proveedor_id` int(11) NOT NULL,
  `proyecto_id` int(11) NOT NULL,
  `solicitud_id` int(11) NOT NULL,
  `cuenta_id` int(11) NOT NULL,
  `ciudad` varchar(100) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `concepto` varchar(255) DEFAULT NULL,
  `total_debe` int(11) NOT NULL DEFAULT '0',
  `total_haber` int(11) NOT NULL DEFAULT '0',
  `tipo_solicitud` varchar(50) DEFAULT NULL,
  `tipo_cheque` varchar(45) DEFAULT NULL,
  `numero_cheque` varchar(100) DEFAULT NULL,
  `observaciones` blob,
  `estado` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_emision_solicitud1_idx` (`solicitud_id`),
  KEY `fk_emision_proyecto1_idx` (`proyecto_id`),
  KEY `fk_emision_proveedor1_idx` (`proveedor_id`),
  KEY `fk_emision_user1` (`user_id`),
  KEY `fk_emision_cuenta1` (`cuenta_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=78 ;

--
-- Volcado de datos para la tabla `emision`
--

INSERT INTO `emision` (`id`, `user_id`, `proveedor_id`, `proyecto_id`, `solicitud_id`, `cuenta_id`, `ciudad`, `fecha`, `concepto`, `total_debe`, `total_haber`, `tipo_solicitud`, `tipo_cheque`, `numero_cheque`, `observaciones`, `estado`) VALUES
(26, 3, 14, 1, 48, 2, NULL, '2013-04-30', NULL, 35000, 0, 'Cheque', 'Cruzado', '7866739', NULL, 'RENDIDA'),
(27, 3, 36, 1, 49, 1, NULL, '2013-05-13', NULL, 720000, 0, 'Cheque', 'Cruzado', '7912907', NULL, 'RENDIDA'),
(28, 3, 16, 4, 54, 2, NULL, '2013-05-15', NULL, 200000, 0, 'Cheque', 'Nominativo', '7886996', NULL, 'RENDIDA'),
(29, 3, 29, 4, 53, 2, NULL, '2013-05-15', NULL, 89250, 0, 'Cheque', 'Nominativo', '7886995', NULL, 'RENDIDA'),
(30, 3, 29, 4, 52, 2, NULL, '2013-05-15', NULL, 400000, 0, 'Cheque', 'Nominativo', '7886994', 0x4d6174657269616c65732042656cc3a96e, 'RENDIDA'),
(31, 3, 10, 1, 51, 2, NULL, '2013-05-15', NULL, 70500, 0, 'Cheque', 'Nominativo', '7866767', NULL, 'RENDIDA'),
(32, 3, 49, 5, 47, 2, NULL, '2013-05-13', NULL, 72000, 0, 'Cheque', 'Nominativo', '7866764', NULL, 'FINALIZADA'),
(33, 3, 2, 1, 56, 2, NULL, '2013-04-30', NULL, 30000, 0, 'Cheque', 'Cruzado', '7866736', NULL, 'RENDIDA'),
(34, 3, 2, 1, 56, 2, NULL, '2013-04-30', NULL, 45000, 0, 'Cheque', 'Nominativo', '7866736', NULL, 'RENDIDA'),
(35, 3, 2, 1, 57, 2, NULL, '2013-04-30', NULL, 45000, 0, 'Cheque', 'Nominativo', '7866737', NULL, 'RENDIDA'),
(36, 3, 2, 1, 55, 2, NULL, '2013-05-16', NULL, 20000, 0, 'Cheque', 'Nominativo', '7886997', NULL, 'RENDIDA'),
(37, 3, 2, 1, 50, 2, NULL, '2013-05-13', NULL, 40000, 0, 'Cheque', 'Nominativo', '7866766', NULL, 'RENDIDA'),
(38, 3, 55, 1, 58, 2, NULL, '2013-05-14', NULL, 193565, 0, 'Cheque', 'Nominativo', '7866760', NULL, 'RENDIDA'),
(39, 3, 56, 1, 59, 2, NULL, '2013-05-15', NULL, 52831, 0, 'Cheque', 'Nominativo', '7866761', NULL, 'RENDIDA'),
(40, 3, 54, 5, 60, 2, NULL, '2013-05-09', NULL, 15000, 0, 'Cheque', 'Nominativo', '7866763', NULL, 'RENDIDA'),
(41, 3, 29, 4, 64, 2, NULL, '2013-05-08', NULL, 300000, 0, 'Cheque', 'Nominativo', '7866755', NULL, 'RENDIDA'),
(42, 3, 14, 1, 62, 2, NULL, '2013-05-22', NULL, 37000, 0, 'Cheque', 'Nominativo', '7887002', NULL, 'ANULADA'),
(43, 3, 60, 5, 63, 2, NULL, '2013-05-22', NULL, 100000, 0, 'Cheque', 'Nominativo', '7887001', NULL, 'RENDIDA'),
(44, 3, 7, 5, 65, 1, NULL, '2013-05-15', NULL, 25000, 0, 'Cheque', 'Nominativo', '7912901', NULL, 'RENDIDA'),
(45, 3, 98, 5, 66, 1, NULL, '2013-05-01', NULL, 50000, 0, 'Cheque', 'Nominativo', '7912902', NULL, 'RENDIDA'),
(46, 3, 12, 1, 67, 1, NULL, '2013-05-30', NULL, 600000, 0, 'Cheque', 'Nominativo', '7912909', NULL, 'RENDIDA'),
(47, 3, 14, 1, 68, 2, NULL, '2013-05-02', NULL, 13395, 0, 'Cheque', 'Nominativo', '7866746', NULL, 'RENDIDA'),
(48, 3, 2, 1, 69, 2, NULL, '2013-05-03', NULL, 20000, 0, 'Cheque', 'Nominativo', '7866748', NULL, 'RENDIDA'),
(49, 3, 2, 1, 70, 2, NULL, '2013-05-06', NULL, 40000, 0, 'Cheque', 'Nominativo', '7866749', NULL, 'RENDIDA'),
(50, 3, 2, 1, 71, 2, NULL, '2013-05-07', NULL, 40000, 0, 'Cheque', 'Nominativo', '7866750', NULL, 'RENDIDA'),
(51, 3, 101, 4, 72, 2, NULL, '2013-05-22', NULL, 1240000, 0, 'Transferencia', 'Transferencia', 'Transferencia', NULL, 'RENDIDA'),
(52, 3, 101, 4, 73, 2, NULL, '2013-05-22', NULL, 1550000, 0, 'Transferencia', 'Transferencia', 'Transferencia', NULL, 'RENDIDA'),
(53, 3, 102, 1, 74, 2, NULL, '2013-05-23', NULL, 821100, 0, 'Transferencia', 'Transferencia', 'Transferencia', NULL, 'RENDIDA'),
(54, 3, 103, 1, 75, 2, NULL, '2013-05-22', NULL, 37000, 0, 'Cheque', 'Nominativo', '7887003', NULL, 'RENDIDA'),
(55, 3, 13, 4, 76, 2, NULL, '2013-05-18', NULL, 130900, 0, 'Transferencia', 'Transferencia', 'Transferencia', NULL, 'RENDIDA'),
(56, 3, 52, 1, 77, 1, NULL, '2013-04-30', NULL, 4000, 0, 'Cheque', 'Nominativo', '7912906', NULL, 'RENDIDA'),
(57, 3, 53, 1, 78, 2, NULL, '2013-05-05', NULL, 1039500, 0, 'Cheque', 'Cruzado', '7866738', NULL, 'RENDIDA'),
(58, 3, 14, 1, 80, 2, NULL, '2013-06-04', NULL, 144000, 0, 'Cheque', 'Nominativo', '7887009', 0x50616761646f20656e20656665637469766f, 'RENDIDA'),
(59, 3, 10, 1, 79, 2, NULL, '2013-06-05', NULL, 240000, 0, 'Cheque', 'Nominativo', '7887014', NULL, 'FINALIZADA'),
(60, 3, 14, 1, 81, 2, NULL, '2013-06-05', NULL, 35000, 0, 'Cheque', 'Nominativo', '7887012', NULL, 'FINALIZADA'),
(61, 3, 60, 15, 82, 2, NULL, '2013-06-13', NULL, 41500, 0, 'Cheque', 'Nominativo', '7887065', NULL, 'RENDIDA'),
(62, 3, 10, 1, 83, 2, NULL, '2013-06-14', NULL, 203300, 0, 'Cheque', 'Nominativo', '7887070', NULL, 'FINALIZADA'),
(63, 3, 2, 1, 84, 2, NULL, '2013-06-14', NULL, 22000, 0, 'Cheque', 'Portador', '7887069', NULL, 'RENDIDA'),
(64, 3, 29, 13, 86, 2, NULL, '2013-06-19', NULL, 100000, 0, 'Cheque', 'Nominativo', '7887077', NULL, 'RENDIDA'),
(65, 3, 43, 1, 88, 2, NULL, '2013-06-19', NULL, 84272, 0, 'Cheque', 'Nominativo', '7887076', NULL, 'RENDIDA'),
(66, 3, 16, 17, 87, 2, NULL, '2013-06-19', NULL, 47390, 0, 'Cheque', 'Nominativo', '7887079', NULL, 'RENDIDA'),
(67, 3, 1, 15, 85, 2, NULL, '2013-06-19', NULL, 65000, 0, 'Cheque', 'Nominativo', '7887078', NULL, 'FINALIZADA'),
(68, 3, 2, 1, 89, 2, NULL, '2013-06-21', NULL, 50000, 0, 'Cheque', 'Nominativo', '7887087', NULL, 'RENDIDA'),
(69, 3, 1, 5, 90, 2, NULL, '2013-06-25', NULL, 23000, 0, 'Cheque', 'Nominativo', '7912912', NULL, 'FINALIZADA'),
(70, 3, 14, 5, 92, 2, NULL, '2013-06-25', NULL, 10000, 0, 'Cheque', 'Transferencia', '7912917', NULL, 'FINALIZADA'),
(71, 3, 15, 5, 93, 1, NULL, '2013-06-25', NULL, 50000, 0, 'Cheque', 'Nominativo', '7912918', NULL, 'RENDIDA'),
(72, 3, 23, 5, 94, 1, NULL, '2013-06-25', NULL, 32000, 0, 'Cheque', 'Nominativo', '7912921', NULL, 'RENDIDA'),
(73, 3, 15, 5, 98, 2, NULL, '2013-06-27', NULL, 46000, 0, 'Cheque', 'Nominativo', '7942568', NULL, 'RENDIDA'),
(74, 3, 17, 1, 101, 2, NULL, '2013-06-28', NULL, 60000, 0, 'Cheque', 'Nominativo', '7942573', NULL, 'FINALIZADA'),
(75, 3, 60, 15, 103, 2, NULL, '2013-06-28', NULL, 75800, 0, 'Cheque', 'Nominativo', '7942576', NULL, 'PENDIENTE'),
(76, 3, 156, 5, 106, 1, NULL, '2013-07-04', NULL, 50000, 0, 'Cheque', 'Nominativo', '7912932', NULL, 'FINALIZADA'),
(77, 3, 14, 7, 105, 1, NULL, '2013-07-04', NULL, 35000, 0, 'Cheque', 'Nominativo', '7912931', NULL, 'FINALIZADA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `financiamiento`
--

CREATE TABLE IF NOT EXISTS `financiamiento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `financiamiento`
--

INSERT INTO `financiamiento` (`id`, `nombre`) VALUES
(1, 'Conadi'),
(2, 'Municipalidad'),
(3, 'Corfo'),
(4, 'Minera Candelaria Copiapó'),
(5, 'reintegro ch/xx'),
(6, 'CORDENOR-CORFO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ingreso`
--

CREATE TABLE IF NOT EXISTS `ingreso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proyecto_id` int(11) NOT NULL,
  `tipo_ingreso_id` int(11) NOT NULL,
  `financiamiento_id` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `monto` int(11) DEFAULT '0',
  `observaciones` blob,
  `neto` int(11) DEFAULT NULL,
  `iva` float DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_presupuesto_financiamiento1_idx` (`financiamiento_id`),
  KEY `fk_presupuesto_tipo_ingreso1_idx` (`tipo_ingreso_id`),
  KEY `fk_presupuesto_proyecto1_idx` (`proyecto_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `ingreso`
--

INSERT INTO `ingreso` (`id`, `proyecto_id`, `tipo_ingreso_id`, `financiamiento_id`, `fecha`, `monto`, `observaciones`, `neto`, `iva`, `total`) VALUES
(1, 10, 3, 4, '2013-04-05', 3500000, NULL, NULL, NULL, NULL),
(2, 10, 3, 4, '2013-08-30', 3500000, NULL, NULL, NULL, NULL),
(3, 1, 1, 5, '2013-05-01', 50000, 0x7265696e746567726f2064652063682f, NULL, NULL, NULL),
(4, 15, 3, 6, '2013-06-03', 66000000, 0x496e677265736f732064652033342079203332206d696c6c6f6e65732c20677275706f732050454c2043616d61726f6e6573, 66000000, 0, 66000000),
(5, 13, 1, 3, '2013-06-17', 64628445, 0x496e677265736f2050445420636f72666f2d52757461206465206c6173204d6973696f6e6573, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `item_presupuesto`
--

CREATE TABLE IF NOT EXISTS `item_presupuesto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `presupuesto_id` int(11) NOT NULL,
  `cuenta_contable_id` int(11) NOT NULL,
  `proyecto_id` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `mes` varchar(45) DEFAULT NULL,
  `agno` varchar(45) DEFAULT NULL,
  `monto` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_item_presupuesto_presupuesto1_idx` (`presupuesto_id`),
  KEY `fk_item_presupuesto_cuenta_contable1_idx` (`cuenta_contable_id`),
  KEY `fk_item_presupuesto_proyecto1_idx` (`proyecto_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=38 ;

--
-- Volcado de datos para la tabla `item_presupuesto`
--

INSERT INTO `item_presupuesto` (`id`, `presupuesto_id`, `cuenta_contable_id`, `proyecto_id`, `fecha`, `mes`, `agno`, `monto`) VALUES
(1, 1, 1, 1, '2012-03-15', NULL, NULL, 160000000),
(2, 1, 2, 1, '2012-03-15', NULL, NULL, 300000000),
(3, 1, 3, 1, '2012-03-15', NULL, NULL, 10000000),
(4, 1, 4, 1, '2012-03-15', NULL, NULL, 50000000),
(5, 1, 5, 1, '2012-03-15', NULL, NULL, 68000000),
(6, 2, 1, 2, '2012-10-10', NULL, NULL, 4000000),
(7, 2, 2, 2, '2012-10-10', NULL, NULL, 3000000),
(8, 2, 4, 2, '2012-10-15', NULL, NULL, 1500000),
(9, 2, 5, 2, '2012-10-10', NULL, NULL, 1500000),
(10, 3, 1, 3, '2013-03-11', NULL, NULL, 15500000),
(11, 3, 2, 3, '2013-03-11', NULL, NULL, 21800000),
(12, 3, 4, 3, '2013-03-11', NULL, NULL, 10000000),
(13, 3, 5, 3, '2013-03-11', NULL, NULL, 550000),
(14, 4, 1, 4, '2013-03-11', NULL, NULL, 8000000),
(15, 4, 2, 4, '2013-03-11', NULL, NULL, 45000000),
(16, 4, 4, 4, '2013-03-11', NULL, NULL, 15000000),
(17, 4, 5, 4, '2013-03-11', NULL, NULL, 5000000),
(18, 5, 1, 5, '2013-03-11', NULL, NULL, 3000000),
(19, 5, 2, 5, NULL, NULL, NULL, 3000000),
(20, 5, 3, 5, NULL, NULL, NULL, 2000000),
(21, 5, 5, 5, NULL, NULL, NULL, 3000000),
(22, 6, 1, 6, NULL, NULL, NULL, 9000000),
(23, 6, 2, 6, '2013-03-11', NULL, NULL, 5360000),
(24, 6, 4, 6, NULL, NULL, NULL, 2000000),
(25, 6, 5, 6, NULL, NULL, NULL, 3000000),
(26, 7, 1, 7, '2013-03-11', NULL, NULL, 45000000),
(27, 7, 2, 7, NULL, NULL, NULL, 30000000),
(28, 7, 4, 7, NULL, NULL, NULL, 10000000),
(29, 7, 5, 7, NULL, NULL, NULL, 15000000),
(30, 8, 1, 8, NULL, NULL, NULL, 15000000),
(31, 8, 2, 8, NULL, NULL, NULL, 6600000),
(32, 8, 4, 8, NULL, NULL, NULL, 2000000),
(33, 8, 5, 8, NULL, NULL, NULL, 3000000),
(34, 10, 2, 10, '2013-05-31', NULL, NULL, 1385000),
(35, 10, 1, 10, '2013-04-02', NULL, NULL, 2444448),
(36, 10, 3, 10, '2013-04-02', NULL, NULL, 220000),
(37, 10, 5, 10, '2013-08-30', NULL, NULL, 607417);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `item_rendicion`
--

CREATE TABLE IF NOT EXISTS `item_rendicion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proveedor_id` int(11) NOT NULL,
  `cuenta_contable_id` int(11) NOT NULL,
  `cuenta_especifica_id` int(11) NOT NULL,
  `rendicion_id` int(11) DEFAULT NULL,
  `proyecto_id` int(11) NOT NULL,
  `cuenta_id` int(11) NOT NULL,
  `tipo_documento` varchar(100) DEFAULT NULL,
  `numero_documento` varchar(100) DEFAULT NULL,
  `tipo_solicitud` varchar(45) DEFAULT NULL,
  `tipo_cheque` varchar(45) DEFAULT NULL,
  `numero_cheque` varchar(45) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `mes` varchar(45) DEFAULT NULL,
  `agno` varchar(45) DEFAULT NULL,
  `monto` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_item_rendicion_rendicion1_idx` (`rendicion_id`),
  KEY `fk_item_rendicion_proyecto1_idx` (`proyecto_id`),
  KEY `fk_item_rendicion_proveedor1_idx` (`proveedor_id`),
  KEY `fk_item_rendicion_cuenta_especifica1_idx` (`cuenta_especifica_id`),
  KEY `fk_item_rendicion_cuenta_contable1` (`cuenta_contable_id`),
  KEY `fk_item_rendicion_cuenta1` (`cuenta_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=204 ;

--
-- Volcado de datos para la tabla `item_rendicion`
--

INSERT INTO `item_rendicion` (`id`, `proveedor_id`, `cuenta_contable_id`, `cuenta_especifica_id`, `rendicion_id`, `proyecto_id`, `cuenta_id`, `tipo_documento`, `numero_documento`, `tipo_solicitud`, `tipo_cheque`, `numero_cheque`, `fecha`, `mes`, `agno`, `monto`) VALUES
(27, 2, 2, 15, 11, 1, 2, 'Guia de Despacho', '758881', 'Cheque', NULL, '7866739', '2013-05-13', NULL, NULL, 35000),
(28, 36, 2, 27, 12, 1, 1, 'Factura', '856', 'Cheque', NULL, '7912907', '2013-04-30', NULL, NULL, 720000),
(29, 61, 2, 12, 13, 4, 2, 'Factura', '477', 'Cheque', NULL, '7886995', '2013-05-15', NULL, NULL, 89250),
(30, 2, 2, 15, 16, 1, 2, 'Guia de Despacho', '758809', 'Cheque', NULL, '7866736', '2013-04-30', NULL, NULL, 45000),
(31, 2, 2, 15, 17, 1, 2, 'Guia de Despacho', '758789', 'Cheque', NULL, '7866737', '2013-04-30', NULL, NULL, 45000),
(32, 55, 2, 29, 18, 1, 2, 'Factura', '29515430', 'Cheque', NULL, '7866760', '2013-04-17', NULL, NULL, 193565),
(33, 56, 2, 29, 19, 1, 2, 'Boleta', '999503', 'Cheque', NULL, '7866761', '2013-05-15', NULL, NULL, 52831),
(34, 57, 5, 45, 20, 5, 2, 'Boleta', '401515', 'Cheque', NULL, '7866763', '2013-05-13', NULL, NULL, 4869),
(35, 58, 5, 39, 20, 5, 2, 'Boleta', '32213', 'Cheque', NULL, '7866763', '2013-05-09', NULL, NULL, 5400),
(36, 52, 2, 20, 20, 5, 2, 'Boleta', '1271473', 'Cheque', NULL, '7866763', '2013-05-10', NULL, NULL, 2000),
(37, 24, 2, 11, 20, 5, 2, 'Boleta', 'Pendiente', 'Cheque', NULL, '7866763', '2013-05-15', NULL, NULL, 1500),
(38, 59, 5, 45, 20, 5, 2, 'S/D', 'S/N', 'Cheque', NULL, '7866763', '2013-05-14', NULL, NULL, 1231),
(39, 62, 2, 12, 14, 4, 2, 'Boleta', '62630', 'Cheque', NULL, '7886994', '2013-05-16', NULL, NULL, 3600),
(40, 63, 2, 12, 14, 4, 2, 'Boleta', '33449', 'Cheque', NULL, '7886994', '2013-05-16', NULL, NULL, 3500),
(41, 64, 2, 12, 14, 4, 2, 'Boleta', '106965', 'Cheque', NULL, '7886994', '2013-05-18', NULL, NULL, 400),
(42, 65, 5, 49, 14, 4, 2, 'Recibo de Dinero', 'S/N', 'Cheque', NULL, '7886994', '2013-05-17', NULL, NULL, 5000),
(43, 66, 2, 16, 14, 4, 2, 'Boleta', '29011', 'Cheque', NULL, '7886994', '2013-05-21', NULL, NULL, 2000),
(44, 67, 2, 12, 14, 4, 2, 'Factura', '4121552', 'Cheque', NULL, '7886994', '2013-05-16', NULL, NULL, 32260),
(45, 68, 2, 12, 14, 4, 2, 'Boleta', '165485', 'Cheque', NULL, '7886994', '2013-05-17', NULL, NULL, 8330),
(46, 67, 2, 12, 14, 4, 2, 'Factura', '4121551', 'Cheque', NULL, '7886994', '2013-05-16', NULL, NULL, 39295),
(47, 69, 5, 49, 14, 4, 2, 'Recibo de Dinero', '55380', 'Cheque', NULL, '7886994', '2013-05-14', NULL, NULL, 5000),
(48, 67, 2, 12, 14, 4, 2, 'Boleta', '391684220', 'Cheque', NULL, '7886994', '2013-05-18', NULL, NULL, 2865),
(49, 71, 2, 24, 14, 4, 2, 'Boleta', '56933', 'Cheque', NULL, '7886994', '2013-05-17', NULL, NULL, 6000),
(50, 72, 2, 12, 14, 4, 2, 'Boleta', '22365', 'Cheque', NULL, '7886994', '2013-05-17', NULL, NULL, 11400),
(51, 72, 2, 12, 14, 4, 2, 'Boleta', '22366', 'Cheque', NULL, '7886994', '2013-05-17', NULL, NULL, 11400),
(52, 72, 2, 12, 14, 4, 2, 'Boleta', '22363', 'Cheque', NULL, '7886994', '2013-05-17', NULL, NULL, 11400),
(53, 72, 2, 12, 14, 4, 2, 'Boleta', '22364', 'Cheque', NULL, '7886994', '2013-05-17', NULL, NULL, 11400),
(54, 72, 2, 12, 14, 4, 2, 'Boleta', '22367', 'Cheque', NULL, '7886994', '2013-05-17', NULL, NULL, 11400),
(55, 72, 2, 12, 14, 4, 2, 'Boleta', '22368', 'Cheque', NULL, '7886994', '2013-05-17', NULL, NULL, 11400),
(56, 59, 5, 49, 14, 4, 2, 'S/D', 'S/N', 'Cheque', NULL, '7886994', NULL, NULL, NULL, 29859),
(57, 1, 2, 24, 14, 4, 2, 'Boleta', '230696861', 'Cheque', NULL, '7886994', '2013-05-20', NULL, NULL, 3491),
(58, 74, 2, 12, 14, 4, 2, 'Recibo de Dinero', 'S/N', 'Cheque', NULL, '7886994', '2013-05-09', NULL, NULL, 100000),
(59, 10, 2, 24, 14, 4, 2, 'Recibo de Dinero', 'S/N', 'Cheque', NULL, '7886994', '2013-05-16', NULL, NULL, 60000),
(60, 75, 5, 49, 14, 4, 2, 'Recibo de Dinero', '11430', 'Cheque', NULL, '7886994', '2013-05-15', NULL, NULL, 5000),
(61, 2, 2, 15, 14, 4, 2, 'Guia de Despacho', '761112', 'Cheque', NULL, '7886994', '2013-05-18', NULL, NULL, 15000),
(62, 69, 5, 49, 14, 4, 2, 'Recibo de Dinero', '55379', 'Cheque', NULL, '7886994', NULL, NULL, NULL, 10000),
(63, 79, 2, 18, 21, 4, 2, 'Boleta', '141925', 'Cheque', NULL, '7866755', '2013-05-15', NULL, NULL, 450),
(64, 69, 5, 49, 21, 4, 2, 'Recibo de Dinero', '32292', 'Cheque', NULL, '7866755', '2013-05-16', NULL, NULL, 4000),
(65, 80, 2, 12, 21, 4, 2, 'Boleta', '665982', 'Cheque', NULL, '7866755', '2013-05-11', NULL, NULL, 3150),
(66, 81, 5, 49, 21, 4, 2, 'Recibo de Dinero', '7625', 'Cheque', NULL, '7866755', '2013-05-19', NULL, NULL, 5000),
(67, 62, 2, 12, 21, 4, 2, 'Boleta', '62472', 'Cheque', NULL, '7866755', '2013-05-14', NULL, NULL, 3000),
(68, 82, 5, 49, 21, 4, 2, 'Recibo de Dinero', 'S/N', 'Cheque', NULL, '7866755', NULL, NULL, NULL, 7000),
(69, 83, 2, 16, 21, 4, 2, 'Boleta', '233351240', 'Cheque', NULL, '7866755', '2013-05-11', NULL, NULL, 5000),
(70, 84, 5, 45, 21, 4, 2, 'Boleta', '101839', 'Cheque', NULL, '7866755', '2013-05-14', NULL, NULL, 2000),
(71, 85, 5, 49, 21, 4, 2, 'Recibo de Dinero', '37175', 'Cheque', NULL, '7866755', '2013-05-13', NULL, NULL, 2000),
(72, 78, 2, 25, 21, 4, 2, 'Boleta', '41972', 'Cheque', NULL, '7866755', '2013-05-13', NULL, NULL, 26600),
(73, 86, 2, 12, 21, 4, 2, 'Boleta', '519119', 'Cheque', NULL, '7866755', '2013-05-15', NULL, NULL, 9350),
(74, 87, 2, 12, 21, 4, 2, 'Boleta', '260', 'Cheque', NULL, '7866755', '2013-05-10', NULL, NULL, 5000),
(75, 88, 5, 45, 21, 4, 2, 'Boleta', '136590', 'Cheque', NULL, '7866755', '2013-05-10', NULL, NULL, 5000),
(76, 78, 2, 25, 21, 4, 2, 'Boleta', '43124', 'Cheque', NULL, '7866755', '2013-05-10', NULL, NULL, 35300),
(77, 1, 2, 24, 21, 4, 2, 'Boleta', '232617221', 'Cheque', NULL, '7866755', '2013-05-09', NULL, NULL, 12540),
(78, 89, 2, 15, 21, 4, 2, 'Boleta', '580415', 'Cheque', NULL, '7866755', '2013-05-10', NULL, NULL, 20000),
(79, 86, 2, 12, 21, 4, 2, 'Boleta', '519259', 'Cheque', NULL, '7866755', '2013-05-11', NULL, NULL, 5550),
(80, 73, 2, 12, 21, 4, 2, 'Boleta', '620144', 'Cheque', NULL, '7866755', '2013-05-09', NULL, NULL, 3040),
(81, 78, 2, 25, 21, 4, 2, 'Boleta', '43125', 'Cheque', NULL, '7866755', '2013-05-08', NULL, NULL, 39560),
(82, 90, 5, 49, 21, 4, 2, 'Boleta', '36492', 'Cheque', NULL, '7866755', '2013-05-09', NULL, NULL, 1000),
(83, 91, 2, 24, 21, 4, 2, 'Boleta', '63658', 'Cheque', NULL, '7866755', '2013-05-09', NULL, NULL, 4200),
(84, 92, 2, 24, 21, 4, 2, 'Boleta', '897068', 'Cheque', NULL, '7866755', '2013-05-09', NULL, NULL, 970),
(85, 93, 2, 24, 21, 4, 2, 'Boleta', '3317', 'Cheque', NULL, '7866755', '2013-05-09', NULL, NULL, 4000),
(86, 94, 5, 49, 21, 4, 2, 'Boleta', '831', 'Cheque', NULL, '7866755', '2013-05-15', NULL, NULL, 3000),
(87, 1, 2, 24, 21, 4, 2, 'Boleta', '230703470', 'Cheque', NULL, '7866755', '2013-05-12', NULL, NULL, 3090),
(88, 1, 2, 24, 21, 4, 2, 'Boleta', '229386648', 'Cheque', NULL, '7866755', '2013-05-11', NULL, NULL, 6180),
(89, 95, 2, 28, 21, 4, 2, 'Boleta', '11336', 'Cheque', NULL, '7866755', '2013-05-11', NULL, NULL, 7200),
(90, 96, 2, 12, 21, 4, 2, 'Boleta', '20038', 'Cheque', NULL, '7866755', '2013-05-12', NULL, NULL, 3000),
(91, 89, 2, 15, 21, 4, 2, 'Boleta', '616096', 'Cheque', NULL, '7866755', '2013-05-09', NULL, NULL, 20000),
(92, 7, 2, 25, 21, 4, 2, 'Factura', '33884', 'Cheque', NULL, '7866755', '2013-05-08', NULL, NULL, 15000),
(93, 29, 5, 49, 21, 4, 2, 'Boleta', NULL, 'Cheque', NULL, '7866755', '2013-05-15', NULL, NULL, 18820),
(94, 97, 5, 45, 21, 4, 2, 'S/D', 'S/N', 'Cheque', NULL, '7866755', '2013-05-24', NULL, NULL, 20000),
(95, 7, 2, 25, 22, 5, 1, 'Factura', '33975', 'Cheque', NULL, '7912901', '2013-05-15', NULL, NULL, 22000),
(96, 59, 5, 49, 22, 5, 1, 'S/D', 'S/D', 'Cheque', NULL, '7912901', '2013-05-15', NULL, NULL, 3000),
(97, 98, 1, 2, 23, 5, 1, 'Boleta de Honorario', NULL, 'Cheque', NULL, '7912902', '2013-05-01', NULL, NULL, 50000),
(98, 12, 1, 2, 24, 1, 1, 'Boleta de Honorario', NULL, 'Cheque', NULL, '7912909', '2013-05-30', NULL, NULL, 600000),
(99, 99, 5, 45, 25, 1, 2, 'Boleta', '19238', 'Cheque', NULL, '7866746', '2013-05-02', NULL, NULL, 4500),
(100, 1, 5, 45, 25, 1, 2, 'Factura', '52887439', 'Cheque', NULL, '7866746', '2013-04-30', NULL, NULL, 6295),
(101, 100, 5, 49, 25, 1, 2, 'Boleta', '46754', 'Cheque', NULL, '7866746', '2013-04-29', NULL, NULL, 2600),
(102, 2, 2, 15, 26, 1, 2, 'Guia de Despacho', '759263', 'Cheque', NULL, '7866748', '2013-05-03', NULL, NULL, 20000),
(103, 2, 2, 15, 27, 1, 2, 'Guia de Despacho', '759566', 'Cheque', NULL, '7866749', '2013-05-06', NULL, NULL, 40000),
(104, 2, 2, 15, 28, 1, 2, 'Guia de Despacho', '759686', 'Cheque', NULL, '7866750', '2013-05-07', NULL, NULL, 40000),
(105, 101, 4, 34, 29, 4, 2, 'Factura', '1', 'Transferencia', NULL, 'Transferencia', '2013-05-22', NULL, NULL, 1240000),
(106, 101, 4, 34, 30, 4, 2, 'Factura', '1', 'Transferencia', NULL, 'Transferencia', '2013-05-22', NULL, NULL, 1550000),
(107, 102, 2, 24, 31, 1, 2, 'Factura', '42', 'Transferencia', NULL, 'Transferencia', '2013-05-23', NULL, NULL, 821100),
(108, 103, 2, 24, 32, 1, 2, 'Factura', '15943', 'Cheque', NULL, '7887003', '2013-05-22', NULL, NULL, 37000),
(109, 13, 2, 12, 33, 4, 2, 'Factura', '49', 'Transferencia', NULL, 'Transferencia', '2013-05-18', NULL, NULL, 130900),
(110, 52, 2, 20, 34, 1, 1, 'Boleta', '1070342', 'Cheque', NULL, '7912906', '2013-04-30', NULL, NULL, 2000),
(111, 52, 2, 20, 34, 1, 1, 'Boleta', '1070341', 'Cheque', NULL, '7912906', '2013-04-30', NULL, NULL, 2000),
(112, 53, 2, 24, 35, 1, 2, 'Boleta de Honorario', '9', 'Cheque', NULL, '7866738', '2013-05-05', NULL, NULL, 1039500),
(113, 1, 2, 12, 36, 4, 2, 'Boleta', '33448', 'Cheque', NULL, '7886996', '2013-05-16', NULL, NULL, 30870),
(114, 1, 2, 12, 36, 4, 2, 'Boleta', '33425', 'Cheque', NULL, '7886996', '2013-05-15', NULL, NULL, 21130),
(115, 104, 2, 12, 36, 4, 2, 'Boleta', '7696', 'Cheque', NULL, '7886996', '2013-05-15', NULL, NULL, 36000),
(116, 63, 2, 12, 36, 4, 2, 'Boleta', '33363', 'Cheque', NULL, '7886996', '2013-05-13', NULL, NULL, 12500),
(117, 105, 2, 24, 36, 4, 2, 'Boleta', '427441', 'Cheque', NULL, '7886996', '2013-05-15', NULL, NULL, 5000),
(118, 106, 2, 15, 36, 4, 2, 'Boleta', '468454', 'Cheque', NULL, '7886996', '2013-05-13', NULL, NULL, 5001),
(119, 107, 2, 12, 36, 4, 2, 'Boleta', '1917383', 'Cheque', NULL, '7886996', '2013-05-11', NULL, NULL, 2050),
(120, 108, 2, 12, 36, 4, 2, 'Boleta', '166001', 'Cheque', NULL, '7886996', '2013-05-18', NULL, NULL, 2700),
(121, 109, 2, 12, 36, 4, 2, 'Boleta', '167598', 'Cheque', NULL, '7886996', '2013-05-16', NULL, NULL, 1300),
(122, 110, 5, 45, 36, 4, 2, 'Boleta', '319', 'Cheque', NULL, '7886996', '2013-05-16', NULL, NULL, 200),
(123, 111, 2, 17, 36, 4, 2, 'Boleta', 'S/N', 'Cheque', NULL, '7886996', '2013-05-23', NULL, NULL, 10000),
(124, 111, 2, 17, 36, 4, 2, 'Boleta', 'S/N', 'Cheque', NULL, '7886996', '2013-05-23', NULL, NULL, 10000),
(125, 112, 2, 15, 36, 4, 2, 'Boleta', '437610', 'Cheque', NULL, '7886996', '2013-05-25', NULL, NULL, 7000),
(126, 89, 2, 15, 36, 4, 2, 'Boleta', '524171', 'Cheque', NULL, '7886996', '2013-05-25', NULL, NULL, 10000),
(127, 3, 2, 25, 36, 4, 2, 'Boleta', '57374', 'Cheque', NULL, '7886996', '2013-05-14', NULL, NULL, 5500),
(128, 113, 5, 49, 36, 4, 2, 'Boleta', '18510', 'Cheque', NULL, '7886996', '2013-05-24', NULL, NULL, 1000),
(129, 114, 2, 15, 36, 4, 2, 'Boleta', '36933', 'Cheque', NULL, '7886996', '2013-05-23', NULL, NULL, 2000),
(130, 68, 2, 15, 36, 4, 2, 'Boleta', '317250', 'Cheque', NULL, '7886996', '2013-05-26', NULL, NULL, 10000),
(131, 7, 2, 25, 36, 4, 2, 'Boleta', '145861', 'Cheque', NULL, '7886996', '2013-05-15', NULL, NULL, 10000),
(132, 115, 2, 15, 36, 4, 2, 'Boleta', '104331', 'Cheque', NULL, '7886996', '2013-05-27', NULL, NULL, 5000),
(133, 116, 2, 12, 36, 4, 2, 'Boleta', '59959', 'Cheque', NULL, '7886996', '2013-05-12', NULL, NULL, 6000),
(134, 7, 2, 25, 36, 4, 2, 'Boleta', '145725', 'Cheque', NULL, '7886996', '2013-04-16', NULL, NULL, 6000),
(135, 117, 2, 24, 36, 4, 2, 'Boleta', '625304', 'Cheque', NULL, '7886996', '2013-05-13', NULL, NULL, 749),
(136, 2, 2, 15, 37, 1, 2, 'Guia de Despacho', '760819', 'Cheque', NULL, '7886997', '2013-05-16', NULL, NULL, 20000),
(137, 118, 2, 12, 38, 1, 2, 'Recibo de Dinero', '7866767', 'Cheque', NULL, '7866767', '2013-05-14', NULL, NULL, 10500),
(138, 119, 2, 17, 38, 1, 2, 'Recibo de Dinero', '7866767', 'Cheque', NULL, '7866767', '2013-05-14', NULL, NULL, 30000),
(139, 5, 2, 24, 38, 1, 2, 'Factura', '55828', 'Cheque', NULL, '7866767', '2013-06-22', NULL, NULL, 30000),
(140, 2, 2, 15, 39, 1, 2, 'Guia de Despacho', '760439', 'Cheque', NULL, '7866766', '2013-05-14', NULL, NULL, 35900),
(141, 120, 2, 12, 39, 1, 2, 'Boleta', '1068933', 'Cheque', NULL, '7866766', '2013-05-18', NULL, NULL, 4100),
(142, 121, 2, 24, 40, 5, 2, 'Factura', '62', 'Cheque', NULL, '7887001', '2013-05-27', NULL, NULL, 100000),
(143, 7, 2, 25, 42, 1, 2, 'Factura', '34298', 'Cheque', NULL, '7887009', '2013-06-19', NULL, NULL, 144000),
(144, 2, 2, 15, 43, 1, 2, 'Guia de Despacho', '764542', 'Cheque', NULL, '7887069', '2013-06-14', NULL, NULL, 22000),
(145, 120, 2, 15, 44, 17, 2, 'Boleta', '131997', 'Cheque', NULL, '7887079', '2013-06-15', NULL, NULL, 10000),
(146, 120, 2, 12, 44, 17, 2, 'Boleta', '442179', 'Cheque', NULL, '7887079', '2013-06-19', NULL, NULL, 1500),
(147, 124, 2, 12, 44, 17, 2, 'Boleta', '4490', 'Cheque', NULL, '7887079', '2013-06-14', NULL, NULL, 9700),
(148, 80, 2, 12, 44, 17, 2, 'Boleta', '671674', 'Cheque', NULL, '7887079', '2013-06-19', NULL, NULL, 3450),
(149, 7, 2, 25, 44, 17, 2, 'Boleta', '147027', 'Cheque', NULL, '7887079', '2013-06-19', NULL, NULL, 10000),
(150, 80, 2, 12, 44, 17, 2, 'Boleta', '671577', 'Cheque', NULL, '7887079', '2013-06-19', NULL, NULL, 6150),
(151, 125, 2, 12, 44, 17, 2, 'Boleta', '51493', 'Cheque', NULL, '7887079', '2013-06-14', NULL, NULL, 2200),
(152, 120, 2, 12, 44, 17, 2, 'Boleta', '442178', 'Cheque', NULL, '7887079', '2013-06-15', NULL, NULL, 2290),
(153, 126, 2, 12, 44, 17, 2, 'Boleta', '439618', 'Cheque', NULL, '7887079', '2013-06-15', NULL, NULL, 1200),
(154, 127, 2, 12, 44, 17, 2, 'Boleta', '27519', 'Cheque', NULL, '7887079', '2013-06-09', NULL, NULL, 900),
(155, 130, 2, 12, 45, 15, 2, 'Boleta', '9731', 'Cheque', NULL, '7887078', '2013-06-14', NULL, NULL, 9700),
(156, 130, 2, 12, 45, 15, 2, 'Boleta', '9732', 'Cheque', NULL, '7887078', '2013-06-14', NULL, NULL, 10800),
(157, 130, 2, 12, 45, 15, 2, 'Boleta', '9735', 'Cheque', NULL, '7887078', '2013-06-18', NULL, NULL, 10800),
(158, 130, 2, 12, 45, 15, 2, 'Boleta', '9734', 'Cheque', NULL, '7887078', '2013-06-18', NULL, NULL, 4000),
(159, 131, 5, 49, 45, 15, 2, 'Boleta', '18719', 'Cheque', NULL, '7887078', '2013-06-17', NULL, NULL, 1000),
(160, 133, 5, 42, 45, 15, 2, 'Boleta', '1482', 'Cheque', NULL, '7887078', '2013-06-18', NULL, NULL, 13000),
(161, 108, 2, 12, 45, 15, 2, 'Boleta', '166626', 'Cheque', NULL, '7887078', '2013-06-19', NULL, NULL, 800),
(162, 64, 2, 12, 45, 15, 2, 'Boleta', '107602', 'Cheque', NULL, '7887078', '2013-06-19', NULL, NULL, 8100),
(163, 108, 2, 12, 45, 15, 2, 'Boleta', '166675', 'Cheque', NULL, '7887078', '2013-06-20', NULL, NULL, 6800),
(164, 2, 2, 15, 46, 1, 2, 'Guia de Despacho', '765492', 'Cheque', NULL, '7887087', '2013-06-21', NULL, NULL, 50000),
(165, 43, 2, 24, 47, 1, 2, 'Factura', '49931', 'Cheque', NULL, '7887076', '2013-06-18', NULL, NULL, 67682),
(166, 43, 2, 15, 47, 1, 2, 'Factura', '49905', 'Cheque', NULL, '7887076', '2013-06-14', NULL, NULL, 16590),
(167, 135, 2, 12, 48, 5, 1, 'Boleta', '7943', 'Cheque', NULL, '7912918', '2013-06-26', NULL, NULL, 7480),
(168, 2, 2, 15, 48, 5, 1, 'Boleta', '168865', 'Cheque', NULL, '7912918', '2013-06-26', NULL, NULL, 3500),
(169, 136, 2, 12, 48, 5, 1, 'Boleta', '484822', 'Cheque', NULL, '7912918', '2013-06-26', NULL, NULL, 23060),
(170, 137, 2, 12, 48, 5, 1, 'Boleta', '445996', 'Cheque', NULL, '7912918', '2013-06-25', NULL, NULL, 4430),
(171, 138, 2, 12, 48, 5, 1, 'Boleta', '746687', 'Cheque', NULL, '7912918', '2013-06-26', NULL, NULL, 650),
(172, 139, 2, 12, 48, 5, 1, 'Boleta', '91359', 'Cheque', NULL, '7912918', '2013-06-26', NULL, NULL, 4660),
(173, 140, 2, 12, 48, 5, 1, 'Boleta', '41181', 'Cheque', NULL, '7912918', '2013-06-26', NULL, NULL, 3160),
(174, 141, 2, 12, 48, 5, 1, 'Boleta', '126308', 'Cheque', NULL, '7912918', '2013-06-26', NULL, NULL, 1000),
(175, 143, 2, 12, 48, 5, 1, 'Boleta', '77141', 'Cheque', NULL, '7912918', '2013-06-26', NULL, NULL, 1370),
(176, 145, 2, 12, 48, 5, 1, 'Boleta', '127930', 'Cheque', NULL, '7912918', '2013-06-26', NULL, NULL, 250),
(177, 146, 5, 45, 48, 5, 1, 'S/D', NULL, 'Cheque', NULL, '7912918', NULL, NULL, NULL, 440),
(178, 1, 2, 24, 49, 5, 2, 'Factura', '50989727', 'Cheque', NULL, '7942568', '2013-06-26', NULL, NULL, 9388),
(179, 2, 2, 15, 49, 5, 2, 'Guia de Despacho', '766398', 'Cheque', NULL, '7942568', '2013-06-27', NULL, NULL, 30000),
(180, 147, 2, 24, 49, 5, 2, 'Recibo de Dinero', '01', 'Cheque', NULL, '7942568', '2013-06-26', NULL, NULL, 5000),
(181, 147, 2, 24, 49, 5, 2, 'Recibo de Dinero', '02', 'Cheque', NULL, '7942568', '2013-06-26', NULL, NULL, 1500),
(182, 146, 5, 45, 49, 5, 2, 'S/D', NULL, 'Cheque', NULL, '7942568', NULL, NULL, NULL, 112),
(183, 57, 5, 45, 50, 5, 1, 'Factura', '11019947', 'Cheque', NULL, '7912921', '2013-07-26', NULL, NULL, 31689),
(184, 146, 5, 45, 50, 5, 1, 'S/D', NULL, 'Cheque', NULL, '7912921', '2013-07-26', NULL, NULL, 311),
(185, 148, 2, 24, 51, 13, 2, 'Boleta', '1500', 'Cheque', NULL, '7887077', '2013-06-18', NULL, NULL, 1500),
(186, 149, 2, 24, 51, 13, 2, 'Boleta', '24878', 'Cheque', NULL, '7887077', '2013-06-18', NULL, NULL, 7200),
(187, 78, 2, 25, 51, 13, 2, 'Boleta', '82707', 'Cheque', NULL, '7887077', '2013-07-18', NULL, NULL, 26570),
(188, 92, 2, 24, 51, 13, 2, 'Boleta', '900011', 'Cheque', NULL, '7887077', '2013-06-18', NULL, NULL, 2750),
(189, 150, 2, 12, 51, 13, 2, 'Boleta', '480685', 'Cheque', NULL, '7887077', '2013-06-18', NULL, NULL, 2100),
(190, 68, 2, 15, 51, 13, 2, 'Boleta', '328668', 'Cheque', NULL, '7887077', '2013-06-18', NULL, NULL, 10000),
(191, 151, 2, 12, 51, 13, 2, 'Boleta', '31416', 'Cheque', NULL, '7887077', '2013-06-19', NULL, NULL, 600),
(192, 78, 2, 25, 51, 13, 2, 'Boleta', '82701', 'Cheque', NULL, '7887077', '2013-06-18', NULL, NULL, 22000),
(193, 152, 2, 24, 51, 13, 2, 'Boleta', '238787', 'Cheque', NULL, '7887077', '2013-06-18', NULL, NULL, 5400),
(194, 153, 2, 25, 51, 13, 2, 'Boleta', '110200', 'Cheque', NULL, '7887077', '2013-06-18', NULL, NULL, 2160),
(195, 154, 2, 25, 51, 13, 2, 'Boleta', '27251', 'Cheque', NULL, '7887077', '2013-06-19', NULL, NULL, 13300),
(196, 155, 2, 12, 51, 13, 2, 'Boleta', '27024', 'Cheque', NULL, '7887077', '2013-06-19', NULL, NULL, 6420),
(197, 2, 2, 15, 52, 15, 2, 'Guia de Despacho', '765915', 'Cheque', NULL, '7887065', '2013-06-23', NULL, NULL, 20000),
(198, 57, 5, 36, 52, 15, 2, 'Boleta', '780282070043', 'Cheque', NULL, '7887065', '2013-06-21', NULL, NULL, 6809),
(199, 157, 5, 36, 52, 15, 2, 'Boleta', '20076', 'Cheque', NULL, '7887065', '2013-06-12', NULL, NULL, 1890),
(200, 158, 5, 36, 52, 15, 2, 'Boleta', '019157', 'Cheque', NULL, '7887065', '2013-06-21', NULL, NULL, 1500),
(201, 2, 5, 36, 52, 15, 2, 'Boleta', '78598190', 'Cheque', NULL, '7887065', '2013-06-29', NULL, NULL, 5050),
(202, 130, 2, 13, 52, 15, 2, 'Boleta', '009761', 'Cheque', NULL, '7887065', '2013-06-29', NULL, NULL, 4000),
(203, 55, 2, 29, 52, 15, 2, 'Boleta', '3651209701', 'Cheque', NULL, '7887065', '2013-06-28', NULL, NULL, 2251);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `item_rendicion_directa`
--

CREATE TABLE IF NOT EXISTS `item_rendicion_directa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rendicion_directa_id` int(11) NOT NULL,
  `proveedor_id` int(11) NOT NULL,
  `cuenta_contable_id` int(11) NOT NULL,
  `cuenta_especifica_id` int(11) NOT NULL,
  `proyecto_id` int(11) NOT NULL,
  `tipo_documento` varchar(45) DEFAULT NULL,
  `numero_documento` varchar(45) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `mes` varchar(45) DEFAULT NULL,
  `agno` varchar(45) DEFAULT NULL,
  `monto` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_item_rendicion_directa_rendicion_directa` (`rendicion_directa_id`),
  KEY `fk_item_rendicion_directa_proveedor` (`proveedor_id`),
  KEY `fk_item_rendicion_directa_cuenta_contable` (`cuenta_contable_id`),
  KEY `fk_item_rendicion_directa_cuenta_especifica` (`cuenta_especifica_id`),
  KEY `fk_item_rendicion_directa_proyecto` (`proyecto_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Volcado de datos para la tabla `item_rendicion_directa`
--

INSERT INTO `item_rendicion_directa` (`id`, `rendicion_directa_id`, `proveedor_id`, `cuenta_contable_id`, `cuenta_especifica_id`, `proyecto_id`, `tipo_documento`, `numero_documento`, `fecha`, `mes`, `agno`, `monto`) VALUES
(5, 6, 53, 2, 24, 1, 'Boleta de Honorario', '9', '2013-05-13', NULL, NULL, 1039500),
(8, 8, 52, 2, 20, 1, 'Boleta', '1270342', '2013-05-22', NULL, NULL, 2000),
(9, 8, 52, 2, 20, 1, 'Boleta', '1270341', '2013-05-22', NULL, NULL, 2000),
(10, 10, 122, 4, 33, 4, 'Factura', '2390', '2013-05-29', NULL, NULL, 150000),
(11, 1, 2, 2, 15, 1, 'Guia de Despacho', '762975', '2013-06-21', NULL, NULL, 40000),
(12, 2, 2, 2, 15, 1, 'Guia de Despacho', '758679', '2013-04-20', NULL, NULL, 31500),
(13, 2, 68, 2, 15, 1, 'Boleta', '309385', '2013-05-10', NULL, NULL, 10000),
(14, 2, 3, 2, 25, 1, 'Factura', '298762', '2013-05-15', NULL, NULL, 11220),
(15, 3, 128, 2, 20, 5, 'Boleta', '930581', '2013-06-04', NULL, NULL, 1800),
(16, 3, 128, 2, 20, 5, 'Boleta', '930792', '2013-06-05', NULL, NULL, 2000),
(17, 3, 128, 2, 20, 5, 'Boleta', '930578', '2013-06-04', NULL, NULL, 2000),
(18, 3, 57, 2, 12, 5, 'Boleta', '330888', '2013-06-06', NULL, NULL, 4336),
(19, 3, 24, 2, 11, 5, 'Boleta', '57122', '2013-06-04', NULL, NULL, 7000),
(20, 3, 128, 2, 20, 5, 'Boleta', '931359', '2013-06-10', NULL, NULL, 1800),
(21, 3, 129, 5, 45, 5, 'Boleta', '277470', '2013-06-10', NULL, NULL, 1450),
(22, 3, 59, 5, 49, 5, 'S/D', 'S/N', '2013-06-04', NULL, NULL, 4614);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `item_solicitud`
--

CREATE TABLE IF NOT EXISTS `item_solicitud` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `solicitud_id` int(11) NOT NULL,
  `cuenta_contable_id` int(11) DEFAULT NULL,
  `valor_unitario` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `descripcion` blob,
  `total` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_item_solicitud_solicitud1_idx` (`solicitud_id`),
  KEY `fk_item_solicitud_cuenta_contable1_idx` (`cuenta_contable_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=134 ;

--
-- Volcado de datos para la tabla `item_solicitud`
--

INSERT INTO `item_solicitud` (`id`, `solicitud_id`, `cuenta_contable_id`, `valor_unitario`, `cantidad`, `descripcion`, `total`) VALUES
(56, 47, 1, 72000, 1, 0x536572766963696f20646520616c696d656e74616369c3b36e2061637469766964616420656e2042656cc3a96e, 72000),
(57, 48, 2, 35000, 1, 0x436f6d6275737469626c65204d6f76696c697a616369c3b36e, 35000),
(58, 49, 2, 720000, 1, 0x4d6f76696c697a616369c3b36e20536f636f726f6d61, 720000),
(59, 50, 2, 40000, 1, 0x436f6d6275737469626c65, 40000),
(60, 51, 2, 70500, 1, NULL, 70500),
(61, 52, 2, 200000, 1, 0x4d6174657269616c657320656e6d6172636163696f6e65732079206d6f6e74616a65206578706f73696369c3b36e2042656cc3a96e2053616261646f, 200000),
(62, 52, 2, 100000, 1, 0x416c696d656e74616369c3b36e20566965726e657320792053616261646f206578706f73696369c3b36e2042656c656e, 100000),
(63, 52, 2, 100000, 1, 0x4d6f76696c697a616369c3b36e20566965726e657320792053616261646f206578706f73696369c3b36e2042656cc3a96e, 100000),
(64, 53, 2, 89250, 1, 0x5061676f20706f7220636f636b74656c20756e6976657273696461642053616e746f20546f6d6173, 89250),
(65, 54, 2, 100000, 1, 0x4f7065726163696f6e657320656e2074657272656e6f2c20676173746f7320792f6f20696d707265766973746f73206d696572636f6c6573, 100000),
(66, 54, 2, 100000, 1, 0x416c696d656e74616369c3b36e206d69c3a972636f6c657320, 100000),
(67, 55, 2, 20000, 1, 0x436f6d6275737469626c652073756269646120536f636f726f6d612d43617175656e61, 20000),
(68, 56, 2, 45000, 1, 0x436f6d6275737469626c65204d6f76696c697a616369c3b36e, 45000),
(69, 57, 2, 45000, 1, 0x436f6d6275737469626c65204d6f76696c697a616369c3b36e, 45000),
(70, 58, 2, 193565, 1, 0x5061676f2064652054656cc3a9666f6e6f, 193565),
(71, 59, 2, 52831, 1, NULL, 52831),
(72, 60, 2, 15000, 1, NULL, 15000),
(73, 62, 2, 37000, 1, 0x686f6a61206465207265736f7274652064652074756e647261, 37000),
(74, 63, 2, 100000, 1, 0x4d61646572612070617261207365c3b1616c6574696361207475726973746963612d536f636f726f6d61, 100000),
(75, 64, 2, 300000, 1, 0x436f6d707261206465204d6174657269616c65732079207061676f2070726f766565646f726573, 300000),
(76, 65, 2, 25000, 1, 0x436f6d7072612064652054696e7461, 25000),
(77, 66, 2, 50000, 1, NULL, 50000),
(78, 67, 1, 600000, 1, 0x486f6e6f726172696f73204d61796f2c20736572766963696f7320646520496e7374616c616369c3b36e20456cc3a9637472696361, 600000),
(79, 68, 2, 13395, 1, 0x436f6d6275737469626c65, 13395),
(80, 69, 2, 20000, 1, 0x436f6d6275737469626c65, 20000),
(81, 70, 2, 40000, 1, NULL, 40000),
(82, 70, 1, NULL, 1, NULL, 0),
(83, 71, 2, 40000, 1, 0x436f6d6275737469626c65, 40000),
(84, 72, 2, 1240000, 1, 0x416d706c69666963616369c3b36e20456e6375656e74726f20426172726f636f, 1240000),
(85, 73, 2, 1550000, 1, 0x416d706c69666963616369c3b36e20456e6375656e74726f20426172726f636f, 1550000),
(86, 74, 2, 821100, 1, NULL, 821100),
(87, 75, 2, 37000, 1, NULL, 37000),
(88, 76, 2, 130900, 1, 0x416c696d656e74616369c3b36e2042656cc3a96e, 130900),
(89, 77, 2, 4000, 1, NULL, 4000),
(90, 78, 2, 1039500, 1, NULL, 1039500),
(91, 79, 2, 240000, 1, 0x436f6374656c20616e64696e6f20456e6375656e74726f20626172726f636f20284a6f7661204265726e61626529, 240000),
(92, 80, 1, 144000, 1, 0x74696e746173206f666963696e61, 144000),
(93, 81, 1, 35000, 1, 0x7061676f20646520636f6d707261207920656e76696f206c696a61732063697263756c6172657320636f6d70726164617320656e20616c746f20686f73706963696f, 35000),
(94, 82, 5, 2500, 5, 0x416c6d7565727a6f7320656e20434f4450412d64c3ad612031322f30322f3133, 12500),
(95, 82, 5, 2500, 4, 0x416c6d7565727a6f7320436f6470612064c3ad612031352d30362d3133, 10000),
(96, 82, 1, 2500, 4, 0x416c6d7565727a6f7320434f4450412064c3ad612031352d30362d3133, 10000),
(97, 82, 5, 1800, 5, 0x4465736179756e6f20436f6470612031322d30362d3133, 9000),
(98, 83, 1, 203300, 1, 0x416c646f20476f6d657a2c2054726162616a6f206120747261746f20286f7264656e206520696e76656e746172696f20646520626f6465676120736f636f726f6d612c2064657361726d6520646520696e73742e206465206661656e617329, 203300),
(99, 84, 2, 22000, 1, 0x6c6c656e61722063616d696f6e6574612061727269656e646f, 22000),
(100, 85, 2, 45000, 1, 0x436f6d6275737469626c652053616c69646120612054657272656e6f, 45000),
(101, 85, 5, 2500, 4, 0x616c6d7565727a6f7320646961206d61727465732031382d6a756e696f, 10000),
(102, 85, 5, 2500, 4, 0x416c6d7565727a6f7320646961206d6972636f6c65732031392d6a756e696f, 10000),
(103, 86, 2, 100000, 1, 0x4d6174657269616c6573206465206f666963696e61, 100000),
(104, 87, 2, 47390, 1, 0x5265656d626f6c736f20526567697374726f204172696361207920506172696e61636f7461204a617a7a20466573746976616c, 47390),
(105, 88, 2, 84272, 1, 0x56696472696f73206c616d696e61646f732031306d6d202863686571756520616c2064c3ad6129, 84272),
(106, 89, 2, 50000, 1, 0x656e74726567612063616d696f6e65746173, 50000),
(107, 90, 2, 20000, 1, 0x6772756120747261736c61646172206175746f2064652074616c6c6572, 20000),
(108, 90, 2, 3000, 1, 0x7265636172676172206261746572696120, 3000),
(109, 91, 2, 40000, 2, NULL, 80000),
(110, 92, 2, 10000, 1, NULL, 10000),
(111, 93, 2, 50000, 1, 0x616c696d656e74616369c3b36e20636f62696a61, 50000),
(112, 94, 5, 20000, 1, 0x416c696d656e746f207061726120706572726f205361636f20646520436f6d696461, 20000),
(113, 94, 5, 12000, 1, 0x416c696d656e746f207061726120706572726f20436f6d69646120656e6c6174616461, 12000),
(114, 97, 2, 46000, 1, 0x6d616e74656e6369c3b36e20636f62696a61, 46000),
(115, 98, 2, 46000, 1, 0x6d616e74656e6369c3b36e20636f62696a61, 46000),
(116, 100, 2, 30000, 2, 0x5265736572766120686f7374616c20506972636173204e65677261732c20436f70696170c3b32c20646f7320706572736f6e6173, 60000),
(117, 101, 2, 45000, 1, 0x636f6d6275737469626c65207061726120656e747265676172207665686963756c6f732064652061727269656e646f, 45000),
(118, 102, 2, 35800, 1, NULL, 35800),
(119, 101, 5, 15000, 1, 0x434f4d505241444520434f524f4e41204956414e20c391415645, 15000),
(120, 103, 2, 2700, 4, 0x416c6d7565727a6f20656e20436f647061, 10800),
(121, 103, 1, 65000, 1, 0x436f6d6275737469626c65, 65000),
(122, 104, 2, 30000, 6, 0x416c6f6a616d69656e746f207061726120646f7320706572736f6e617320686f7374616c20506972636173204e656772617320706f722074726573206e6f63686573, 180000),
(123, 104, 2, 40000, 2, 0x616c696d656e74616369c3b36e207061726120646f7320706572736f6e617320647572616e746520342064c3ad617320656e20436f70696170c3b3, 80000),
(124, 104, 2, 30000, 2, 0x7472616e73706f72746573, 60000),
(125, 104, 2, 10000, 2, 0x56c3ad7665726573207061726120656c207669616a65206964612079207675656c7461, 20000),
(126, 104, 3, 30000, 1, 0x54726162616a6f206465206172636869766f2079206d756c7469636f706961646f, 30000),
(127, 104, 1, 10000, 1, 0x50696c617320, 10000),
(128, 100, 2, NULL, NULL, NULL, 0),
(129, 105, 2, 15000, 1, 0x436f6d6275737469626c652072657365727661, 15000),
(130, 105, 2, 20000, 1, 0x416c696d656e74616369c3b36e2065717569706f20646520726567697374726f, 20000),
(131, 106, 2, 50000, 1, 0x636f6d70726120646520616c696d656e746f732073616c69646120612074657272656e6f207461636f7261, 50000),
(132, 107, 2, 50000, 1, 0x416c696d656e746163696f6e207669616a6520612063616d61726f6e6573, 50000),
(133, 107, 2, 20000, 1, 0x6d6174657269616c6573206465206f666963696e61, 20000);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `libro_compra`
--
CREATE TABLE IF NOT EXISTS `libro_compra` (
`item_rendicion_id` int(11)
,`fecha_emision` date
,`fecha_item_rendicion` date
,`dia_emision` int(2)
,`mes_emision` varchar(10)
,`ano_emision` int(4)
,`dia_item_rendicion` int(2)
,`mes_item_rendicion` varchar(10)
,`ano_item_rendicion` int(4)
,`tipo_documento` varchar(100)
,`numero_documento` varchar(100)
,`proveedor` varchar(255)
,`monto` int(11)
,`tipo_solicitud` varchar(45)
,`numero_cheque` varchar(100)
,`asignacion` varchar(100)
,`centro_costo` varchar(255)
,`proyecto` varchar(255)
,`cuenta_contable` varchar(100)
,`cuenta_especifica` varchar(255)
);
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `presupuesto`
--

CREATE TABLE IF NOT EXISTS `presupuesto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proyecto_id` int(11) NOT NULL,
  `tipo_ingreso_id` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `mes` varchar(45) DEFAULT NULL,
  `agno` varchar(45) DEFAULT NULL,
  `monto` int(11) DEFAULT '0',
  `observaciones` blob,
  PRIMARY KEY (`id`),
  KEY `fk_presupuesto_proyecto2_idx` (`proyecto_id`),
  KEY `fk_presupuesto_tipo_ingreso2_idx` (`tipo_ingreso_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Volcado de datos para la tabla `presupuesto`
--

INSERT INTO `presupuesto` (`id`, `proyecto_id`, `tipo_ingreso_id`, `fecha`, `mes`, `agno`, `monto`, `observaciones`) VALUES
(1, 1, 1, '2012-03-15', NULL, NULL, 588000000, NULL),
(2, 2, 1, '2012-10-10', NULL, NULL, 10000000, NULL),
(3, 3, 1, '2013-03-11', NULL, NULL, 47850000, NULL),
(4, 4, 1, '2013-03-11', NULL, NULL, 73000000, NULL),
(5, 5, 4, '2013-03-11', NULL, NULL, 11000000, NULL),
(6, 6, 1, '2013-03-11', NULL, NULL, 19360000, NULL),
(7, 7, 1, '2013-03-11', NULL, NULL, 100000000, NULL),
(8, 8, 1, NULL, NULL, NULL, 26600000, NULL),
(9, 9, 3, NULL, NULL, NULL, 0, NULL),
(10, 10, 3, '2013-03-22', NULL, NULL, 4656865, NULL),
(11, 10, 3, '2013-03-22', NULL, NULL, 0, NULL),
(12, 11, 2, '2013-04-04', NULL, NULL, 0, NULL),
(13, 15, 3, '2013-05-23', NULL, NULL, 0, 0x4669726d6120646520636f6e747261746f73203233206465206d61796f2032303133);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE IF NOT EXISTS `proveedor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rut` varchar(13) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=160 ;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`id`, `rut`, `nombre`) VALUES
(1, '96792430-k', 'Sodimac S.A.'),
(2, '78598190-1', 'Sociedad Victor Hugo Cortes Bravo y Cia Ltda.'),
(3, '81056900-K', 'Yanulaque y Cia Ltda.'),
(4, '76184244-7', 'Agroindustrial Pablo Lagos Ku E.I.R.L.'),
(5, '79.889.650-4', 'Ferreterias Iberia Ltda.'),
(6, '96511460-2', 'Construmart S.A.'),
(7, '5009405-7', 'MIRIAM HERESI JORDAN'),
(8, '80314700-0', 'Empresa de Transportes Rurales Limitada'),
(9, '16153864-7', 'María Javiera Maino González'),
(10, '16.666.547-7', 'Benjamin Garcia'),
(11, '10.696.080-1', 'Francisco León'),
(12, '12.609.575-9', 'Pablo Choque Flores'),
(13, '4.232.934-7', 'Adela Cutipa Santos'),
(14, '14107505-5', 'Miguel Rojas Henriquez'),
(15, '16.770.824-2', 'Israel Quispe Lazaro'),
(16, '16.225.568-1', 'Cristian Brandau Mardonez'),
(17, '12.023.864-7', 'Ronald Caicedo Garay'),
(18, NULL, 'FRANCISCO TAPIA'),
(19, NULL, 'JORGE CHAU'),
(20, '92.648.000-6', 'MADERAS ENCO S.A.'),
(21, '76099427-8', 'SOCIEDAD NEUROPSICOLOGICA ARICA LTDA.'),
(22, '15000952-9', 'Nicolas Miño zA'),
(23, '15000952-9', 'Nicolas Miño Zapata'),
(24, '76044798-6', 'AGUA DEL TOMAS LIMITADA'),
(25, '12.086.055-0', 'María Cristina Santos Rojas"T. Mechita"'),
(26, NULL, 'Quadgraphics Chile S.A.'),
(27, '76.055.072-8', 'Printlab Digital Ltda.'),
(28, NULL, 'Joaquín Astaburuaga'),
(29, NULL, 'Daniela Zegarra'),
(30, NULL, 'Aguas del Altiplano S.A.'),
(31, NULL, 'Aguas del Altiplano S.A.'),
(32, NULL, 'Aseguradora Magallanes S.A.'),
(33, NULL, 'Hotel Arica Limitada'),
(34, NULL, 'Federal Express Agencia en Chile'),
(35, NULL, 'Filomena Emilia Humire Vasquez'),
(36, NULL, 'Hugo Pascual Paco Bolaños'),
(37, NULL, 'Andrea Romina Páez Torres'),
(38, NULL, 'Empresa Lipigas S.A.'),
(39, NULL, 'Autorentas del Pacífico S.A. '),
(40, NULL, 'Artículos Dentales Arequipa S.R.Ltda.'),
(41, NULL, 'Ximena Enriqueta Ochoa Araya'),
(42, NULL, 'Herco Editores S.A.'),
(43, NULL, 'Sociedad Crispieri y Compañia Limitada'),
(44, '15026747-1', 'YOCELYN PINILLA ALANIZ'),
(45, '00000', NULL),
(46, '0000', 'FINIQUITO HERIBERTO ZEGARRA'),
(47, NULL, 'NIVALDO PINO'),
(48, NULL, 'SUELDOS MAESTROS'),
(49, '4.394.277-8', 'Maria Nila Santos'),
(50, '10.113.210-2', 'Felipe Choque Marca'),
(51, '4.394.277-8', 'Maria Nila Santos'),
(52, NULL, 'Victor Warner Sarria'),
(53, NULL, 'Vicente Mamani'),
(54, NULL, 'Gerardo Carrasco'),
(55, NULL, 'Entel PCS Telecomunicaciones S.A.'),
(56, NULL, 'Servicios Empresariales Claro S.A.'),
(57, NULL, 'Cencosud S.A.'),
(58, NULL, 'Chilexpress S.A.'),
(59, NULL, 'Movilización'),
(60, '14105542-9', 'Freddy Alvarez mamani'),
(61, '12.438.652-7', 'Maritza Noelia Medina Escobar'),
(62, NULL, 'Servicios Gastronomicos Ordoñez y Milla Ltda.'),
(63, NULL, 'Cecilia de las Mercedes Gonzalez Pino'),
(64, NULL, 'Zunilda Candelaria Huanca Carrasco'),
(65, NULL, 'Radio Taxi Futuro'),
(66, NULL, 'Fasa Chile S.A.'),
(67, NULL, 'Admin. de Supermercados Hiper Limitada'),
(68, NULL, 'Estaciones de Servicio Seguel y Cia. Ltda.'),
(69, NULL, 'Radio Taxi Tucapel'),
(70, NULL, 'Alfredo Nicolas Navarrete Maldonado'),
(71, NULL, 'Ferreteria y Merceria Tucapel Limitada'),
(72, NULL, 'Comercial Castillon y Yucra Ltda.'),
(73, NULL, 'Aida Virgilia Blanco Flores'),
(74, NULL, 'Victoria Mollo Contreras'),
(75, NULL, 'Soc. Com. e Inversiones Moroni S.A.C.'),
(76, NULL, 'Sociedad Anonima Cerrada Soduchtacol'),
(77, NULL, 'Soc. de Transporte Ltda.'),
(78, NULL, 'Nivaldo Exequias Pino Farias'),
(79, NULL, 'Eduardo Ernesto Cienfuegos Zamora'),
(80, NULL, 'Ivan Eugenio Paniagua Venturini'),
(81, NULL, 'Radio Taxi Eiffel'),
(82, NULL, 'Radio Taxi Tarapacá'),
(83, NULL, 'Alicia Estefany Rojas Simpertigue'),
(84, NULL, 'Sociedad Importadora Dragon de Oro Limitada'),
(85, NULL, 'Soc. Com. Nuevas Estrellas S.A.C.'),
(86, NULL, 'Simon Jaime Pantoja Villegas'),
(87, NULL, 'Balvina Ayma Ccallomamani'),
(88, NULL, 'Gino Jose Barras Lemo'),
(89, NULL, 'Rodrigo Eugenio Poblete Muñoz'),
(90, NULL, 'Sergio Alberto Funes Funes'),
(91, NULL, 'Jorge Edgardo Campos Toledo'),
(92, NULL, 'Carlos Fredi Paredes Mejia'),
(93, NULL, 'Deisy Esmeralda Garcia Maya'),
(94, NULL, 'Radio Taxi 26-6000'),
(95, NULL, 'Elvira Silva Rubilar'),
(96, NULL, 'Comercial Consorcio Maipu Limitada'),
(97, NULL, 'Devolución de Dinero'),
(98, NULL, 'Constanza Manriquez'),
(99, NULL, 'Lucia Salinas Gomez'),
(100, NULL, 'Aeropuerto Chacalluta S.C.S.A.'),
(101, NULL, 'Gianni Rivera Martino'),
(102, NULL, 'Iliana Vanessa Villar Aguilera'),
(103, NULL, 'Manuel Eleuterio Paredes Paredes'),
(104, NULL, 'Bonifacio Caceres y Cia.'),
(105, NULL, 'Sociedad Comercial Emmanuel Limitada'),
(106, NULL, 'Estacion de Servicio Cornejo y Cia. Ltda.'),
(107, NULL, 'Li Chan Zhou'),
(108, NULL, 'Julia Humire Sanchez'),
(109, NULL, 'Lidenka Johanna Andrade Preuss'),
(110, NULL, 'Tomas Pablo Oyanedel Azocar'),
(111, NULL, 'Juan Varas'),
(112, NULL, 'Sociedad Reyes Tamayo Limitada'),
(113, NULL, 'Maria Cristina Figueroa Figueroa'),
(114, NULL, 'Baltolu E Hijos y Cia. SAC'),
(115, NULL, 'Combustibles Arica Ltda.'),
(116, NULL, 'Gloria Elizabeth Fajardo Sanchez'),
(117, NULL, 'Abraham Moises Leyton Poblete'),
(118, NULL, 'Rafael Humire'),
(119, NULL, 'Carlos Apata'),
(120, NULL, 'Soc. Adm. y Comercializadora las Nortinas Ltda.'),
(121, '22807852-2', 'Ana Tulia Saavedra Ovando'),
(122, '76652970-4', 'ASESORIAS E INV. COM. CIUDADANAS S.A.'),
(123, '4.196.924-5', 'Ivan Eugenio Paniagua Venturi'),
(124, NULL, 'Gonzalo Rodrigo Gallardo Zavala'),
(125, NULL, 'Julio Alexandro Barretas Garcia y Paqueteria y Confiteria'),
(126, NULL, 'Pamela Natalie Martinez Escobar'),
(127, NULL, 'Eliana del Carmen Oliveros Buzeta'),
(128, NULL, 'Armando Segundo Sanchez Risi'),
(129, NULL, 'Distribuidora Norte Grande Limitada'),
(130, '5262603-k', 'Felicinda Romero Zabala'),
(131, '4456068', 'Maria Cristina Figueroa Figueroa'),
(132, '17012275-5', 'Whitney Estafany Lillo Lequipi'),
(133, '17012275-5', 'Whitney Estafany Lillo Lequipi'),
(134, NULL, 'Alejandra Vargas'),
(135, '11466057-4', 'violeta victoria caris alvaez'),
(136, '13642257-k', 'yolanda maritza mamani mamani'),
(137, '14104840-6', 'diana del rosario calle canaviri'),
(138, '9310357-2', 'isabel veronica alvarez choquechambe'),
(139, '6560133-8', 'octavia sabina alave blas'),
(140, '1646845', 'katherine bernalda mendoza serrano'),
(141, '10772613-6', 'maria marta soliz morales'),
(142, '17553299-4', 'america sp'),
(143, '17553299-4', 'america sonia mamanai soliz'),
(144, NULL, 'Alejandra Vargas'),
(145, '71004100-8', 'asociación gremial de medianos y pequeños agricultores de la xv región'),
(146, NULL, 'ajuste de sencillo'),
(147, '12633455-6', 'hector rojas'),
(148, NULL, 'Sociedad Comercial Modastop Limitada'),
(149, NULL, 'Importadora la Bodega de la Economía y Compañia Limitada'),
(150, NULL, 'Exequiel del Rosario Jofre Adaros'),
(151, NULL, 'Agustin German Mollo Morales'),
(152, NULL, 'Exequiel Jose Vega Diaz'),
(153, NULL, 'Patricio Vejar Lopez Bazar Melquisedec E.I.R.L.'),
(154, NULL, 'Ximena Sandra Albiñez Vino'),
(155, NULL, 'Georgina del Carmen Vargas Marchant'),
(156, '13777445-3', 'diego pino'),
(157, '11610482-2', 'Ema Nilda Araya Cruz'),
(158, '12093956-4', 'Jose Manuel Mancilla Sanchez'),
(159, NULL, 'Fanny Goupil');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyecto`
--

CREATE TABLE IF NOT EXISTS `proyecto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rrhh_id` int(11) NOT NULL,
  `centro_costo_id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `sigla` varchar(100) DEFAULT NULL,
  `descripcion` blob,
  `archivo` blob,
  `contrato` blob,
  `comuna` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_proyecto_centro_costo1_idx` (`centro_costo_id`),
  KEY `fk_proyecto_rrhh1_idx` (`rrhh_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=37 ;

--
-- Volcado de datos para la tabla `proyecto`
--

INSERT INTO `proyecto` (`id`, `rrhh_id`, `centro_costo_id`, `nombre`, `sigla`, `descripcion`, `archivo`, `contrato`, `comuna`, `region`) VALUES
(1, 1, 1, 'Restauración de la Iglesia de Socoroma, Comuna de Putre', 'Socoroma', 0x52657374617572616369c3b36e2049676c6573696120646520536f636f726f6d612c20636f6d756e61206465205075747265, 0x6172636869766f2d312e706466, 0x6172636869766f2d312e706466, 'ARICA', '15'),
(2, 15, 4, 'CRESPIAL', 'CRESPIAL', NULL, 0x6172636869766f2d312e706466, 0x6172636869766f2d312e706466, 'ARICA', '15'),
(3, 21, 2, 'Festival internacional de Cine rural Arica Nativa', 'festival de cine 2013', 0x466573746976616c2064652063696e65, 0x6172636869766f2d312e706466, 0x6172636869766f2d312e706466, 'ARICA', '15'),
(4, 15, 4, 'Encuentro Barroco en Arica', 'Encuentro Barroco', 0x506f6e656e636961732c20636f6e63696572746f732c206578706f736963696f6e65732c2074616c6c65726573207920636972637569746f2052757461206465206c6173204d6973696f6e65732e, 0x70726f6772616d61206d61696c2e706466, NULL, 'ARICA', '15'),
(5, 3, 3, 'Proyecto Administración', 'Administración', NULL, 0x6172636869766f2d312e706466, 0x6172636869766f2d312e706466, 'ARICA', '15'),
(6, 1, 1, 'Proyectos Plazas', 'Plazas', NULL, 0x6172636869766f2d312e706466, 0x6172636869766f2d312e706466, 'ARICA', '15'),
(7, 1, 1, 'Proyecto de Declaratorias', 'Declaratoría', NULL, 0x6172636869766f2d312e706466, 0x6172636869766f2d312e706466, 'ARICA', '15'),
(8, 1, 1, 'Diseño Iglesia de Pachama', 'Diseño Pachama', NULL, 0x6172636869766f2d312e706466, 0x6172636869766f2d312e706466, 'ARICA', '15'),
(9, 15, 4, 'Informe e investigación Histórica Estética para el Santuario de la Virgen de la Candelaria de Copiapó', 'Candelaria Copiapó', 0x50726f796563746f206465206c6576616e74616d69656e746f2068697374c3b37269636f2c2063756c747572616c207920617271756974656374c3b36e69636f2064656c2053616e74756172696f206465206c612043616e64656c6172696120646520436f70696170c3b32e, NULL, NULL, 'ARICA', '15'),
(10, 22, 4, 'INFORME E INVESTIGACIÓN HISTÓRICA ESTÉTICA PARA EL SANTUARIO DE LA VIRGEN DE LA CANDELARIA DE COPIAPO', 'Candelaria Copiapó', 0x50726f796563746f206465206c6576616e74616d69656e746f2068697374c3b37269636f2c2063756c747572616c207920617271756974656374c3b36e69636f2064656c2053616e74756172696f206465206c612043616e64656c6172696120646520436f70696170c3b32e, NULL, NULL, 'ARICA', '15'),
(11, 15, 4, 'Libro Las Pinturas Murales de Parinacota en el último bofedal de la ruta de la plata', 'Libro Parinacota', 0x4c6962726f20646520666f746f67726166c3ad61207920746578746f2064652070696e7475726173206d7572616c657320646520506172696e61636f7461, NULL, NULL, 'ARICA', '15'),
(12, 2, 5, 'disponible', 'disponible 1', 0x50726f6772616d612064652063617061636974616369c3b36e2079206173697374656e6369612074c3a9636e696361207061726120636170616369746163696f6e657320792064697365c3b16f20656e20656c20616d6269746f2064652054757269736d6f, NULL, NULL, 'ARICA', '15'),
(13, 2, 5, 'Modelo de Gestión del Circuito de Turismo Patrimonial Ruta de las Misiones', 'PDT Ruta de las Misiones', 0x50445420436f72666f2064652052757461206465206c6173204d6973696f6e6573, NULL, NULL, 'ARICA', '15'),
(14, 2, 5, 'Venta Ruta de las Misiones', 'Venta Ruta', 0x536572766963696f7320646520677569617320706572736f6e616c697a61646f20706f7220656c20636972637569746f2052757461206465206c6173204d6973696f6e6573, NULL, NULL, 'ARICA', '15'),
(15, 2, 5, 'Programa de Emprendimiento Local 1 y 2', 'PEL Camarones', 0x2e2e2e2e2e, 0x70726f796563746f2070656c2063616d61726f6e65732e706466, 0x436f6e747261746f20436f6e73756c746f72612046617365204465736172726f6c6c6f5f70617472696d6f6e69616c20312e706466, 'ARICA', '15'),
(16, 21, 2, 'PROYECTO DISPONIBLE', 'PROY DISPONIBLE', 0x526567697374726f206465206c612020637561727461207665727369c3b36e2064656c204172696361207920506172696e6163746f61204a617a7a20466573746976616c, NULL, NULL, 'ARICA', '15'),
(17, 21, 2, 'Registro IV Festival de Jazz', 'IV Arica y Parinacota Jazz Festival', 0x526567697374726f206465206c612020637561727461207665727369c3b36e2064656c204172696361207920506172696e6163746f61204a617a7a20466573746976616c, NULL, NULL, 'ARICA', '15'),
(18, 22, 4, 'Candelaria Copiapo', NULL, NULL, NULL, NULL, 'ARICA', '3'),
(19, 1, 1, 'DECLARATORIA M.N IGLESIA DE AIRO', 'DECLARATORIA M.N IGLESIA DE AIRO', NULL, NULL, NULL, 'ARICA', '15'),
(20, 1, 1, 'DECLARATORIA M.N IGLESIA DE AZAPA', 'DECLARATORIA M.N IGLESIA DE AZAPA', NULL, NULL, NULL, 'ARICA', '15'),
(21, 1, 1, 'DECLARATORIA M.N IGLESIA DE CAQUENA', 'DECLARATORIA M.N IGLESIA DE CAQUENA', NULL, NULL, NULL, 'ARICA', '15'),
(22, 1, 1, 'DECLARATORIA M.N IGLESIA DE CHAPOCO', 'DECLARATORIA M.N IGLESIA DE CHAPOCO', NULL, NULL, NULL, 'ARICA', '15'),
(23, 1, 1, 'DECLARATORIA M.N IGLESIA DE CHITITA', 'DECLARATORIA M.N IGLESIA DE CHITITA', NULL, NULL, NULL, 'ARICA', '15'),
(24, 1, 1, 'DECLARATORIA M.N IGLESIA DE CHOQUELIMPIE', 'DECLARATORIA M.N IGLESIA DE CHOQUELIMPIE', NULL, NULL, NULL, 'ARICA', '15'),
(25, 1, 1, 'DECLARATORIA M.N IGLESIA DE CODPA', 'DECLARATORIA M.N IGLESIA DE CODPA', NULL, NULL, NULL, 'ARICA', '15'),
(26, 1, 1, 'DECLARATORIA M.N IGLESIA DE COSAPILLA', 'DECLARATORIA M.N IGLESIA DE COSAPILLA', NULL, NULL, NULL, 'ARICA', '15'),
(27, 1, 1, 'DECLARATORIA M.N IGLESIA DE ESQUIÑA', 'DECLARATORIA M.N IGLESIA DE ESQUIÑA', NULL, NULL, NULL, 'ARICA', '15'),
(28, 1, 1, 'DECLARATORIA M.N IGLESIA DE GUACOLLO', 'DECLARATORIA M.N IGLESIA DE GUACOLLO', NULL, NULL, NULL, 'ARICA', '15'),
(29, 1, 1, 'DECLARATORIA M.N IGLESIA DE PACHICA', 'DECLARATORIA M.N IGLESIA DE PACHICA', NULL, NULL, NULL, 'ARICA', '15'),
(30, 1, 1, 'DECLARATORIA M.N IGLESIA DE POCONCHILE', 'DECLARATORIA M.N IGLESIA DE POCONCHILE', NULL, NULL, NULL, 'ARICA', '15'),
(31, 1, 1, 'DECLARATORIA M.N IGLESIA DE PUTANI', 'DECLARATORIA M.N IGLESIA DE PUTANI', NULL, NULL, NULL, 'ARICA', '15'),
(32, 1, 1, 'DECLARATORIA M.N IGLESIA DE PUTRE', 'DECLARATORIA M.N IGLESIA DE PUTRE', NULL, NULL, NULL, 'ARICA', '15'),
(33, 1, 1, 'DECLARATORIA M.N IGLESIA DE SUCUNA', 'DECLARATORIA M.N IGLESIA DE SUCUNA', NULL, NULL, NULL, 'ARICA', '15'),
(34, 1, 1, 'DECLARATORIA M.N IGLESIA DE TACORA', 'DECLARATORIA M.N IGLESIA DE TACORA', NULL, NULL, NULL, 'ARICA', '15'),
(35, 1, 1, 'DECLARATORIA M.N IGLESIA DE TIMAR', 'DECLARATORIA M.N IGLESIA DE TIMAR', NULL, NULL, NULL, 'ARICA', '15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `region`
--

CREATE TABLE IF NOT EXISTS `region` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Volcado de datos para la tabla `region`
--

INSERT INTO `region` (`id`, `nombre`) VALUES
(1, 'Tarapaca'),
(2, 'Antofagasta'),
(3, 'Atacama'),
(4, 'Coquimbo'),
(5, 'Valparaiso'),
(6, 'del Libertador Gral. Bernardo Ohiggins'),
(7, 'del Maule'),
(8, 'del BioBio'),
(9, 'de la Araucania'),
(10, 'de los Lagos'),
(11, 'Aysén del Gral. Carlos Ibáñez del Campo'),
(12, 'Magallanes'),
(13, 'Metropolitana'),
(14, 'Los Ríos'),
(15, 'Arica y Parinacota');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rendicion`
--

CREATE TABLE IF NOT EXISTS `rendicion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `emision_id` int(11) NOT NULL,
  `cuenta_id` int(11) NOT NULL,
  `asignacion` varchar(100) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `comuna` varchar(100) DEFAULT NULL,
  `region` varchar(100) DEFAULT NULL,
  `tipo_solicitud` varchar(45) DEFAULT NULL,
  `numero_cheque` varchar(45) DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL,
  `total_entrada` int(11) DEFAULT NULL,
  `total_salida` int(11) NOT NULL DEFAULT '0',
  `total_saldo` int(11) NOT NULL DEFAULT '0',
  `observacion` blob,
  PRIMARY KEY (`id`),
  KEY `fk_rendicion_emision1_idx` (`emision_id`),
  KEY `fk_rendicion_user1_idx` (`user_id`),
  KEY `fk_rendicion_cuenta1` (`cuenta_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=53 ;

--
-- Volcado de datos para la tabla `rendicion`
--

INSERT INTO `rendicion` (`id`, `user_id`, `emision_id`, `cuenta_id`, `asignacion`, `fecha`, `comuna`, `region`, `tipo_solicitud`, `numero_cheque`, `estado`, `total_entrada`, `total_salida`, `total_saldo`, `observacion`) VALUES
(11, 3, 26, 2, 'Directa', '2013-05-13', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7866739', 'FINALIZADA', 35000, 35000, 0, NULL),
(12, 3, 27, 1, 'Directa', '2013-04-30', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7912907', 'FINALIZADA', 720000, 720000, 0, NULL),
(13, 3, 29, 2, 'Directa', '2013-05-22', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7886995', 'FINALIZADA', 89250, 89250, 0, 0x5061676f20436f636b74656c20555354),
(14, 3, 30, 2, 'Directa', '2013-05-22', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7886994', 'FINALIZADA', 400000, 400000, 0, NULL),
(15, 3, 33, 2, 'Directa', '2013-04-30', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7866736', 'ANULADA', 30000, 0, 30000, NULL),
(16, 3, 34, 2, 'Directa', '2013-04-30', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7866736', 'FINALIZADA', 45000, 45000, 0, NULL),
(17, 3, 35, 2, 'Directa', '2013-04-30', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7866737', 'FINALIZADA', 45000, 45000, 0, NULL),
(18, 3, 38, 2, 'Directa', '2013-04-17', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7866760', 'FINALIZADA', 193565, 193565, 0, NULL),
(19, 3, 39, 2, 'Directa', '2013-05-15', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7866761', 'FINALIZADA', 52831, 52831, 0, NULL),
(20, 3, 40, 2, 'Directa', '2013-05-16', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7866763', 'FINALIZADA', 15000, 15000, 0, NULL),
(21, 3, 41, 2, 'Directa', '2013-05-23', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7866755', 'FINALIZADA', 300000, 300000, 0, NULL),
(22, 3, 44, 1, 'Directa', '2013-05-15', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7912901', 'FINALIZADA', 25000, 25000, 0, NULL),
(23, 3, 45, 1, 'Directa', '2013-05-01', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7912902', 'FINALIZADA', 50000, 50000, 0, NULL),
(24, 3, 46, 1, 'Directa', '2013-05-30', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7912909', 'FINALIZADA', 600000, 600000, 0, NULL),
(25, 3, 47, 2, 'Directa', '2013-05-02', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7866746', 'FINALIZADA', 13395, 13395, 0, NULL),
(26, 3, 48, 2, 'Directa', '2013-05-03', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7866748', 'FINALIZADA', 20000, 20000, 0, NULL),
(27, 3, 49, 2, 'Directa', '2013-05-06', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7866749', 'FINALIZADA', 40000, 40000, 0, NULL),
(28, 3, 50, 2, 'Directa', '2013-05-07', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7866750', 'FINALIZADA', 40000, 40000, 0, NULL),
(29, 3, 51, 2, 'Directa', '2013-05-22', 'ARICA', 'Región de Arica y Parinacota', 'Transferencia', 'Transferencia', 'FINALIZADA', 1240000, 1240000, 0, 0x5061676f2032c2ba2043756f746120416d706c69666963616369c3b36e20456e6375656e74726f20426172726f636f),
(30, 3, 52, 2, 'Directa', '2013-05-22', 'ARICA', 'Región de Arica y Parinacota', 'Transferencia', 'Transferencia', 'FINALIZADA', 1550000, 1550000, 0, NULL),
(31, 3, 53, 2, 'Directa', '2013-05-23', 'ARICA', 'Región de Arica y Parinacota', 'Transferencia', 'Transferencia', 'FINALIZADA', 821100, 821100, 0, NULL),
(32, 3, 54, 2, 'Directa', '2013-05-22', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7887003', 'FINALIZADA', 37000, 37000, 0, NULL),
(33, 3, 55, 2, 'Directa', '2013-05-18', 'ARICA', 'Región de Arica y Parinacota', 'Transferencia', 'Transferencia', 'FINALIZADA', 130900, 130900, 0, NULL),
(34, 3, 56, 1, 'Directa', '2013-04-30', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7912906', 'FINALIZADA', 4000, 4000, 0, NULL),
(35, 3, 57, 2, 'Directa', '2013-05-05', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7866738', 'FINALIZADA', 1039500, 1039500, 0, NULL),
(36, 3, 28, 2, 'Directa', '2013-05-30', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7886996', 'FINALIZADA', 200000, 200000, 0, NULL),
(37, 3, 36, 2, 'Directa', '2013-05-16', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7886997', 'FINALIZADA', 20000, 20000, 0, NULL),
(38, 3, 31, 2, 'Directa', '2013-06-03', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7866767', 'FINALIZADA', 70500, 70500, 0, NULL),
(39, 3, 37, 2, 'Directa', '2013-06-03', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7866766', 'FINALIZADA', 40000, 40000, 0, NULL),
(40, 14, 43, 2, 'Directa', '2013-06-13', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7887001', 'FINALIZADA', 100000, 100000, 0, 0x52656e646963696f6e206d6164657261207365c3b1616c657469636120736f636f726f6d61),
(41, 3, 33, 2, 'Directa', '2013-06-19', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7866736', 'ACTIVA', 30000, 0, 30000, NULL),
(42, 3, 58, 2, 'Directa', '2013-06-19', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7887009', 'FINALIZADA', 144000, 144000, 0, 0x50616761646f20656e20456665637469766f),
(43, 3, 63, 2, 'Directa', '2013-06-19', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7887069', 'FINALIZADA', 22000, 22000, 0, NULL),
(44, 3, 66, 2, 'Directa', '2013-06-19', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7887079', 'FINALIZADA', 47390, 47390, 0, NULL),
(45, 14, 67, 2, 'Directa', '2013-06-21', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7887078', 'FINALIZADA', 65000, 65000, 0, 0x52656e646963696f6e2050456c2043616d61726f6e6573),
(46, 3, 68, 2, 'Directa', '2013-06-25', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7887087', 'FINALIZADA', 50000, 50000, 0, NULL),
(47, 3, 65, 2, 'Directa', '2013-06-25', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7887076', 'FINALIZADA', 84272, 84272, 0, NULL),
(48, 18, 71, 1, 'Directa', '2013-06-27', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7912918', 'FINALIZADA', 50000, 50000, 0, NULL),
(49, 18, 73, 2, 'Directa', '2013-06-28', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7942568', 'FINALIZADA', 46000, 46000, 0, NULL),
(50, 3, 72, 1, 'Directa', '2013-07-01', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7912921', 'FINALIZADA', 32000, 32000, 0, NULL),
(51, 3, 64, 2, 'Directa', '2013-06-24', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7887077', 'FINALIZADA', 100000, 100000, 0, NULL),
(52, 14, 61, 2, 'Directa', '2013-07-03', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '7887065', 'FINALIZADA', 41500, 41500, 0, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rendicion_directa`
--

CREATE TABLE IF NOT EXISTS `rendicion_directa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cuenta_id` int(11) NOT NULL,
  `tipo_solicitud` varchar(45) DEFAULT NULL,
  `tipo_cheque` varchar(45) DEFAULT NULL,
  `numero_cheque` varchar(45) DEFAULT NULL,
  `estado` varchar(45) DEFAULT 'ACTIVA',
  PRIMARY KEY (`id`),
  KEY `fk_rendicion_directa_cuenta` (`cuenta_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `rendicion_directa`
--

INSERT INTO `rendicion_directa` (`id`, `cuenta_id`, `tipo_solicitud`, `tipo_cheque`, `numero_cheque`, `estado`) VALUES
(1, 2, 'Cheque', 'Cruzado', '7887005', 'FINALIZADA'),
(2, 2, 'Cheque', 'Nominativo', '7887006', 'FINALIZADA'),
(3, 2, 'Cheque', 'Nominativo', '7866768', 'FINALIZADA'),
(4, 1, 'Cheque', 'Cruzado', NULL, 'ANULADA'),
(5, 1, 'Cheque', 'Cruzado', NULL, 'ACTIVA'),
(6, 2, 'Cheque', 'Nominativo', '7866738', 'ACTIVA'),
(7, 1, 'Cheque', 'Nominativo', NULL, 'ANULADA'),
(8, 1, 'Cheque', 'Nominativo', '7912906', 'ACTIVA'),
(9, 2, 'Transferencia', 'Transferencia', NULL, 'ANULADA'),
(10, 2, 'Cheque', 'Nominativo', '7887034', 'FINALIZADA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rights`
--

CREATE TABLE IF NOT EXISTS `rights` (
  `itemname` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  PRIMARY KEY (`itemname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rights`
--

INSERT INTO `rights` (`itemname`, `type`, `weight`) VALUES
('admin', 2, 0),
('Director Ejecutivo', 2, 1),
('Jefe de Taller', 2, 2),
('Usuario Comun', 2, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rrhh`
--

CREATE TABLE IF NOT EXISTS `rrhh` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rut` varchar(13) DEFAULT NULL,
  `nombres` varchar(255) DEFAULT NULL,
  `apellidos` varchar(255) DEFAULT NULL,
  `documento` varchar(45) DEFAULT NULL,
  `nacionalidad` varchar(45) DEFAULT NULL,
  `sexo` varchar(15) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `estado_civil` varchar(45) DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `telefono` varchar(50) DEFAULT NULL,
  `celular` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `ciudad` varchar(45) DEFAULT NULL,
  `salud` varchar(45) DEFAULT NULL,
  `afp_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_rrhh_afp1_idx` (`afp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Volcado de datos para la tabla `rrhh`
--

INSERT INTO `rrhh` (`id`, `rut`, `nombres`, `apellidos`, `documento`, `nacionalidad`, `sexo`, `fecha_nacimiento`, `estado_civil`, `direccion`, `telefono`, `celular`, `email`, `ciudad`, `salud`, `afp_id`) VALUES
(1, '12.023.864-7', 'Ronald Andrés', 'Caicedo Garay', 'Carnet', 'Chilena', 'Masculino', '1969-12-31', 'Casado(a)', 'Santiago Bennit Chavez 581, Villa Saucache', '058-2242049', '77662399', 'caicedoarquitecto@gmail.com ', 'Arica', 'Isapre', NULL),
(2, '14.105.542-9', 'Freddy Bernardo', 'Alvarez Mamani', 'Carnet', 'Chilena', 'Masculino', '1981-05-20', 'Soltero(a)', 'Los Olmos 2689', '058-2-253616', '68340992', 'falvarez_m@hotmail.com', 'Arica', 'Fonasa', 3),
(3, '15692760-0', 'Irma del Carmen', 'Magnan Contreras', 'Carnet', 'Chilena', 'Femenino', '1983-07-20', 'Soltero(a)', 'Andres Bello 1515', '58-2-253616', '77661200', 'i.magnan.contreras@gmail.com', 'Arica', 'Fonasa', 1),
(5, '13862371-8', 'Álvaro Emanuel', 'Rivera Ramirez', 'Carnet', 'Chilena', 'Masculino', '1969-12-31', 'Soltero(a)', NULL, NULL, '56238693', 'ae.riveraramirez@gmail.com', 'Arica', NULL, 4),
(6, '13.639.240-9', 'Andrea Alejandra', 'Heredia Palacios', 'Carnet', 'Chilena', 'Femenino', '1969-12-31', 'Soltero(a)', NULL, NULL, '87254689', 'topografo.palacios@gmail.com', 'Arica', 'Fonasa', 1),
(7, '16.666.547-7', 'Benjamin Eduardo', 'Garcia Gallardo', 'Carnet', 'Chilena', 'Masculino', '1969-12-31', 'Soltero(a)', NULL, NULL, '92116796', 'benja.edu@gmail.com', 'Arica', 'Fonasa', 3),
(8, '15.000.952-9', 'Nicolas Victor ', 'Miño Zapata', 'Carnet', 'Chilena', 'Masculino', '1969-12-31', 'Soltero(a)', NULL, '058-317176', '87852006', 'nicolasmino@yahoo.com', 'Arica', 'Fonasa', 4),
(9, '16.466.739-1', 'Daniela', 'Zegarra Bori', 'Carnet', 'Chilena', 'Femenino', '1969-12-31', 'Soltero(a)', NULL, NULL, '68420109', 'boridaniela@gmail.com', 'Arica', NULL, NULL),
(10, '18.615.910-1', 'Francisco Javier', 'Tarque Cañipa', 'Carnet', 'Chilena', 'Masculino', '1969-12-31', 'Soltero(a)', NULL, '058-2226801', '90363201', 'franciscotarque@gmail.com', 'Arica', 'Fonasa', 3),
(11, '15.000.951-0', 'Sebastian Humberto', 'Miño Zapata', 'Carnet', 'Chilena', 'Masculino', '1969-12-31', 'Soltero(a)', NULL, '058-2317176', '85192318', 'correos.sm@gmail.com', 'Arica', 'Fonasa', 4),
(12, '14.107.505-5', 'Miguel Angel', 'Rojas Henriquez', 'Carnet', 'Chilena', 'Masculino', '1969-12-31', 'Soltero(a)', NULL, NULL, '93418509', 'miguel.rojas.henriquez@gmail.com', 'Arica', 'Fonasa', 1),
(13, '12.584.456-1', 'Cristian José', 'Heinsen Planella', 'Carnet', 'Chilena', 'Masculino', '1969-12-31', 'Casado(a)', 'Andrés Bello N° 1854', '058-2319573', '77658693', 'cristianheinsen@gmail.com', 'Arica', NULL, NULL),
(14, '21.431.230-1', 'Matilde', 'Cruz Tupa', 'Carnet', 'chilena', 'Femenino', '1969-12-31', 'Casado(a)', NULL, NULL, '87311882', 'matilda.cruz7@gmail.com', 'Arica', 'Fonasa', 4),
(15, '16.153.864-7', 'María Javiera ', 'Maino Gónzalez', 'Carnet', 'Chilena', 'Femenino', '1969-12-31', 'Soltero(a)', 'Las Codornices 1561', NULL, '88913712', 'javieramaino@gmail.com', 'Arica', NULL, NULL),
(16, '15.026.747-1', 'Yocely', 'Pinilla Alaniz', 'Carnet', 'Chilena', 'Masculino', NULL, 'Casado(a)', 'Santiago Chávez 581,', NULL, '89025723', 'pinilla.alaniz@gmail.com', 'Arica', NULL, NULL),
(17, '13.637.454-0', 'Rafaela Regina', 'Rabello Condori', 'Carnet', 'Chilena', 'Femenino', '1969-12-31', 'Soltero(a)', NULL, NULL, '89865989', 'seemy_8@hotmail.com', 'Arica', NULL, NULL),
(18, '16770824-2', 'Israel Antonio', 'Quispe Lazaro', 'Carnet', 'Chilena', 'Masculino', '1969-12-31', 'Soltero(a)', 'Las Maitas 2056', '058-2242153', '16770824-2', 'quispearq@gmail.com', 'Arica', 'Fonasa', NULL),
(19, '13255357-2', 'Magdalena', 'Pereira Campos', 'Carnet', 'Chilena', 'Femenino', '1969-12-31', 'Casado(a)', 'Andrés Bello 1854', '058-2319573', '77519298', 'pereiramagdalena@gmail.com', 'Arica', NULL, NULL),
(20, '9053277-4', 'Carlos Alberto', 'Nuñez Soza', 'Carnet', 'Chilena', 'Masculino', '1969-12-31', 'Soltero(a)', 'Agustin Edwards 2238', NULL, '94041970', 'carlosvisviri@gmail.com', 'Arica', 'Fonasa', 1),
(21, '16.225.568-1', 'Crsitian Antonio', 'Brandau Mardonez', 'Carnet', 'Chilena', 'Masculino', '1969-12-31', 'Soltero(a)', 'Los Artesanos  819', NULL, '82274316', 'cbrandau@live.cl', 'Arica', 'Isapre', 3),
(22, '14.174.422-4', 'Alejandra Valeska', 'Vargas Sepúlveda', 'Carnet', 'chilena', 'Femenino', '1969-12-31', 'Soltero(a)', 'Diego de Almagro 1913', NULL, '92063738', 'alejandravargas.uchile@gmail.com', 'Arica', NULL, NULL),
(23, '23400121-3', 'Fanny', 'Goupil', 'Carnet', 'Francesa', 'Femenino', '1969-12-31', 'Soltero(a)', 'Pablo Picasso 2081', NULL, NULL, 'fanny_gl@hotmail.fr', 'Arica', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitud`
--

CREATE TABLE IF NOT EXISTS `solicitud` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proyecto_id` int(11) NOT NULL,
  `proveedor_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `tipo_solicitud` varchar(50) NOT NULL,
  `total` int(11) NOT NULL DEFAULT '0',
  `estado` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_solicitud_proyecto1_idx` (`proyecto_id`),
  KEY `fk_solicitud_proveedor1_idx` (`proveedor_id`),
  KEY `fk_solicitud_user1_idx` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=108 ;

--
-- Volcado de datos para la tabla `solicitud`
--

INSERT INTO `solicitud` (`id`, `proyecto_id`, `proveedor_id`, `user_id`, `fecha`, `tipo_solicitud`, `total`, `estado`) VALUES
(47, 5, 51, 14, '2013-05-13', 'Cheque', 72000, 'EMITIDA'),
(48, 1, 14, 3, '2013-05-13', 'Cheque', 35000, 'EMITIDA'),
(49, 1, 36, 3, '2013-05-13', 'Cheque', 720000, 'EMITIDA'),
(50, 1, 2, 8, '2013-05-13', 'Cheque', 40000, 'EMITIDA'),
(51, 1, 10, 8, '2013-05-13', 'Cheque', 70500, 'EMITIDA'),
(52, 4, 29, 3, '2013-05-14', 'Cheque', 400000, 'EMITIDA'),
(53, 4, 54, 3, '2013-05-15', 'Cheque', 89250, 'EMITIDA'),
(54, 4, 16, 3, '2013-05-15', 'Cheque', 200000, 'EMITIDA'),
(55, 1, 2, 8, '2013-05-16', 'Cheque', 20000, 'EMITIDA'),
(56, 1, 2, 3, '2013-05-30', 'Cheque', 45000, 'EMITIDA'),
(57, 1, 2, 3, '2013-04-30', 'Cheque', 45000, 'EMITIDA'),
(58, 1, 55, 3, '2013-05-14', 'Cheque', 193565, 'EMITIDA'),
(59, 1, 56, 3, '2013-05-15', 'Cheque', 52831, 'EMITIDA'),
(60, 5, 54, 3, '2013-05-09', 'Cheque', 15000, 'EMITIDA'),
(61, 1, 7, 3, '2013-05-14', 'Cheque', 0, 'ANULADA'),
(62, 1, 14, 9, '2013-05-22', 'Cheque', 37000, 'ANULADA'),
(63, 5, 60, 14, '2013-05-22', 'Cheque', 100000, 'EMITIDA'),
(64, 4, 29, 3, '2013-05-08', 'Cheque', 300000, 'EMITIDA'),
(65, 5, 7, 3, '2013-05-14', 'Cheque', 25000, 'EMITIDA'),
(66, 5, 98, 3, '2013-05-01', 'Cheque', 50000, 'EMITIDA'),
(67, 1, 12, 3, '2013-05-30', 'Cheque', 600000, 'EMITIDA'),
(68, 1, 14, 3, '2013-05-02', 'Cheque', 13395, 'EMITIDA'),
(69, 1, 2, 3, '2013-05-03', 'Cheque', 20000, 'EMITIDA'),
(70, 1, 2, 3, '2013-05-06', 'Cheque', 40000, 'EMITIDA'),
(71, 1, 2, 3, '2013-05-07', 'Cheque', 40000, 'EMITIDA'),
(72, 4, 101, 3, '2013-05-22', 'Cheque', 1240000, 'EMITIDA'),
(73, 4, 101, 3, '2013-05-22', 'Cheque', 1550000, 'EMITIDA'),
(74, 1, 102, 3, '2013-05-23', 'Cheque', 821100, 'EMITIDA'),
(75, 1, 103, 3, '2013-05-22', 'Cheque', 37000, 'ANULADA'),
(76, 4, 13, 3, '2013-05-18', 'Cheque', 130900, 'EMITIDA'),
(77, 1, 1, 3, '2013-04-30', 'Cheque', 4000, 'EMITIDA'),
(78, 1, 53, 3, '2013-05-05', 'Cheque', 1039500, 'EMITIDA'),
(79, 1, 10, 8, '2013-06-03', 'Cheque', 240000, 'EMITIDA'),
(80, 1, 14, 9, '2013-06-04', 'Cheque', 144000, 'EMITIDA'),
(81, 1, 14, 9, '2013-06-05', 'Cheque', 35000, 'EMITIDA'),
(82, 15, 60, 3, '2013-06-11', 'Cheque', 41500, 'EMITIDA'),
(83, 1, 10, 8, '2013-06-14', 'Cheque', 203300, 'EMITIDA'),
(84, 1, 2, 9, '2013-06-14', 'Cheque', 22000, 'EMITIDA'),
(85, 15, 60, 14, '2013-06-17', 'Cheque', 65000, 'EMITIDA'),
(86, 13, 29, 14, '2013-06-17', 'Cheque', 100000, 'EMITIDA'),
(87, 17, 16, 16, '2013-06-17', 'Transferencia', 47390, 'EMITIDA'),
(88, 1, 43, 8, '2013-06-17', 'Cheque', 84272, 'EMITIDA'),
(89, 1, 2, 9, '2013-06-21', 'Cheque', 50000, 'EMITIDA'),
(90, 5, 14, 9, '2013-06-24', 'Cheque', 23000, 'EMITIDA'),
(91, 9, 134, 23, '2013-06-24', 'Cheque', 80000, 'ANULADA'),
(92, 5, 1, 9, '2013-06-25', 'Cheque', 10000, 'EMITIDA'),
(93, 5, 15, 18, '2013-06-25', 'Cheque', 50000, 'EMITIDA'),
(94, 5, 23, 3, '2013-06-25', 'Cheque', 32000, 'EMITIDA'),
(95, 5, 15, 18, '2013-06-26', 'Cheque', 0, 'ANULADA'),
(96, 5, 15, 18, '2013-06-26', 'Cheque', 0, 'ANULADA'),
(97, 1, 1, 18, '2013-06-26', 'Cheque', 46000, 'ANULADA'),
(98, 5, 15, 18, '2013-06-26', 'Cheque', 46000, 'EMITIDA'),
(99, 9, 144, 23, '2013-06-27', 'Transferencia', 0, 'ANULADA'),
(100, 9, 144, 23, '2013-06-27', 'Transferencia', 60000, 'FINALIZADA'),
(101, 1, 17, 22, '2013-06-28', 'Cheque', 60000, 'EMITIDA'),
(102, 1, 23, 10, '2013-06-28', 'Cheque', 35800, 'FINALIZADA'),
(103, 15, 60, 14, '2013-06-28', 'Cheque', 75800, 'EMITIDA'),
(104, 9, 144, 23, '2013-07-01', 'Transferencia', 380000, 'FINALIZADA'),
(105, 7, 14, 16, NULL, 'Cheque', 35000, 'EMITIDA'),
(106, 5, 156, 18, '2013-07-03', 'Cheque', 50000, 'EMITIDA'),
(107, 15, 159, 3, '2013-07-04', 'Cheque', 70000, 'ANULADA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_contrato`
--

CREATE TABLE IF NOT EXISTS `tipo_contrato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(45) NOT NULL,
  `caracteristicas` blob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `tipo_contrato`
--

INSERT INTO `tipo_contrato` (`id`, `tipo`, `caracteristicas`) VALUES
(1, 'Honorario', NULL),
(2, 'Por Obra', NULL),
(3, 'Indefinido', 0x436f6e747261746f206465207469656d706f20696e646566696e69646f),
(4, 'Plazo Fijo', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_ingreso`
--

CREATE TABLE IF NOT EXISTS `tipo_ingreso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `tipo_ingreso`
--

INSERT INTO `tipo_ingreso` (`id`, `nombre`) VALUES
(1, 'Convenio'),
(2, 'Donaciones'),
(3, 'Venta de Servicios'),
(4, 'Otros'),
(5, 'D.L. 889');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) DEFAULT NULL,
  `tipo` varchar(45) DEFAULT NULL,
  `rrhh_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_rrhh1_idx` (`rrhh_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `email`, `tipo`, `rrhh_id`) VALUES
(1, 'admin', '3039aba1e1c33b68282a56b8afa047bc8fcd0154', NULL, 'admin', NULL),
(2, 'cheinsen', 'eb39dfc1b8c562544b2cfbc9e6af327074112e59', NULL, 'admin', 13),
(3, 'imagnan', 'ae5ceec654fd01f675e1e8fbb3632e7a8a54f81d', NULL, 'admin', 3),
(4, 'rrabello', 'a2d845a66ed199346cb483a3463648864206fa22', NULL, 'admin', 17),
(5, 'rcaicedo', '91c84a9a609c38cf41d7430919a1728a1584690f', NULL, 'Jefe de Taller', 1),
(6, 'jedwards', '849edd3841a00276e1da66fde86d0e66fc6183bc', NULL, 'Usuario Comun', NULL),
(7, 'aheredia', 'e57ec27e9f3f4c9f990683db11227077e3434dc2', NULL, 'Usuario Comun', 6),
(8, 'bgarcia', 'c42168455082fbdecd34c492444bdc2f2ed4f411', NULL, 'Usuario Comun', 7),
(9, 'mrojas', '3a5f7eb95d80108be0c7eab9946955903a883ca2', NULL, 'Usuario Comun', 12),
(10, 'nmiño', '4328714fe40bc2cb2bb3693e45462e3b83353d1d', NULL, 'Usuario Comun', 8),
(11, 'mpereira', '312e6381d13a31d8318451d84674e8dc84b69715', NULL, 'Jefe de Taller', 19),
(12, 'mmaino', '1d5eb5a87b0ab407fae6ed1d7522e870ab5f1908', NULL, 'Jefe de Taller', 15),
(13, 'arivera', '52846307f8d9a30d475a3a51f53e84de52e22c2f', NULL, 'Usuario Comun', 5),
(14, 'falvarez', '2ebaa03abaf27df75394ad36e44db225cee80bbe', NULL, 'Jefe de Taller', 2),
(15, 'smiño', '5eb73b57e50421c0227f9aefda967d74762433dd', NULL, 'Usuario Comun', 11),
(16, 'cbrandau', '756c2b92537a543d55a4d2568928cff16a842426', NULL, 'Jefe de Taller', 21),
(17, 'dzegarra', '4029ddef689a6c4fe62d8cadab21c2e58c7afe59', NULL, 'Usuario Comun', 9),
(18, 'iquispe', 'd89131353756c9c7928d83df73cb0b75942928ec', NULL, 'Usuario Comun', 18),
(19, 'aguillen', '41c8e995c265d8ff4e94f841fb063db293378097', NULL, 'Usuario Comun', NULL),
(20, 'frivera', '601c690fa5f3a2bca63e3d470c45b481a197642c', NULL, 'Jefe de Taller', NULL),
(21, 'ftarque', '6a27b8e8a0171f20a4c7371a6a38ada3ba95dcb7', NULL, 'Usuario Comun', 10),
(22, 'ypinilla', '44576ca64c02a8da5a311392850067616e64bfa6', NULL, 'Usuario Comun', 16),
(23, 'avargas', 'c030f9cd8e39ccafb1666d1fcaef933609db2926', NULL, 'Usuario Comun', 22),
(24, 'cwagner', '48863b82290bd98529f617a918daa2ed3cd0a3cb', NULL, 'Usuario Comun', NULL),
(25, 'fgoupil', '548d72b6929ae2a94577a6bcc95acb04092a05dd', 'fanny_gl@hotmail.fr', 'Usuario Comun', 23);

-- --------------------------------------------------------

--
-- Estructura para la vista `a_egresos_reales_sr`
--
DROP TABLE IF EXISTS `a_egresos_reales_sr`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `a_egresos_reales_sr` AS select 'No Directa' AS `rendicion`,`item_rendicion`.`id` AS `item_rendicion_id`,NULL AS `rendicion_directa_id`,NULL AS `item_rendicion_directa_id`,`item_rendicion`.`proveedor_id` AS `proveedor_id`,`item_rendicion`.`cuenta_contable_id` AS `cuenta_contable_id`,`item_rendicion`.`cuenta_especifica_id` AS `cuenta_especifica_id`,`item_rendicion`.`proyecto_id` AS `proyecto_id`,`item_rendicion`.`cuenta_id` AS `cuenta_id`,`item_rendicion`.`tipo_documento` AS `tipo_documento`,`item_rendicion`.`numero_documento` AS `numero_documento`,`item_rendicion`.`tipo_solicitud` AS `tipo_solicitud`,`item_rendicion`.`tipo_cheque` AS `tipo_cheque`,`item_rendicion`.`numero_cheque` AS `numero_cheque`,`item_rendicion`.`fecha` AS `fecha`,`item_rendicion`.`mes` AS `mes`,`item_rendicion`.`agno` AS `agno`,`item_rendicion`.`monto` AS `monto`,NULL AS `rendicion_directa.estado` from `item_rendicion` union all select 'Directa' AS `Rendicion`,NULL AS `item_rendicion.id`,`rendicion_directa`.`id` AS `id`,`item_rendicion_directa`.`id` AS `id`,`item_rendicion_directa`.`proveedor_id` AS `proveedor_id`,`item_rendicion_directa`.`cuenta_contable_id` AS `cuenta_contable_id`,`item_rendicion_directa`.`cuenta_especifica_id` AS `cuenta_especifica_id`,`item_rendicion_directa`.`proyecto_id` AS `proyecto_id`,`rendicion_directa`.`cuenta_id` AS `cuenta_id`,`item_rendicion_directa`.`tipo_documento` AS `tipo_documento`,`item_rendicion_directa`.`numero_documento` AS `numero_documento`,`rendicion_directa`.`tipo_solicitud` AS `tipo_solicitud`,`rendicion_directa`.`tipo_cheque` AS `tipo_cheque`,`rendicion_directa`.`numero_cheque` AS `numero_cheque`,`item_rendicion_directa`.`fecha` AS `fecha`,`item_rendicion_directa`.`mes` AS `mes`,`item_rendicion_directa`.`agno` AS `agno`,`item_rendicion_directa`.`monto` AS `monto`,`rendicion_directa`.`estado` AS `estado` from (`rendicion_directa` join `item_rendicion_directa` on((`item_rendicion_directa`.`rendicion_directa_id` = `rendicion_directa`.`id`)));

-- --------------------------------------------------------

--
-- Estructura para la vista `egresos_reales`
--
DROP TABLE IF EXISTS `egresos_reales`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `egresos_reales` AS select `a_egresos_reales_sr`.`rendicion` AS `rendicion`,`a_egresos_reales_sr`.`item_rendicion_id` AS `item_rendicion_id`,`a_egresos_reales_sr`.`rendicion_directa_id` AS `rendicion_directa_id`,`a_egresos_reales_sr`.`item_rendicion_directa_id` AS `item_rendicion_directa_id`,`a_egresos_reales_sr`.`proveedor_id` AS `proveedor_id`,`proveedor`.`rut` AS `rut`,`proveedor`.`nombre` AS `proveedor`,`a_egresos_reales_sr`.`cuenta_contable_id` AS `cuenta_contable_id`,`cuenta_contable`.`nombre` AS `cuenta_contable`,`a_egresos_reales_sr`.`cuenta_especifica_id` AS `cuenta_especifica_id`,`cuenta_especifica`.`nombre` AS `cuenta_especifica`,`a_egresos_reales_sr`.`proyecto_id` AS `proyecto_id`,`proyecto`.`sigla` AS `sigla`,`proyecto`.`nombre` AS `proyecto`,`a_egresos_reales_sr`.`cuenta_id` AS `cuenta_id`,`cuenta`.`numero_cuenta` AS `numero_cuenta`,`cuenta`.`banco` AS `banco`,`a_egresos_reales_sr`.`tipo_documento` AS `tipo_documento`,`a_egresos_reales_sr`.`numero_documento` AS `numero_documento`,`a_egresos_reales_sr`.`tipo_solicitud` AS `tipo_solicitud`,`a_egresos_reales_sr`.`tipo_cheque` AS `tipo_cheque`,`a_egresos_reales_sr`.`numero_cheque` AS `numero_cheque`,`a_egresos_reales_sr`.`fecha` AS `fecha`,`a_egresos_reales_sr`.`mes` AS `mes`,`a_egresos_reales_sr`.`agno` AS `agno`,`a_egresos_reales_sr`.`monto` AS `monto`,`a_egresos_reales_sr`.`rendicion_directa.estado` AS `estado` from (((((`a_egresos_reales_sr` join `proveedor` on((`a_egresos_reales_sr`.`proveedor_id` = `proveedor`.`id`))) join `proyecto` on((`a_egresos_reales_sr`.`proyecto_id` = `proyecto`.`id`))) join `cuenta` on((`a_egresos_reales_sr`.`cuenta_id` = `cuenta`.`id`))) join `cuenta_contable` on((`a_egresos_reales_sr`.`cuenta_contable_id` = `cuenta_contable`.`id`))) join `cuenta_especifica` on((`a_egresos_reales_sr`.`cuenta_especifica_id` = `cuenta_especifica`.`id`)));

-- --------------------------------------------------------

--
-- Estructura para la vista `libro_compra`
--
DROP TABLE IF EXISTS `libro_compra`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `libro_compra` AS select `item_rendicion`.`id` AS `item_rendicion_id`,`emision`.`fecha` AS `fecha_emision`,`item_rendicion`.`fecha` AS `fecha_item_rendicion`,dayofmonth(`emision`.`fecha`) AS `dia_emision`,elt(month(`emision`.`fecha`),'Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre') AS `mes_emision`,year(`emision`.`fecha`) AS `ano_emision`,dayofmonth(`item_rendicion`.`fecha`) AS `dia_item_rendicion`,elt(month(`item_rendicion`.`fecha`),'Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre') AS `mes_item_rendicion`,year(`item_rendicion`.`fecha`) AS `ano_item_rendicion`,`item_rendicion`.`tipo_documento` AS `tipo_documento`,`item_rendicion`.`numero_documento` AS `numero_documento`,`proveedor`.`nombre` AS `proveedor`,`item_rendicion`.`monto` AS `monto`,`item_rendicion`.`tipo_solicitud` AS `tipo_solicitud`,`emision`.`numero_cheque` AS `numero_cheque`,`rendicion`.`asignacion` AS `asignacion`,`centro_costo`.`nombre` AS `centro_costo`,`proyecto`.`nombre` AS `proyecto`,`cuenta_contable`.`nombre` AS `cuenta_contable`,`cuenta_especifica`.`nombre` AS `cuenta_especifica` from (((((((`proyecto` join `centro_costo` on((`proyecto`.`centro_costo_id` = `centro_costo`.`id`))) join `item_rendicion` on((`item_rendicion`.`proyecto_id` = `proyecto`.`id`))) join `proveedor` on((`item_rendicion`.`proveedor_id` = `proveedor`.`id`))) left join `rendicion` on((`item_rendicion`.`rendicion_id` = `rendicion`.`id`))) left join `emision` on((`rendicion`.`emision_id` = `emision`.`id`))) join `cuenta_contable` on((`item_rendicion`.`cuenta_contable_id` = `cuenta_contable`.`id`))) join `cuenta_especifica` on((`item_rendicion`.`cuenta_especifica_id` = `cuenta_especifica`.`id`)));

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `authassignment`
--
ALTER TABLE `authassignment`
  ADD CONSTRAINT `authassignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `authitemchild`
--
ALTER TABLE `authitemchild`
  ADD CONSTRAINT `authitemchild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `authitemchild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `comuna`
--
ALTER TABLE `comuna`
  ADD CONSTRAINT `fk_comuna_region` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `contrato`
--
ALTER TABLE `contrato`
  ADD CONSTRAINT `fk_contrato_proyecto1` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_contrato_rrhh1` FOREIGN KEY (`rrhh_id`) REFERENCES `rrhh` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_contrato_tipo_contrato1` FOREIGN KEY (`tipo_contrato_id`) REFERENCES `tipo_contrato` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `cuenta_especifica`
--
ALTER TABLE `cuenta_especifica`
  ADD CONSTRAINT `fk_cuenta_especifica_cuenta_contable1` FOREIGN KEY (`cuenta_contable_id`) REFERENCES `cuenta_contable` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `emision`
--
ALTER TABLE `emision`
  ADD CONSTRAINT `fk_emision_cuenta1` FOREIGN KEY (`cuenta_id`) REFERENCES `cuenta` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_emision_proveedor1` FOREIGN KEY (`proveedor_id`) REFERENCES `proveedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_emision_proyecto1` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_emision_solicitud1` FOREIGN KEY (`solicitud_id`) REFERENCES `solicitud` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_emision_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ingreso`
--
ALTER TABLE `ingreso`
  ADD CONSTRAINT `fk_presupuesto_financiamiento1` FOREIGN KEY (`financiamiento_id`) REFERENCES `financiamiento` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_presupuesto_proyecto1` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_presupuesto_tipo_ingreso1` FOREIGN KEY (`tipo_ingreso_id`) REFERENCES `tipo_ingreso` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `item_presupuesto`
--
ALTER TABLE `item_presupuesto`
  ADD CONSTRAINT `fk_item_presupuesto_cuenta_contable1` FOREIGN KEY (`cuenta_contable_id`) REFERENCES `cuenta_contable` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_presupuesto_presupuesto1` FOREIGN KEY (`presupuesto_id`) REFERENCES `presupuesto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_presupuesto_proyecto1` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `item_rendicion`
--
ALTER TABLE `item_rendicion`
  ADD CONSTRAINT `fk_item_rendicion_cuenta1` FOREIGN KEY (`cuenta_id`) REFERENCES `cuenta` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_rendicion_cuenta_contable1` FOREIGN KEY (`cuenta_contable_id`) REFERENCES `cuenta_contable` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_rendicion_cuenta_especifica1` FOREIGN KEY (`cuenta_especifica_id`) REFERENCES `cuenta_especifica` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_rendicion_proveedor1` FOREIGN KEY (`proveedor_id`) REFERENCES `proveedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_rendicion_proyecto1` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_rendicion_rendicion1` FOREIGN KEY (`rendicion_id`) REFERENCES `rendicion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `item_rendicion_directa`
--
ALTER TABLE `item_rendicion_directa`
  ADD CONSTRAINT `fk_item_rendicion_directa_cuenta_contable` FOREIGN KEY (`cuenta_contable_id`) REFERENCES `cuenta_contable` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_rendicion_directa_cuenta_especifica` FOREIGN KEY (`cuenta_especifica_id`) REFERENCES `cuenta_especifica` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_rendicion_directa_proveedor` FOREIGN KEY (`proveedor_id`) REFERENCES `proveedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_rendicion_directa_proyecto` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_rendicion_directa_rendicion_directa` FOREIGN KEY (`rendicion_directa_id`) REFERENCES `rendicion_directa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `item_solicitud`
--
ALTER TABLE `item_solicitud`
  ADD CONSTRAINT `fk_item_solicitud_cuenta_contable1` FOREIGN KEY (`cuenta_contable_id`) REFERENCES `cuenta_contable` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_solicitud_solicitud1` FOREIGN KEY (`solicitud_id`) REFERENCES `solicitud` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `presupuesto`
--
ALTER TABLE `presupuesto`
  ADD CONSTRAINT `fk_presupuesto_proyecto2` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_presupuesto_tipo_ingreso2` FOREIGN KEY (`tipo_ingreso_id`) REFERENCES `tipo_ingreso` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `proyecto`
--
ALTER TABLE `proyecto`
  ADD CONSTRAINT `fk_proyecto_centro_costo1` FOREIGN KEY (`centro_costo_id`) REFERENCES `centro_costo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_proyecto_rrhh1` FOREIGN KEY (`rrhh_id`) REFERENCES `rrhh` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `rendicion`
--
ALTER TABLE `rendicion`
  ADD CONSTRAINT `fk_rendicion_cuenta1` FOREIGN KEY (`cuenta_id`) REFERENCES `cuenta` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_rendicion_emision1` FOREIGN KEY (`emision_id`) REFERENCES `emision` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_rendicion_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `rendicion_directa`
--
ALTER TABLE `rendicion_directa`
  ADD CONSTRAINT `fk_rendicion_directa_cuenta` FOREIGN KEY (`cuenta_id`) REFERENCES `cuenta` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `rights`
--
ALTER TABLE `rights`
  ADD CONSTRAINT `rights_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `rrhh`
--
ALTER TABLE `rrhh`
  ADD CONSTRAINT `fk_rrhh_afp1` FOREIGN KEY (`afp_id`) REFERENCES `afp` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `solicitud`
--
ALTER TABLE `solicitud`
  ADD CONSTRAINT `fk_solicitud_proveedor1` FOREIGN KEY (`proveedor_id`) REFERENCES `proveedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_solicitud_proyecto1` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_solicitud_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_user_rrhh1` FOREIGN KEY (`rrhh_id`) REFERENCES `rrhh` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
