/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50525
 Source Host           : localhost
 Source Database       : sisproy

 Target Server Type    : MySQL
 Target Server Version : 50525
 File Encoding         : utf-8

 Date: 04/12/2013 23:00:33 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `afp`
-- ----------------------------
DROP TABLE IF EXISTS `afp`;
CREATE TABLE `afp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `afp`
-- ----------------------------
BEGIN;
INSERT INTO `afp` VALUES ('1', 'Habitat'), ('2', 'Cuprum'), ('3', 'Modelo'), ('4', 'Capital'), ('5', 'Provida'), ('6', 'Plan Vital'), ('7', 'Inp'), ('8', 'Jubilado');
COMMIT;

-- ----------------------------
--  Table structure for `authassignment`
-- ----------------------------
DROP TABLE IF EXISTS `authassignment`;
CREATE TABLE `authassignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`),
  CONSTRAINT `authassignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `authassignment`
-- ----------------------------
BEGIN;
INSERT INTO `authassignment` VALUES ('admin', '1', null, 'N;'), ('Jefe de Proyecto', '2', null, 'N;');
COMMIT;

-- ----------------------------
--  Table structure for `authitem`
-- ----------------------------
DROP TABLE IF EXISTS `authitem`;
CREATE TABLE `authitem` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `authitem`
-- ----------------------------
BEGIN;
INSERT INTO `authitem` VALUES ('admin', '2', 'Administrador', null, 'N;'), ('Admin.Admin.*', '1', null, null, 'N;'), ('Admin.Admin.Index', '0', null, null, 'N;'), ('Admin.CentroCosto.*', '1', null, null, 'N;'), ('Admin.CentroCosto.Admin', '0', null, null, 'N;'), ('Admin.CentroCosto.Create', '0', null, null, 'N;'), ('Admin.CentroCosto.Delete', '0', null, null, 'N;'), ('Admin.CentroCosto.Editable', '0', null, null, 'N;'), ('Admin.CentroCosto.GenerateExcel', '0', null, null, 'N;'), ('Admin.CentroCosto.GeneratePdf', '0', null, null, 'N;'), ('Admin.CentroCosto.Index', '0', null, null, 'N;'), ('Admin.CentroCosto.Update', '0', null, null, 'N;'), ('Admin.CentroCosto.View', '0', null, null, 'N;'), ('Admin.Contrato.*', '1', null, null, 'N;'), ('Admin.Contrato.Admin', '0', null, null, 'N;'), ('Admin.Contrato.Create', '0', null, null, 'N;'), ('Admin.Contrato.Delete', '0', null, null, 'N;'), ('Admin.Contrato.Editable', '0', null, null, 'N;'), ('Admin.Contrato.GenerateExcel', '0', null, null, 'N;'), ('Admin.Contrato.GeneratePdf', '0', null, null, 'N;'), ('Admin.Contrato.Index', '0', null, null, 'N;'), ('Admin.Contrato.Update', '0', null, null, 'N;'), ('Admin.Contrato.View', '0', null, null, 'N;'), ('Admin.CuentaContable.*', '1', null, null, 'N;'), ('Admin.CuentaContable.Admin', '0', null, null, 'N;'), ('Admin.CuentaContable.Create', '0', null, null, 'N;'), ('Admin.CuentaContable.Delete', '0', null, null, 'N;'), ('Admin.CuentaContable.Editable', '0', null, null, 'N;'), ('Admin.CuentaContable.GenerateExcel', '0', null, null, 'N;'), ('Admin.CuentaContable.GeneratePdf', '0', null, null, 'N;'), ('Admin.CuentaContable.Index', '0', null, null, 'N;'), ('Admin.CuentaContable.Update', '0', null, null, 'N;'), ('Admin.CuentaContable.View', '0', null, null, 'N;'), ('Admin.CuentaEspecifica.*', '1', null, null, 'N;'), ('Admin.CuentaEspecifica.Admin', '0', null, null, 'N;'), ('Admin.CuentaEspecifica.Create', '0', null, null, 'N;'), ('Admin.CuentaEspecifica.Delete', '0', null, null, 'N;'), ('Admin.CuentaEspecifica.Editable', '0', null, null, 'N;'), ('Admin.CuentaEspecifica.GenerateExcel', '0', null, null, 'N;'), ('Admin.CuentaEspecifica.GeneratePdf', '0', null, null, 'N;'), ('Admin.CuentaEspecifica.Index', '0', null, null, 'N;'), ('Admin.CuentaEspecifica.Update', '0', null, null, 'N;'), ('Admin.CuentaEspecifica.View', '0', null, null, 'N;'), ('Admin.Default.*', '1', null, null, 'N;'), ('Admin.Default.Index', '0', null, null, 'N;'), ('Admin.Default.Solicitud', '0', null, null, 'N;'), ('Admin.Emision.*', '1', null, null, 'N;'), ('Admin.Emision.Admin', '0', null, null, 'N;'), ('Admin.Emision.Create', '0', null, null, 'N;'), ('Admin.Emision.Delete', '0', null, null, 'N;'), ('Admin.Emision.Editable', '0', null, null, 'N;'), ('Admin.Emision.GenerateExcel', '0', null, null, 'N;'), ('Admin.Emision.GeneratePdf', '0', null, null, 'N;'), ('Admin.Emision.Index', '0', null, null, 'N;'), ('Admin.Emision.Update', '0', null, null, 'N;'), ('Admin.Emision.View', '0', null, null, 'N;'), ('Admin.Financiamiento.*', '1', null, null, 'N;'), ('Admin.Financiamiento.Admin', '0', null, null, 'N;'), ('Admin.Financiamiento.Create', '0', null, null, 'N;'), ('Admin.Financiamiento.Delete', '0', null, null, 'N;'), ('Admin.Financiamiento.Editable', '0', null, null, 'N;'), ('Admin.Financiamiento.GenerateExcel', '0', null, null, 'N;'), ('Admin.Financiamiento.GeneratePdf', '0', null, null, 'N;'), ('Admin.Financiamiento.Index', '0', null, null, 'N;'), ('Admin.Financiamiento.Update', '0', null, null, 'N;'), ('Admin.Financiamiento.View', '0', null, null, 'N;'), ('Admin.Ingreso.*', '1', null, null, 'N;'), ('Admin.Ingreso.Admin', '0', null, null, 'N;'), ('Admin.Ingreso.Create', '0', null, null, 'N;'), ('Admin.Ingreso.Delete', '0', null, null, 'N;'), ('Admin.Ingreso.Editable', '0', null, null, 'N;'), ('Admin.Ingreso.GenerateExcel', '0', null, null, 'N;'), ('Admin.Ingreso.GeneratePdf', '0', null, null, 'N;'), ('Admin.Ingreso.Index', '0', null, null, 'N;'), ('Admin.Ingreso.Update', '0', null, null, 'N;'), ('Admin.Ingreso.View', '0', null, null, 'N;'), ('Admin.ItemPresupuesto.*', '1', null, null, 'N;'), ('Admin.ItemPresupuesto.Admin', '0', null, null, 'N;'), ('Admin.ItemPresupuesto.Create', '0', null, null, 'N;'), ('Admin.ItemPresupuesto.Delete', '0', null, null, 'N;'), ('Admin.ItemPresupuesto.Editable', '0', null, null, 'N;'), ('Admin.ItemPresupuesto.GenerateExcel', '0', null, null, 'N;'), ('Admin.ItemPresupuesto.GeneratePdf', '0', null, null, 'N;'), ('Admin.ItemPresupuesto.Index', '0', null, null, 'N;'), ('Admin.ItemPresupuesto.Update', '0', null, null, 'N;'), ('Admin.ItemPresupuesto.View', '0', null, null, 'N;'), ('Admin.ItemRendicion.*', '1', null, null, 'N;'), ('Admin.ItemRendicion.Admin', '0', null, null, 'N;'), ('Admin.ItemRendicion.Create', '0', null, null, 'N;'), ('Admin.ItemRendicion.Delete', '0', null, null, 'N;'), ('Admin.ItemRendicion.Editable', '0', null, null, 'N;'), ('Admin.ItemRendicion.GenerateExcel', '0', null, null, 'N;'), ('Admin.ItemRendicion.GeneratePdf', '0', null, null, 'N;'), ('Admin.ItemRendicion.Index', '0', null, null, 'N;'), ('Admin.ItemRendicion.Update', '0', null, null, 'N;'), ('Admin.ItemRendicion.View', '0', null, null, 'N;'), ('Admin.ItemSolicitud.*', '1', null, null, 'N;'), ('Admin.ItemSolicitud.Admin', '0', null, null, 'N;'), ('Admin.ItemSolicitud.Create', '0', null, null, 'N;'), ('Admin.ItemSolicitud.Delete', '0', null, null, 'N;'), ('Admin.ItemSolicitud.Editable', '0', null, null, 'N;'), ('Admin.ItemSolicitud.GenerateExcel', '0', null, null, 'N;'), ('Admin.ItemSolicitud.GeneratePdf', '0', null, null, 'N;'), ('Admin.ItemSolicitud.Index', '0', null, null, 'N;'), ('Admin.ItemSolicitud.Update', '0', null, null, 'N;'), ('Admin.ItemSolicitud.View', '0', null, null, 'N;'), ('Admin.Presupuesto.*', '1', null, null, 'N;'), ('Admin.Presupuesto.Admin', '0', null, null, 'N;'), ('Admin.Presupuesto.Create', '0', null, null, 'N;'), ('Admin.Presupuesto.Delete', '0', null, null, 'N;'), ('Admin.Presupuesto.Editable', '0', null, null, 'N;'), ('Admin.Presupuesto.GenerateExcel', '0', null, null, 'N;'), ('Admin.Presupuesto.GeneratePdf', '0', null, null, 'N;'), ('Admin.Presupuesto.Index', '0', null, null, 'N;'), ('Admin.Presupuesto.Update', '0', null, null, 'N;'), ('Admin.Presupuesto.View', '0', null, null, 'N;'), ('Admin.Proveedor.*', '1', null, null, 'N;'), ('Admin.Proveedor.Actualizar', '0', null, null, 'N;'), ('Admin.Proveedor.Admin', '0', null, null, 'N;'), ('Admin.Proveedor.Agregar', '0', null, null, 'N;'), ('Admin.Proveedor.Crear', '0', null, null, 'N;'), ('Admin.Proveedor.Create', '0', null, null, 'N;'), ('Admin.Proveedor.Delete', '0', null, null, 'N;'), ('Admin.Proveedor.Editable', '0', null, null, 'N;'), ('Admin.Proveedor.GenerateExcel', '0', null, null, 'N;'), ('Admin.Proveedor.GeneratePdf', '0', null, null, 'N;'), ('Admin.Proveedor.GetTodos', '0', null, null, 'N;'), ('Admin.Proveedor.Index', '0', null, null, 'N;'), ('Admin.Proveedor.Update', '0', null, null, 'N;'), ('Admin.Proveedor.Ver', '0', null, null, 'N;'), ('Admin.Proveedor.VerTodos', '0', null, null, 'N;'), ('Admin.Proveedor.View', '0', null, null, 'N;'), ('Admin.Proyecto.*', '1', null, null, 'N;'), ('Admin.Proyecto.Admin', '0', null, null, 'N;'), ('Admin.Proyecto.Create', '0', null, null, 'N;'), ('Admin.Proyecto.Delete', '0', null, null, 'N;'), ('Admin.Proyecto.Editable', '0', null, null, 'N;'), ('Admin.Proyecto.GenerateExcel', '0', null, null, 'N;'), ('Admin.Proyecto.GeneratePdf', '0', null, null, 'N;'), ('Admin.Proyecto.Index', '0', null, null, 'N;'), ('Admin.Proyecto.Update', '0', null, null, 'N;'), ('Admin.Proyecto.View', '0', null, null, 'N;'), ('Admin.Rendicion.*', '1', null, null, 'N;'), ('Admin.Rendicion.Admin', '0', null, null, 'N;'), ('Admin.Rendicion.Create', '0', null, null, 'N;'), ('Admin.Rendicion.Delete', '0', null, null, 'N;'), ('Admin.Rendicion.Editable', '0', null, null, 'N;'), ('Admin.Rendicion.GenerateExcel', '0', null, null, 'N;'), ('Admin.Rendicion.GeneratePdf', '0', null, null, 'N;'), ('Admin.Rendicion.Index', '0', null, null, 'N;'), ('Admin.Rendicion.Update', '0', null, null, 'N;'), ('Admin.Rendicion.View', '0', null, null, 'N;'), ('Admin.Rrhh.*', '1', null, null, 'N;'), ('Admin.Rrhh.Admin', '0', null, null, 'N;'), ('Admin.Rrhh.Create', '0', null, null, 'N;'), ('Admin.Rrhh.Delete', '0', null, null, 'N;'), ('Admin.Rrhh.Editable', '0', null, null, 'N;'), ('Admin.Rrhh.GenerateExcel', '0', null, null, 'N;'), ('Admin.Rrhh.GeneratePdf', '0', null, null, 'N;'), ('Admin.Rrhh.Index', '0', null, null, 'N;'), ('Admin.Rrhh.Update', '0', null, null, 'N;'), ('Admin.Rrhh.View', '0', null, null, 'N;'), ('Admin.Solicitud.*', '1', null, null, 'N;'), ('Admin.Solicitud.Admin', '0', null, null, 'N;'), ('Admin.Solicitud.Create', '0', null, null, 'N;'), ('Admin.Solicitud.Delete', '0', null, null, 'N;'), ('Admin.Solicitud.Editable', '0', null, null, 'N;'), ('Admin.Solicitud.GenerateExcel', '0', null, null, 'N;'), ('Admin.Solicitud.GeneratePdf', '0', null, null, 'N;'), ('Admin.Solicitud.Index', '0', null, null, 'N;'), ('Admin.Solicitud.Update', '0', null, null, 'N;'), ('Admin.Solicitud.View', '0', null, null, 'N;'), ('Admin.TipoContrato.*', '1', null, null, 'N;'), ('Admin.TipoContrato.Admin', '0', null, null, 'N;'), ('Admin.TipoContrato.Create', '0', null, null, 'N;'), ('Admin.TipoContrato.Delete', '0', null, null, 'N;'), ('Admin.TipoContrato.Editable', '0', null, null, 'N;'), ('Admin.TipoContrato.GenerateExcel', '0', null, null, 'N;'), ('Admin.TipoContrato.GeneratePdf', '0', null, null, 'N;'), ('Admin.TipoContrato.Index', '0', null, null, 'N;'), ('Admin.TipoContrato.Update', '0', null, null, 'N;'), ('Admin.TipoContrato.View', '0', null, null, 'N;'), ('Admin.TipoIngreso.*', '1', null, null, 'N;'), ('Admin.TipoIngreso.Admin', '0', null, null, 'N;'), ('Admin.TipoIngreso.Create', '0', null, null, 'N;'), ('Admin.TipoIngreso.Delete', '0', null, null, 'N;'), ('Admin.TipoIngreso.Editable', '0', null, null, 'N;'), ('Admin.TipoIngreso.GenerateExcel', '0', null, null, 'N;'), ('Admin.TipoIngreso.GeneratePdf', '0', null, null, 'N;'), ('Admin.TipoIngreso.Index', '0', null, null, 'N;'), ('Admin.TipoIngreso.Update', '0', null, null, 'N;'), ('Admin.TipoIngreso.View', '0', null, null, 'N;'), ('Admin.User.*', '1', null, null, 'N;'), ('Admin.User.Admin', '0', null, null, 'N;'), ('Admin.User.ChangePassword', '0', null, null, 'N;'), ('Admin.User.Create', '0', null, null, 'N;'), ('Admin.User.Delete', '0', null, null, 'N;'), ('Admin.User.Editable', '0', null, null, 'N;'), ('Admin.User.GenerateExcel', '0', null, null, 'N;'), ('Admin.User.GeneratePdf', '0', null, null, 'N;'), ('Admin.User.Index', '0', null, null, 'N;'), ('Admin.User.Update', '0', null, null, 'N;'), ('Admin.User.View', '0', null, null, 'N;'), ('CentroCosto.*', '1', null, null, 'N;'), ('CentroCosto.Actualizar', '0', null, null, 'N;'), ('CentroCosto.Admin', '0', null, null, 'N;'), ('CentroCosto.Crear', '0', null, null, 'N;'), ('CentroCosto.Create', '0', null, null, 'N;'), ('CentroCosto.Delete', '0', null, null, 'N;'), ('CentroCosto.GenerateExcel', '0', null, null, 'N;'), ('CentroCosto.GeneratePdf', '0', null, null, 'N;'), ('CentroCosto.Index', '0', null, null, 'N;'), ('CentroCosto.Update', '0', null, null, 'N;'), ('CentroCosto.Ver', '0', null, null, 'N;'), ('CentroCosto.VerTodos', '0', null, null, 'N;'), ('CentroCosto.View', '0', null, null, 'N;'), ('Contrato.*', '1', null, null, 'N;'), ('Contrato.Actualizar', '0', null, null, 'N;'), ('Contrato.Admin', '0', null, null, 'N;'), ('Contrato.Crear', '0', null, null, 'N;'), ('Contrato.Create', '0', null, null, 'N;'), ('Contrato.Delete', '0', null, null, 'N;'), ('Contrato.Finalizar', '0', null, null, 'N;'), ('Contrato.GenerateExcel', '0', null, null, 'N;'), ('Contrato.GeneratePdf', '0', null, null, 'N;'), ('Contrato.Index', '0', null, null, 'N;'), ('Contrato.Update', '0', null, null, 'N;'), ('Contrato.Ver', '0', null, null, 'N;'), ('Contrato.VerRrhh', '0', null, null, 'N;'), ('Contrato.VerTodos', '0', null, null, 'N;'), ('Contrato.View', '0', null, null, 'N;'), ('CuentaContable.*', '1', null, null, 'N;'), ('CuentaContable.Actualizar', '0', null, null, 'N;'), ('CuentaContable.Admin', '0', null, null, 'N;'), ('CuentaContable.Crear', '0', null, null, 'N;'), ('CuentaContable.Create', '0', null, null, 'N;'), ('CuentaContable.Delete', '0', null, null, 'N;'), ('CuentaContable.GenerateExcel', '0', null, null, 'N;'), ('CuentaContable.GeneratePdf', '0', null, null, 'N;'), ('CuentaContable.Index', '0', null, null, 'N;'), ('CuentaContable.Update', '0', null, null, 'N;'), ('CuentaContable.Ver', '0', null, null, 'N;'), ('CuentaContable.VerTodos', '0', null, null, 'N;'), ('CuentaContable.View', '0', null, null, 'N;'), ('CuentaEspecifica.*', '1', null, null, 'N;'), ('CuentaEspecifica.Actualizar', '0', null, null, 'N;'), ('CuentaEspecifica.Admin', '0', null, null, 'N;'), ('CuentaEspecifica.Crear', '0', null, null, 'N;'), ('CuentaEspecifica.Create', '0', null, null, 'N;'), ('CuentaEspecifica.Delete', '0', null, null, 'N;'), ('CuentaEspecifica.GenerateExcel', '0', null, null, 'N;'), ('CuentaEspecifica.GeneratePdf', '0', null, null, 'N;'), ('CuentaEspecifica.Index', '0', null, null, 'N;'), ('CuentaEspecifica.Update', '0', null, null, 'N;'), ('CuentaEspecifica.Ver', '0', null, null, 'N;'), ('CuentaEspecifica.VerTodos', '0', null, null, 'N;'), ('CuentaEspecifica.View', '0', null, null, 'N;'), ('Director Ejecutivo', '2', 'Director Ejecutivo', null, 'N;'), ('Donante.*', '1', null, null, 'N;'), ('Donante.Admin', '0', null, null, 'N;'), ('Donante.Create', '0', null, null, 'N;'), ('Donante.Delete', '0', null, null, 'N;'), ('Donante.GenerateExcel', '0', null, null, 'N;'), ('Donante.GeneratePdf', '0', null, null, 'N;'), ('Donante.Index', '0', null, null, 'N;'), ('Donante.Update', '0', null, null, 'N;'), ('Donante.View', '0', null, null, 'N;'), ('Emision.*', '1', null, null, 'N;'), ('Emision.Actualizar', '0', null, null, 'N;'), ('Emision.Admin', '0', null, null, 'N;'), ('Emision.Anular', '0', null, null, 'N;'), ('Emision.Crear', '0', null, null, 'N;'), ('Emision.Create', '0', null, null, 'N;'), ('Emision.Delete', '0', null, null, 'N;'), ('Emision.Eliminar', '0', null, null, 'N;'), ('Emision.Finalizadas', '0', null, null, 'N;'), ('Emision.Finalizar', '0', null, null, 'N;'), ('Emision.GenerateExcel', '0', null, null, 'N;'), ('Emision.GeneratePdf', '0', null, null, 'N;'), ('Emision.Index', '0', null, null, 'N;'), ('Emision.Update', '0', null, null, 'N;'), ('Emision.Ver', '0', null, null, 'N;'), ('Emision.VerEmisionRendicion', '0', null, null, 'N;'), ('Emision.VerTodos', '0', null, null, 'N;'), ('Emision.View', '0', null, null, 'N;'), ('Financiamiento.*', '1', null, null, 'N;'), ('Financiamiento.Actualizar', '0', null, null, 'N;'), ('Financiamiento.Admin', '0', null, null, 'N;'), ('Financiamiento.Crear', '0', null, null, 'N;'), ('Financiamiento.Create', '0', null, null, 'N;'), ('Financiamiento.Delete', '0', null, null, 'N;'), ('Financiamiento.GenerateExcel', '0', null, null, 'N;'), ('Financiamiento.GeneratePdf', '0', null, null, 'N;'), ('Financiamiento.Index', '0', null, null, 'N;'), ('Financiamiento.Update', '0', null, null, 'N;'), ('Financiamiento.Ver', '0', null, null, 'N;'), ('Financiamiento.VerTodos', '0', null, null, 'N;'), ('Financiamiento.View', '0', null, null, 'N;'), ('Informe.*', '1', null, null, 'N;'), ('Informe.Crear', '0', null, null, 'N;'), ('Informe.Test', '0', null, null, 'N;'), ('Ingreso.*', '1', null, null, 'N;'), ('Ingreso.Actualizar', '0', null, null, 'N;'), ('Ingreso.Admin', '0', null, null, 'N;'), ('Ingreso.Crear', '0', null, null, 'N;'), ('Ingreso.Create', '0', null, null, 'N;'), ('Ingreso.Delete', '0', null, null, 'N;'), ('Ingreso.GenerateExcel', '0', null, null, 'N;'), ('Ingreso.GeneratePdf', '0', null, null, 'N;'), ('Ingreso.Index', '0', null, null, 'N;'), ('Ingreso.Update', '0', null, null, 'N;'), ('Ingreso.Ver', '0', null, null, 'N;'), ('Ingreso.VerTodos', '0', null, null, 'N;'), ('Ingreso.View', '0', null, null, 'N;'), ('IngresoProyecto.*', '1', null, null, 'N;'), ('IngresoProyecto.Admin', '0', null, null, 'N;'), ('IngresoProyecto.Create', '0', null, null, 'N;'), ('IngresoProyecto.Delete', '0', null, null, 'N;'), ('IngresoProyecto.GenerateExcel', '0', null, null, 'N;'), ('IngresoProyecto.GeneratePdf', '0', null, null, 'N;'), ('IngresoProyecto.Index', '0', null, null, 'N;'), ('IngresoProyecto.Update', '0', null, null, 'N;'), ('IngresoProyecto.View', '0', null, null, 'N;'), ('Inventario.*', '1', null, null, 'N;'), ('Inventario.Admin', '0', null, null, 'N;'), ('Inventario.Create', '0', null, null, 'N;'), ('Inventario.Delete', '0', null, null, 'N;'), ('Inventario.GenerateExcel', '0', null, null, 'N;'), ('Inventario.GeneratePdf', '0', null, null, 'N;'), ('Inventario.Index', '0', null, null, 'N;'), ('Inventario.Update', '0', null, null, 'N;'), ('Inventario.View', '0', null, null, 'N;'), ('ItemEmision.*', '1', null, null, 'N;'), ('ItemEmision.Admin', '0', null, null, 'N;'), ('ItemEmision.Agregar', '0', null, null, 'N;'), ('ItemEmision.Create', '0', null, null, 'N;'), ('ItemEmision.Delete', '0', null, null, 'N;'), ('ItemEmision.GenerateExcel', '0', null, null, 'N;'), ('ItemEmision.GeneratePdf', '0', null, null, 'N;'), ('ItemEmision.Index', '0', null, null, 'N;'), ('ItemEmision.Update', '0', null, null, 'N;'), ('ItemEmision.View', '0', null, null, 'N;'), ('ItemPresupuesto.*', '1', null, null, 'N;'), ('ItemPresupuesto.Actualizar', '0', null, null, 'N;'), ('ItemPresupuesto.ActualizarProyecto', '0', null, null, 'N;'), ('ItemPresupuesto.Admin', '0', null, null, 'N;'), ('ItemPresupuesto.Crear', '0', null, null, 'N;'), ('ItemPresupuesto.CrearProyecto', '0', null, null, 'N;'), ('ItemPresupuesto.Delete', '0', null, null, 'N;'), ('ItemPresupuesto.GenerateExcel', '0', null, null, 'N;'), ('ItemPresupuesto.GeneratePdf', '0', null, null, 'N;'), ('ItemPresupuesto.Index', '0', null, null, 'N;'), ('ItemPresupuesto.ItemPresupuestoVsItemRendicion', '0', null, null, 'N;'), ('ItemPresupuesto.Update', '0', null, null, 'N;'), ('ItemPresupuesto.Ver', '0', null, null, 'N;'), ('ItemPresupuesto.VerProyecto', '0', null, null, 'N;'), ('ItemPresupuesto.VerTodos', '0', null, null, 'N;'), ('ItemPresupuesto.View', '0', null, null, 'N;'), ('ItemRendicion.*', '1', null, null, 'N;'), ('ItemRendicion.Actualizar', '0', null, null, 'N;'), ('ItemRendicion.Admin', '0', null, null, 'N;'), ('ItemRendicion.Agregar', '0', null, null, 'N;'), ('ItemRendicion.Crear', '0', null, null, 'N;'), ('ItemRendicion.Create', '0', null, null, 'N;'), ('ItemRendicion.Delete', '0', null, null, 'N;'), ('ItemRendicion.GenerateExcel', '0', null, null, 'N;'), ('ItemRendicion.GeneratePdf', '0', null, null, 'N;'), ('ItemRendicion.GetCuenta', '0', null, null, 'N;'), ('ItemRendicion.Index', '0', null, null, 'N;'), ('ItemRendicion.Update', '0', null, null, 'N;'), ('ItemRendicion.Ver', '0', null, null, 'N;'), ('ItemRendicion.VerTodos', '0', null, null, 'N;'), ('ItemRendicion.View', '0', null, null, 'N;'), ('ItemSolicitud.*', '1', null, null, 'N;'), ('ItemSolicitud.Actualizar', '0', null, null, 'N;'), ('ItemSolicitud.Admin', '0', null, null, 'N;'), ('ItemSolicitud.Agregar', '0', null, null, 'N;'), ('ItemSolicitud.Create', '0', null, null, 'N;'), ('ItemSolicitud.Delete', '0', null, null, 'N;'), ('ItemSolicitud.GenerateExcel', '0', null, null, 'N;'), ('ItemSolicitud.GeneratePdf', '0', null, null, 'N;'), ('ItemSolicitud.Index', '0', null, null, 'N;'), ('ItemSolicitud.Update', '0', null, null, 'N;'), ('ItemSolicitud.Ver', '0', null, null, 'N;'), ('ItemSolicitud.View', '0', null, null, 'N;'), ('Jefe de Proyecto', '2', 'Jefe de Proyecto', null, 'N;'), ('Jefe de Taller', '2', 'Jefe de Taller', null, 'N;'), ('Presupuesto.*', '1', null, null, 'N;'), ('Presupuesto.Actualizar', '0', null, null, 'N;'), ('Presupuesto.Admin', '0', null, null, 'N;'), ('Presupuesto.Crear', '0', null, null, 'N;'), ('Presupuesto.Create', '0', null, null, 'N;'), ('Presupuesto.Delete', '0', null, null, 'N;'), ('Presupuesto.GenerateExcel', '0', null, null, 'N;'), ('Presupuesto.GeneratePdf', '0', null, null, 'N;'), ('Presupuesto.Index', '0', null, null, 'N;'), ('Presupuesto.Update', '0', null, null, 'N;'), ('Presupuesto.Ver', '0', null, null, 'N;'), ('Presupuesto.VerProyecto', '0', null, null, 'N;'), ('Presupuesto.VerTodos', '0', null, null, 'N;'), ('Presupuesto.View', '0', null, null, 'N;'), ('Proveedor.*', '1', null, null, 'N;'), ('Proveedor.Actualizar', '0', null, null, 'N;'), ('Proveedor.Admin', '0', null, null, 'N;'), ('Proveedor.Agregar', '0', null, null, 'N;'), ('Proveedor.Crear', '0', null, null, 'N;'), ('Proveedor.Create', '0', null, null, 'N;'), ('Proveedor.Delete', '0', null, null, 'N;'), ('Proveedor.GenerateExcel', '0', null, null, 'N;'), ('Proveedor.GeneratePdf', '0', null, null, 'N;'), ('Proveedor.GetTodos', '0', null, null, 'N;'), ('Proveedor.Index', '0', null, null, 'N;'), ('Proveedor.Update', '0', null, null, 'N;'), ('Proveedor.Ver', '0', null, null, 'N;'), ('Proveedor.VerTodos', '0', null, null, 'N;'), ('Proveedor.View', '0', null, null, 'N;'), ('Proyecto.*', '1', null, null, 'N;'), ('Proyecto.Actualizar', '0', null, null, 'N;'), ('Proyecto.Admin', '0', null, null, 'N;'), ('Proyecto.Crear', '0', null, null, 'N;'), ('Proyecto.Create', '0', null, null, 'N;'), ('Proyecto.Delete', '0', null, null, 'N;'), ('Proyecto.GenerateExcel', '0', null, null, 'N;'), ('Proyecto.GeneratePdf', '0', null, null, 'N;'), ('Proyecto.GetComuna', '0', null, null, 'N;'), ('Proyecto.Index', '0', null, null, 'N;'), ('Proyecto.PresupuestoVsIngreso', '0', null, null, 'N;'), ('Proyecto.Update', '0', null, null, 'N;'), ('Proyecto.Ver', '0', null, null, 'N;'), ('Proyecto.VerPresupuesto', '0', null, null, 'N;'), ('Proyecto.VerTodos', '0', null, null, 'N;'), ('Proyecto.View', '0', null, null, 'N;'), ('Rendicion.*', '1', null, null, 'N;'), ('Rendicion.Actualizar', '0', null, null, 'N;'), ('Rendicion.Admin', '0', null, null, 'N;'), ('Rendicion.Anular', '0', null, null, 'N;'), ('Rendicion.Crear', '0', null, null, 'N;'), ('Rendicion.Create', '0', null, null, 'N;'), ('Rendicion.Delete', '0', null, null, 'N;'), ('Rendicion.Finalizar', '0', null, null, 'N;'), ('Rendicion.GenerateExcel', '0', null, null, 'N;'), ('Rendicion.GeneratePdf', '0', null, null, 'N;'), ('Rendicion.Index', '0', null, null, 'N;'), ('Rendicion.Update', '0', null, null, 'N;'), ('Rendicion.Ver', '0', null, null, 'N;'), ('Rendicion.VerTodos', '0', null, null, 'N;'), ('Rendicion.View', '0', null, null, 'N;'), ('Rrhh.*', '1', null, null, 'N;'), ('Rrhh.Actualizar', '0', null, null, 'N;'), ('Rrhh.Admin', '0', null, null, 'N;'), ('Rrhh.Crear', '0', null, null, 'N;'), ('Rrhh.Create', '0', null, null, 'N;'), ('Rrhh.Delete', '0', null, null, 'N;'), ('Rrhh.GenerateExcel', '0', null, null, 'N;'), ('Rrhh.GeneratePdf', '0', null, null, 'N;'), ('Rrhh.Index', '0', null, null, 'N;'), ('Rrhh.Ver', '0', null, null, 'N;'), ('Rrhh.VerContrato', '0', null, null, 'N;'), ('Rrhh.VerTodos', '0', null, null, 'N;'), ('Rrhh.View', '0', null, null, 'N;'), ('Site.*', '1', null, null, 'N;'), ('Site.Contact', '0', null, null, 'N;'), ('Site.Error', '0', null, null, 'N;'), ('Site.Index', '0', null, null, 'N;'), ('Site.Login', '0', null, null, 'N;'), ('Site.Logout', '0', null, null, 'N;'), ('Solicitud.*', '1', null, null, 'N;'), ('Solicitud.Actualizar', '0', null, null, 'N;'), ('Solicitud.Admin', '0', null, null, 'N;'), ('Solicitud.Anular', '0', null, null, 'N;'), ('Solicitud.Crear', '0', null, null, 'N;'), ('Solicitud.CrearSolicitud', '0', null, null, 'N;'), ('Solicitud.Create', '0', null, null, 'N;'), ('Solicitud.Delete', '0', null, null, 'N;'), ('Solicitud.Finalizadas', '0', null, null, 'N;'), ('Solicitud.Finalizar', '0', null, null, 'N;'), ('Solicitud.GenerateExcel', '0', null, null, 'N;'), ('Solicitud.GeneratePdf', '0', null, null, 'N;'), ('Solicitud.Index', '0', null, null, 'N;'), ('Solicitud.Update', '0', null, null, 'N;'), ('Solicitud.Ver', '0', null, null, 'N;'), ('Solicitud.VerSolicitudEmision', '0', null, null, 'N;'), ('Solicitud.VerTodos', '0', null, null, 'N;'), ('Solicitud.View', '0', null, null, 'N;'), ('TipoContrato.*', '1', null, null, 'N;'), ('TipoContrato.Actualizar', '0', null, null, 'N;'), ('TipoContrato.Admin', '0', null, null, 'N;'), ('TipoContrato.Crear', '0', null, null, 'N;'), ('TipoContrato.Create', '0', null, null, 'N;'), ('TipoContrato.Delete', '0', null, null, 'N;'), ('TipoContrato.GenerateExcel', '0', null, null, 'N;'), ('TipoContrato.GeneratePdf', '0', null, null, 'N;'), ('TipoContrato.Index', '0', null, null, 'N;'), ('TipoContrato.Update', '0', null, null, 'N;'), ('TipoContrato.Ver', '0', null, null, 'N;'), ('TipoContrato.VerTodos', '0', null, null, 'N;'), ('TipoContrato.View', '0', null, null, 'N;'), ('TipoIngreso.*', '1', null, null, 'N;'), ('TipoIngreso.Actualizar', '0', null, null, 'N;'), ('TipoIngreso.Admin', '0', null, null, 'N;'), ('TipoIngreso.Crear', '0', null, null, 'N;'), ('TipoIngreso.Create', '0', null, null, 'N;'), ('TipoIngreso.Delete', '0', null, null, 'N;'), ('TipoIngreso.GenerateExcel', '0', null, null, 'N;'), ('TipoIngreso.GeneratePdf', '0', null, null, 'N;'), ('TipoIngreso.Index', '0', null, null, 'N;'), ('TipoIngreso.Update', '0', null, null, 'N;'), ('TipoIngreso.Ver', '0', null, null, 'N;'), ('TipoIngreso.VerTodos', '0', null, null, 'N;'), ('TipoIngreso.View', '0', null, null, 'N;'), ('User.*', '1', null, null, 'N;'), ('User.Admin', '0', null, null, 'N;'), ('User.ChangePassword', '0', null, null, 'N;'), ('User.Create', '0', null, null, 'N;'), ('User.Delete', '0', null, null, 'N;'), ('User.GenerateExcel', '0', null, null, 'N;'), ('User.GeneratePdf', '0', null, null, 'N;'), ('User.Index', '0', null, null, 'N;'), ('User.Update', '0', null, null, 'N;'), ('User.View', '0', null, null, 'N;');
COMMIT;

-- ----------------------------
--  Table structure for `authitemchild`
-- ----------------------------
DROP TABLE IF EXISTS `authitemchild`;
CREATE TABLE `authitemchild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `authitemchild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `authitemchild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `authitemchild`
-- ----------------------------
BEGIN;
INSERT INTO `authitemchild` VALUES ('Director Ejecutivo', 'CentroCosto.Actualizar'), ('Director Ejecutivo', 'CentroCosto.Crear'), ('Director Ejecutivo', 'CentroCosto.GenerateExcel'), ('Director Ejecutivo', 'CentroCosto.GeneratePdf'), ('Director Ejecutivo', 'CentroCosto.Ver'), ('Director Ejecutivo', 'CentroCosto.VerTodos'), ('Director Ejecutivo', 'CuentaContable.Crear'), ('Jefe de Proyecto', 'CuentaContable.Crear'), ('Jefe de Taller', 'CuentaContable.Crear'), ('Director Ejecutivo', 'CuentaContable.GenerateExcel'), ('Jefe de Proyecto', 'CuentaContable.GenerateExcel'), ('Jefe de Taller', 'CuentaContable.GenerateExcel'), ('Director Ejecutivo', 'CuentaContable.GeneratePdf'), ('Jefe de Proyecto', 'CuentaContable.GeneratePdf'), ('Jefe de Taller', 'CuentaContable.GeneratePdf'), ('Director Ejecutivo', 'CuentaContable.Ver'), ('Jefe de Proyecto', 'CuentaContable.Ver'), ('Jefe de Taller', 'CuentaContable.Ver'), ('Director Ejecutivo', 'CuentaContable.VerTodos'), ('Jefe de Proyecto', 'CuentaContable.VerTodos'), ('Jefe de Taller', 'CuentaContable.VerTodos'), ('Director Ejecutivo', 'CuentaEspecifica.Actualizar'), ('Director Ejecutivo', 'CuentaEspecifica.Crear'), ('Director Ejecutivo', 'CuentaEspecifica.GenerateExcel'), ('Jefe de Proyecto', 'CuentaEspecifica.GenerateExcel'), ('Jefe de Taller', 'CuentaEspecifica.GenerateExcel'), ('Director Ejecutivo', 'CuentaEspecifica.GeneratePdf'), ('Jefe de Proyecto', 'CuentaEspecifica.GeneratePdf'), ('Jefe de Taller', 'CuentaEspecifica.GeneratePdf'), ('Director Ejecutivo', 'CuentaEspecifica.Ver'), ('Jefe de Proyecto', 'CuentaEspecifica.Ver'), ('Jefe de Taller', 'CuentaEspecifica.Ver'), ('Director Ejecutivo', 'Emision.Actualizar'), ('Jefe de Proyecto', 'Emision.Actualizar'), ('Jefe de Taller', 'Emision.Actualizar'), ('Director Ejecutivo', 'Emision.Anular'), ('Jefe de Proyecto', 'Emision.Anular'), ('Jefe de Taller', 'Emision.Anular'), ('Director Ejecutivo', 'Emision.Crear'), ('Jefe de Proyecto', 'Emision.Crear'), ('Jefe de Taller', 'Emision.Crear'), ('Director Ejecutivo', 'Emision.Finalizadas'), ('Jefe de Proyecto', 'Emision.Finalizadas'), ('Jefe de Taller', 'Emision.Finalizadas'), ('Director Ejecutivo', 'Emision.Finalizar'), ('Jefe de Proyecto', 'Emision.Finalizar'), ('Jefe de Taller', 'Emision.Finalizar'), ('Director Ejecutivo', 'Emision.GenerateExcel'), ('Jefe de Proyecto', 'Emision.GenerateExcel'), ('Jefe de Taller', 'Emision.GenerateExcel'), ('Director Ejecutivo', 'Emision.GeneratePdf'), ('Jefe de Proyecto', 'Emision.GeneratePdf'), ('Jefe de Taller', 'Emision.GeneratePdf'), ('Director Ejecutivo', 'Emision.Ver'), ('Jefe de Proyecto', 'Emision.Ver'), ('Jefe de Taller', 'Emision.Ver'), ('Director Ejecutivo', 'Emision.VerEmisionRendicion'), ('Jefe de Proyecto', 'Emision.VerEmisionRendicion'), ('Jefe de Taller', 'Emision.VerEmisionRendicion'), ('Director Ejecutivo', 'Emision.VerTodos'), ('Jefe de Proyecto', 'Emision.VerTodos'), ('Jefe de Taller', 'Emision.VerTodos'), ('Director Ejecutivo', 'ItemRendicion.Actualizar'), ('Jefe de Proyecto', 'ItemRendicion.Actualizar'), ('Jefe de Taller', 'ItemRendicion.Actualizar'), ('Director Ejecutivo', 'ItemRendicion.Agregar'), ('Jefe de Proyecto', 'ItemRendicion.Agregar'), ('Jefe de Taller', 'ItemRendicion.Agregar'), ('Director Ejecutivo', 'ItemRendicion.GenerateExcel'), ('Jefe de Proyecto', 'ItemRendicion.GenerateExcel'), ('Jefe de Taller', 'ItemRendicion.GenerateExcel'), ('Director Ejecutivo', 'ItemRendicion.GeneratePdf'), ('Jefe de Proyecto', 'ItemRendicion.GeneratePdf'), ('Jefe de Taller', 'ItemRendicion.GeneratePdf'), ('Director Ejecutivo', 'ItemRendicion.GetCuenta'), ('Jefe de Proyecto', 'ItemRendicion.GetCuenta'), ('Jefe de Taller', 'ItemRendicion.GetCuenta'), ('Director Ejecutivo', 'ItemRendicion.Ver'), ('Jefe de Proyecto', 'ItemRendicion.Ver'), ('Jefe de Taller', 'ItemRendicion.Ver'), ('Director Ejecutivo', 'ItemSolicitud.Actualizar'), ('Jefe de Proyecto', 'ItemSolicitud.Actualizar'), ('Jefe de Taller', 'ItemSolicitud.Actualizar'), ('Director Ejecutivo', 'ItemSolicitud.Agregar'), ('Jefe de Proyecto', 'ItemSolicitud.Agregar'), ('Jefe de Taller', 'ItemSolicitud.Agregar'), ('Director Ejecutivo', 'ItemSolicitud.GenerateExcel'), ('Jefe de Proyecto', 'ItemSolicitud.GenerateExcel'), ('Jefe de Taller', 'ItemSolicitud.GenerateExcel'), ('Director Ejecutivo', 'ItemSolicitud.GeneratePdf'), ('Jefe de Proyecto', 'ItemSolicitud.GeneratePdf'), ('Jefe de Taller', 'ItemSolicitud.GeneratePdf'), ('Director Ejecutivo', 'ItemSolicitud.Ver'), ('Jefe de Proyecto', 'ItemSolicitud.Ver'), ('Jefe de Taller', 'ItemSolicitud.Ver'), ('Director Ejecutivo', 'Proveedor.Actualizar'), ('Jefe de Proyecto', 'Proveedor.Actualizar'), ('Jefe de Taller', 'Proveedor.Actualizar'), ('Director Ejecutivo', 'Proveedor.Agregar'), ('Jefe de Proyecto', 'Proveedor.Agregar'), ('Jefe de Taller', 'Proveedor.Agregar'), ('Director Ejecutivo', 'Proveedor.Crear'), ('Jefe de Proyecto', 'Proveedor.Crear'), ('Jefe de Taller', 'Proveedor.Crear'), ('Director Ejecutivo', 'Proveedor.GenerateExcel'), ('Jefe de Proyecto', 'Proveedor.GenerateExcel'), ('Jefe de Taller', 'Proveedor.GenerateExcel'), ('Director Ejecutivo', 'Proveedor.GeneratePdf'), ('Jefe de Proyecto', 'Proveedor.GeneratePdf'), ('Jefe de Taller', 'Proveedor.GeneratePdf'), ('Director Ejecutivo', 'Proveedor.GetTodos'), ('Jefe de Proyecto', 'Proveedor.GetTodos'), ('Jefe de Taller', 'Proveedor.GetTodos'), ('Director Ejecutivo', 'Proveedor.Ver'), ('Jefe de Proyecto', 'Proveedor.Ver'), ('Jefe de Taller', 'Proveedor.Ver'), ('Director Ejecutivo', 'Proveedor.VerTodos'), ('Jefe de Proyecto', 'Proveedor.VerTodos'), ('Jefe de Taller', 'Proveedor.VerTodos'), ('Director Ejecutivo', 'Proyecto.Actualizar'), ('Jefe de Proyecto', 'Proyecto.Actualizar'), ('Jefe de Taller', 'Proyecto.Actualizar'), ('Director Ejecutivo', 'Proyecto.Crear'), ('Jefe de Proyecto', 'Proyecto.Crear'), ('Jefe de Taller', 'Proyecto.Crear'), ('Director Ejecutivo', 'Proyecto.GenerateExcel'), ('Jefe de Proyecto', 'Proyecto.GenerateExcel'), ('Jefe de Taller', 'Proyecto.GenerateExcel'), ('Director Ejecutivo', 'Proyecto.GeneratePdf'), ('Jefe de Proyecto', 'Proyecto.GeneratePdf'), ('Jefe de Taller', 'Proyecto.GeneratePdf'), ('Director Ejecutivo', 'Proyecto.GetComuna'), ('Jefe de Proyecto', 'Proyecto.GetComuna'), ('Jefe de Taller', 'Proyecto.GetComuna'), ('Director Ejecutivo', 'Proyecto.Ver'), ('Jefe de Proyecto', 'Proyecto.Ver'), ('Jefe de Taller', 'Proyecto.Ver'), ('Director Ejecutivo', 'Proyecto.VerTodos'), ('Jefe de Proyecto', 'Proyecto.VerTodos'), ('Jefe de Taller', 'Proyecto.VerTodos'), ('Director Ejecutivo', 'Rendicion.Actualizar'), ('Jefe de Proyecto', 'Rendicion.Actualizar'), ('Jefe de Taller', 'Rendicion.Actualizar'), ('Director Ejecutivo', 'Rendicion.Anular'), ('Jefe de Proyecto', 'Rendicion.Anular'), ('Jefe de Taller', 'Rendicion.Anular'), ('Director Ejecutivo', 'Rendicion.Crear'), ('Jefe de Proyecto', 'Rendicion.Crear'), ('Jefe de Taller', 'Rendicion.Crear'), ('Director Ejecutivo', 'Rendicion.Finalizar'), ('Jefe de Proyecto', 'Rendicion.Finalizar'), ('Jefe de Taller', 'Rendicion.Finalizar'), ('Director Ejecutivo', 'Rendicion.GenerateExcel'), ('Jefe de Proyecto', 'Rendicion.GenerateExcel'), ('Jefe de Taller', 'Rendicion.GenerateExcel'), ('Director Ejecutivo', 'Rendicion.GeneratePdf'), ('Jefe de Proyecto', 'Rendicion.GeneratePdf'), ('Jefe de Taller', 'Rendicion.GeneratePdf'), ('Director Ejecutivo', 'Rendicion.Ver'), ('Jefe de Proyecto', 'Rendicion.Ver'), ('Jefe de Taller', 'Rendicion.Ver'), ('Director Ejecutivo', 'Rendicion.VerTodos'), ('Jefe de Proyecto', 'Rendicion.VerTodos'), ('Jefe de Taller', 'Rendicion.VerTodos'), ('Director Ejecutivo', 'Rrhh.Actualizar'), ('Jefe de Proyecto', 'Rrhh.Actualizar'), ('Jefe de Taller', 'Rrhh.Actualizar'), ('Director Ejecutivo', 'Rrhh.Crear'), ('Jefe de Proyecto', 'Rrhh.Crear'), ('Jefe de Taller', 'Rrhh.Crear'), ('Director Ejecutivo', 'Rrhh.GenerateExcel'), ('Jefe de Proyecto', 'Rrhh.GenerateExcel'), ('Jefe de Taller', 'Rrhh.GenerateExcel'), ('Director Ejecutivo', 'Rrhh.GeneratePdf'), ('Jefe de Proyecto', 'Rrhh.GeneratePdf'), ('Jefe de Taller', 'Rrhh.GeneratePdf'), ('Director Ejecutivo', 'Rrhh.Ver'), ('Jefe de Proyecto', 'Rrhh.Ver'), ('Jefe de Taller', 'Rrhh.Ver'), ('Director Ejecutivo', 'Rrhh.VerContrato'), ('Jefe de Proyecto', 'Rrhh.VerContrato'), ('Jefe de Taller', 'Rrhh.VerContrato'), ('Director Ejecutivo', 'Rrhh.VerTodos'), ('Jefe de Proyecto', 'Rrhh.VerTodos'), ('Jefe de Taller', 'Rrhh.VerTodos'), ('Director Ejecutivo', 'Solicitud.Actualizar'), ('Jefe de Proyecto', 'Solicitud.Actualizar'), ('Jefe de Taller', 'Solicitud.Actualizar'), ('Director Ejecutivo', 'Solicitud.Anular'), ('Jefe de Proyecto', 'Solicitud.Anular'), ('Jefe de Taller', 'Solicitud.Anular'), ('Director Ejecutivo', 'Solicitud.Crear'), ('Jefe de Proyecto', 'Solicitud.Crear'), ('Jefe de Taller', 'Solicitud.Crear'), ('Director Ejecutivo', 'Solicitud.CrearSolicitud'), ('Jefe de Proyecto', 'Solicitud.CrearSolicitud'), ('Jefe de Taller', 'Solicitud.CrearSolicitud'), ('Director Ejecutivo', 'Solicitud.Finalizadas'), ('Jefe de Proyecto', 'Solicitud.Finalizadas'), ('Jefe de Taller', 'Solicitud.Finalizadas'), ('Director Ejecutivo', 'Solicitud.Finalizar'), ('Jefe de Proyecto', 'Solicitud.Finalizar'), ('Jefe de Taller', 'Solicitud.Finalizar'), ('Director Ejecutivo', 'Solicitud.GenerateExcel'), ('Jefe de Proyecto', 'Solicitud.GenerateExcel'), ('Jefe de Taller', 'Solicitud.GenerateExcel'), ('Director Ejecutivo', 'Solicitud.GeneratePdf'), ('Jefe de Proyecto', 'Solicitud.GeneratePdf'), ('Jefe de Taller', 'Solicitud.GeneratePdf'), ('Director Ejecutivo', 'Solicitud.Ver'), ('Jefe de Proyecto', 'Solicitud.Ver'), ('Jefe de Taller', 'Solicitud.Ver'), ('Director Ejecutivo', 'Solicitud.VerSolicitudEmision'), ('Jefe de Proyecto', 'Solicitud.VerSolicitudEmision'), ('Jefe de Taller', 'Solicitud.VerSolicitudEmision'), ('Director Ejecutivo', 'Solicitud.VerTodos'), ('Jefe de Taller', 'Solicitud.VerTodos');
COMMIT;

-- ----------------------------
--  Table structure for `centro_costo`
-- ----------------------------
DROP TABLE IF EXISTS `centro_costo`;
CREATE TABLE `centro_costo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `centro_costo`
-- ----------------------------
BEGIN;
INSERT INTO `centro_costo` VALUES ('1', 'Taller Arquitectura'), ('2', 'Taller de Difusion'), ('3', 'Administracion'), ('4', 'Taller Historia del Arte'), ('5', 'Taller Investigacion y Desarrollo');
COMMIT;

-- ----------------------------
--  Table structure for `comuna`
-- ----------------------------
DROP TABLE IF EXISTS `comuna`;
CREATE TABLE `comuna` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `region_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_comuna_region1_idx` (`region_id`),
  CONSTRAINT `fk_comuna_region1` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `comuna`
-- ----------------------------
BEGIN;
INSERT INTO `comuna` VALUES ('1', 'Arica', '1'), ('2', 'Putre', '1');
COMMIT;

-- ----------------------------
--  Table structure for `contrato`
-- ----------------------------
DROP TABLE IF EXISTS `contrato`;
CREATE TABLE `contrato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rrhh_id` int(11) NOT NULL,
  `proyecto_id` int(11) NOT NULL,
  `tipo_contrato_id` int(11) NOT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_termino` date DEFAULT NULL,
  `remuneracion` int(11) DEFAULT NULL,
  `estado` varchar(45) DEFAULT 'VIGENTE',
  PRIMARY KEY (`id`),
  KEY `fk_contrato_proyecto1_idx` (`proyecto_id`),
  KEY `fk_contrato_tipo_contrato1_idx` (`tipo_contrato_id`),
  KEY `fk_contrato_rrhh1_idx` (`rrhh_id`),
  CONSTRAINT `fk_contrato_proyecto1` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_contrato_rrhh1` FOREIGN KEY (`rrhh_id`) REFERENCES `rrhh` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_contrato_tipo_contrato1` FOREIGN KEY (`tipo_contrato_id`) REFERENCES `tipo_contrato` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `cuenta_contable`
-- ----------------------------
DROP TABLE IF EXISTS `cuenta_contable`;
CREATE TABLE `cuenta_contable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `cuenta_contable`
-- ----------------------------
BEGIN;
INSERT INTO `cuenta_contable` VALUES ('1', 'Recursos Humano'), ('2', 'Operacion'), ('3', 'Inversion'), ('4', 'Difusion'), ('5', 'Gastos Generales');
COMMIT;

-- ----------------------------
--  Table structure for `cuenta_especifica`
-- ----------------------------
DROP TABLE IF EXISTS `cuenta_especifica`;
CREATE TABLE `cuenta_especifica` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cuenta_contable_id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cuenta_especifica_cuenta_contable1_idx` (`cuenta_contable_id`),
  CONSTRAINT `fk_cuenta_especifica_cuenta_contable1` FOREIGN KEY (`cuenta_contable_id`) REFERENCES `cuenta_contable` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `cuenta_especifica`
-- ----------------------------
BEGIN;
INSERT INTO `cuenta_especifica` VALUES ('1', '1', 'Bonos'), ('2', '1', 'Boleta de Honorario'), ('3', '3', 'Maquinas de Construccion'), ('4', '1', 'Capacitacion'), ('5', '1', 'Finiquitos'), ('6', '1', 'Imposiciones'), ('7', '1', 'IVA'), ('8', '1', 'Remuneraciones'), ('9', '1', 'Retenciones'), ('10', '1', 'Viaticos'), ('11', '2', 'Agua Potable'), ('12', '2', 'Alimentacion'), ('13', '2', 'Alojamiento'), ('14', '2', 'Arriendo Vehiculo'), ('15', '2', 'Combustible/Peajes'), ('16', '2', 'Celular'), ('17', '2', 'Fletes'), ('18', '2', 'Fotocopias, Impresiones, Ploteo'), ('19', '2', 'Gastos de Representacion'), ('20', '2', 'Gastos Notariales'), ('21', '2', 'Impl. de Seguridad'), ('22', '2', 'Luz'), ('23', '2', 'Mantencion de Vehiculos'), ('24', '2', 'Materiales de Construccion'), ('25', '2', 'Materiales de Oficina'), ('26', '2', 'Partes'), ('27', '2', 'Pasajes'), ('28', '2', 'Ropa de Trabajo'), ('29', '2', 'Telecomunicacion'), ('30', '3', 'Tecnologia, Computadores, Impresiones, Maq. Fotografica, Etc.'), ('31', '3', 'Otros Activos'), ('32', '3', 'Vehiculos'), ('33', '4', 'Difusion/Publicidad'), ('34', '4', 'Honorario Evento'), ('35', '4', 'Webhosting Internet'), ('36', '5', 'Abarrotes, Manaje y Otros'), ('37', '5', 'Arreglos Oficina'), ('38', '5', 'Arriendo Oficina'), ('39', '5', 'Correspondencia'), ('40', '5', 'Donaciones'), ('41', '5', 'Fotografia y Ediciones'), ('42', '5', 'Gastos Comunes'), ('43', '5', 'Gastos Financieros'), ('44', '5', 'Mantencion de Equipos'), ('45', '5', 'Otros Gastos Generales'), ('46', '5', 'Patente'), ('47', '5', 'Seguros'), ('48', '5', 'Servicios Legales'), ('49', '5', 'Taxis Colectivos/Estacionamiento');
COMMIT;

-- ----------------------------
--  Table structure for `emision`
-- ----------------------------
DROP TABLE IF EXISTS `emision`;
CREATE TABLE `emision` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `proveedor_id` int(11) NOT NULL,
  `proyecto_id` int(11) NOT NULL,
  `solicitud_id` int(11) NOT NULL,
  `ciudad` varchar(100) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `concepto` varchar(255) DEFAULT NULL,
  `total_debe` int(11) NOT NULL DEFAULT '0',
  `total_haber` int(11) NOT NULL DEFAULT '0',
  `cuenta` varchar(100) DEFAULT NULL,
  `tipo_solicitud` varchar(50) DEFAULT NULL,
  `tipo_cheque` varchar(45) DEFAULT NULL,
  `numero_cheque` varchar(100) DEFAULT NULL,
  `observaciones` blob,
  `estado` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_emision_solicitud1_idx` (`solicitud_id`),
  KEY `fk_emision_proyecto1_idx` (`proyecto_id`),
  KEY `fk_emision_proveedor1_idx` (`proveedor_id`),
  KEY `fk_emision_user1` (`user_id`),
  CONSTRAINT `fk_emision_proveedor1` FOREIGN KEY (`proveedor_id`) REFERENCES `proveedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_emision_proyecto1` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_emision_solicitud1` FOREIGN KEY (`solicitud_id`) REFERENCES `solicitud` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_emision_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `emision`
-- ----------------------------
BEGIN;
INSERT INTO `emision` VALUES ('1', '1', '1', '1', '1', null, '2013-01-09', null, '1000000', '0', '01.Banco de Chile', 'Transferencia', null, '34634643', null, 'RENDIDA'), ('2', '1', '1', '2', '2', null, '2013-01-01', null, '12000', '0', '01.Banco de Chile', 'Cheque', null, '325262352', 0x4f62736572766163696f6e65732032, 'RENDIDA'), ('3', '1', '1', '1', '3', null, '2013-02-01', null, '1000000', '0', '01.Banco de Chile', 'Cheque', null, '322131', null, 'RENDIDA'), ('4', '1', '1', '3', '4', null, '2013-02-01', null, '100000000', '0', '01.Banco de Chile', 'Cheque', null, '21312321', null, 'RENDIDA');
COMMIT;

-- ----------------------------
--  Table structure for `financiamiento`
-- ----------------------------
DROP TABLE IF EXISTS `financiamiento`;
CREATE TABLE `financiamiento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `financiamiento`
-- ----------------------------
BEGIN;
INSERT INTO `financiamiento` VALUES ('1', 'Conadi'), ('2', 'Municipalidad'), ('3', 'Corfo');
COMMIT;

-- ----------------------------
--  Table structure for `ingreso`
-- ----------------------------
DROP TABLE IF EXISTS `ingreso`;
CREATE TABLE `ingreso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proyecto_id` int(11) NOT NULL,
  `tipo_ingreso_id` int(11) NOT NULL,
  `financiamiento_id` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `monto` int(11) DEFAULT '0',
  `observaciones` blob,
  PRIMARY KEY (`id`),
  KEY `fk_presupuesto_financiamiento1_idx` (`financiamiento_id`),
  KEY `fk_presupuesto_tipo_ingreso1_idx` (`tipo_ingreso_id`),
  KEY `fk_presupuesto_proyecto1_idx` (`proyecto_id`),
  CONSTRAINT `fk_presupuesto_financiamiento1` FOREIGN KEY (`financiamiento_id`) REFERENCES `financiamiento` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_presupuesto_proyecto1` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_presupuesto_tipo_ingreso1` FOREIGN KEY (`tipo_ingreso_id`) REFERENCES `tipo_ingreso` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `ingreso`
-- ----------------------------
BEGIN;
INSERT INTO `ingreso` VALUES ('1', '1', '1', '1', '2013-01-08', '80000', null), ('2', '1', '2', '1', '2013-01-08', '2000000', null), ('3', '1', '4', '3', '2013-01-01', '1000000', 0x5241424f204954), ('4', '3', '1', '1', '2013-02-01', '10000000', null);
COMMIT;

-- ----------------------------
--  Table structure for `item_presupuesto`
-- ----------------------------
DROP TABLE IF EXISTS `item_presupuesto`;
CREATE TABLE `item_presupuesto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `presupuesto_id` int(11) NOT NULL,
  `cuenta_contable_id` int(11) NOT NULL,
  `proyecto_id` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `mes` varchar(45) DEFAULT NULL,
  `agno` varchar(45) DEFAULT NULL,
  `monto` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_item_presupuesto_presupuesto1_idx` (`presupuesto_id`),
  KEY `fk_item_presupuesto_cuenta_contable1_idx` (`cuenta_contable_id`),
  KEY `fk_item_presupuesto_proyecto1_idx` (`proyecto_id`),
  CONSTRAINT `fk_item_presupuesto_cuenta_contable1` FOREIGN KEY (`cuenta_contable_id`) REFERENCES `cuenta_contable` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_item_presupuesto_presupuesto1` FOREIGN KEY (`presupuesto_id`) REFERENCES `presupuesto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_item_presupuesto_proyecto1` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `item_presupuesto`
-- ----------------------------
BEGIN;
INSERT INTO `item_presupuesto` VALUES ('1', '1', '1', '1', '2013-01-01', null, null, '2000000'), ('2', '1', '2', '1', '2013-01-01', null, null, '200000'), ('3', '2', '1', '1', '2013-01-01', null, null, '400000'), ('4', '3', '1', '1', '2013-02-01', null, null, '50000'), ('5', '5', '1', '1', '2013-03-01', null, null, '50000'), ('6', '5', '2', '1', '2013-03-01', null, null, '100000'), ('7', '4', '1', '1', '2013-02-01', null, null, '10000'), ('8', '6', '1', '2', '2013-01-01', null, null, '1200'), ('9', '7', '1', '3', '2013-02-01', null, null, '10000000'), ('10', '8', '1', '1', '2013-04-01', null, null, '2000000'), ('11', '1', '5', '1', '2013-01-01', null, null, '500000');
COMMIT;

-- ----------------------------
--  Table structure for `item_rendicion`
-- ----------------------------
DROP TABLE IF EXISTS `item_rendicion`;
CREATE TABLE `item_rendicion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proveedor_id` int(11) NOT NULL,
  `cuenta_contable_id` int(11) NOT NULL,
  `cuenta_especifica_id` int(11) NOT NULL,
  `rendicion_id` int(11) DEFAULT NULL,
  `proyecto_id` int(11) NOT NULL,
  `tipo_documento` varchar(100) DEFAULT NULL,
  `numero_documento` varchar(100) DEFAULT NULL,
  `tipo_pago` varchar(100) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `mes` varchar(45) DEFAULT NULL,
  `agno` varchar(45) DEFAULT NULL,
  `monto` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_item_rendicion_rendicion1_idx` (`rendicion_id`),
  KEY `fk_item_rendicion_proyecto1_idx` (`proyecto_id`),
  KEY `fk_item_rendicion_proveedor1_idx` (`proveedor_id`),
  KEY `fk_item_rendicion_cuenta_especifica1_idx` (`cuenta_especifica_id`),
  KEY `fk_item_rendicion_cuenta_contable1` (`cuenta_contable_id`),
  CONSTRAINT `fk_item_rendicion_cuenta_contable1` FOREIGN KEY (`cuenta_contable_id`) REFERENCES `cuenta_contable` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_item_rendicion_cuenta_especifica1` FOREIGN KEY (`cuenta_especifica_id`) REFERENCES `cuenta_especifica` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_item_rendicion_proveedor1` FOREIGN KEY (`proveedor_id`) REFERENCES `proveedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_item_rendicion_proyecto1` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_item_rendicion_rendicion1` FOREIGN KEY (`rendicion_id`) REFERENCES `rendicion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `item_rendicion`
-- ----------------------------
BEGIN;
INSERT INTO `item_rendicion` VALUES ('1', '1', '1', '1', '1', '1', 'Boleta', '4543', null, '2013-01-01', null, null, '700000'), ('2', '1', '1', '5', '1', '1', 'Boleta de Honorario', '34324', null, '2013-01-01', null, null, '250000'), ('3', '1', '1', '1', '1', '1', 'Boleta', '345345', null, '2013-02-01', null, null, '60000'), ('4', '1', '1', '1', '2', '2', 'Boleta', '35434', null, '2013-01-01', null, null, '1200'), ('5', '1', '3', '3', '3', '1', 'Boleta', '345345', null, '2013-02-01', null, null, '10000000'), ('6', '1', '3', '30', '4', '3', 'Boleta', '35434', null, '2013-02-01', null, null, '10000000'), ('7', '1', '1', '1', '5', '1', 'Boleta', null, null, '2013-01-01', null, null, '500000'), ('8', '1', '2', '11', '5', '1', 'Boleta', null, null, '2013-01-01', null, null, '400000'), ('9', '1', '3', '3', '5', '1', 'Boleta', null, null, '2013-01-01', null, null, '100000'), ('10', '1', '4', '34', '1', '1', 'Boleta', '23432', null, '2013-01-01', null, null, '100000'), ('11', '1', '5', '42', null, '1', 'Boleta', '74837', null, '2013-01-01', null, null, '150000'), ('12', '1', '1', '4', null, '1', 'Boleta', 'asdsa', null, '2013-04-16', null, null, '34324');
COMMIT;

-- ----------------------------
--  Table structure for `item_solicitud`
-- ----------------------------
DROP TABLE IF EXISTS `item_solicitud`;
CREATE TABLE `item_solicitud` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `solicitud_id` int(11) NOT NULL,
  `cuenta_contable_id` int(11) DEFAULT NULL,
  `valor_unitario` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `descripcion` blob,
  `total` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_item_solicitud_solicitud1_idx` (`solicitud_id`),
  KEY `fk_item_solicitud_cuenta_contable1_idx` (`cuenta_contable_id`),
  CONSTRAINT `fk_item_solicitud_cuenta_contable1` FOREIGN KEY (`cuenta_contable_id`) REFERENCES `cuenta_contable` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_item_solicitud_solicitud1` FOREIGN KEY (`solicitud_id`) REFERENCES `solicitud` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `item_solicitud`
-- ----------------------------
BEGIN;
INSERT INTO `item_solicitud` VALUES ('1', '1', null, '1000000', '1', 0x41706f727465, '1000000'), ('2', '2', null, '12000', '1', 0x41706f727465, '12000'), ('3', '3', null, '1000000', '1', 0x41706f727465, '1000000'), ('4', '1', null, '10000', '1', 0x4974656d31, '10000'), ('5', '1', null, '100', '3', 0x4974656d32, '300'), ('6', '1', null, '100', '1', 0x4974656d33, '100'), ('7', '1', null, '100', '1', 0x4974656d34, '100'), ('8', '1', null, '100', '1', 0x4974656d35, '100'), ('9', '1', null, '100', '1', 0x4974656d36, '100'), ('10', '1', null, '100', '1', 0x4974656d37, '100'), ('11', '1', null, '100', '1', 0x4974656d38, '100'), ('12', '1', null, '100', '1', 0x4974656d39, '100'), ('13', '1', null, '100', '1', 0x4974656d3130, '100'), ('14', '1', null, '100', '1', 0x4974656d3131, '100'), ('15', '1', null, '100', '1', 0x4974656d3132, '100'), ('16', '1', null, '100', '1', 0x4974656d3133, '100'), ('17', '1', null, '100', '1', 0x4974656d3134, '100'), ('18', '1', null, '100', '1', 0x4974656d3135, '100'), ('19', '1', null, '100', '1', 0x4974656d3136, '100'), ('20', '1', null, '100', '1', 0x4974656d3137, '100'), ('21', '1', null, '100', '1', 0x4974656d3138, '100'), ('22', '1', null, '100', '1', 0x4974656d3139, '100'), ('23', '1', null, '100', '1', 0x4974656d3230, '100'), ('24', '4', null, '100000000', '1', 0x4974656d31, '100000000');
COMMIT;

-- ----------------------------
--  Table structure for `presupuesto`
-- ----------------------------
DROP TABLE IF EXISTS `presupuesto`;
CREATE TABLE `presupuesto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proyecto_id` int(11) NOT NULL,
  `tipo_ingreso_id` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `mes` varchar(45) DEFAULT NULL,
  `agno` varchar(45) DEFAULT NULL,
  `monto` int(11) DEFAULT '0',
  `observaciones` blob,
  PRIMARY KEY (`id`),
  KEY `fk_presupuesto_proyecto2_idx` (`proyecto_id`),
  KEY `fk_presupuesto_tipo_ingreso2_idx` (`tipo_ingreso_id`),
  CONSTRAINT `fk_presupuesto_proyecto2` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_presupuesto_tipo_ingreso2` FOREIGN KEY (`tipo_ingreso_id`) REFERENCES `tipo_ingreso` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `presupuesto`
-- ----------------------------
BEGIN;
INSERT INTO `presupuesto` VALUES ('1', '1', '2', '2013-01-01', null, null, '2700000', 0x5241424f204954), ('2', '1', '1', '2013-01-01', null, null, '400000', 0x5241424f204954), ('3', '1', '3', '2013-02-01', null, null, '50000', 0x5241424f204954), ('4', '1', '4', '2013-02-01', null, null, '10000', 0x5241424f204954), ('5', '1', '1', '2013-03-01', null, null, '150000', null), ('6', '2', '1', '2013-01-01', null, null, '1200', 0x4f62736572766163696f6e65732032), ('7', '3', '3', '2013-02-01', null, null, '10000000', 0x5241424f204954), ('8', '1', '1', '2013-04-01', null, null, '2000000', null), ('9', '5', '1', '2013-01-01', null, null, '0', null), ('10', '6', '1', '2013-01-01', null, null, '0', null);
COMMIT;

-- ----------------------------
--  Table structure for `proveedor`
-- ----------------------------
DROP TABLE IF EXISTS `proveedor`;
CREATE TABLE `proveedor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rut` varchar(13) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `proveedor`
-- ----------------------------
BEGIN;
INSERT INTO `proveedor` VALUES ('1', '76255575-1', 'RABO IT');
COMMIT;

-- ----------------------------
--  Table structure for `proyecto`
-- ----------------------------
DROP TABLE IF EXISTS `proyecto`;
CREATE TABLE `proyecto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rrhh_id` int(11) NOT NULL,
  `centro_costo_id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `sigla` varchar(100) DEFAULT NULL,
  `descripcion` blob,
  `archivo` blob,
  `contrato` blob,
  `comuna` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_proyecto_centro_costo1_idx` (`centro_costo_id`),
  KEY `fk_proyecto_rrhh1_idx` (`rrhh_id`),
  CONSTRAINT `fk_proyecto_centro_costo1` FOREIGN KEY (`centro_costo_id`) REFERENCES `centro_costo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_proyecto_rrhh1` FOREIGN KEY (`rrhh_id`) REFERENCES `rrhh` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `proyecto`
-- ----------------------------
BEGIN;
INSERT INTO `proyecto` VALUES ('1', '1', '1', 'Proyecto 1', 'P1', 0x446573637269706369c3b36e2031, 0x303173696d706c652e706466, 0x722d31352e706466, 'ARICA', '15'), ('2', '1', '1', 'Proyecto 2', 'P2', 0x4465736372697063696f6e2032, 0x73636162642066696e616c2e73716c, 0x6c6f676f2e706e67, 'ARICA', '15'), ('3', '1', '1', 'Proyecto 3', 'P3', 0x4465736372697063696f6e2033, 0x30626331366638342d323862352d343032312d616564642d3162363233663136333363332e6a7067, 0x30633363623336372d653237352d346330612d616464352d3732346266663461306630312e6a7067, 'ARICA', '15'), ('4', '1', '1', 'Proyecto 4', 'P4', null, null, null, 'ARICA', '15'), ('5', '1', '1', 'Proyecto 5', 'P5', null, null, null, 'ARICA', '15'), ('6', '1', '1', 'Prouecto 6', 'P6', null, null, null, 'ARICA', '15');
COMMIT;

-- ----------------------------
--  Table structure for `region`
-- ----------------------------
DROP TABLE IF EXISTS `region`;
CREATE TABLE `region` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `region`
-- ----------------------------
BEGIN;
INSERT INTO `region` VALUES ('1', 'XV Arica y Parinacota');
COMMIT;

-- ----------------------------
--  Table structure for `rendicion`
-- ----------------------------
DROP TABLE IF EXISTS `rendicion`;
CREATE TABLE `rendicion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `emision_id` int(11) NOT NULL,
  `asignacion` varchar(100) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `comuna` varchar(100) DEFAULT NULL,
  `region` varchar(100) DEFAULT NULL,
  `tipo_pago` varchar(100) DEFAULT NULL,
  `total_entrada` int(11) DEFAULT NULL,
  `total_salida` int(11) NOT NULL DEFAULT '0',
  `total_saldo` int(11) NOT NULL DEFAULT '0',
  `observacion` blob,
  `estado` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_rendicion_emision1_idx` (`emision_id`),
  KEY `fk_rendicion_user1_idx` (`user_id`),
  CONSTRAINT `fk_rendicion_emision1` FOREIGN KEY (`emision_id`) REFERENCES `emision` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_rendicion_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `rendicion`
-- ----------------------------
BEGIN;
INSERT INTO `rendicion` VALUES ('1', '1', '1', 'Directa', '2013-01-09', 'ARICA', 'Región de Arica y Parinacota', 'Transferencia', '1000000', '1110000', '-110000', null, 'ANULADA'), ('2', '1', '2', 'Directa', '2013-01-01', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '12000', '1200', '10800', 0x4f62736572766163696f6e65732032, 'FINALIZADA'), ('3', '1', '3', 'Directa', '2013-02-01', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '1000000', '10000000', '-9000000', null, 'FINALIZADA'), ('4', '1', '4', 'Directa', '2013-02-01', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '100000000', '10000000', '90000000', null, 'FINALIZADA'), ('5', '1', '1', 'Directa', '2013-01-01', 'ARICA', 'Región de Arica y Parinacota', 'Transferencia', '1000000', '1000000', '0', null, 'ANULADA'), ('6', '1', '1', 'Directa', '2013-01-01', 'ARICA', 'Región de Arica y Parinacota', 'Transferencia', '1000000', '0', '0', null, 'ACTIVA'), ('7', '1', '1', 'Directa', '2013-01-01', 'ARICA', 'Región de Arica y Parinacota', 'Transferencia', '1000000', '0', '0', null, 'ACTIVA');
COMMIT;

-- ----------------------------
--  Table structure for `rights`
-- ----------------------------
DROP TABLE IF EXISTS `rights`;
CREATE TABLE `rights` (
  `itemname` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  PRIMARY KEY (`itemname`),
  CONSTRAINT `rights_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `rrhh`
-- ----------------------------
DROP TABLE IF EXISTS `rrhh`;
CREATE TABLE `rrhh` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rut` varchar(13) DEFAULT NULL,
  `nombres` varchar(255) DEFAULT NULL,
  `apellidos` varchar(255) DEFAULT NULL,
  `documento` varchar(45) DEFAULT NULL,
  `nacionalidad` varchar(45) DEFAULT NULL,
  `sexo` varchar(15) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `estado_civil` varchar(45) DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `telefono` varchar(50) DEFAULT NULL,
  `celular` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `ciudad` varchar(45) DEFAULT NULL,
  `salud` varchar(45) DEFAULT NULL,
  `afp_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_rrhh_afp1_idx` (`afp_id`),
  CONSTRAINT `fk_rrhh_afp1` FOREIGN KEY (`afp_id`) REFERENCES `afp` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `rrhh`
-- ----------------------------
BEGIN;
INSERT INTO `rrhh` VALUES ('1', '17013873-2', 'Sebastián Ignacio', 'Monge Lay', 'Carnet', 'Chilena', 'Masculino', '2013-01-08', 'Soltero(a)', 'Chacabuco 735', '232338', '96748558', 'smonge@live.cl', 'Arica', 'Fonasa', '7'), ('2', '17013874-0', null, null, null, null, 'Masculino', null, null, null, null, null, null, null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for `solicitud`
-- ----------------------------
DROP TABLE IF EXISTS `solicitud`;
CREATE TABLE `solicitud` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proyecto_id` int(11) NOT NULL,
  `proveedor_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `tipo_solicitud` varchar(50) NOT NULL,
  `total` int(11) NOT NULL DEFAULT '0',
  `estado` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_solicitud_proyecto1_idx` (`proyecto_id`),
  KEY `fk_solicitud_proveedor1_idx` (`proveedor_id`),
  KEY `fk_solicitud_user1_idx` (`user_id`),
  CONSTRAINT `fk_solicitud_proveedor1` FOREIGN KEY (`proveedor_id`) REFERENCES `proveedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_solicitud_proyecto1` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_solicitud_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `solicitud`
-- ----------------------------
BEGIN;
INSERT INTO `solicitud` VALUES ('1', '1', '1', '1', '2013-01-01', 'Transferencia', '1012000', 'FINALIZADA'), ('2', '2', '1', '1', '2013-01-01', 'Cheque', '120000', 'FINALIZADA'), ('3', '1', '1', '1', '2013-01-16', 'Cheque', '1000000', 'EMITIDA'), ('4', '3', '1', '1', '2013-03-01', 'Cheque', '100000000', 'EMITIDA');
COMMIT;

-- ----------------------------
--  Table structure for `tipo_contrato`
-- ----------------------------
DROP TABLE IF EXISTS `tipo_contrato`;
CREATE TABLE `tipo_contrato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(45) NOT NULL,
  `caracteristicas` blob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `tipo_contrato`
-- ----------------------------
BEGIN;
INSERT INTO `tipo_contrato` VALUES ('2', 'Honorario', null), ('3', 'Planta', null), ('4', 'Indefinido', 0x436f6e747261746f206465207469656d706f20696e646566696e69646f);
COMMIT;

-- ----------------------------
--  Table structure for `tipo_ingreso`
-- ----------------------------
DROP TABLE IF EXISTS `tipo_ingreso`;
CREATE TABLE `tipo_ingreso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `tipo_ingreso`
-- ----------------------------
BEGIN;
INSERT INTO `tipo_ingreso` VALUES ('1', 'Convenio'), ('2', 'Donaciones'), ('3', 'Venta de Servicios'), ('4', 'Otros');
COMMIT;

-- ----------------------------
--  Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) DEFAULT NULL,
  `tipo` varchar(45) DEFAULT NULL,
  `rrhh_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_rrhh1_idx` (`rrhh_id`),
  CONSTRAINT `fk_user_rrhh1` FOREIGN KEY (`rrhh_id`) REFERENCES `rrhh` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `user`
-- ----------------------------
BEGIN;
INSERT INTO `user` VALUES ('1', 'admin', 'c63b447bcee0c5064cac9d264e62114e2d50516e', null, 'admin', '1'), ('2', 'smonge', '73995910cde1acc61443d421a308189623585acb', 's.monge@raboit.com', 'Jefe de Proyecto', '1'), ('3', 'fabalan', '73995910cde1acc61443d421a308189623585acb', null, 'admin', null);
COMMIT;

-- ----------------------------
--  View structure for `libro_compra`
-- ----------------------------
DROP VIEW IF EXISTS `libro_compra`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `libro_compra` AS select `item_rendicion`.`id` AS `item_rendicion_id`,`emision`.`fecha` AS `fecha_emision`,`item_rendicion`.`fecha` AS `fecha_item_rendicion`,dayofmonth(`emision`.`fecha`) AS `dia_emision`,elt(month(`emision`.`fecha`),'Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre') AS `mes_emision`,year(`emision`.`fecha`) AS `ano_emision`,dayofmonth(`item_rendicion`.`fecha`) AS `dia_item_rendicion`,elt(month(`item_rendicion`.`fecha`),'Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre') AS `mes_item_rendicion`,year(`item_rendicion`.`fecha`) AS `ano_item_rendicion`,`item_rendicion`.`tipo_documento` AS `tipo_documento`,`item_rendicion`.`numero_documento` AS `numero_documento`,`proveedor`.`nombre` AS `proveedor`,`item_rendicion`.`monto` AS `monto`,`item_rendicion`.`tipo_solicitud` AS `tipo_solicitud`,`emision`.`numero_cheque` AS `numero_cheque`,`rendicion`.`asignacion` AS `asignacion`,`centro_costo`.`nombre` AS `centro_costo`,`proyecto`.`nombre` AS `proyecto`,`cuenta_contable`.`nombre` AS `cuenta_contable`,`cuenta_especifica`.`nombre` AS `cuenta_especifica` from (((((((`proyecto` join `centro_costo` on((`proyecto`.`centro_costo_id` = `centro_costo`.`id`))) join `item_rendicion` on((`item_rendicion`.`proyecto_id` = `proyecto`.`id`))) join `proveedor` on((`item_rendicion`.`proveedor_id` = `proveedor`.`id`))) left join `rendicion` on((`item_rendicion`.`rendicion_id` = `rendicion`.`id`))) left join `emision` on((`rendicion`.`emision_id` = `emision`.`id`))) join `cuenta_contable` on((`item_rendicion`.`cuenta_contable_id` = `cuenta_contable`.`id`))) join `cuenta_especifica` on((`item_rendicion`.`cuenta_especifica_id` = `cuenta_especifica`.`id`)));

SET FOREIGN_KEY_CHECKS = 1;
