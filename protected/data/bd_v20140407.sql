-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 07-04-2014 a las 20:24:00
-- Versión del servidor: 5.1.41
-- Versión de PHP: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `sisproy`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `afp`
--

CREATE TABLE IF NOT EXISTS `afp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Volcar la base de datos para la tabla `afp`
--

INSERT INTO `afp` (`id`, `nombre`) VALUES
(1, 'Habitat'),
(2, 'Cuprum'),
(3, 'Modelo'),
(4, 'Capital'),
(5, 'Provida'),
(6, 'Plan Vital'),
(7, 'Inp'),
(8, 'Jubilado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `authassignment`
--

CREATE TABLE IF NOT EXISTS `authassignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcar la base de datos para la tabla `authassignment`
--

INSERT INTO `authassignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('admin', '1', NULL, 'N;'),
('admin', '2', NULL, 'N;'),
('admin', '3', NULL, 'N;'),
('admin', '4', NULL, 'N;'),
('Director Ejecutivo', '28', NULL, 'N;'),
('Director Ejecutivo', '29', NULL, 'N;'),
('Jefe de Taller', '11', NULL, 'N;'),
('Jefe de Taller', '12', NULL, 'N;'),
('Jefe de Taller', '14', NULL, 'N;'),
('Jefe de Taller', '16', NULL, 'N;'),
('Jefe de Taller', '20', NULL, 'N;'),
('Jefe de Taller', '5', NULL, 'N;'),
('Usuario Comun', '10', NULL, 'N;'),
('Usuario Comun', '13', NULL, 'N;'),
('Usuario Comun', '15', NULL, 'N;'),
('Usuario Comun', '17', NULL, 'N;'),
('Usuario Comun', '18', NULL, 'N;'),
('Usuario Comun', '19', NULL, 'N;'),
('Usuario Comun', '21', NULL, 'N;'),
('Usuario Comun', '22', NULL, 'N;'),
('Usuario Comun', '23', NULL, 'N;'),
('Usuario Comun', '24', NULL, 'N;'),
('Usuario Comun', '25', NULL, 'N;'),
('Usuario Comun', '27', NULL, 'N;'),
('Usuario Comun', '30', NULL, 'N;'),
('Usuario Comun', '31', NULL, 'N;'),
('Usuario Comun', '6', NULL, 'N;'),
('Usuario Comun', '7', NULL, 'N;'),
('Usuario Comun', '8', NULL, 'N;'),
('Usuario Comun', '9', NULL, 'N;');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `authitem`
--

CREATE TABLE IF NOT EXISTS `authitem` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcar la base de datos para la tabla `authitem`
--

INSERT INTO `authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('admin', 2, 'Administrador', NULL, 'N;'),
('Admin.Admin.*', 1, NULL, NULL, 'N;'),
('Admin.Admin.Index', 0, NULL, NULL, 'N;'),
('Admin.CentroCosto.*', 1, NULL, NULL, 'N;'),
('Admin.CentroCosto.Admin', 0, NULL, NULL, 'N;'),
('Admin.CentroCosto.Create', 0, NULL, NULL, 'N;'),
('Admin.CentroCosto.Delete', 0, NULL, NULL, 'N;'),
('Admin.CentroCosto.Editable', 0, NULL, NULL, 'N;'),
('Admin.CentroCosto.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.CentroCosto.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.CentroCosto.Index', 0, NULL, NULL, 'N;'),
('Admin.CentroCosto.Update', 0, NULL, NULL, 'N;'),
('Admin.CentroCosto.View', 0, NULL, NULL, 'N;'),
('Admin.Contrato.*', 1, NULL, NULL, 'N;'),
('Admin.Contrato.Admin', 0, NULL, NULL, 'N;'),
('Admin.Contrato.Create', 0, NULL, NULL, 'N;'),
('Admin.Contrato.Delete', 0, NULL, NULL, 'N;'),
('Admin.Contrato.Editable', 0, NULL, NULL, 'N;'),
('Admin.Contrato.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.Contrato.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.Contrato.Index', 0, NULL, NULL, 'N;'),
('Admin.Contrato.Update', 0, NULL, NULL, 'N;'),
('Admin.Contrato.View', 0, NULL, NULL, 'N;'),
('Admin.CuentaContable.*', 1, NULL, NULL, 'N;'),
('Admin.CuentaContable.Admin', 0, NULL, NULL, 'N;'),
('Admin.CuentaContable.Create', 0, NULL, NULL, 'N;'),
('Admin.CuentaContable.Delete', 0, NULL, NULL, 'N;'),
('Admin.CuentaContable.Editable', 0, NULL, NULL, 'N;'),
('Admin.CuentaContable.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.CuentaContable.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.CuentaContable.Index', 0, NULL, NULL, 'N;'),
('Admin.CuentaContable.Update', 0, NULL, NULL, 'N;'),
('Admin.CuentaContable.View', 0, NULL, NULL, 'N;'),
('Admin.CuentaEspecifica.*', 1, NULL, NULL, 'N;'),
('Admin.CuentaEspecifica.Admin', 0, NULL, NULL, 'N;'),
('Admin.CuentaEspecifica.Create', 0, NULL, NULL, 'N;'),
('Admin.CuentaEspecifica.Delete', 0, NULL, NULL, 'N;'),
('Admin.CuentaEspecifica.Editable', 0, NULL, NULL, 'N;'),
('Admin.CuentaEspecifica.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.CuentaEspecifica.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.CuentaEspecifica.Index', 0, NULL, NULL, 'N;'),
('Admin.CuentaEspecifica.Update', 0, NULL, NULL, 'N;'),
('Admin.CuentaEspecifica.View', 0, NULL, NULL, 'N;'),
('Admin.Default.*', 1, NULL, NULL, 'N;'),
('Admin.Default.Index', 0, NULL, NULL, 'N;'),
('Admin.Default.Solicitud', 0, NULL, NULL, 'N;'),
('Admin.Emision.*', 1, NULL, NULL, 'N;'),
('Admin.Emision.Admin', 0, NULL, NULL, 'N;'),
('Admin.Emision.Create', 0, NULL, NULL, 'N;'),
('Admin.Emision.Delete', 0, NULL, NULL, 'N;'),
('Admin.Emision.Editable', 0, NULL, NULL, 'N;'),
('Admin.Emision.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.Emision.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.Emision.Index', 0, NULL, NULL, 'N;'),
('Admin.Emision.Update', 0, NULL, NULL, 'N;'),
('Admin.Emision.View', 0, NULL, NULL, 'N;'),
('Admin.Financiamiento.*', 1, NULL, NULL, 'N;'),
('Admin.Financiamiento.Admin', 0, NULL, NULL, 'N;'),
('Admin.Financiamiento.Create', 0, NULL, NULL, 'N;'),
('Admin.Financiamiento.Delete', 0, NULL, NULL, 'N;'),
('Admin.Financiamiento.Editable', 0, NULL, NULL, 'N;'),
('Admin.Financiamiento.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.Financiamiento.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.Financiamiento.Index', 0, NULL, NULL, 'N;'),
('Admin.Financiamiento.Update', 0, NULL, NULL, 'N;'),
('Admin.Financiamiento.View', 0, NULL, NULL, 'N;'),
('Admin.Ingreso.*', 1, NULL, NULL, 'N;'),
('Admin.Ingreso.Admin', 0, NULL, NULL, 'N;'),
('Admin.Ingreso.Create', 0, NULL, NULL, 'N;'),
('Admin.Ingreso.Delete', 0, NULL, NULL, 'N;'),
('Admin.Ingreso.Editable', 0, NULL, NULL, 'N;'),
('Admin.Ingreso.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.Ingreso.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.Ingreso.Index', 0, NULL, NULL, 'N;'),
('Admin.Ingreso.Update', 0, NULL, NULL, 'N;'),
('Admin.Ingreso.View', 0, NULL, NULL, 'N;'),
('Admin.ItemPresupuesto.*', 1, NULL, NULL, 'N;'),
('Admin.ItemPresupuesto.Admin', 0, NULL, NULL, 'N;'),
('Admin.ItemPresupuesto.Create', 0, NULL, NULL, 'N;'),
('Admin.ItemPresupuesto.Delete', 0, NULL, NULL, 'N;'),
('Admin.ItemPresupuesto.Editable', 0, NULL, NULL, 'N;'),
('Admin.ItemPresupuesto.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.ItemPresupuesto.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.ItemPresupuesto.Index', 0, NULL, NULL, 'N;'),
('Admin.ItemPresupuesto.Update', 0, NULL, NULL, 'N;'),
('Admin.ItemPresupuesto.View', 0, NULL, NULL, 'N;'),
('Admin.ItemRendicion.*', 1, NULL, NULL, 'N;'),
('Admin.ItemRendicion.Admin', 0, NULL, NULL, 'N;'),
('Admin.ItemRendicion.Create', 0, NULL, NULL, 'N;'),
('Admin.ItemRendicion.Delete', 0, NULL, NULL, 'N;'),
('Admin.ItemRendicion.Editable', 0, NULL, NULL, 'N;'),
('Admin.ItemRendicion.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.ItemRendicion.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.ItemRendicion.Index', 0, NULL, NULL, 'N;'),
('Admin.ItemRendicion.Update', 0, NULL, NULL, 'N;'),
('Admin.ItemRendicion.View', 0, NULL, NULL, 'N;'),
('Admin.ItemSolicitud.*', 1, NULL, NULL, 'N;'),
('Admin.ItemSolicitud.Admin', 0, NULL, NULL, 'N;'),
('Admin.ItemSolicitud.Create', 0, NULL, NULL, 'N;'),
('Admin.ItemSolicitud.Delete', 0, NULL, NULL, 'N;'),
('Admin.ItemSolicitud.Editable', 0, NULL, NULL, 'N;'),
('Admin.ItemSolicitud.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.ItemSolicitud.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.ItemSolicitud.Index', 0, NULL, NULL, 'N;'),
('Admin.ItemSolicitud.Update', 0, NULL, NULL, 'N;'),
('Admin.ItemSolicitud.View', 0, NULL, NULL, 'N;'),
('Admin.Presupuesto.*', 1, NULL, NULL, 'N;'),
('Admin.Presupuesto.Admin', 0, NULL, NULL, 'N;'),
('Admin.Presupuesto.Create', 0, NULL, NULL, 'N;'),
('Admin.Presupuesto.Delete', 0, NULL, NULL, 'N;'),
('Admin.Presupuesto.Editable', 0, NULL, NULL, 'N;'),
('Admin.Presupuesto.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.Presupuesto.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.Presupuesto.Index', 0, NULL, NULL, 'N;'),
('Admin.Presupuesto.Update', 0, NULL, NULL, 'N;'),
('Admin.Presupuesto.View', 0, NULL, NULL, 'N;'),
('Admin.Proveedor.*', 1, NULL, NULL, 'N;'),
('Admin.Proveedor.Actualizar', 0, NULL, NULL, 'N;'),
('Admin.Proveedor.Admin', 0, NULL, NULL, 'N;'),
('Admin.Proveedor.Agregar', 0, NULL, NULL, 'N;'),
('Admin.Proveedor.Crear', 0, NULL, NULL, 'N;'),
('Admin.Proveedor.Create', 0, NULL, NULL, 'N;'),
('Admin.Proveedor.Delete', 0, NULL, NULL, 'N;'),
('Admin.Proveedor.Editable', 0, NULL, NULL, 'N;'),
('Admin.Proveedor.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.Proveedor.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.Proveedor.GetTodos', 0, NULL, NULL, 'N;'),
('Admin.Proveedor.Index', 0, NULL, NULL, 'N;'),
('Admin.Proveedor.Update', 0, NULL, NULL, 'N;'),
('Admin.Proveedor.Ver', 0, NULL, NULL, 'N;'),
('Admin.Proveedor.VerTodos', 0, NULL, NULL, 'N;'),
('Admin.Proveedor.View', 0, NULL, NULL, 'N;'),
('Admin.Proyecto.*', 1, NULL, NULL, 'N;'),
('Admin.Proyecto.Admin', 0, NULL, NULL, 'N;'),
('Admin.Proyecto.Create', 0, NULL, NULL, 'N;'),
('Admin.Proyecto.Delete', 0, NULL, NULL, 'N;'),
('Admin.Proyecto.Editable', 0, NULL, NULL, 'N;'),
('Admin.Proyecto.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.Proyecto.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.Proyecto.Index', 0, NULL, NULL, 'N;'),
('Admin.Proyecto.Update', 0, NULL, NULL, 'N;'),
('Admin.Proyecto.View', 0, NULL, NULL, 'N;'),
('Admin.Rendicion.*', 1, NULL, NULL, 'N;'),
('Admin.Rendicion.Admin', 0, NULL, NULL, 'N;'),
('Admin.Rendicion.Create', 0, NULL, NULL, 'N;'),
('Admin.Rendicion.Delete', 0, NULL, NULL, 'N;'),
('Admin.Rendicion.Editable', 0, NULL, NULL, 'N;'),
('Admin.Rendicion.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.Rendicion.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.Rendicion.Index', 0, NULL, NULL, 'N;'),
('Admin.Rendicion.Update', 0, NULL, NULL, 'N;'),
('Admin.Rendicion.View', 0, NULL, NULL, 'N;'),
('Admin.Rrhh.*', 1, NULL, NULL, 'N;'),
('Admin.Rrhh.Admin', 0, NULL, NULL, 'N;'),
('Admin.Rrhh.Create', 0, NULL, NULL, 'N;'),
('Admin.Rrhh.Delete', 0, NULL, NULL, 'N;'),
('Admin.Rrhh.Editable', 0, NULL, NULL, 'N;'),
('Admin.Rrhh.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.Rrhh.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.Rrhh.Index', 0, NULL, NULL, 'N;'),
('Admin.Rrhh.Update', 0, NULL, NULL, 'N;'),
('Admin.Rrhh.View', 0, NULL, NULL, 'N;'),
('Admin.Solicitud.*', 1, NULL, NULL, 'N;'),
('Admin.Solicitud.Admin', 0, NULL, NULL, 'N;'),
('Admin.Solicitud.Create', 0, NULL, NULL, 'N;'),
('Admin.Solicitud.Delete', 0, NULL, NULL, 'N;'),
('Admin.Solicitud.Editable', 0, NULL, NULL, 'N;'),
('Admin.Solicitud.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.Solicitud.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.Solicitud.Index', 0, NULL, NULL, 'N;'),
('Admin.Solicitud.Update', 0, NULL, NULL, 'N;'),
('Admin.Solicitud.View', 0, NULL, NULL, 'N;'),
('Admin.TipoContrato.*', 1, NULL, NULL, 'N;'),
('Admin.TipoContrato.Admin', 0, NULL, NULL, 'N;'),
('Admin.TipoContrato.Create', 0, NULL, NULL, 'N;'),
('Admin.TipoContrato.Delete', 0, NULL, NULL, 'N;'),
('Admin.TipoContrato.Editable', 0, NULL, NULL, 'N;'),
('Admin.TipoContrato.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.TipoContrato.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.TipoContrato.Index', 0, NULL, NULL, 'N;'),
('Admin.TipoContrato.Update', 0, NULL, NULL, 'N;'),
('Admin.TipoContrato.View', 0, NULL, NULL, 'N;'),
('Admin.TipoIngreso.*', 1, NULL, NULL, 'N;'),
('Admin.TipoIngreso.Admin', 0, NULL, NULL, 'N;'),
('Admin.TipoIngreso.Create', 0, NULL, NULL, 'N;'),
('Admin.TipoIngreso.Delete', 0, NULL, NULL, 'N;'),
('Admin.TipoIngreso.Editable', 0, NULL, NULL, 'N;'),
('Admin.TipoIngreso.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.TipoIngreso.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.TipoIngreso.Index', 0, NULL, NULL, 'N;'),
('Admin.TipoIngreso.Update', 0, NULL, NULL, 'N;'),
('Admin.TipoIngreso.View', 0, NULL, NULL, 'N;'),
('Admin.User.*', 1, NULL, NULL, 'N;'),
('Admin.User.Admin', 0, NULL, NULL, 'N;'),
('Admin.User.ChangePassword', 0, NULL, NULL, 'N;'),
('Admin.User.Create', 0, NULL, NULL, 'N;'),
('Admin.User.Delete', 0, NULL, NULL, 'N;'),
('Admin.User.Editable', 0, NULL, NULL, 'N;'),
('Admin.User.GenerateExcel', 0, NULL, NULL, 'N;'),
('Admin.User.GeneratePdf', 0, NULL, NULL, 'N;'),
('Admin.User.Index', 0, NULL, NULL, 'N;'),
('Admin.User.Update', 0, NULL, NULL, 'N;'),
('Admin.User.View', 0, NULL, NULL, 'N;'),
('Afp.*', 1, NULL, NULL, 'N;'),
('Afp.Actualizar', 0, NULL, NULL, 'N;'),
('Afp.Admin', 0, NULL, NULL, 'N;'),
('Afp.Crear', 0, NULL, NULL, 'N;'),
('Afp.Create', 0, NULL, NULL, 'N;'),
('Afp.Delete', 0, NULL, NULL, 'N;'),
('Afp.GenerateExcel', 0, NULL, NULL, 'N;'),
('Afp.GeneratePdf', 0, NULL, NULL, 'N;'),
('Afp.Index', 0, NULL, NULL, 'N;'),
('Afp.Update', 0, NULL, NULL, 'N;'),
('Afp.Ver', 0, NULL, NULL, 'N;'),
('Afp.VerTodos', 0, NULL, NULL, 'N;'),
('Afp.View', 0, NULL, NULL, 'N;'),
('CentroCosto.*', 1, NULL, NULL, 'N;'),
('CentroCosto.Actualizar', 0, NULL, NULL, 'N;'),
('CentroCosto.Admin', 0, NULL, NULL, 'N;'),
('CentroCosto.Crear', 0, NULL, NULL, 'N;'),
('CentroCosto.Create', 0, NULL, NULL, 'N;'),
('CentroCosto.Delete', 0, NULL, NULL, 'N;'),
('CentroCosto.GenerateExcel', 0, NULL, NULL, 'N;'),
('CentroCosto.GeneratePdf', 0, NULL, NULL, 'N;'),
('CentroCosto.Index', 0, NULL, NULL, 'N;'),
('CentroCosto.Update', 0, NULL, NULL, 'N;'),
('CentroCosto.Ver', 0, NULL, NULL, 'N;'),
('CentroCosto.VerTodos', 0, NULL, NULL, 'N;'),
('CentroCosto.View', 0, NULL, NULL, 'N;'),
('Contrato.*', 1, NULL, NULL, 'N;'),
('Contrato.Actualizar', 0, NULL, NULL, 'N;'),
('Contrato.Admin', 0, NULL, NULL, 'N;'),
('Contrato.Crear', 0, NULL, NULL, 'N;'),
('Contrato.Create', 0, NULL, NULL, 'N;'),
('Contrato.Delete', 0, NULL, NULL, 'N;'),
('Contrato.Finalizar', 0, NULL, NULL, 'N;'),
('Contrato.GenerateExcel', 0, NULL, NULL, 'N;'),
('Contrato.GeneratePdf', 0, NULL, NULL, 'N;'),
('Contrato.Index', 0, NULL, NULL, 'N;'),
('Contrato.Update', 0, NULL, NULL, 'N;'),
('Contrato.Ver', 0, NULL, NULL, 'N;'),
('Contrato.VerRrhh', 0, NULL, NULL, 'N;'),
('Contrato.VerTodos', 0, NULL, NULL, 'N;'),
('Contrato.View', 0, NULL, NULL, 'N;'),
('CuentaContable.*', 1, NULL, NULL, 'N;'),
('CuentaContable.Actualizar', 0, NULL, NULL, 'N;'),
('CuentaContable.Admin', 0, NULL, NULL, 'N;'),
('CuentaContable.Crear', 0, NULL, NULL, 'N;'),
('CuentaContable.Create', 0, NULL, NULL, 'N;'),
('CuentaContable.Delete', 0, NULL, NULL, 'N;'),
('CuentaContable.GenerateExcel', 0, NULL, NULL, 'N;'),
('CuentaContable.GeneratePdf', 0, NULL, NULL, 'N;'),
('CuentaContable.Index', 0, NULL, NULL, 'N;'),
('CuentaContable.Update', 0, NULL, NULL, 'N;'),
('CuentaContable.Ver', 0, NULL, NULL, 'N;'),
('CuentaContable.VerTodos', 0, NULL, NULL, 'N;'),
('CuentaContable.View', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.*', 1, NULL, NULL, 'N;'),
('CuentaEspecifica.Actualizar', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.Admin', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.Crear', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.Create', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.Delete', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.GenerateExcel', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.GeneratePdf', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.Index', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.Update', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.Ver', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.VerTodos', 0, NULL, NULL, 'N;'),
('CuentaEspecifica.View', 0, NULL, NULL, 'N;'),
('Director Ejecutivo', 2, 'Director Ejecutivo', NULL, 'N;'),
('Donante.*', 1, NULL, NULL, 'N;'),
('Donante.Admin', 0, NULL, NULL, 'N;'),
('Donante.Create', 0, NULL, NULL, 'N;'),
('Donante.Delete', 0, NULL, NULL, 'N;'),
('Donante.GenerateExcel', 0, NULL, NULL, 'N;'),
('Donante.GeneratePdf', 0, NULL, NULL, 'N;'),
('Donante.Index', 0, NULL, NULL, 'N;'),
('Donante.Update', 0, NULL, NULL, 'N;'),
('Donante.View', 0, NULL, NULL, 'N;'),
('Emision.*', 1, NULL, NULL, 'N;'),
('Emision.Actualizar', 0, NULL, NULL, 'N;'),
('Emision.Admin', 0, NULL, NULL, 'N;'),
('Emision.Anular', 0, NULL, NULL, 'N;'),
('Emision.Crear', 0, NULL, NULL, 'N;'),
('Emision.Create', 0, NULL, NULL, 'N;'),
('Emision.Delete', 0, NULL, NULL, 'N;'),
('Emision.Eliminar', 0, NULL, NULL, 'N;'),
('Emision.Finalizadas', 0, NULL, NULL, 'N;'),
('Emision.Finalizar', 0, NULL, NULL, 'N;'),
('Emision.GenerateExcel', 0, NULL, NULL, 'N;'),
('Emision.GeneratePdf', 0, NULL, NULL, 'N;'),
('Emision.Index', 0, NULL, NULL, 'N;'),
('Emision.Update', 0, NULL, NULL, 'N;'),
('Emision.Ver', 0, NULL, NULL, 'N;'),
('Emision.VerEmisionRendicion', 0, NULL, NULL, 'N;'),
('Emision.VerTodos', 0, NULL, NULL, 'N;'),
('Emision.View', 0, NULL, NULL, 'N;'),
('Financiamiento.*', 1, NULL, NULL, 'N;'),
('Financiamiento.Actualizar', 0, NULL, NULL, 'N;'),
('Financiamiento.Admin', 0, NULL, NULL, 'N;'),
('Financiamiento.Crear', 0, NULL, NULL, 'N;'),
('Financiamiento.Create', 0, NULL, NULL, 'N;'),
('Financiamiento.Delete', 0, NULL, NULL, 'N;'),
('Financiamiento.GenerateExcel', 0, NULL, NULL, 'N;'),
('Financiamiento.GeneratePdf', 0, NULL, NULL, 'N;'),
('Financiamiento.Index', 0, NULL, NULL, 'N;'),
('Financiamiento.Update', 0, NULL, NULL, 'N;'),
('Financiamiento.Ver', 0, NULL, NULL, 'N;'),
('Financiamiento.VerTodos', 0, NULL, NULL, 'N;'),
('Financiamiento.View', 0, NULL, NULL, 'N;'),
('Informe.*', 1, NULL, NULL, 'N;'),
('Informe.Crear', 0, NULL, NULL, 'N;'),
('Informe.Test', 0, NULL, NULL, 'N;'),
('Ingreso.*', 1, NULL, NULL, 'N;'),
('Ingreso.Actualizar', 0, NULL, NULL, 'N;'),
('Ingreso.Admin', 0, NULL, NULL, 'N;'),
('Ingreso.Crear', 0, NULL, NULL, 'N;'),
('Ingreso.Create', 0, NULL, NULL, 'N;'),
('Ingreso.Delete', 0, NULL, NULL, 'N;'),
('Ingreso.GenerateExcel', 0, NULL, NULL, 'N;'),
('Ingreso.GeneratePdf', 0, NULL, NULL, 'N;'),
('Ingreso.Index', 0, NULL, NULL, 'N;'),
('Ingreso.Update', 0, NULL, NULL, 'N;'),
('Ingreso.Ver', 0, NULL, NULL, 'N;'),
('Ingreso.VerTodos', 0, NULL, NULL, 'N;'),
('Ingreso.View', 0, NULL, NULL, 'N;'),
('IngresoProyecto.*', 1, NULL, NULL, 'N;'),
('IngresoProyecto.Admin', 0, NULL, NULL, 'N;'),
('IngresoProyecto.Create', 0, NULL, NULL, 'N;'),
('IngresoProyecto.Delete', 0, NULL, NULL, 'N;'),
('IngresoProyecto.GenerateExcel', 0, NULL, NULL, 'N;'),
('IngresoProyecto.GeneratePdf', 0, NULL, NULL, 'N;'),
('IngresoProyecto.Index', 0, NULL, NULL, 'N;'),
('IngresoProyecto.Update', 0, NULL, NULL, 'N;'),
('IngresoProyecto.View', 0, NULL, NULL, 'N;'),
('Inventario.*', 1, NULL, NULL, 'N;'),
('Inventario.Admin', 0, NULL, NULL, 'N;'),
('Inventario.Create', 0, NULL, NULL, 'N;'),
('Inventario.Delete', 0, NULL, NULL, 'N;'),
('Inventario.GenerateExcel', 0, NULL, NULL, 'N;'),
('Inventario.GeneratePdf', 0, NULL, NULL, 'N;'),
('Inventario.Index', 0, NULL, NULL, 'N;'),
('Inventario.Update', 0, NULL, NULL, 'N;'),
('Inventario.View', 0, NULL, NULL, 'N;'),
('ItemEmision.*', 1, NULL, NULL, 'N;'),
('ItemEmision.Admin', 0, NULL, NULL, 'N;'),
('ItemEmision.Agregar', 0, NULL, NULL, 'N;'),
('ItemEmision.Create', 0, NULL, NULL, 'N;'),
('ItemEmision.Delete', 0, NULL, NULL, 'N;'),
('ItemEmision.GenerateExcel', 0, NULL, NULL, 'N;'),
('ItemEmision.GeneratePdf', 0, NULL, NULL, 'N;'),
('ItemEmision.Index', 0, NULL, NULL, 'N;'),
('ItemEmision.Update', 0, NULL, NULL, 'N;'),
('ItemEmision.View', 0, NULL, NULL, 'N;'),
('ItemPresupuesto.*', 1, NULL, NULL, 'N;'),
('ItemPresupuesto.Actualizar', 0, NULL, NULL, 'N;'),
('ItemPresupuesto.ActualizarProyecto', 0, NULL, NULL, 'N;'),
('ItemPresupuesto.Admin', 0, NULL, NULL, 'N;'),
('ItemPresupuesto.Crear', 0, NULL, NULL, 'N;'),
('ItemPresupuesto.CrearProyecto', 0, NULL, NULL, 'N;'),
('ItemPresupuesto.Delete', 0, NULL, NULL, 'N;'),
('ItemPresupuesto.GenerateExcel', 0, NULL, NULL, 'N;'),
('ItemPresupuesto.GeneratePdf', 0, NULL, NULL, 'N;'),
('ItemPresupuesto.Index', 0, NULL, NULL, 'N;'),
('ItemPresupuesto.ItemPresupuestoVsItemRendicion', 0, NULL, NULL, 'N;'),
('ItemPresupuesto.Update', 0, NULL, NULL, 'N;'),
('ItemPresupuesto.Ver', 0, NULL, NULL, 'N;'),
('ItemPresupuesto.VerProyecto', 0, NULL, NULL, 'N;'),
('ItemPresupuesto.VerTodos', 0, NULL, NULL, 'N;'),
('ItemPresupuesto.View', 0, NULL, NULL, 'N;'),
('ItemRendicion.*', 1, NULL, NULL, 'N;'),
('ItemRendicion.Actualizar', 0, NULL, NULL, 'N;'),
('ItemRendicion.Admin', 0, NULL, NULL, 'N;'),
('ItemRendicion.Agregar', 0, NULL, NULL, 'N;'),
('ItemRendicion.Crear', 0, NULL, NULL, 'N;'),
('ItemRendicion.Create', 0, NULL, NULL, 'N;'),
('ItemRendicion.Delete', 0, NULL, NULL, 'N;'),
('ItemRendicion.GenerateExcel', 0, NULL, NULL, 'N;'),
('ItemRendicion.GeneratePdf', 0, NULL, NULL, 'N;'),
('ItemRendicion.GetCuenta', 0, NULL, NULL, 'N;'),
('ItemRendicion.Index', 0, NULL, NULL, 'N;'),
('ItemRendicion.Update', 0, NULL, NULL, 'N;'),
('ItemRendicion.Ver', 0, NULL, NULL, 'N;'),
('ItemRendicion.VerTodos', 0, NULL, NULL, 'N;'),
('ItemRendicion.View', 0, NULL, NULL, 'N;'),
('ItemSolicitud.*', 1, NULL, NULL, 'N;'),
('ItemSolicitud.Actualizar', 0, NULL, NULL, 'N;'),
('ItemSolicitud.Admin', 0, NULL, NULL, 'N;'),
('ItemSolicitud.Agregar', 0, NULL, NULL, 'N;'),
('ItemSolicitud.Create', 0, NULL, NULL, 'N;'),
('ItemSolicitud.Delete', 0, NULL, NULL, 'N;'),
('ItemSolicitud.GenerateExcel', 0, NULL, NULL, 'N;'),
('ItemSolicitud.GeneratePdf', 0, NULL, NULL, 'N;'),
('ItemSolicitud.Index', 0, NULL, NULL, 'N;'),
('ItemSolicitud.Update', 0, NULL, NULL, 'N;'),
('ItemSolicitud.Ver', 0, NULL, NULL, 'N;'),
('ItemSolicitud.View', 0, NULL, NULL, 'N;'),
('Jefe de Taller', 2, 'Jefe de Taller', NULL, 'N;'),
('LibroCompra.*', 1, NULL, NULL, 'N;'),
('LibroCompra.Admin', 0, NULL, NULL, 'N;'),
('LibroCompra.Create', 0, NULL, NULL, 'N;'),
('LibroCompra.Delete', 0, NULL, NULL, 'N;'),
('LibroCompra.GenerateExcel', 0, NULL, NULL, 'N;'),
('LibroCompra.GeneratePdf', 0, NULL, NULL, 'N;'),
('LibroCompra.Index', 0, NULL, NULL, 'N;'),
('LibroCompra.Update', 0, NULL, NULL, 'N;'),
('LibroCompra.VerTodos', 0, NULL, NULL, 'N;'),
('LibroCompra.View', 0, NULL, NULL, 'N;'),
('Presupuesto.*', 1, NULL, NULL, 'N;'),
('Presupuesto.Actualizar', 0, NULL, NULL, 'N;'),
('Presupuesto.Admin', 0, NULL, NULL, 'N;'),
('Presupuesto.Crear', 0, NULL, NULL, 'N;'),
('Presupuesto.Create', 0, NULL, NULL, 'N;'),
('Presupuesto.Delete', 0, NULL, NULL, 'N;'),
('Presupuesto.GenerateExcel', 0, NULL, NULL, 'N;'),
('Presupuesto.GeneratePdf', 0, NULL, NULL, 'N;'),
('Presupuesto.Index', 0, NULL, NULL, 'N;'),
('Presupuesto.Update', 0, NULL, NULL, 'N;'),
('Presupuesto.Ver', 0, NULL, NULL, 'N;'),
('Presupuesto.VerProyecto', 0, NULL, NULL, 'N;'),
('Presupuesto.VerTodos', 0, NULL, NULL, 'N;'),
('Presupuesto.View', 0, NULL, NULL, 'N;'),
('Proveedor.*', 1, NULL, NULL, 'N;'),
('Proveedor.Actualizar', 0, NULL, NULL, 'N;'),
('Proveedor.Admin', 0, NULL, NULL, 'N;'),
('Proveedor.Agregar', 0, NULL, NULL, 'N;'),
('Proveedor.Crear', 0, NULL, NULL, 'N;'),
('Proveedor.Create', 0, NULL, NULL, 'N;'),
('Proveedor.Delete', 0, NULL, NULL, 'N;'),
('Proveedor.GenerateExcel', 0, NULL, NULL, 'N;'),
('Proveedor.GeneratePdf', 0, NULL, NULL, 'N;'),
('Proveedor.GetTodos', 0, NULL, NULL, 'N;'),
('Proveedor.Index', 0, NULL, NULL, 'N;'),
('Proveedor.Update', 0, NULL, NULL, 'N;'),
('Proveedor.Ver', 0, NULL, NULL, 'N;'),
('Proveedor.VerTodos', 0, NULL, NULL, 'N;'),
('Proveedor.View', 0, NULL, NULL, 'N;'),
('Proyecto.*', 1, NULL, NULL, 'N;'),
('Proyecto.Actualizar', 0, NULL, NULL, 'N;'),
('Proyecto.Admin', 0, NULL, NULL, 'N;'),
('Proyecto.Crear', 0, NULL, NULL, 'N;'),
('Proyecto.Create', 0, NULL, NULL, 'N;'),
('Proyecto.Delete', 0, NULL, NULL, 'N;'),
('Proyecto.GenerateExcel', 0, NULL, NULL, 'N;'),
('Proyecto.GeneratePdf', 0, NULL, NULL, 'N;'),
('Proyecto.GetComuna', 0, NULL, NULL, 'N;'),
('Proyecto.Index', 0, NULL, NULL, 'N;'),
('Proyecto.PresupuestoVsIngreso', 0, NULL, NULL, 'N;'),
('Proyecto.Update', 0, NULL, NULL, 'N;'),
('Proyecto.Ver', 0, NULL, NULL, 'N;'),
('Proyecto.VerPresupuesto', 0, NULL, NULL, 'N;'),
('Proyecto.VerTodos', 0, NULL, NULL, 'N;'),
('Proyecto.View', 0, NULL, NULL, 'N;'),
('Rendicion.*', 1, NULL, NULL, 'N;'),
('Rendicion.Actualizar', 0, NULL, NULL, 'N;'),
('Rendicion.Admin', 0, NULL, NULL, 'N;'),
('Rendicion.Anular', 0, NULL, NULL, 'N;'),
('Rendicion.Crear', 0, NULL, NULL, 'N;'),
('Rendicion.Create', 0, NULL, NULL, 'N;'),
('Rendicion.Delete', 0, NULL, NULL, 'N;'),
('Rendicion.Finalizar', 0, NULL, NULL, 'N;'),
('Rendicion.GenerateExcel', 0, NULL, NULL, 'N;'),
('Rendicion.GeneratePdf', 0, NULL, NULL, 'N;'),
('Rendicion.Index', 0, NULL, NULL, 'N;'),
('Rendicion.Update', 0, NULL, NULL, 'N;'),
('Rendicion.Ver', 0, NULL, NULL, 'N;'),
('Rendicion.VerTodos', 0, NULL, NULL, 'N;'),
('Rendicion.View', 0, NULL, NULL, 'N;'),
('Rrhh.*', 1, NULL, NULL, 'N;'),
('Rrhh.Actualizar', 0, NULL, NULL, 'N;'),
('Rrhh.Admin', 0, NULL, NULL, 'N;'),
('Rrhh.Crear', 0, NULL, NULL, 'N;'),
('Rrhh.Create', 0, NULL, NULL, 'N;'),
('Rrhh.Delete', 0, NULL, NULL, 'N;'),
('Rrhh.GenerateExcel', 0, NULL, NULL, 'N;'),
('Rrhh.GeneratePdf', 0, NULL, NULL, 'N;'),
('Rrhh.Index', 0, NULL, NULL, 'N;'),
('Rrhh.Ver', 0, NULL, NULL, 'N;'),
('Rrhh.VerContrato', 0, NULL, NULL, 'N;'),
('Rrhh.VerTodos', 0, NULL, NULL, 'N;'),
('Rrhh.View', 0, NULL, NULL, 'N;'),
('Site.*', 1, NULL, NULL, 'N;'),
('Site.Contact', 0, NULL, NULL, 'N;'),
('Site.Error', 0, NULL, NULL, 'N;'),
('Site.Index', 0, NULL, NULL, 'N;'),
('Site.Login', 0, NULL, NULL, 'N;'),
('Site.Logout', 0, NULL, NULL, 'N;'),
('Solicitud.*', 1, NULL, NULL, 'N;'),
('Solicitud.Actualizar', 0, NULL, NULL, 'N;'),
('Solicitud.Admin', 0, NULL, NULL, 'N;'),
('Solicitud.Anular', 0, NULL, NULL, 'N;'),
('Solicitud.Crear', 0, NULL, NULL, 'N;'),
('Solicitud.CrearSolicitud', 0, NULL, NULL, 'N;'),
('Solicitud.Create', 0, NULL, NULL, 'N;'),
('Solicitud.Delete', 0, NULL, NULL, 'N;'),
('Solicitud.Finalizadas', 0, NULL, NULL, 'N;'),
('Solicitud.Finalizar', 0, NULL, NULL, 'N;'),
('Solicitud.GenerateExcel', 0, NULL, NULL, 'N;'),
('Solicitud.GeneratePdf', 0, NULL, NULL, 'N;'),
('Solicitud.Index', 0, NULL, NULL, 'N;'),
('Solicitud.Update', 0, NULL, NULL, 'N;'),
('Solicitud.Ver', 0, NULL, NULL, 'N;'),
('Solicitud.VerSolicitudEmision', 0, NULL, NULL, 'N;'),
('Solicitud.VerTodos', 0, NULL, NULL, 'N;'),
('Solicitud.View', 0, NULL, NULL, 'N;'),
('TipoContrato.*', 1, NULL, NULL, 'N;'),
('TipoContrato.Actualizar', 0, NULL, NULL, 'N;'),
('TipoContrato.Admin', 0, NULL, NULL, 'N;'),
('TipoContrato.Crear', 0, NULL, NULL, 'N;'),
('TipoContrato.Create', 0, NULL, NULL, 'N;'),
('TipoContrato.Delete', 0, NULL, NULL, 'N;'),
('TipoContrato.GenerateExcel', 0, NULL, NULL, 'N;'),
('TipoContrato.GeneratePdf', 0, NULL, NULL, 'N;'),
('TipoContrato.Index', 0, NULL, NULL, 'N;'),
('TipoContrato.Update', 0, NULL, NULL, 'N;'),
('TipoContrato.Ver', 0, NULL, NULL, 'N;'),
('TipoContrato.VerTodos', 0, NULL, NULL, 'N;'),
('TipoContrato.View', 0, NULL, NULL, 'N;'),
('TipoIngreso.*', 1, NULL, NULL, 'N;'),
('TipoIngreso.Actualizar', 0, NULL, NULL, 'N;'),
('TipoIngreso.Admin', 0, NULL, NULL, 'N;'),
('TipoIngreso.Crear', 0, NULL, NULL, 'N;'),
('TipoIngreso.Create', 0, NULL, NULL, 'N;'),
('TipoIngreso.Delete', 0, NULL, NULL, 'N;'),
('TipoIngreso.GenerateExcel', 0, NULL, NULL, 'N;'),
('TipoIngreso.GeneratePdf', 0, NULL, NULL, 'N;'),
('TipoIngreso.Index', 0, NULL, NULL, 'N;'),
('TipoIngreso.Update', 0, NULL, NULL, 'N;'),
('TipoIngreso.Ver', 0, NULL, NULL, 'N;'),
('TipoIngreso.VerTodos', 0, NULL, NULL, 'N;'),
('TipoIngreso.View', 0, NULL, NULL, 'N;'),
('User.*', 1, NULL, NULL, 'N;'),
('User.Admin', 0, NULL, NULL, 'N;'),
('User.ChangePassword', 0, NULL, NULL, 'N;'),
('User.CrearRrhh', 0, NULL, NULL, 'N;'),
('User.Create', 0, NULL, NULL, 'N;'),
('User.Delete', 0, NULL, NULL, 'N;'),
('User.GenerateExcel', 0, NULL, NULL, 'N;'),
('User.GeneratePdf', 0, NULL, NULL, 'N;'),
('User.Index', 0, NULL, NULL, 'N;'),
('User.Update', 0, NULL, NULL, 'N;'),
('User.View', 0, NULL, NULL, 'N;'),
('Usuario Comun', 2, 'Usuario Común', NULL, 'N;');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `authitemchild`
--

CREATE TABLE IF NOT EXISTS `authitemchild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcar la base de datos para la tabla `authitemchild`
--

INSERT INTO `authitemchild` (`parent`, `child`) VALUES
('Jefe de Taller', 'Admin.User.ChangePassword'),
('Director Ejecutivo', 'CentroCosto.Actualizar'),
('Director Ejecutivo', 'CentroCosto.Crear'),
('Director Ejecutivo', 'CentroCosto.GenerateExcel'),
('Director Ejecutivo', 'CentroCosto.GeneratePdf'),
('Director Ejecutivo', 'CentroCosto.Ver'),
('Director Ejecutivo', 'CentroCosto.VerTodos'),
('Director Ejecutivo', 'CuentaContable.Crear'),
('Jefe de Taller', 'CuentaContable.Crear'),
('Usuario Comun', 'CuentaContable.Crear'),
('Director Ejecutivo', 'CuentaContable.GenerateExcel'),
('Jefe de Taller', 'CuentaContable.GenerateExcel'),
('Usuario Comun', 'CuentaContable.GenerateExcel'),
('Director Ejecutivo', 'CuentaContable.GeneratePdf'),
('Jefe de Taller', 'CuentaContable.GeneratePdf'),
('Usuario Comun', 'CuentaContable.GeneratePdf'),
('Director Ejecutivo', 'CuentaContable.Ver'),
('Jefe de Taller', 'CuentaContable.Ver'),
('Usuario Comun', 'CuentaContable.Ver'),
('Director Ejecutivo', 'CuentaContable.VerTodos'),
('Jefe de Taller', 'CuentaContable.VerTodos'),
('Usuario Comun', 'CuentaContable.VerTodos'),
('Director Ejecutivo', 'CuentaEspecifica.Actualizar'),
('Director Ejecutivo', 'CuentaEspecifica.Crear'),
('Director Ejecutivo', 'CuentaEspecifica.GenerateExcel'),
('Jefe de Taller', 'CuentaEspecifica.GenerateExcel'),
('Usuario Comun', 'CuentaEspecifica.GenerateExcel'),
('Director Ejecutivo', 'CuentaEspecifica.GeneratePdf'),
('Jefe de Taller', 'CuentaEspecifica.GeneratePdf'),
('Usuario Comun', 'CuentaEspecifica.GeneratePdf'),
('Director Ejecutivo', 'CuentaEspecifica.Ver'),
('Jefe de Taller', 'CuentaEspecifica.Ver'),
('Usuario Comun', 'CuentaEspecifica.Ver'),
('Director Ejecutivo', 'Emision.Actualizar'),
('Jefe de Taller', 'Emision.Actualizar'),
('Usuario Comun', 'Emision.Actualizar'),
('Director Ejecutivo', 'Emision.Anular'),
('Jefe de Taller', 'Emision.Anular'),
('Usuario Comun', 'Emision.Anular'),
('Director Ejecutivo', 'Emision.Crear'),
('Jefe de Taller', 'Emision.Crear'),
('Usuario Comun', 'Emision.Crear'),
('Director Ejecutivo', 'Emision.Finalizadas'),
('Jefe de Taller', 'Emision.Finalizadas'),
('Usuario Comun', 'Emision.Finalizadas'),
('Director Ejecutivo', 'Emision.Finalizar'),
('Jefe de Taller', 'Emision.Finalizar'),
('Usuario Comun', 'Emision.Finalizar'),
('Director Ejecutivo', 'Emision.GenerateExcel'),
('Jefe de Taller', 'Emision.GenerateExcel'),
('Usuario Comun', 'Emision.GenerateExcel'),
('Director Ejecutivo', 'Emision.GeneratePdf'),
('Jefe de Taller', 'Emision.GeneratePdf'),
('Usuario Comun', 'Emision.GeneratePdf'),
('Director Ejecutivo', 'Emision.Ver'),
('Jefe de Taller', 'Emision.Ver'),
('Usuario Comun', 'Emision.Ver'),
('Director Ejecutivo', 'Emision.VerEmisionRendicion'),
('Jefe de Taller', 'Emision.VerEmisionRendicion'),
('Usuario Comun', 'Emision.VerEmisionRendicion'),
('Director Ejecutivo', 'Emision.VerTodos'),
('Jefe de Taller', 'Emision.VerTodos'),
('Usuario Comun', 'Emision.VerTodos'),
('Director Ejecutivo', 'ItemRendicion.Actualizar'),
('Jefe de Taller', 'ItemRendicion.Actualizar'),
('Usuario Comun', 'ItemRendicion.Actualizar'),
('Director Ejecutivo', 'ItemRendicion.Agregar'),
('Jefe de Taller', 'ItemRendicion.Agregar'),
('Usuario Comun', 'ItemRendicion.Agregar'),
('Director Ejecutivo', 'ItemRendicion.GenerateExcel'),
('Jefe de Taller', 'ItemRendicion.GenerateExcel'),
('Usuario Comun', 'ItemRendicion.GenerateExcel'),
('Director Ejecutivo', 'ItemRendicion.GeneratePdf'),
('Jefe de Taller', 'ItemRendicion.GeneratePdf'),
('Usuario Comun', 'ItemRendicion.GeneratePdf'),
('Director Ejecutivo', 'ItemRendicion.GetCuenta'),
('Jefe de Taller', 'ItemRendicion.GetCuenta'),
('Usuario Comun', 'ItemRendicion.GetCuenta'),
('Director Ejecutivo', 'ItemRendicion.Ver'),
('Jefe de Taller', 'ItemRendicion.Ver'),
('Usuario Comun', 'ItemRendicion.Ver'),
('Director Ejecutivo', 'ItemSolicitud.Actualizar'),
('Jefe de Taller', 'ItemSolicitud.Actualizar'),
('Usuario Comun', 'ItemSolicitud.Actualizar'),
('Director Ejecutivo', 'ItemSolicitud.Agregar'),
('Jefe de Taller', 'ItemSolicitud.Agregar'),
('Usuario Comun', 'ItemSolicitud.Agregar'),
('Director Ejecutivo', 'ItemSolicitud.GenerateExcel'),
('Jefe de Taller', 'ItemSolicitud.GenerateExcel'),
('Usuario Comun', 'ItemSolicitud.GenerateExcel'),
('Director Ejecutivo', 'ItemSolicitud.GeneratePdf'),
('Jefe de Taller', 'ItemSolicitud.GeneratePdf'),
('Usuario Comun', 'ItemSolicitud.GeneratePdf'),
('Director Ejecutivo', 'ItemSolicitud.Ver'),
('Jefe de Taller', 'ItemSolicitud.Ver'),
('Usuario Comun', 'ItemSolicitud.Ver'),
('Director Ejecutivo', 'Proveedor.Actualizar'),
('Jefe de Taller', 'Proveedor.Actualizar'),
('Usuario Comun', 'Proveedor.Actualizar'),
('Director Ejecutivo', 'Proveedor.Agregar'),
('Jefe de Taller', 'Proveedor.Agregar'),
('Usuario Comun', 'Proveedor.Agregar'),
('Director Ejecutivo', 'Proveedor.Crear'),
('Jefe de Taller', 'Proveedor.Crear'),
('Usuario Comun', 'Proveedor.Crear'),
('Director Ejecutivo', 'Proveedor.GenerateExcel'),
('Jefe de Taller', 'Proveedor.GenerateExcel'),
('Usuario Comun', 'Proveedor.GenerateExcel'),
('Director Ejecutivo', 'Proveedor.GeneratePdf'),
('Jefe de Taller', 'Proveedor.GeneratePdf'),
('Usuario Comun', 'Proveedor.GeneratePdf'),
('Director Ejecutivo', 'Proveedor.GetTodos'),
('Jefe de Taller', 'Proveedor.GetTodos'),
('Usuario Comun', 'Proveedor.GetTodos'),
('Director Ejecutivo', 'Proveedor.Ver'),
('Jefe de Taller', 'Proveedor.Ver'),
('Usuario Comun', 'Proveedor.Ver'),
('Director Ejecutivo', 'Proveedor.VerTodos'),
('Jefe de Taller', 'Proveedor.VerTodos'),
('Usuario Comun', 'Proveedor.VerTodos'),
('Director Ejecutivo', 'Proyecto.Actualizar'),
('Jefe de Taller', 'Proyecto.Actualizar'),
('Usuario Comun', 'Proyecto.Actualizar'),
('Director Ejecutivo', 'Proyecto.Crear'),
('Jefe de Taller', 'Proyecto.Crear'),
('Usuario Comun', 'Proyecto.Crear'),
('Director Ejecutivo', 'Proyecto.GenerateExcel'),
('Jefe de Taller', 'Proyecto.GenerateExcel'),
('Usuario Comun', 'Proyecto.GenerateExcel'),
('Director Ejecutivo', 'Proyecto.GeneratePdf'),
('Jefe de Taller', 'Proyecto.GeneratePdf'),
('Usuario Comun', 'Proyecto.GeneratePdf'),
('Director Ejecutivo', 'Proyecto.GetComuna'),
('Jefe de Taller', 'Proyecto.GetComuna'),
('Usuario Comun', 'Proyecto.GetComuna'),
('Director Ejecutivo', 'Proyecto.Ver'),
('Jefe de Taller', 'Proyecto.Ver'),
('Usuario Comun', 'Proyecto.Ver'),
('Director Ejecutivo', 'Proyecto.VerTodos'),
('Jefe de Taller', 'Proyecto.VerTodos'),
('Usuario Comun', 'Proyecto.VerTodos'),
('Director Ejecutivo', 'Rendicion.Actualizar'),
('Jefe de Taller', 'Rendicion.Actualizar'),
('Usuario Comun', 'Rendicion.Actualizar'),
('Director Ejecutivo', 'Rendicion.Anular'),
('Jefe de Taller', 'Rendicion.Anular'),
('Usuario Comun', 'Rendicion.Anular'),
('Director Ejecutivo', 'Rendicion.Crear'),
('Jefe de Taller', 'Rendicion.Crear'),
('Usuario Comun', 'Rendicion.Crear'),
('Director Ejecutivo', 'Rendicion.Finalizar'),
('Jefe de Taller', 'Rendicion.Finalizar'),
('Usuario Comun', 'Rendicion.Finalizar'),
('Director Ejecutivo', 'Rendicion.GenerateExcel'),
('Jefe de Taller', 'Rendicion.GenerateExcel'),
('Usuario Comun', 'Rendicion.GenerateExcel'),
('Director Ejecutivo', 'Rendicion.GeneratePdf'),
('Jefe de Taller', 'Rendicion.GeneratePdf'),
('Usuario Comun', 'Rendicion.GeneratePdf'),
('Director Ejecutivo', 'Rendicion.Ver'),
('Jefe de Taller', 'Rendicion.Ver'),
('Usuario Comun', 'Rendicion.Ver'),
('Director Ejecutivo', 'Rendicion.VerTodos'),
('Jefe de Taller', 'Rendicion.VerTodos'),
('Usuario Comun', 'Rendicion.VerTodos'),
('Director Ejecutivo', 'Rrhh.Actualizar'),
('Jefe de Taller', 'Rrhh.Actualizar'),
('Usuario Comun', 'Rrhh.Actualizar'),
('Director Ejecutivo', 'Rrhh.Crear'),
('Jefe de Taller', 'Rrhh.Crear'),
('Usuario Comun', 'Rrhh.Crear'),
('Director Ejecutivo', 'Rrhh.GenerateExcel'),
('Jefe de Taller', 'Rrhh.GenerateExcel'),
('Usuario Comun', 'Rrhh.GenerateExcel'),
('Director Ejecutivo', 'Rrhh.GeneratePdf'),
('Jefe de Taller', 'Rrhh.GeneratePdf'),
('Usuario Comun', 'Rrhh.GeneratePdf'),
('Director Ejecutivo', 'Rrhh.Ver'),
('Jefe de Taller', 'Rrhh.Ver'),
('Usuario Comun', 'Rrhh.Ver'),
('Director Ejecutivo', 'Rrhh.VerContrato'),
('Jefe de Taller', 'Rrhh.VerContrato'),
('Usuario Comun', 'Rrhh.VerContrato'),
('Director Ejecutivo', 'Rrhh.VerTodos'),
('Jefe de Taller', 'Rrhh.VerTodos'),
('Usuario Comun', 'Rrhh.VerTodos'),
('Director Ejecutivo', 'Solicitud.Actualizar'),
('Jefe de Taller', 'Solicitud.Actualizar'),
('Usuario Comun', 'Solicitud.Actualizar'),
('Director Ejecutivo', 'Solicitud.Anular'),
('Jefe de Taller', 'Solicitud.Anular'),
('Usuario Comun', 'Solicitud.Anular'),
('Director Ejecutivo', 'Solicitud.Crear'),
('Jefe de Taller', 'Solicitud.Crear'),
('Usuario Comun', 'Solicitud.Crear'),
('Director Ejecutivo', 'Solicitud.CrearSolicitud'),
('Jefe de Taller', 'Solicitud.CrearSolicitud'),
('Usuario Comun', 'Solicitud.CrearSolicitud'),
('Director Ejecutivo', 'Solicitud.Finalizadas'),
('Jefe de Taller', 'Solicitud.Finalizadas'),
('Usuario Comun', 'Solicitud.Finalizadas'),
('Director Ejecutivo', 'Solicitud.Finalizar'),
('Jefe de Taller', 'Solicitud.Finalizar'),
('Usuario Comun', 'Solicitud.Finalizar'),
('Director Ejecutivo', 'Solicitud.GenerateExcel'),
('Jefe de Taller', 'Solicitud.GenerateExcel'),
('Usuario Comun', 'Solicitud.GenerateExcel'),
('Director Ejecutivo', 'Solicitud.GeneratePdf'),
('Jefe de Taller', 'Solicitud.GeneratePdf'),
('Usuario Comun', 'Solicitud.GeneratePdf'),
('Director Ejecutivo', 'Solicitud.Ver'),
('Jefe de Taller', 'Solicitud.Ver'),
('Usuario Comun', 'Solicitud.Ver'),
('Director Ejecutivo', 'Solicitud.VerSolicitudEmision'),
('Jefe de Taller', 'Solicitud.VerSolicitudEmision'),
('Usuario Comun', 'Solicitud.VerSolicitudEmision'),
('Director Ejecutivo', 'Solicitud.VerTodos'),
('Jefe de Taller', 'Solicitud.VerTodos'),
('Usuario Comun', 'Solicitud.VerTodos'),
('Director Ejecutivo', 'User.ChangePassword'),
('Jefe de Taller', 'User.ChangePassword'),
('Usuario Comun', 'User.ChangePassword'),
('Director Ejecutivo', 'User.CrearRrhh');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `a_egresos_reales_sr`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `sisproy`.`a_egresos_reales_sr` AS select 'No Directa' AS `rendicion`,`sisproy`.`item_rendicion`.`id` AS `item_rendicion_id`,NULL AS `rendicion_directa_id`,NULL AS `item_rendicion_directa_id`,`sisproy`.`item_rendicion`.`proveedor_id` AS `proveedor_id`,`sisproy`.`item_rendicion`.`cuenta_contable_id` AS `cuenta_contable_id`,`sisproy`.`item_rendicion`.`cuenta_especifica_id` AS `cuenta_especifica_id`,`sisproy`.`item_rendicion`.`proyecto_id` AS `proyecto_id`,`sisproy`.`item_rendicion`.`cuenta_id` AS `cuenta_id`,`sisproy`.`item_rendicion`.`tipo_documento` AS `tipo_documento`,`sisproy`.`item_rendicion`.`numero_documento` AS `numero_documento`,`sisproy`.`item_rendicion`.`tipo_solicitud` AS `tipo_solicitud`,`sisproy`.`item_rendicion`.`tipo_cheque` AS `tipo_cheque`,`sisproy`.`item_rendicion`.`numero_cheque` AS `numero_cheque`,`sisproy`.`item_rendicion`.`fecha` AS `fecha`,`sisproy`.`item_rendicion`.`mes` AS `mes`,`sisproy`.`item_rendicion`.`agno` AS `agno`,`sisproy`.`item_rendicion`.`monto` AS `monto`,NULL AS `rendicion_directa.estado` from `sisproy`.`item_rendicion` union all select 'Directa' AS `Rendicion`,NULL AS `item_rendicion.id`,`sisproy`.`rendicion_directa`.`id` AS `id`,`sisproy`.`item_rendicion_directa`.`id` AS `id`,`sisproy`.`item_rendicion_directa`.`proveedor_id` AS `proveedor_id`,`sisproy`.`item_rendicion_directa`.`cuenta_contable_id` AS `cuenta_contable_id`,`sisproy`.`item_rendicion_directa`.`cuenta_especifica_id` AS `cuenta_especifica_id`,`sisproy`.`item_rendicion_directa`.`proyecto_id` AS `proyecto_id`,`sisproy`.`rendicion_directa`.`cuenta_id` AS `cuenta_id`,`sisproy`.`item_rendicion_directa`.`tipo_documento` AS `tipo_documento`,`sisproy`.`item_rendicion_directa`.`numero_documento` AS `numero_documento`,`sisproy`.`rendicion_directa`.`tipo_solicitud` AS `tipo_solicitud`,`sisproy`.`rendicion_directa`.`tipo_cheque` AS `tipo_cheque`,`sisproy`.`rendicion_directa`.`numero_cheque` AS `numero_cheque`,`sisproy`.`item_rendicion_directa`.`fecha` AS `fecha`,`sisproy`.`item_rendicion_directa`.`mes` AS `mes`,`sisproy`.`item_rendicion_directa`.`agno` AS `agno`,`sisproy`.`item_rendicion_directa`.`monto` AS `monto`,`sisproy`.`rendicion_directa`.`estado` AS `estado` from (`sisproy`.`rendicion_directa` join `sisproy`.`item_rendicion_directa` on((`sisproy`.`item_rendicion_directa`.`rendicion_directa_id` = `sisproy`.`rendicion_directa`.`id`)));

--
-- Volcar la base de datos para la tabla `a_egresos_reales_sr`
--

INSERT INTO `a_egresos_reales_sr` (`rendicion`, `item_rendicion_id`, `rendicion_directa_id`, `item_rendicion_directa_id`, `proveedor_id`, `cuenta_contable_id`, `cuenta_especifica_id`, `proyecto_id`, `cuenta_id`, `tipo_documento`, `numero_documento`, `tipo_solicitud`, `tipo_cheque`, `numero_cheque`, `fecha`, `mes`, `agno`, `monto`, `rendicion_directa.estado`) VALUES
('No Directa', 7, NULL, NULL, 1, 3, 3, 3, 1, 'Boleta de Pres. de 3ros', '13223', 'Transferencia', NULL, 'Transferencia', '2013-05-10', NULL, NULL, 10000, NULL),
('No Directa', 8, NULL, NULL, 1, 2, 14, 1, 1, 'Boleta', NULL, 'Transferencia', NULL, 'Transferencia', '2013-06-14', NULL, NULL, NULL, NULL),
('No Directa', 9, NULL, NULL, 1, 3, 30, 2, 1, 'Boleta', NULL, 'Transferencia', NULL, 'Transferencia', '2013-06-07', NULL, NULL, NULL, NULL),
('No Directa', 10, NULL, NULL, 1, 4, 34, 1, 1, 'Boleta', NULL, 'Cheque', NULL, '5556689', '2013-06-12', NULL, NULL, 5500, NULL),
('No Directa', 11, NULL, NULL, 1, 2, 24, 1, 1, 'Boleta', NULL, 'Cheque', NULL, '5556689', '2013-06-06', NULL, NULL, 5500, NULL),
('No Directa', 12, NULL, NULL, 3, 2, 28, 2, 2, 'Boleta', NULL, 'Cheque', NULL, '2222', NULL, NULL, NULL, 5000, NULL),
('No Directa', 13, NULL, NULL, 3, 2, 11, 2, 2, 'Boleta', NULL, 'Cheque', NULL, '2222', NULL, NULL, NULL, 5000, NULL),
('No Directa', 14, NULL, NULL, 1, 1, 2, 13, 1, 'Boleta', '1123', 'Transferencia', NULL, 'Transferencia', '2014-03-18', NULL, NULL, 1000, NULL),
('Directa', NULL, 6, 2, 1, 3, 31, 1, 1, 'Boleta de Pres. de 3ros', '123', 'Cheque', 'Abierto', '345345', '2013-05-22', NULL, NULL, 40000, 'ACTIVA'),
('Directa', NULL, 2, 3, 1, 1, 6, 3, 2, 'Factura', NULL, 'Transferencia', 'Transferencia', NULL, '2013-06-21', NULL, NULL, NULL, 'ACTIVA'),
('Directa', NULL, 2, 4, 1, 2, 11, 1, 2, 'Boleta', NULL, 'Transferencia', 'Transferencia', NULL, '2013-06-28', NULL, NULL, NULL, 'ACTIVA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `centro_costo`
--

CREATE TABLE IF NOT EXISTS `centro_costo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Volcar la base de datos para la tabla `centro_costo`
--

INSERT INTO `centro_costo` (`id`, `nombre`) VALUES
(1, 'Taller Arquitectura'),
(2, 'Taller de Difusion'),
(3, 'Administracion'),
(4, 'Taller Historia del Arte'),
(5, 'Taller Investigacion y Desarrollo'),
(6, 'Otro centro de costos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comuna`
--

CREATE TABLE IF NOT EXISTS `comuna` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `region_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_comuna_region1_idx` (`region_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcar la base de datos para la tabla `comuna`
--

INSERT INTO `comuna` (`id`, `nombre`, `region_id`) VALUES
(1, 'Arica', 1),
(2, 'Putre', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contrato`
--

CREATE TABLE IF NOT EXISTS `contrato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rrhh_id` int(11) NOT NULL,
  `proyecto_id` int(11) NOT NULL,
  `tipo_contrato_id` int(11) NOT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_termino` date DEFAULT NULL,
  `remuneracion` int(11) DEFAULT NULL,
  `estado` varchar(45) DEFAULT 'VIGENTE',
  PRIMARY KEY (`id`),
  KEY `fk_contrato_proyecto1_idx` (`proyecto_id`),
  KEY `fk_contrato_tipo_contrato1_idx` (`tipo_contrato_id`),
  KEY `fk_contrato_rrhh1_idx` (`rrhh_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Volcar la base de datos para la tabla `contrato`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuenta`
--

CREATE TABLE IF NOT EXISTS `cuenta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero_cuenta` varchar(100) DEFAULT NULL,
  `banco` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcar la base de datos para la tabla `cuenta`
--

INSERT INTO `cuenta` (`id`, `numero_cuenta`, `banco`) VALUES
(1, '01', 'Banco de Chile'),
(2, '02', 'Banco de Chile');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuenta_contable`
--

CREATE TABLE IF NOT EXISTS `cuenta_contable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcar la base de datos para la tabla `cuenta_contable`
--

INSERT INTO `cuenta_contable` (`id`, `nombre`) VALUES
(1, 'Recursos Humano'),
(2, 'Operacion'),
(3, 'Inversion'),
(4, 'Difusion'),
(5, 'Gastos Generales');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuenta_especifica`
--

CREATE TABLE IF NOT EXISTS `cuenta_especifica` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cuenta_contable_id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cuenta_especifica_cuenta_contable1_idx` (`cuenta_contable_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=50 ;

--
-- Volcar la base de datos para la tabla `cuenta_especifica`
--

INSERT INTO `cuenta_especifica` (`id`, `cuenta_contable_id`, `nombre`) VALUES
(1, 1, 'Bonos'),
(2, 1, 'Boleta de Honorario'),
(3, 3, 'Maquinas de Construccion'),
(4, 1, 'Capacitacion'),
(5, 1, 'Finiquitos'),
(6, 1, 'Imposiciones'),
(7, 1, 'IVA'),
(8, 1, 'Remuneraciones'),
(9, 1, 'Retenciones'),
(10, 1, 'Viaticos'),
(11, 2, 'Agua Potable'),
(12, 2, 'Alimentacion'),
(13, 2, 'Alojamiento'),
(14, 2, 'Arriendo Vehiculo'),
(15, 2, 'Combustible/Peajes'),
(16, 2, 'Celular'),
(17, 2, 'Fletes'),
(18, 2, 'Fotocopias, Impresiones, Ploteo'),
(19, 2, 'Gastos de Representacion'),
(20, 2, 'Gastos Notariales'),
(21, 2, 'Impl. de Seguridad'),
(22, 2, 'Luz'),
(23, 2, 'Mantencion de Vehiculos'),
(24, 2, 'Materiales de Construccion'),
(25, 2, 'Materiales de Oficina'),
(26, 2, 'Partes'),
(27, 2, 'Pasajes'),
(28, 2, 'Ropa de Trabajo'),
(29, 2, 'Telecomunicacion'),
(30, 3, 'Tecnologia, Computadores, Impresiones, Maq. Fotografica, Etc.'),
(31, 3, 'Otros Activos'),
(32, 3, 'Vehiculos'),
(33, 4, 'Difusion/Publicidad'),
(34, 4, 'Honorario Evento'),
(35, 4, 'Webhosting Internet'),
(36, 5, 'Abarrotes, Manaje y Otros'),
(37, 5, 'Arreglos Oficina'),
(38, 5, 'Arriendo Oficina'),
(39, 5, 'Correspondencia'),
(40, 5, 'Donaciones'),
(41, 5, 'Fotografia y Ediciones'),
(42, 5, 'Gastos Comunes'),
(43, 5, 'Gastos Financieros'),
(44, 5, 'Mantencion de Equipos'),
(45, 5, 'Otros Gastos Generales'),
(46, 5, 'Patente'),
(47, 5, 'Seguros'),
(48, 5, 'Servicios Legales'),
(49, 5, 'Taxis Colectivos/Estacionamiento');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `egresos_reales`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `sisproy`.`egresos_reales` AS select `a_egresos_reales_sr`.`rendicion` AS `rendicion`,`a_egresos_reales_sr`.`item_rendicion_id` AS `item_rendicion_id`,`a_egresos_reales_sr`.`rendicion_directa_id` AS `rendicion_directa_id`,`a_egresos_reales_sr`.`item_rendicion_directa_id` AS `item_rendicion_directa_id`,`a_egresos_reales_sr`.`proveedor_id` AS `proveedor_id`,`sisproy`.`proveedor`.`rut` AS `rut`,`sisproy`.`proveedor`.`nombre` AS `proveedor`,`a_egresos_reales_sr`.`cuenta_contable_id` AS `cuenta_contable_id`,`sisproy`.`cuenta_contable`.`nombre` AS `cuenta_contable`,`a_egresos_reales_sr`.`cuenta_especifica_id` AS `cuenta_especifica_id`,`sisproy`.`cuenta_especifica`.`nombre` AS `cuenta_especifica`,`a_egresos_reales_sr`.`proyecto_id` AS `proyecto_id`,`sisproy`.`proyecto`.`sigla` AS `sigla`,`sisproy`.`proyecto`.`nombre` AS `proyecto`,`a_egresos_reales_sr`.`cuenta_id` AS `cuenta_id`,`sisproy`.`cuenta`.`numero_cuenta` AS `numero_cuenta`,`sisproy`.`cuenta`.`banco` AS `banco`,`a_egresos_reales_sr`.`tipo_documento` AS `tipo_documento`,`a_egresos_reales_sr`.`numero_documento` AS `numero_documento`,`a_egresos_reales_sr`.`tipo_solicitud` AS `tipo_solicitud`,`a_egresos_reales_sr`.`tipo_cheque` AS `tipo_cheque`,`a_egresos_reales_sr`.`numero_cheque` AS `numero_cheque`,`a_egresos_reales_sr`.`fecha` AS `fecha`,`a_egresos_reales_sr`.`mes` AS `mes`,`a_egresos_reales_sr`.`agno` AS `agno`,`a_egresos_reales_sr`.`monto` AS `monto`,`a_egresos_reales_sr`.`rendicion_directa.estado` AS `estado` from (((((`sisproy`.`a_egresos_reales_sr` join `sisproy`.`proveedor` on((`a_egresos_reales_sr`.`proyecto_id` = `sisproy`.`proveedor`.`id`))) join `sisproy`.`proyecto` on((`a_egresos_reales_sr`.`proyecto_id` = `sisproy`.`proyecto`.`id`))) join `sisproy`.`cuenta` on((`a_egresos_reales_sr`.`cuenta_id` = `sisproy`.`cuenta`.`id`))) join `sisproy`.`cuenta_contable` on((`a_egresos_reales_sr`.`cuenta_contable_id` = `sisproy`.`cuenta_contable`.`id`))) join `sisproy`.`cuenta_especifica` on((`a_egresos_reales_sr`.`cuenta_especifica_id` = `sisproy`.`cuenta_especifica`.`id`)));

--
-- Volcar la base de datos para la tabla `egresos_reales`
--

INSERT INTO `egresos_reales` (`rendicion`, `item_rendicion_id`, `rendicion_directa_id`, `item_rendicion_directa_id`, `proveedor_id`, `rut`, `proveedor`, `cuenta_contable_id`, `cuenta_contable`, `cuenta_especifica_id`, `cuenta_especifica`, `proyecto_id`, `sigla`, `proyecto`, `cuenta_id`, `numero_cuenta`, `banco`, `tipo_documento`, `numero_documento`, `tipo_solicitud`, `tipo_cheque`, `numero_cheque`, `fecha`, `mes`, `agno`, `monto`, `estado`) VALUES
('No Directa', 7, NULL, NULL, 1, '81056900-K', 'Yanulaque y Cia Ltda.', 3, 'Inversion', 3, 'Maquinas de Construccion', 3, 'festival de cine 2013', 'Festival internacional de Cine rural Arica Nativa', 1, '01', 'Banco de Chile', 'Boleta de Pres. de 3ros', '13223', 'Transferencia', NULL, 'Transferencia', '2013-05-10', NULL, NULL, 10000, NULL),
('No Directa', 8, NULL, NULL, 1, '96792430-k', 'Sodimac S.A.', 2, 'Operacion', 14, 'Arriendo Vehiculo', 1, 'Socoroma', 'Restauración de la Iglesia de Socoroma, Comuna de Putre', 1, '01', 'Banco de Chile', 'Boleta', NULL, 'Transferencia', NULL, 'Transferencia', '2013-06-14', NULL, NULL, NULL, NULL),
('No Directa', 9, NULL, NULL, 1, '78598190-1', 'Sociedad Victor Hugo Cortes Bravo y Cia Ltda.', 3, 'Inversion', 30, 'Tecnologia, Computadores, Impresiones, Maq. Fotografica, Etc.', 2, 'CRESPIAL', 'CRESPIAL', 1, '01', 'Banco de Chile', 'Boleta', NULL, 'Transferencia', NULL, 'Transferencia', '2013-06-07', NULL, NULL, NULL, NULL),
('No Directa', 10, NULL, NULL, 1, '96792430-k', 'Sodimac S.A.', 4, 'Difusion', 34, 'Honorario Evento', 1, 'Socoroma', 'Restauración de la Iglesia de Socoroma, Comuna de Putre', 1, '01', 'Banco de Chile', 'Boleta', NULL, 'Cheque', NULL, '5556689', '2013-06-12', NULL, NULL, 5500, NULL),
('No Directa', 11, NULL, NULL, 1, '96792430-k', 'Sodimac S.A.', 2, 'Operacion', 24, 'Materiales de Construccion', 1, 'Socoroma', 'Restauración de la Iglesia de Socoroma, Comuna de Putre', 1, '01', 'Banco de Chile', 'Boleta', NULL, 'Cheque', NULL, '5556689', '2013-06-06', NULL, NULL, 5500, NULL),
('No Directa', 12, NULL, NULL, 3, '78598190-1', 'Sociedad Victor Hugo Cortes Bravo y Cia Ltda.', 2, 'Operacion', 28, 'Ropa de Trabajo', 2, 'CRESPIAL', 'CRESPIAL', 2, '02', 'Banco de Chile', 'Boleta', NULL, 'Cheque', NULL, '2222', NULL, NULL, NULL, 5000, NULL),
('No Directa', 13, NULL, NULL, 3, '78598190-1', 'Sociedad Victor Hugo Cortes Bravo y Cia Ltda.', 2, 'Operacion', 11, 'Agua Potable', 2, 'CRESPIAL', 'CRESPIAL', 2, '02', 'Banco de Chile', 'Boleta', NULL, 'Cheque', NULL, '2222', NULL, NULL, NULL, 5000, NULL),
('No Directa', 14, NULL, NULL, 1, '4.232.934-7', 'Adela Cutipa Santos', 1, 'Recursos Humano', 2, 'Boleta de Honorario', 13, 'TST', 'TEST', 1, '01', 'Banco de Chile', 'Boleta', '1123', 'Transferencia', NULL, 'Transferencia', '2014-03-18', NULL, NULL, 1000, NULL),
('Directa', NULL, 6, 2, 1, '96792430-k', 'Sodimac S.A.', 3, 'Inversion', 31, 'Otros Activos', 1, 'Socoroma', 'Restauración de la Iglesia de Socoroma, Comuna de Putre', 1, '01', 'Banco de Chile', 'Boleta de Pres. de 3ros', '123', 'Cheque', 'Abierto', '345345', '2013-05-22', NULL, NULL, 40000, 'ACTIVA'),
('Directa', NULL, 2, 3, 1, '81056900-K', 'Yanulaque y Cia Ltda.', 1, 'Recursos Humano', 6, 'Imposiciones', 3, 'festival de cine 2013', 'Festival internacional de Cine rural Arica Nativa', 2, '02', 'Banco de Chile', 'Factura', NULL, 'Transferencia', 'Transferencia', NULL, '2013-06-21', NULL, NULL, NULL, 'ACTIVA'),
('Directa', NULL, 2, 4, 1, '96792430-k', 'Sodimac S.A.', 2, 'Operacion', 11, 'Agua Potable', 1, 'Socoroma', 'Restauración de la Iglesia de Socoroma, Comuna de Putre', 2, '02', 'Banco de Chile', 'Boleta', NULL, 'Transferencia', 'Transferencia', NULL, '2013-06-28', NULL, NULL, NULL, 'ACTIVA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `emision`
--

CREATE TABLE IF NOT EXISTS `emision` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `proveedor_id` int(11) NOT NULL,
  `proyecto_id` int(11) NOT NULL,
  `solicitud_id` int(11) NOT NULL,
  `cuenta_id` int(11) NOT NULL,
  `ciudad` varchar(100) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `concepto` varchar(255) DEFAULT NULL,
  `total_debe` int(11) NOT NULL DEFAULT '0',
  `total_haber` int(11) NOT NULL DEFAULT '0',
  `tipo_solicitud` varchar(50) DEFAULT NULL,
  `tipo_cheque` varchar(45) DEFAULT NULL,
  `numero_cheque` varchar(100) DEFAULT NULL,
  `observaciones` blob,
  `estado` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_emision_solicitud1_idx` (`solicitud_id`),
  KEY `fk_emision_proyecto1_idx` (`proyecto_id`),
  KEY `fk_emision_proveedor1_idx` (`proveedor_id`),
  KEY `fk_emision_user1` (`user_id`),
  KEY `fk_emision_cuenta1` (`cuenta_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Volcar la base de datos para la tabla `emision`
--

INSERT INTO `emision` (`id`, `user_id`, `proveedor_id`, `proyecto_id`, `solicitud_id`, `cuenta_id`, `ciudad`, `fecha`, `concepto`, `total_debe`, `total_haber`, `tipo_solicitud`, `tipo_cheque`, `numero_cheque`, `observaciones`, `estado`) VALUES
(8, 1, 1, 1, 23, 1, NULL, '2013-05-10', NULL, 10000, 0, 'Transferencia', 'Transferencia', 'Transferencia', NULL, 'RENDIDA'),
(9, 1, 1, 1, 24, 1, NULL, '2013-06-06', NULL, 11000, 0, 'Cheque', 'Abierto', '5556689', 0x74657374, 'RENDIDA'),
(10, 1, 1, 2, 25, 2, NULL, '2013-06-04', NULL, 10000, 0, 'Cheque', 'Cruzado', '2222', 0x74657374, 'RENDIDA'),
(11, 1, 1, 1, 27, 1, NULL, '2014-03-18', NULL, 10000, 0, 'Cheque', 'Abierto', '123', 0x74657374, 'RENDIDA'),
(12, 1, 1, 13, 22, 1, NULL, '2014-03-26', NULL, 2000, 0, 'Transferencia', 'Transferencia', 'Transferencia', 0x747374, 'RENDIDA'),
(13, 1, 1, 13, 28, 1, NULL, '2014-03-20', NULL, 4000, 0, 'Transferencia', 'Transferencia', 'Transferencia', 0x74657374, 'FINALIZADA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `financiamiento`
--

CREATE TABLE IF NOT EXISTS `financiamiento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcar la base de datos para la tabla `financiamiento`
--

INSERT INTO `financiamiento` (`id`, `nombre`) VALUES
(1, 'Conadi'),
(2, 'Municipalidad'),
(3, 'Corfo'),
(4, 'Minera Candelaria Copiapó');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ingreso`
--

CREATE TABLE IF NOT EXISTS `ingreso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proyecto_id` int(11) NOT NULL,
  `tipo_ingreso_id` int(11) NOT NULL,
  `financiamiento_id` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `monto` int(11) DEFAULT '0',
  `observaciones` blob,
  `neto` int(11) DEFAULT NULL,
  `iva` float DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_presupuesto_financiamiento1_idx` (`financiamiento_id`),
  KEY `fk_presupuesto_tipo_ingreso1_idx` (`tipo_ingreso_id`),
  KEY `fk_presupuesto_proyecto1_idx` (`proyecto_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcar la base de datos para la tabla `ingreso`
--

INSERT INTO `ingreso` (`id`, `proyecto_id`, `tipo_ingreso_id`, `financiamiento_id`, `fecha`, `monto`, `observaciones`, `neto`, `iva`, `total`) VALUES
(1, 10, 3, 4, '2013-04-05', 3500000, NULL, NULL, NULL, NULL),
(2, 10, 3, 4, '2013-08-30', 3500000, NULL, NULL, NULL, NULL),
(3, 13, 1, 1, '2014-03-18', 4000, 0x74657374, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `item_presupuesto`
--

CREATE TABLE IF NOT EXISTS `item_presupuesto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `presupuesto_id` int(11) NOT NULL,
  `cuenta_contable_id` int(11) NOT NULL,
  `proyecto_id` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `mes` varchar(45) DEFAULT NULL,
  `agno` varchar(45) DEFAULT NULL,
  `monto` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_item_presupuesto_presupuesto1_idx` (`presupuesto_id`),
  KEY `fk_item_presupuesto_cuenta_contable1_idx` (`cuenta_contable_id`),
  KEY `fk_item_presupuesto_proyecto1_idx` (`proyecto_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=40 ;

--
-- Volcar la base de datos para la tabla `item_presupuesto`
--

INSERT INTO `item_presupuesto` (`id`, `presupuesto_id`, `cuenta_contable_id`, `proyecto_id`, `fecha`, `mes`, `agno`, `monto`) VALUES
(1, 1, 1, 1, '2012-03-15', NULL, NULL, 160000000),
(2, 1, 2, 1, '2012-03-15', NULL, NULL, 300000000),
(3, 1, 3, 1, '2012-03-15', NULL, NULL, 10000000),
(4, 1, 4, 1, '2012-03-15', NULL, NULL, 50000000),
(5, 1, 5, 1, '2012-03-15', NULL, NULL, 68000000),
(6, 2, 1, 2, '2012-10-10', NULL, NULL, 4000000),
(7, 2, 2, 2, '2012-10-10', NULL, NULL, 3000000),
(8, 2, 4, 2, '2012-10-15', NULL, NULL, 1500000),
(9, 2, 5, 2, '2012-10-10', NULL, NULL, 1500000),
(10, 3, 1, 3, '2013-03-11', NULL, NULL, 15500000),
(11, 3, 2, 3, '2013-03-11', NULL, NULL, 21800000),
(12, 3, 4, 3, '2013-03-11', NULL, NULL, 10000000),
(13, 3, 5, 3, '2013-03-11', NULL, NULL, 550000),
(14, 4, 1, 4, '2013-03-11', NULL, NULL, 8000000),
(15, 4, 2, 4, '2013-03-11', NULL, NULL, 45000000),
(16, 4, 4, 4, '2013-03-11', NULL, NULL, 15000000),
(17, 4, 5, 4, '2013-03-11', NULL, NULL, 5000000),
(18, 5, 1, 5, '2013-03-11', NULL, NULL, 3000000),
(19, 5, 2, 5, NULL, NULL, NULL, 3000000),
(20, 5, 3, 5, NULL, NULL, NULL, 2000000),
(21, 5, 5, 5, NULL, NULL, NULL, 3000000),
(22, 6, 1, 6, NULL, NULL, NULL, 9000000),
(23, 6, 2, 6, '2013-03-11', NULL, NULL, 5360000),
(24, 6, 4, 6, NULL, NULL, NULL, 2000000),
(25, 6, 5, 6, NULL, NULL, NULL, 3000000),
(26, 7, 1, 7, '2013-03-11', NULL, NULL, 45000000),
(27, 7, 2, 7, NULL, NULL, NULL, 30000000),
(28, 7, 4, 7, NULL, NULL, NULL, 10000000),
(29, 7, 5, 7, NULL, NULL, NULL, 15000000),
(30, 8, 1, 8, NULL, NULL, NULL, 15000000),
(31, 8, 2, 8, NULL, NULL, NULL, 6600000),
(32, 8, 4, 8, NULL, NULL, NULL, 2000000),
(33, 8, 5, 8, NULL, NULL, NULL, 3000000),
(34, 10, 2, 10, '2013-05-31', NULL, NULL, 1385000),
(35, 10, 1, 10, '2013-04-02', NULL, NULL, 2444448),
(36, 10, 3, 10, '2013-04-02', NULL, NULL, 220000),
(37, 10, 5, 10, '2013-08-30', NULL, NULL, 607417),
(38, 13, 1, 13, '2014-03-18', NULL, NULL, 1000),
(39, 14, 2, 13, '2014-03-19', NULL, NULL, 20000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `item_rendicion`
--

CREATE TABLE IF NOT EXISTS `item_rendicion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proveedor_id` int(11) NOT NULL,
  `cuenta_contable_id` int(11) NOT NULL,
  `cuenta_especifica_id` int(11) NOT NULL,
  `rendicion_id` int(11) DEFAULT NULL,
  `proyecto_id` int(11) NOT NULL,
  `cuenta_id` int(11) NOT NULL,
  `tipo_documento` varchar(100) DEFAULT NULL,
  `numero_documento` varchar(100) DEFAULT NULL,
  `tipo_solicitud` varchar(45) DEFAULT NULL,
  `tipo_cheque` varchar(45) DEFAULT NULL,
  `numero_cheque` varchar(45) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `mes` varchar(45) DEFAULT NULL,
  `agno` varchar(45) DEFAULT NULL,
  `monto` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_item_rendicion_rendicion1_idx` (`rendicion_id`),
  KEY `fk_item_rendicion_proyecto1_idx` (`proyecto_id`),
  KEY `fk_item_rendicion_proveedor1_idx` (`proveedor_id`),
  KEY `fk_item_rendicion_cuenta_especifica1_idx` (`cuenta_especifica_id`),
  KEY `fk_item_rendicion_cuenta_contable1` (`cuenta_contable_id`),
  KEY `fk_item_rendicion_cuenta1` (`cuenta_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Volcar la base de datos para la tabla `item_rendicion`
--

INSERT INTO `item_rendicion` (`id`, `proveedor_id`, `cuenta_contable_id`, `cuenta_especifica_id`, `rendicion_id`, `proyecto_id`, `cuenta_id`, `tipo_documento`, `numero_documento`, `tipo_solicitud`, `tipo_cheque`, `numero_cheque`, `fecha`, `mes`, `agno`, `monto`) VALUES
(7, 1, 3, 3, 6, 3, 1, 'Boleta de Pres. de 3ros', '13223', 'Transferencia', NULL, 'Transferencia', '2013-05-10', NULL, NULL, 10000),
(8, 1, 2, 14, 7, 1, 1, 'Boleta', NULL, 'Transferencia', NULL, 'Transferencia', '2013-06-14', NULL, NULL, NULL),
(9, 1, 3, 30, 7, 2, 1, 'Boleta', NULL, 'Transferencia', NULL, 'Transferencia', '2013-06-07', NULL, NULL, NULL),
(10, 1, 4, 34, 8, 1, 1, 'Boleta', NULL, 'Cheque', NULL, '5556689', '2013-06-12', NULL, NULL, 5500),
(11, 1, 2, 24, 8, 1, 1, 'Boleta', NULL, 'Cheque', NULL, '5556689', '2013-06-06', NULL, NULL, 5500),
(12, 3, 2, 28, 9, 2, 2, 'Boleta', NULL, 'Cheque', NULL, '2222', NULL, NULL, NULL, 5000),
(13, 3, 2, 11, 9, 2, 2, 'Boleta', NULL, 'Cheque', NULL, '2222', NULL, NULL, NULL, 5000),
(14, 1, 1, 2, 11, 13, 1, 'Boleta', '1123', 'Transferencia', NULL, 'Transferencia', '2014-03-18', NULL, NULL, 1000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `item_rendicion_directa`
--

CREATE TABLE IF NOT EXISTS `item_rendicion_directa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rendicion_directa_id` int(11) NOT NULL,
  `proveedor_id` int(11) NOT NULL,
  `cuenta_contable_id` int(11) NOT NULL,
  `cuenta_especifica_id` int(11) NOT NULL,
  `proyecto_id` int(11) NOT NULL,
  `tipo_documento` varchar(45) DEFAULT NULL,
  `numero_documento` varchar(45) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `mes` varchar(45) DEFAULT NULL,
  `agno` varchar(45) DEFAULT NULL,
  `monto` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_item_rendicion_directa_rendicion_directa` (`rendicion_directa_id`),
  KEY `fk_item_rendicion_directa_proveedor` (`proveedor_id`),
  KEY `fk_item_rendicion_directa_cuenta_contable` (`cuenta_contable_id`),
  KEY `fk_item_rendicion_directa_cuenta_especifica` (`cuenta_especifica_id`),
  KEY `fk_item_rendicion_directa_proyecto` (`proyecto_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcar la base de datos para la tabla `item_rendicion_directa`
--

INSERT INTO `item_rendicion_directa` (`id`, `rendicion_directa_id`, `proveedor_id`, `cuenta_contable_id`, `cuenta_especifica_id`, `proyecto_id`, `tipo_documento`, `numero_documento`, `fecha`, `mes`, `agno`, `monto`) VALUES
(2, 6, 1, 3, 31, 1, 'Boleta de Pres. de 3ros', '123', '2013-05-22', NULL, NULL, 40000),
(3, 2, 1, 1, 6, 3, 'Factura', NULL, '2013-06-21', NULL, NULL, NULL),
(4, 2, 1, 2, 11, 1, 'Boleta', NULL, '2013-06-28', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `item_solicitud`
--

CREATE TABLE IF NOT EXISTS `item_solicitud` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `solicitud_id` int(11) NOT NULL,
  `cuenta_contable_id` int(11) DEFAULT NULL,
  `valor_unitario` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `descripcion` blob,
  `total` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_item_solicitud_solicitud1_idx` (`solicitud_id`),
  KEY `fk_item_solicitud_cuenta_contable1_idx` (`cuenta_contable_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Volcar la base de datos para la tabla `item_solicitud`
--

INSERT INTO `item_solicitud` (`id`, `solicitud_id`, `cuenta_contable_id`, `valor_unitario`, `cantidad`, `descripcion`, `total`) VALUES
(27, 23, 3, 5000, 2, 0x74657374, 10000),
(28, 24, 1, 5500, 2, 0x74657374, 11000),
(29, 25, 2, 2500, 4, 0x74657374, 10000),
(30, 22, 2, NULL, 1, NULL, 0),
(31, 22, 3, NULL, 1, NULL, 0),
(32, 27, 1, 5000, 2, 0x74657374, 10000),
(33, 28, 1, 2000, 2, 0x74657374, 4000),
(34, 22, 1, 1000, 2, 0x74657374, 2000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `libro_compra`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `sisproy`.`libro_compra` AS select `sisproy`.`item_rendicion`.`id` AS `item_rendicion_id`,`sisproy`.`emision`.`fecha` AS `fecha_emision`,`sisproy`.`item_rendicion`.`fecha` AS `fecha_item_rendicion`,dayofmonth(`sisproy`.`emision`.`fecha`) AS `dia_emision`,elt(month(`sisproy`.`emision`.`fecha`),'Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre') AS `mes_emision`,year(`sisproy`.`emision`.`fecha`) AS `ano_emision`,dayofmonth(`sisproy`.`item_rendicion`.`fecha`) AS `dia_item_rendicion`,elt(month(`sisproy`.`item_rendicion`.`fecha`),'Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre') AS `mes_item_rendicion`,year(`sisproy`.`item_rendicion`.`fecha`) AS `ano_item_rendicion`,`sisproy`.`item_rendicion`.`tipo_documento` AS `tipo_documento`,`sisproy`.`item_rendicion`.`numero_documento` AS `numero_documento`,`sisproy`.`proveedor`.`nombre` AS `proveedor`,`sisproy`.`item_rendicion`.`monto` AS `monto`,`sisproy`.`item_rendicion`.`tipo_solicitud` AS `tipo_solicitud`,`sisproy`.`emision`.`numero_cheque` AS `numero_cheque`,`sisproy`.`rendicion`.`asignacion` AS `asignacion`,`sisproy`.`centro_costo`.`nombre` AS `centro_costo`,`sisproy`.`proyecto`.`nombre` AS `proyecto`,`sisproy`.`cuenta_contable`.`nombre` AS `cuenta_contable`,`sisproy`.`cuenta_especifica`.`nombre` AS `cuenta_especifica` from (((((((`sisproy`.`proyecto` join `sisproy`.`centro_costo` on((`sisproy`.`proyecto`.`centro_costo_id` = `sisproy`.`centro_costo`.`id`))) join `sisproy`.`item_rendicion` on((`sisproy`.`item_rendicion`.`proyecto_id` = `sisproy`.`proyecto`.`id`))) join `sisproy`.`proveedor` on((`sisproy`.`item_rendicion`.`proveedor_id` = `sisproy`.`proveedor`.`id`))) left join `sisproy`.`rendicion` on((`sisproy`.`item_rendicion`.`rendicion_id` = `sisproy`.`rendicion`.`id`))) left join `sisproy`.`emision` on((`sisproy`.`rendicion`.`emision_id` = `sisproy`.`emision`.`id`))) join `sisproy`.`cuenta_contable` on((`sisproy`.`item_rendicion`.`cuenta_contable_id` = `sisproy`.`cuenta_contable`.`id`))) join `sisproy`.`cuenta_especifica` on((`sisproy`.`item_rendicion`.`cuenta_especifica_id` = `sisproy`.`cuenta_especifica`.`id`)));

--
-- Volcar la base de datos para la tabla `libro_compra`
--

INSERT INTO `libro_compra` (`item_rendicion_id`, `fecha_emision`, `fecha_item_rendicion`, `dia_emision`, `mes_emision`, `ano_emision`, `dia_item_rendicion`, `mes_item_rendicion`, `ano_item_rendicion`, `tipo_documento`, `numero_documento`, `proveedor`, `monto`, `tipo_solicitud`, `numero_cheque`, `asignacion`, `centro_costo`, `proyecto`, `cuenta_contable`, `cuenta_especifica`) VALUES
(14, '2014-03-26', '2014-03-18', 26, 'Marzo', 2014, 18, 'Marzo', 2014, 'Boleta', '1123', 'Sodimac S.A.', 1000, 'Transferencia', 'Transferencia', 'Directa', 'Taller Arquitectura', 'TEST', 'Recursos Humano', 'Boleta de Honorario'),
(8, '2013-05-10', '2013-06-14', 10, 'Mayo', 2013, 14, 'Junio', 2013, 'Boleta', NULL, 'Sodimac S.A.', NULL, 'Transferencia', 'Transferencia', 'Directa', 'Taller Arquitectura', 'Restauración de la Iglesia de Socoroma, Comuna de Putre', 'Operacion', 'Arriendo Vehiculo'),
(11, '2013-06-06', '2013-06-06', 6, 'Junio', 2013, 6, 'Junio', 2013, 'Boleta', NULL, 'Sodimac S.A.', 5500, 'Cheque', '5556689', 'Directa', 'Taller Arquitectura', 'Restauración de la Iglesia de Socoroma, Comuna de Putre', 'Operacion', 'Materiales de Construccion'),
(12, '2013-06-04', NULL, 4, 'Junio', 2013, NULL, NULL, NULL, 'Boleta', NULL, 'Yanulaque y Cia Ltda.', 5000, 'Cheque', '2222', 'Multiple', 'Taller Historia del Arte', 'CRESPIAL', 'Operacion', 'Ropa de Trabajo'),
(13, '2013-06-04', NULL, 4, 'Junio', 2013, NULL, NULL, NULL, 'Boleta', NULL, 'Yanulaque y Cia Ltda.', 5000, 'Cheque', '2222', 'Multiple', 'Taller Historia del Arte', 'CRESPIAL', 'Operacion', 'Agua Potable'),
(7, '2013-05-10', '2013-05-10', 10, 'Mayo', 2013, 10, 'Mayo', 2013, 'Boleta de Pres. de 3ros', '13223', 'Sodimac S.A.', 10000, 'Transferencia', 'Transferencia', 'Directa', 'Taller de Difusion', 'Festival internacional de Cine rural Arica Nativa', 'Inversion', 'Maquinas de Construccion'),
(9, '2013-05-10', '2013-06-07', 10, 'Mayo', 2013, 7, 'Junio', 2013, 'Boleta', NULL, 'Sodimac S.A.', NULL, 'Transferencia', 'Transferencia', 'Directa', 'Taller Historia del Arte', 'CRESPIAL', 'Inversion', 'Tecnologia, Computadores, Impresiones, Maq. Fotografica, Etc.'),
(10, '2013-06-06', '2013-06-12', 6, 'Junio', 2013, 12, 'Junio', 2013, 'Boleta', NULL, 'Sodimac S.A.', 5500, 'Cheque', '5556689', 'Directa', 'Taller Arquitectura', 'Restauración de la Iglesia de Socoroma, Comuna de Putre', 'Difusion', 'Honorario Evento');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `presupuesto`
--

CREATE TABLE IF NOT EXISTS `presupuesto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proyecto_id` int(11) NOT NULL,
  `tipo_ingreso_id` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `mes` varchar(45) DEFAULT NULL,
  `agno` varchar(45) DEFAULT NULL,
  `monto` int(11) DEFAULT '0',
  `observaciones` blob,
  PRIMARY KEY (`id`),
  KEY `fk_presupuesto_proyecto2_idx` (`proyecto_id`),
  KEY `fk_presupuesto_tipo_ingreso2_idx` (`tipo_ingreso_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Volcar la base de datos para la tabla `presupuesto`
--

INSERT INTO `presupuesto` (`id`, `proyecto_id`, `tipo_ingreso_id`, `fecha`, `mes`, `agno`, `monto`, `observaciones`) VALUES
(1, 1, 1, '2012-03-15', NULL, NULL, 588000000, NULL),
(2, 2, 1, '2012-10-10', NULL, NULL, 10000000, NULL),
(3, 3, 1, '2013-03-11', NULL, NULL, 47850000, NULL),
(4, 4, 1, '2013-03-11', NULL, NULL, 73000000, NULL),
(5, 5, 4, '2013-03-11', NULL, NULL, 11000000, NULL),
(6, 6, 1, '2013-03-11', NULL, NULL, 19360000, NULL),
(7, 7, 1, '2013-03-11', NULL, NULL, 100000000, NULL),
(8, 8, 1, NULL, NULL, NULL, 26600000, NULL),
(9, 9, 3, NULL, NULL, NULL, 0, NULL),
(10, 10, 3, '2013-03-22', NULL, NULL, 4656865, NULL),
(11, 10, 3, '2013-03-22', NULL, NULL, 0, NULL),
(12, 11, 2, '2013-04-04', NULL, NULL, 0, NULL),
(13, 13, 1, '2014-03-18', NULL, NULL, 1000, 0x54455354),
(14, 13, 2, '2014-03-18', NULL, NULL, 20000, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE IF NOT EXISTS `proveedor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rut` varchar(13) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Volcar la base de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`id`, `rut`, `nombre`) VALUES
(1, '96792430-k', 'Sodimac S.A.'),
(2, '78598190-1', 'Sociedad Victor Hugo Cortes Bravo y Cia Ltda.'),
(3, '81056900-K', 'Yanulaque y Cia Ltda.'),
(4, '76184244-7', 'Agroindustrial Pablo Lagos Ku E.I.R.L.'),
(5, '79.889.650-4', 'Ferreterias Iberia Ltda.'),
(6, '96511460-2', 'Construmart S.A.'),
(7, '5009405-7', 'MIRIAM HERESI JORDAN'),
(8, '80314700-0', 'Empresa de Transportes Rurales Limitada'),
(9, '16153864-7', 'María Javiera Maino González'),
(10, '16.666.547-7', 'Benjamin Garcia'),
(11, NULL, 'Francisco León'),
(12, '12.609.575-9', 'Pablo Choque Flores'),
(13, '4.232.934-7', 'Adela Cutipa Santos'),
(14, '14107505-5', 'Miguel Rojas Henriquez'),
(15, '16.770.824-2', 'Israel Quispe Lazaro'),
(16, '16.225.568-1', 'Cristian Brandau Mardonez'),
(17, '12.023.864-7', 'Ronald Caicedo Garay'),
(18, NULL, 'FRANCISCO TAPIA'),
(19, NULL, 'JORGE CHAU'),
(20, '92.648.000-6', 'MADERAS ENCO S.A.'),
(21, '76099427-8', 'SOCIEDAD NEUROPSICOLOGICA ARICA LTDA.'),
(22, '15000952-9', 'Nicolas Miño zA'),
(23, '15000952-9', 'Nicolas Miño Zapata'),
(24, '76044798-6', 'AGUA DEL TOMAS LIMITADA'),
(25, '12.086.055-0', 'María Cristina Santos Rojas"T. Mechita"'),
(26, '123', 'test');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyecto`
--

CREATE TABLE IF NOT EXISTS `proyecto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rrhh_id` int(11) NOT NULL,
  `centro_costo_id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `sigla` varchar(100) DEFAULT NULL,
  `descripcion` blob,
  `archivo` blob,
  `contrato` blob,
  `comuna` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_proyecto_centro_costo1_idx` (`centro_costo_id`),
  KEY `fk_proyecto_rrhh1_idx` (`rrhh_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Volcar la base de datos para la tabla `proyecto`
--

INSERT INTO `proyecto` (`id`, `rrhh_id`, `centro_costo_id`, `nombre`, `sigla`, `descripcion`, `archivo`, `contrato`, `comuna`, `region`) VALUES
(1, 1, 1, 'Restauración de la Iglesia de Socoroma, Comuna de Putre', 'Socoroma', 0x52657374617572616369c3b36e2049676c6573696120646520536f636f726f6d612c20636f6d756e61206465205075747265, 0x6172636869766f2d312e706466, 0x6172636869766f2d312e706466, 'ARICA', '15'),
(2, 15, 4, 'CRESPIAL', 'CRESPIAL', NULL, 0x6172636869766f2d312e706466, 0x6172636869766f2d312e706466, 'ARICA', '15'),
(3, 21, 2, 'Festival internacional de Cine rural Arica Nativa', 'festival de cine 2013', 0x466573746976616c2064652063696e65, 0x6172636869766f2d312e706466, 0x6172636869766f2d312e706466, 'ARICA', '15'),
(4, 15, 4, 'Encuentro Barroco en Arica', 'Encuentro Barroco', NULL, 0x6172636869766f2d312e706466, 0x6172636869766f2d312e706466, 'ARICA', '15'),
(5, 3, 3, 'Proyecto Administración', 'Administración', NULL, 0x6172636869766f2d312e706466, 0x6172636869766f2d312e706466, 'ARICA', '15'),
(6, 1, 1, 'Proyectos Plazas', 'Plazas', NULL, 0x6172636869766f2d312e706466, 0x6172636869766f2d312e706466, 'ARICA', '15'),
(7, 1, 1, 'Proyecto de Declaratorias', 'Declaratoría', NULL, 0x6172636869766f2d312e706466, 0x6172636869766f2d312e706466, 'ARICA', '15'),
(8, 1, 1, 'Diseño Iglesia de Pachama', 'Diseño Pachama', NULL, 0x6172636869766f2d312e706466, 0x6172636869766f2d312e706466, 'ARICA', '15'),
(9, 15, 4, 'Informe e investigación Histórica Estética para el Santuario de la Virgen de la Candelaria de Copiapó', 'Candelaria Copiapó', 0x50726f796563746f206465206c6576616e74616d69656e746f2068697374c3b37269636f2c2063756c747572616c207920617271756974656374c3b36e69636f2064656c2053616e74756172696f206465206c612043616e64656c6172696120646520436f70696170c3b32e, NULL, NULL, NULL, '15'),
(10, 15, 4, 'INFORME E INVESTIGACIÓN HISTÓRICA ESTÉTICA PARA EL SANTUARIO DE LA VIRGEN DE LA CANDELARIA DE COPIAPO', 'Candelaria Copiapó', 0x50726f796563746f206465206c6576616e74616d69656e746f2068697374c3b37269636f2c2063756c747572616c207920617271756974656374c3b36e69636f2064656c2053616e74756172696f206465206c612043616e64656c6172696120646520436f70696170c3b32e, NULL, NULL, 'ARICA', '15'),
(11, 15, 4, 'Libro Las Pinturas Murales de Parinacota en el último bofedal de la ruta de la plata', 'Libro Parinacota', 0x4c6962726f20646520666f746f67726166c3ad61207920746578746f2064652070696e7475726173206d7572616c657320646520506172696e61636f7461, NULL, NULL, 'ARICA', '15'),
(12, 1, 1, 'asd', 'asd', 0x617364, NULL, 0x496e666f726d65536f6c696369747564526571756572696d69656e746f2e646f6378, 'ARICA', '15'),
(13, 1, 1, 'TEST', 'TST', 0x544553542054455354, NULL, NULL, 'ARICA', '15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `region`
--

CREATE TABLE IF NOT EXISTS `region` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcar la base de datos para la tabla `region`
--

INSERT INTO `region` (`id`, `nombre`) VALUES
(1, 'XV Arica y Parinacota');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rendicion`
--

CREATE TABLE IF NOT EXISTS `rendicion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `emision_id` int(11) NOT NULL,
  `cuenta_id` int(11) NOT NULL,
  `asignacion` varchar(100) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `comuna` varchar(100) DEFAULT NULL,
  `region` varchar(100) DEFAULT NULL,
  `tipo_solicitud` varchar(45) DEFAULT NULL,
  `numero_cheque` varchar(45) DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL,
  `total_entrada` int(11) DEFAULT NULL,
  `total_salida` int(11) NOT NULL DEFAULT '0',
  `total_saldo` int(11) NOT NULL DEFAULT '0',
  `observacion` blob,
  PRIMARY KEY (`id`),
  KEY `fk_rendicion_emision1_idx` (`emision_id`),
  KEY `fk_rendicion_user1_idx` (`user_id`),
  KEY `fk_rendicion_cuenta1` (`cuenta_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Volcar la base de datos para la tabla `rendicion`
--

INSERT INTO `rendicion` (`id`, `user_id`, `emision_id`, `cuenta_id`, `asignacion`, `fecha`, `comuna`, `region`, `tipo_solicitud`, `numero_cheque`, `estado`, `total_entrada`, `total_salida`, `total_saldo`, `observacion`) VALUES
(6, 25, 8, 1, 'Directa', '2013-05-10', 'ARICA', 'Región de Arica y Parinacota', 'Transferencia', 'Transferencia', 'FINALIZADA', 10000, 10000, 0, 0x74657374),
(7, 1, 8, 1, 'Directa', '2013-06-14', 'ARICA', 'Región de Arica y Parinacota', 'Transferencia', 'Transferencia', 'ACTIVA', 10000, 0, 10000, NULL),
(8, 1, 9, 1, 'Directa', '2013-06-13', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '5556689', 'FINALIZADA', 11000, 11000, 0, 0x74657374),
(9, 1, 10, 2, 'Multiple', '2013-06-12', 'ARICA', 'Región de Antofagasta', 'Cheque', '2222', 'FINALIZADA', 10000, 10000, 0, NULL),
(10, 1, 11, 1, 'Directa', '2014-03-18', 'ARICA', 'Región de Arica y Parinacota', 'Cheque', '123', 'ACTIVA', 10000, 0, 10000, 0x74657374),
(11, 1, 12, 1, 'Directa', '2014-03-18', 'ARICA', 'Región de Arica y Parinacota', 'Transferencia', 'Transferencia', 'ACTIVA', 2000, 1000, 1000, 0x74657374);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rendicion_directa`
--

CREATE TABLE IF NOT EXISTS `rendicion_directa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cuenta_id` int(11) NOT NULL,
  `tipo_solicitud` varchar(45) DEFAULT NULL,
  `tipo_cheque` varchar(45) DEFAULT NULL,
  `numero_cheque` varchar(45) DEFAULT NULL,
  `estado` varchar(45) DEFAULT 'ACTIVA',
  PRIMARY KEY (`id`),
  KEY `fk_rendicion_directa_cuenta` (`cuenta_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Volcar la base de datos para la tabla `rendicion_directa`
--

INSERT INTO `rendicion_directa` (`id`, `cuenta_id`, `tipo_solicitud`, `tipo_cheque`, `numero_cheque`, `estado`) VALUES
(1, 2, 'Cheque', 'Abierto', '55566689', 'ANULADA'),
(2, 2, 'Transferencia', 'Transferencia', NULL, 'ACTIVA'),
(3, 2, 'Cheque', 'Abierto', '656576', 'ACTIVA'),
(4, 1, 'Cheque', 'Abierto', '5555', 'ACTIVA'),
(5, 1, 'Cheque', 'Abierto', '123123', 'ACTIVA'),
(6, 1, 'Cheque', 'Abierto', '345345', 'ACTIVA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rights`
--

CREATE TABLE IF NOT EXISTS `rights` (
  `itemname` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  PRIMARY KEY (`itemname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcar la base de datos para la tabla `rights`
--

INSERT INTO `rights` (`itemname`, `type`, `weight`) VALUES
('admin', 2, 0),
('Director Ejecutivo', 2, 1),
('Jefe de Taller', 2, 2),
('Usuario Comun', 2, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rrhh`
--

CREATE TABLE IF NOT EXISTS `rrhh` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rut` varchar(13) DEFAULT NULL,
  `nombres` varchar(255) DEFAULT NULL,
  `apellidos` varchar(255) DEFAULT NULL,
  `documento` varchar(45) DEFAULT NULL,
  `nacionalidad` varchar(45) DEFAULT NULL,
  `sexo` varchar(15) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `estado_civil` varchar(45) DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `telefono` varchar(50) DEFAULT NULL,
  `celular` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `ciudad` varchar(45) DEFAULT NULL,
  `salud` varchar(45) DEFAULT NULL,
  `afp_id` int(11) DEFAULT NULL,
  `tipo_usuario` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_rrhh_afp1_idx` (`afp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=30 ;

--
-- Volcar la base de datos para la tabla `rrhh`
--

INSERT INTO `rrhh` (`id`, `rut`, `nombres`, `apellidos`, `documento`, `nacionalidad`, `sexo`, `fecha_nacimiento`, `estado_civil`, `direccion`, `telefono`, `celular`, `email`, `ciudad`, `salud`, `afp_id`, `tipo_usuario`) VALUES
(1, '12.023.864-7', 'Ronald Andrés', 'Caicedo Garay', 'Carnet', 'Chilena', 'Masculino', '1969-12-31', 'Casado(a)', 'Santiago Bennit Chavez 581, Villa Saucache', '058-2242049', '77662399', 'caicedoarquitecto@gmail.com ', 'Arica', 'Isapre', NULL, NULL),
(2, '14.105.542-9', 'Freddy Bernardo', 'Alvarez Mamani', 'Carnet', 'Chilena', 'Masculino', '1981-05-20', 'Soltero(a)', 'Los Olmos 2689', '058-2-253616', '68340992', 'falvarez_m@hotmail.com', 'Arica', 'Fonasa', 3, NULL),
(3, '15692760-0', 'Irma del Carmen', 'Magnan Contreras', 'Carnet', 'Chilena', 'Femenino', '1983-07-20', 'Soltero(a)', 'Andres Bello 1515', '58-2-253616', '77661200', 'i.magnan.contreras@gmail.com', 'Arica', 'Fonasa', 1, NULL),
(5, '13862371-8', 'Álvaro Emanuel', 'Rivera Ramirez', 'Carnet', 'Chilena', 'Masculino', '1969-12-31', 'Soltero(a)', NULL, NULL, '56238693', 'ae.riveraramirez@gmail.com', 'Arica', NULL, 4, NULL),
(6, '13.639.240-9', 'Andrea Alejandra', 'Heredia Palacios', 'Carnet', 'Chilena', 'Femenino', '1969-12-31', 'Soltero(a)', NULL, NULL, '87254689', 'topografo.palacios@gmail.com', 'Arica', 'Fonasa', 1, NULL),
(7, '16.666.547-7', 'Benjamin Eduardo', 'Garcia Gallardo', 'Carnet', 'Chilena', 'Masculino', '1969-12-31', 'Soltero(a)', NULL, NULL, '92116796', 'benja.edu@gmail.com', 'Arica', 'Fonasa', 3, NULL),
(8, '15.000.952-9', 'Nicolas Victor ', 'Miño Zapata', 'Carnet', 'Chilena', 'Masculino', '1969-12-31', 'Soltero(a)', NULL, '058-317176', '87852006', 'nicolasmino@yahoo.com', 'Arica', 'Fonasa', 4, NULL),
(9, '16.466.739-1', 'Daniela', 'Zegarra Bori', 'Carnet', 'Chilena', 'Femenino', '1969-12-31', 'Soltero(a)', NULL, NULL, '68420109', 'boridaniela@gmail.com', 'Arica', NULL, NULL, NULL),
(10, '18.615.910-1', 'Francisco Javier', 'Tarque Cañipa', 'Carnet', 'Chilena', 'Masculino', '1969-12-31', 'Soltero(a)', NULL, '058-2226801', '90363201', 'franciscotarque@gmail.com', 'Arica', 'Fonasa', 3, NULL),
(11, '15.000.951-0', 'Sebastian Humberto', 'Miño Zapata', 'Carnet', 'Chilena', 'Masculino', '1969-12-31', 'Soltero(a)', NULL, '058-2317176', '85192318', 'correos.sm@gmail.com', 'Arica', 'Fonasa', 4, NULL),
(12, '14.107.505-5', 'Miguel Angel', 'Rojas Henriquez', 'Carnet', 'Chilena', 'Masculino', '1969-12-31', 'Soltero(a)', NULL, NULL, '93418509', 'miguel.rojas.henriquez@gmail.com', 'Arica', 'Fonasa', 1, NULL),
(13, '12.584.456-1', 'Cristian José', 'Heinsen Planella', 'Carnet', 'Chilena', 'Masculino', '1969-12-31', 'Casado(a)', 'Andrés Bello N° 1854', '058-2319573', '77658693', 'cristianheinsen@gmail.com', 'Arica', NULL, NULL, NULL),
(14, '21.431.230-1', 'Matilde', 'Cruz Tupa', 'Carnet', 'chilena', 'Femenino', '1969-12-31', 'Casado(a)', NULL, NULL, '87311882', 'matilda.cruz7@gmail.com', 'Arica', 'Fonasa', 4, NULL),
(15, '16.153.864-7', 'María Javiera ', 'Maino Gónzalez', 'Carnet', 'Chilena', 'Femenino', '1969-12-31', 'Soltero(a)', 'Las Codornices 1561', NULL, '88913712', 'javieramaino@gmail.com', 'Arica', NULL, NULL, NULL),
(16, '15.026.747-1', 'Yocely', 'Pinilla Alaniz', 'Carnet', 'Chilena', 'Masculino', NULL, 'Casado(a)', 'Santiago Chávez 581,', NULL, '89025723', 'pinilla.alaniz@gmail.com', 'Arica', NULL, NULL, NULL),
(17, '13.637.454-0', 'Rafaela Regina', 'Rabello Condori', 'Carnet', 'Chilena', 'Femenino', '1969-12-31', 'Soltero(a)', NULL, NULL, '89865989', 'seemy_8@hotmail.com', 'Arica', NULL, NULL, NULL),
(18, '16770824-2', 'Israel Antonio', 'Quispe Lazaro', 'Carnet', 'Chilena', 'Masculino', '1969-12-31', 'Soltero(a)', 'Las Maitas 2056', '058-2242153', '16770824-2', 'quispearq@gmail.com', 'Arica', 'Fonasa', NULL, NULL),
(19, '13255357-2', 'Magdalena', 'Pereira Campos', 'Carnet', 'Chilena', 'Femenino', '1969-12-31', 'Casado(a)', 'Andrés Bello 1854', '058-2319573', '77519298', 'pereiramagdalena@gmail.com', 'Arica', NULL, NULL, NULL),
(20, '9053277-4', 'Carlos Alberto', 'Nuñez Soza', 'Carnet', 'Chilena', 'Masculino', '1969-12-31', 'Soltero(a)', 'Agustin Edwards 2238', NULL, '94041970', 'carlosvisviri@gmail.com', 'Arica', 'Fonasa', 1, NULL),
(21, '16.225.568-1', 'Crsitian Antonio', 'Brandau Mardonez', 'Carnet', 'Chilena', 'Masculino', '1969-12-31', 'Soltero(a)', 'Los Artesanos  819', NULL, '82274316', 'cbrandau@live.cl', 'Arica', 'Isapre', 3, NULL),
(22, '14.174.422-4', 'Alejandra Valeska', 'Vargas Sepúlveda', 'Carnet', 'chilena', 'Femenino', '1969-12-31', 'Soltero(a)', 'Diego de Almagro 1913', NULL, '92063738', 'alejandravargas.uchile@gmail.com', 'Arica', NULL, NULL, NULL),
(25, '16469470-4', 'Francisco Javier', 'Abalan Osorio', 'Carnet', 'Chilena', 'Masculino', '1969-12-31', 'Soltero(a)', 'test', '2222', '2222', 'rapbore@gmail.com', 'Arica', 'Fonasa', 1, 'Director Ejecutivo'),
(28, '17098765-9', 'test', 'test', 'Carnet', 'test', 'Masculino', '1969-12-31', 'Soltero(a)', 'test', '222', '222', 'test@test.cl', 'test', NULL, NULL, 'Usuario Comun'),
(29, '16773187-2', 'Test', 'test', 'Carnet', 'Chilena', 'Masculino', '1969-12-31', 'Soltero(a)', 'TEst', '222', '222', 'test@test.cl', 'ARica', 'Fonasa', NULL, 'Usuario Comun');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitud`
--

CREATE TABLE IF NOT EXISTS `solicitud` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proyecto_id` int(11) NOT NULL,
  `proveedor_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `tipo_solicitud` varchar(50) NOT NULL,
  `total` int(11) NOT NULL DEFAULT '0',
  `estado` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_solicitud_proyecto1_idx` (`proyecto_id`),
  KEY `fk_solicitud_proveedor1_idx` (`proveedor_id`),
  KEY `fk_solicitud_user1_idx` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- Volcar la base de datos para la tabla `solicitud`
--

INSERT INTO `solicitud` (`id`, `proyecto_id`, `proveedor_id`, `user_id`, `fecha`, `tipo_solicitud`, `total`, `estado`) VALUES
(22, 13, 1, 1, '2013-05-03', 'Transferencia', 2000, 'EMITIDA'),
(23, 1, 1, 25, '2013-05-11', 'Transferencia', 10000, 'EMITIDA'),
(24, 1, 1, 1, '2013-06-13', 'Cheque', 11000, 'EMITIDA'),
(25, 2, 16, 1, '2013-06-17', 'Transferencia', 10000, 'EMITIDA'),
(26, 2, 1, 1, NULL, 'Cheque', 0, 'PENDIENTE'),
(27, 1, 1, 1, '2014-03-11', 'Cheque', 10000, 'EMITIDA'),
(28, 13, 1, 1, '2014-03-18', 'Cheque', 4000, 'EMITIDA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_contrato`
--

CREATE TABLE IF NOT EXISTS `tipo_contrato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(45) NOT NULL,
  `caracteristicas` blob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcar la base de datos para la tabla `tipo_contrato`
--

INSERT INTO `tipo_contrato` (`id`, `tipo`, `caracteristicas`) VALUES
(1, 'Honorario', NULL),
(2, 'Por Obra', NULL),
(3, 'Indefinido', 0x436f6e747261746f206465207469656d706f20696e646566696e69646f),
(4, 'Plazo Fijo', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_ingreso`
--

CREATE TABLE IF NOT EXISTS `tipo_ingreso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcar la base de datos para la tabla `tipo_ingreso`
--

INSERT INTO `tipo_ingreso` (`id`, `nombre`) VALUES
(1, 'Convenio'),
(2, 'Donaciones'),
(3, 'Venta de Servicios'),
(4, 'Otros');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) DEFAULT NULL,
  `tipo` varchar(45) DEFAULT NULL,
  `rrhh_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_rrhh1_idx` (`rrhh_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

--
-- Volcar la base de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `email`, `tipo`, `rrhh_id`) VALUES
(1, 'admin', 'c63b447bcee0c5064cac9d264e62114e2d50516e', NULL, 'admin', NULL),
(2, 'cheinsen', 'eb39dfc1b8c562544b2cfbc9e6af327074112e59', NULL, 'admin', NULL),
(3, 'imagnan', 'ae5ceec654fd01f675e1e8fbb3632e7a8a54f81d', NULL, 'admin', NULL),
(4, 'rrabello', 'a2d845a66ed199346cb483a3463648864206fa22', NULL, 'admin', NULL),
(5, 'rcaicedo', '91c84a9a609c38cf41d7430919a1728a1584690f', NULL, 'Jefe de Taller', NULL),
(6, 'jedwards', '849edd3841a00276e1da66fde86d0e66fc6183bc', NULL, 'Usuario Comun', NULL),
(7, 'aheredia', 'e57ec27e9f3f4c9f990683db11227077e3434dc2', NULL, 'Usuario Comun', NULL),
(8, 'bgarcia', 'c42168455082fbdecd34c492444bdc2f2ed4f411', NULL, 'Usuario Comun', NULL),
(9, 'mrojas', '3a5f7eb95d80108be0c7eab9946955903a883ca2', NULL, 'Usuario Comun', NULL),
(10, 'nmiño', '4328714fe40bc2cb2bb3693e45462e3b83353d1d', NULL, 'Usuario Comun', NULL),
(11, 'mpereira', '312e6381d13a31d8318451d84674e8dc84b69715', NULL, 'Jefe de Taller', NULL),
(12, 'mmaino', '1d5eb5a87b0ab407fae6ed1d7522e870ab5f1908', NULL, 'Jefe de Taller', NULL),
(13, 'arivera', '52846307f8d9a30d475a3a51f53e84de52e22c2f', NULL, 'Usuario Comun', NULL),
(14, 'falvarez', '2ebaa03abaf27df75394ad36e44db225cee80bbe', NULL, 'Jefe de Taller', NULL),
(15, 'smiño', '5eb73b57e50421c0227f9aefda967d74762433dd', NULL, 'Usuario Comun', NULL),
(16, 'cbrandau', '756c2b92537a543d55a4d2568928cff16a842426', NULL, 'Jefe de Taller', NULL),
(17, 'dzegarra', '4029ddef689a6c4fe62d8cadab21c2e58c7afe59', NULL, 'Usuario Comun', NULL),
(18, 'iquispe', 'd89131353756c9c7928d83df73cb0b75942928ec', NULL, 'Usuario Comun', NULL),
(19, 'aguillen', '41c8e995c265d8ff4e94f841fb063db293378097', NULL, 'Usuario Comun', NULL),
(20, 'frivera', '601c690fa5f3a2bca63e3d470c45b481a197642c', NULL, 'Jefe de Taller', NULL),
(21, 'ftarque', '6a27b8e8a0171f20a4c7371a6a38ada3ba95dcb7', NULL, 'Usuario Comun', NULL),
(22, 'ypinilla', '44576ca64c02a8da5a311392850067616e64bfa6', NULL, 'Usuario Comun', NULL),
(23, 'avargas', 'c030f9cd8e39ccafb1666d1fcaef933609db2926', NULL, 'Usuario Comun', 22),
(24, 'cwagner', '48863b82290bd98529f617a918daa2ed3cd0a3cb', NULL, 'Usuario Comun', NULL),
(25, 'usuario_comun', '11a020deebfce89ba5aae991084c5ec3e41babe3', NULL, 'Usuario Comun', NULL),
(28, '16469470-4', '3f8d9186a2061678fcb7aa26e53e7caf3089f32b', 'rapbore@gmail.com', 'Director Ejecutivo', 25),
(30, '17098765-9', '7f7ef266aeeb7fcb16134919eb67cd351e281ec2', 'test@test.cl', 'Usuario Comun', 28),
(31, '16773187-2', '0153716dafe6253dd602920b143f09e36731ea2c', 'test@test.cl', 'Usuario Comun', 29);

--
-- Filtros para las tablas descargadas (dump)
--

--
-- Filtros para la tabla `authassignment`
--
ALTER TABLE `authassignment`
  ADD CONSTRAINT `authassignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `authitemchild`
--
ALTER TABLE `authitemchild`
  ADD CONSTRAINT `authitemchild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `authitemchild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `comuna`
--
ALTER TABLE `comuna`
  ADD CONSTRAINT `fk_comuna_region1` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `contrato`
--
ALTER TABLE `contrato`
  ADD CONSTRAINT `fk_contrato_proyecto1` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_contrato_rrhh1` FOREIGN KEY (`rrhh_id`) REFERENCES `rrhh` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_contrato_tipo_contrato1` FOREIGN KEY (`tipo_contrato_id`) REFERENCES `tipo_contrato` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `cuenta_especifica`
--
ALTER TABLE `cuenta_especifica`
  ADD CONSTRAINT `fk_cuenta_especifica_cuenta_contable1` FOREIGN KEY (`cuenta_contable_id`) REFERENCES `cuenta_contable` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `emision`
--
ALTER TABLE `emision`
  ADD CONSTRAINT `fk_emision_cuenta1` FOREIGN KEY (`cuenta_id`) REFERENCES `cuenta` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_emision_proveedor1` FOREIGN KEY (`proveedor_id`) REFERENCES `proveedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_emision_proyecto1` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_emision_solicitud1` FOREIGN KEY (`solicitud_id`) REFERENCES `solicitud` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_emision_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ingreso`
--
ALTER TABLE `ingreso`
  ADD CONSTRAINT `fk_presupuesto_financiamiento1` FOREIGN KEY (`financiamiento_id`) REFERENCES `financiamiento` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_presupuesto_proyecto1` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_presupuesto_tipo_ingreso1` FOREIGN KEY (`tipo_ingreso_id`) REFERENCES `tipo_ingreso` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `item_presupuesto`
--
ALTER TABLE `item_presupuesto`
  ADD CONSTRAINT `fk_item_presupuesto_cuenta_contable1` FOREIGN KEY (`cuenta_contable_id`) REFERENCES `cuenta_contable` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_presupuesto_presupuesto1` FOREIGN KEY (`presupuesto_id`) REFERENCES `presupuesto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_presupuesto_proyecto1` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `item_rendicion`
--
ALTER TABLE `item_rendicion`
  ADD CONSTRAINT `fk_item_rendicion_cuenta1` FOREIGN KEY (`cuenta_id`) REFERENCES `cuenta` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_rendicion_cuenta_contable1` FOREIGN KEY (`cuenta_contable_id`) REFERENCES `cuenta_contable` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_rendicion_cuenta_especifica1` FOREIGN KEY (`cuenta_especifica_id`) REFERENCES `cuenta_especifica` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_rendicion_proveedor1` FOREIGN KEY (`proveedor_id`) REFERENCES `proveedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_rendicion_proyecto1` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_rendicion_rendicion1` FOREIGN KEY (`rendicion_id`) REFERENCES `rendicion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `item_rendicion_directa`
--
ALTER TABLE `item_rendicion_directa`
  ADD CONSTRAINT `fk_item_rendicion_directa_cuenta_contable` FOREIGN KEY (`cuenta_contable_id`) REFERENCES `cuenta_contable` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_rendicion_directa_cuenta_especifica` FOREIGN KEY (`cuenta_especifica_id`) REFERENCES `cuenta_especifica` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_rendicion_directa_proveedor` FOREIGN KEY (`proveedor_id`) REFERENCES `proveedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_rendicion_directa_proyecto` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_rendicion_directa_rendicion_directa` FOREIGN KEY (`rendicion_directa_id`) REFERENCES `rendicion_directa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `item_solicitud`
--
ALTER TABLE `item_solicitud`
  ADD CONSTRAINT `fk_item_solicitud_cuenta_contable1` FOREIGN KEY (`cuenta_contable_id`) REFERENCES `cuenta_contable` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_solicitud_solicitud1` FOREIGN KEY (`solicitud_id`) REFERENCES `solicitud` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `presupuesto`
--
ALTER TABLE `presupuesto`
  ADD CONSTRAINT `fk_presupuesto_proyecto2` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_presupuesto_tipo_ingreso2` FOREIGN KEY (`tipo_ingreso_id`) REFERENCES `tipo_ingreso` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `proyecto`
--
ALTER TABLE `proyecto`
  ADD CONSTRAINT `fk_proyecto_centro_costo1` FOREIGN KEY (`centro_costo_id`) REFERENCES `centro_costo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_proyecto_rrhh1` FOREIGN KEY (`rrhh_id`) REFERENCES `rrhh` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `rendicion`
--
ALTER TABLE `rendicion`
  ADD CONSTRAINT `fk_rendicion_cuenta1` FOREIGN KEY (`cuenta_id`) REFERENCES `cuenta` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_rendicion_emision1` FOREIGN KEY (`emision_id`) REFERENCES `emision` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_rendicion_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `rendicion_directa`
--
ALTER TABLE `rendicion_directa`
  ADD CONSTRAINT `fk_rendicion_directa_cuenta` FOREIGN KEY (`cuenta_id`) REFERENCES `cuenta` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `rights`
--
ALTER TABLE `rights`
  ADD CONSTRAINT `rights_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `rrhh`
--
ALTER TABLE `rrhh`
  ADD CONSTRAINT `fk_rrhh_afp1` FOREIGN KEY (`afp_id`) REFERENCES `afp` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `solicitud`
--
ALTER TABLE `solicitud`
  ADD CONSTRAINT `fk_solicitud_proveedor1` FOREIGN KEY (`proveedor_id`) REFERENCES `proveedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_solicitud_proyecto1` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_solicitud_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_user_rrhh1` FOREIGN KEY (`rrhh_id`) REFERENCES `rrhh` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
