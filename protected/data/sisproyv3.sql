SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `sisproy` DEFAULT CHARACTER SET utf8 ;
USE `sisproy` ;

-- -----------------------------------------------------
-- Table `sisproy`.`authitem`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sisproy`.`authitem` (
  `name` VARCHAR(64) NOT NULL ,
  `type` INT(11) NOT NULL ,
  `description` TEXT NULL DEFAULT NULL ,
  `bizrule` TEXT NULL DEFAULT NULL ,
  `data` TEXT NULL DEFAULT NULL ,
  PRIMARY KEY (`name`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sisproy`.`authassignment`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sisproy`.`authassignment` (
  `itemname` VARCHAR(64) NOT NULL ,
  `userid` VARCHAR(64) NOT NULL ,
  `bizrule` TEXT NULL DEFAULT NULL ,
  `data` TEXT NULL DEFAULT NULL ,
  PRIMARY KEY (`itemname`, `userid`) ,
  CONSTRAINT `authassignment_ibfk_1`
    FOREIGN KEY (`itemname` )
    REFERENCES `sisproy`.`authitem` (`name` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sisproy`.`authitemchild`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sisproy`.`authitemchild` (
  `parent` VARCHAR(64) NOT NULL ,
  `child` VARCHAR(64) NOT NULL ,
  PRIMARY KEY (`parent`, `child`) ,
  INDEX `child` (`child` ASC) ,
  CONSTRAINT `authitemchild_ibfk_1`
    FOREIGN KEY (`parent` )
    REFERENCES `sisproy`.`authitem` (`name` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `authitemchild_ibfk_2`
    FOREIGN KEY (`child` )
    REFERENCES `sisproy`.`authitem` (`name` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sisproy`.`centro_costo`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sisproy`.`centro_costo` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `nombre` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sisproy`.`afp`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sisproy`.`afp` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `nombre` VARCHAR(100) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sisproy`.`rrhh`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sisproy`.`rrhh` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `rut` VARCHAR(13) NULL DEFAULT NULL ,
  `nombres` VARCHAR(255) NULL DEFAULT NULL ,
  `apellidos` VARCHAR(255) NULL DEFAULT NULL ,
  `documento` VARCHAR(45) NULL ,
  `nacionalidad` VARCHAR(45) NULL ,
  `sexo` VARCHAR(15) NULL DEFAULT NULL ,
  `fecha_nacimiento` DATE NULL DEFAULT NULL ,
  `estado_civil` VARCHAR(45) NULL ,
  `direccion` VARCHAR(255) NULL DEFAULT NULL ,
  `telefono` VARCHAR(50) NULL DEFAULT NULL ,
  `celular` VARCHAR(50) NULL DEFAULT NULL ,
  `email` VARCHAR(255) NULL DEFAULT NULL ,
  `ciudad` VARCHAR(45) NULL ,
  `salud` VARCHAR(45) NULL ,
  `afp_id` INT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_rrhh_afp1_idx` (`afp_id` ASC) ,
  CONSTRAINT `fk_rrhh_afp1`
    FOREIGN KEY (`afp_id` )
    REFERENCES `sisproy`.`afp` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sisproy`.`proyecto`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sisproy`.`proyecto` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `rrhh_id` INT(11) NOT NULL ,
  `centro_costo_id` INT(11) NOT NULL ,
  `nombre` VARCHAR(255) NULL DEFAULT NULL ,
  `sigla` VARCHAR(100) NULL ,
  `descripcion` BLOB NULL DEFAULT NULL ,
  `archivo` BLOB NULL DEFAULT NULL ,
  `contrato` BLOB NULL DEFAULT NULL ,
  `comuna` VARCHAR(255) NULL DEFAULT NULL ,
  `region` VARCHAR(255) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_proyecto_centro_costo1_idx` (`centro_costo_id` ASC) ,
  INDEX `fk_proyecto_rrhh1_idx` (`rrhh_id` ASC) ,
  CONSTRAINT `fk_proyecto_centro_costo1`
    FOREIGN KEY (`centro_costo_id` )
    REFERENCES `sisproy`.`centro_costo` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_proyecto_rrhh1`
    FOREIGN KEY (`rrhh_id` )
    REFERENCES `sisproy`.`rrhh` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sisproy`.`tipo_contrato`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sisproy`.`tipo_contrato` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `tipo` VARCHAR(45) NOT NULL ,
  `caracteristicas` BLOB NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sisproy`.`contrato`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sisproy`.`contrato` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `rrhh_id` INT(11) NOT NULL ,
  `proyecto_id` INT(11) NOT NULL ,
  `tipo_contrato_id` INT(11) NOT NULL ,
  `fecha_inicio` DATE NULL DEFAULT NULL ,
  `fecha_termino` DATE NULL DEFAULT NULL ,
  `remuneracion` INT(11) NULL DEFAULT NULL ,
  `estado` VARCHAR(45) NULL DEFAULT 'VIGENTE' ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_contrato_proyecto1_idx` (`proyecto_id` ASC) ,
  INDEX `fk_contrato_tipo_contrato1_idx` (`tipo_contrato_id` ASC) ,
  INDEX `fk_contrato_rrhh1_idx` (`rrhh_id` ASC) ,
  CONSTRAINT `fk_contrato_proyecto1`
    FOREIGN KEY (`proyecto_id` )
    REFERENCES `sisproy`.`proyecto` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_contrato_rrhh1`
    FOREIGN KEY (`rrhh_id` )
    REFERENCES `sisproy`.`rrhh` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_contrato_tipo_contrato1`
    FOREIGN KEY (`tipo_contrato_id` )
    REFERENCES `sisproy`.`tipo_contrato` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sisproy`.`cuenta_contable`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sisproy`.`cuenta_contable` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `nombre` VARCHAR(100) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sisproy`.`cuenta_especifica`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sisproy`.`cuenta_especifica` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `cuenta_contable_id` INT(11) NOT NULL ,
  `nombre` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_cuenta_especifica_cuenta_contable1_idx` (`cuenta_contable_id` ASC) ,
  CONSTRAINT `fk_cuenta_especifica_cuenta_contable1`
    FOREIGN KEY (`cuenta_contable_id` )
    REFERENCES `sisproy`.`cuenta_contable` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 50
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sisproy`.`proveedor`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sisproy`.`proveedor` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `rut` VARCHAR(13) NULL DEFAULT NULL ,
  `nombre` VARCHAR(255) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sisproy`.`user`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sisproy`.`user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `username` VARCHAR(128) NOT NULL ,
  `password` VARCHAR(128) NOT NULL ,
  `email` VARCHAR(128) NULL DEFAULT NULL ,
  `tipo` VARCHAR(45) NULL ,
  `rrhh_id` INT(11) NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_user_rrhh1_idx` (`rrhh_id` ASC) ,
  CONSTRAINT `fk_user_rrhh1`
    FOREIGN KEY (`rrhh_id` )
    REFERENCES `sisproy`.`rrhh` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sisproy`.`solicitud`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sisproy`.`solicitud` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `proyecto_id` INT(11) NOT NULL ,
  `proveedor_id` INT(11) NOT NULL ,
  `user_id` INT(11) NOT NULL ,
  `fecha` DATE NULL DEFAULT NULL ,
  `tipo_solicitud` VARCHAR(50) NOT NULL ,
  `total` INT(11) NOT NULL DEFAULT '0' ,
  `estado` VARCHAR(45) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_solicitud_proyecto1_idx` (`proyecto_id` ASC) ,
  INDEX `fk_solicitud_proveedor1_idx` (`proveedor_id` ASC) ,
  INDEX `fk_solicitud_user1_idx` (`user_id` ASC) ,
  CONSTRAINT `fk_solicitud_proveedor1`
    FOREIGN KEY (`proveedor_id` )
    REFERENCES `sisproy`.`proveedor` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_solicitud_proyecto1`
    FOREIGN KEY (`proyecto_id` )
    REFERENCES `sisproy`.`proyecto` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_solicitud_user1`
    FOREIGN KEY (`user_id` )
    REFERENCES `sisproy`.`user` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 26
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sisproy`.`emision`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sisproy`.`emision` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `user_id` INT(11) NOT NULL ,
  `proveedor_id` INT(11) NOT NULL ,
  `proyecto_id` INT(11) NOT NULL ,
  `solicitud_id` INT(11) NOT NULL ,
  `ciudad` VARCHAR(100) NULL DEFAULT NULL ,
  `fecha` DATE NULL DEFAULT NULL ,
  `concepto` VARCHAR(255) NULL DEFAULT NULL ,
  `total_debe` INT(11) NOT NULL DEFAULT '0' ,
  `total_haber` INT(11) NOT NULL DEFAULT '0' ,
  `cuenta` VARCHAR(100) NULL DEFAULT NULL ,
  `tipo_solicitud` VARCHAR(50) NULL DEFAULT NULL ,
  `tipo_cheque` VARCHAR(45) NULL ,
  `numero_cheque` VARCHAR(100) NULL DEFAULT NULL ,
  `observaciones` BLOB NULL DEFAULT NULL ,
  `estado` VARCHAR(45) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_emision_solicitud1_idx` (`solicitud_id` ASC) ,
  INDEX `fk_emision_proyecto1_idx` (`proyecto_id` ASC) ,
  INDEX `fk_emision_proveedor1_idx` (`proveedor_id` ASC) ,
  INDEX `fk_emision_user1` (`user_id` ASC) ,
  CONSTRAINT `fk_emision_proveedor1`
    FOREIGN KEY (`proveedor_id` )
    REFERENCES `sisproy`.`proveedor` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_emision_proyecto1`
    FOREIGN KEY (`proyecto_id` )
    REFERENCES `sisproy`.`proyecto` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_emision_solicitud1`
    FOREIGN KEY (`solicitud_id` )
    REFERENCES `sisproy`.`solicitud` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_emision_user1`
    FOREIGN KEY (`user_id` )
    REFERENCES `sisproy`.`user` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 29
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sisproy`.`financiamiento`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sisproy`.`financiamiento` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `nombre` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sisproy`.`rendicion`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sisproy`.`rendicion` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `user_id` INT(11) NOT NULL ,
  `emision_id` INT(11) NOT NULL ,
  `asignacion` VARCHAR(100) NULL DEFAULT NULL ,
  `fecha` DATE NULL DEFAULT NULL ,
  `comuna` VARCHAR(100) NULL DEFAULT NULL ,
  `region` VARCHAR(100) NULL DEFAULT NULL ,
  `tipo_pago` VARCHAR(100) NULL DEFAULT NULL ,
  `total_entrada` INT(11) NULL DEFAULT NULL ,
  `total_salida` INT(11) NOT NULL DEFAULT '0' ,
  `total_saldo` INT(11) NOT NULL DEFAULT '0' ,
  `observacion` BLOB NULL DEFAULT NULL ,
  `estado` VARCHAR(45) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_rendicion_emision1_idx` (`emision_id` ASC) ,
  INDEX `fk_rendicion_user1_idx` (`user_id` ASC) ,
  CONSTRAINT `fk_rendicion_emision1`
    FOREIGN KEY (`emision_id` )
    REFERENCES `sisproy`.`emision` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rendicion_user1`
    FOREIGN KEY (`user_id` )
    REFERENCES `sisproy`.`user` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 16
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sisproy`.`item_rendicion`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sisproy`.`item_rendicion` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `proveedor_id` INT(11) NOT NULL ,
  `cuenta_contable_id` INT(11) NOT NULL ,
  `cuenta_especifica_id` INT(11) NOT NULL ,
  `rendicion_id` INT(11) NULL ,
  `proyecto_id` INT(11) NOT NULL ,
  `tipo_documento` VARCHAR(100) NULL ,
  `numero_documento` VARCHAR(100) NULL ,
  `tipo_pago` VARCHAR(100) NULL ,
  `fecha` DATE NULL ,
  `mes` VARCHAR(45) NULL ,
  `agno` VARCHAR(45) NULL ,
  `monto` INT(11) NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_item_rendicion_rendicion1_idx` (`rendicion_id` ASC) ,
  INDEX `fk_item_rendicion_proyecto1_idx` (`proyecto_id` ASC) ,
  INDEX `fk_item_rendicion_proveedor1_idx` (`proveedor_id` ASC) ,
  INDEX `fk_item_rendicion_cuenta_especifica1_idx` (`cuenta_especifica_id` ASC) ,
  INDEX `fk_item_rendicion_cuenta_contable1` (`cuenta_contable_id` ASC) ,
  CONSTRAINT `fk_item_rendicion_cuenta_contable1`
    FOREIGN KEY (`cuenta_contable_id` )
    REFERENCES `sisproy`.`cuenta_contable` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_item_rendicion_cuenta_especifica1`
    FOREIGN KEY (`cuenta_especifica_id` )
    REFERENCES `sisproy`.`cuenta_especifica` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_item_rendicion_proveedor1`
    FOREIGN KEY (`proveedor_id` )
    REFERENCES `sisproy`.`proveedor` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_item_rendicion_proyecto1`
    FOREIGN KEY (`proyecto_id` )
    REFERENCES `sisproy`.`proyecto` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_item_rendicion_rendicion1`
    FOREIGN KEY (`rendicion_id` )
    REFERENCES `sisproy`.`rendicion` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sisproy`.`item_solicitud`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sisproy`.`item_solicitud` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `solicitud_id` INT(11) NOT NULL ,
  `descripcion` BLOB NULL DEFAULT NULL ,
  `valor_unitario` INT(11) NULL DEFAULT NULL ,
  `cantidad` INT(11) NULL DEFAULT NULL ,
  `total` INT(11) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_item_solicitud_solicitud1_idx` (`solicitud_id` ASC) ,
  CONSTRAINT `fk_item_solicitud_solicitud1`
    FOREIGN KEY (`solicitud_id` )
    REFERENCES `sisproy`.`solicitud` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 19
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sisproy`.`tipo_ingreso`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sisproy`.`tipo_ingreso` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `nombre` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sisproy`.`ingreso`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sisproy`.`ingreso` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `proyecto_id` INT(11) NOT NULL ,
  `tipo_ingreso_id` INT(11) NOT NULL ,
  `financiamiento_id` INT(11) NOT NULL ,
  `fecha` DATE NULL DEFAULT NULL ,
  `monto` INT(11) NULL DEFAULT 0 ,
  `observaciones` BLOB NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_presupuesto_financiamiento1_idx` (`financiamiento_id` ASC) ,
  INDEX `fk_presupuesto_tipo_ingreso1_idx` (`tipo_ingreso_id` ASC) ,
  INDEX `fk_presupuesto_proyecto1_idx` (`proyecto_id` ASC) ,
  CONSTRAINT `fk_presupuesto_financiamiento1`
    FOREIGN KEY (`financiamiento_id` )
    REFERENCES `sisproy`.`financiamiento` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_presupuesto_proyecto1`
    FOREIGN KEY (`proyecto_id` )
    REFERENCES `sisproy`.`proyecto` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_presupuesto_tipo_ingreso1`
    FOREIGN KEY (`tipo_ingreso_id` )
    REFERENCES `sisproy`.`tipo_ingreso` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sisproy`.`rights`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sisproy`.`rights` (
  `itemname` VARCHAR(64) NOT NULL ,
  `type` INT(11) NOT NULL ,
  `weight` INT(11) NOT NULL ,
  PRIMARY KEY (`itemname`) ,
  CONSTRAINT `rights_ibfk_1`
    FOREIGN KEY (`itemname` )
    REFERENCES `sisproy`.`authitem` (`name` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sisproy`.`presupuesto`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sisproy`.`presupuesto` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `proyecto_id` INT(11) NOT NULL ,
  `tipo_ingreso_id` INT(11) NOT NULL ,
  `fecha` DATE NULL ,
  `mes` VARCHAR(45) NULL ,
  `agno` VARCHAR(45) NULL ,
  `monto` INT NULL DEFAULT 0 ,
  `observaciones` BLOB NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_presupuesto_proyecto2_idx` (`proyecto_id` ASC) ,
  INDEX `fk_presupuesto_tipo_ingreso2_idx` (`tipo_ingreso_id` ASC) ,
  CONSTRAINT `fk_presupuesto_proyecto2`
    FOREIGN KEY (`proyecto_id` )
    REFERENCES `sisproy`.`proyecto` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_presupuesto_tipo_ingreso2`
    FOREIGN KEY (`tipo_ingreso_id` )
    REFERENCES `sisproy`.`tipo_ingreso` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sisproy`.`item_presupuesto`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sisproy`.`item_presupuesto` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `presupuesto_id` INT NOT NULL ,
  `cuenta_contable_id` INT(11) NOT NULL ,
  `proyecto_id` INT(11) NOT NULL ,
  `fecha` DATE NULL ,
  `mes` VARCHAR(45) NULL ,
  `agno` VARCHAR(45) NULL ,
  `monto` INT NULL DEFAULT 0 ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_item_presupuesto_presupuesto1_idx` (`presupuesto_id` ASC) ,
  INDEX `fk_item_presupuesto_cuenta_contable1_idx` (`cuenta_contable_id` ASC) ,
  INDEX `fk_item_presupuesto_proyecto1_idx` (`proyecto_id` ASC) ,
  CONSTRAINT `fk_item_presupuesto_presupuesto1`
    FOREIGN KEY (`presupuesto_id` )
    REFERENCES `sisproy`.`presupuesto` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_item_presupuesto_cuenta_contable1`
    FOREIGN KEY (`cuenta_contable_id` )
    REFERENCES `sisproy`.`cuenta_contable` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_item_presupuesto_proyecto1`
    FOREIGN KEY (`proyecto_id` )
    REFERENCES `sisproy`.`proyecto` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sisproy`.`region`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sisproy`.`region` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `nombre` VARCHAR(100) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sisproy`.`comuna`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sisproy`.`comuna` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `nombre` VARCHAR(100) NOT NULL ,
  `region_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_comuna_region1_idx` (`region_id` ASC) ,
  CONSTRAINT `fk_comuna_region1`
    FOREIGN KEY (`region_id` )
    REFERENCES `sisproy`.`region` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
