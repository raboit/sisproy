
<?php

?>

<?php $this->widget('bootstrap.widgets.TbAlert', array(
        'block'=>true, // display a larger alert block?
        'fade'=>true, // use transitions?
        'closeText'=>'&times;', // close link text - if set to false, no close link is displayed
        
    )); ?>

<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	Yii::t('app', 'Create'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Cancel') . ' ' . $model->label(), 'url' => array('proyecto/ver','id'=>$model->proyecto_id)),
);
?>

<h1><?php echo Yii::t('app', 'Create') . ' ' . GxHtml::encode($model->label()); ?></h1>


<?php
$this->renderPartial('_crear', array(
		'model' => $model,
		'buttons' => 'create'));
?>