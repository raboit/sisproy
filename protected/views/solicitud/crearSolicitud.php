<?php

$this->breadcrumbs = array(
	$model->label(2) => array('verTodos'),
	Yii::t('app', 'Create'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url' => array('verTodos')),
);
?>

<h1><?php echo Yii::t('app', 'Create') . ' ' . GxHtml::encode($model->label()); ?></h1>

<?php
$this->renderPartial('_crearSolicitud', array(
		'model' => $model,
		'buttons' => 'create'));
?>