<?php $this->widget('bootstrap.widgets.TbAlert', array(
        'block'=>true, // display a larger alert block?
        'fade'=>true, // use transitions?
        'closeText'=>'&times;', // close link text - if set to false, no close link is displayed
        
    )); ?>

<?php
$this->breadcrumbs = array(
	$model->label(2) => array('verTodos'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	
	array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('actualizar', 'id' => $model->id)),
	//array('label'=>Yii::t('app', 'Add') . ' Item', 'url'=>array('itemSolicitud/agregar','solicitud_id'=>$model->id)),
        //array('label'=>Yii::t('app', 'Finish'). ' '. $model->label(), 'url'=>array('solicitud/finalizar','solicitud_id'=>$model->id)),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>
<?php $this->widget('application.extensions.print.printWidget', array(
                   'cssFile'=>'print.css',
                   'printedElement'=>'#content')); 
    ?>
<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
            'id',
            'proyecto',
            'proveedor',
            'user',
            'fecha',
            'tipo_solicitud',
            array(
                'name' => 'total',
                'value' => Yii::app()->format->formatNumber($model->sumaMontos),
            ),
            'estado',
            
	),
)); ?>



<?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
    'buttons'=>array(
        array(
                'label'=>'Finalizar',
            'htmlOptions'=>array(
                'data-toggle'=>'modal',
                'data-target'=>'#modalFinalizar',
            ),
                'url'=>array('solicitud/finalizar','id'=>$model->id),
                'icon'=>'ok white',
                'type'=>'primary'
            ),
        array('label'=>'Anular',
            'htmlOptions'=>array(
                'data-toggle'=>'modal',
                'data-target'=>'#modalAnular',
            ),
            'url'=>array('solicitud/anular','id'=>$model->id),
            'icon'=>'remove white',
            'type'=>'danger',
            ),
        array('label'=>'Agregar Item',
            
            'url'=>array('itemSolicitud/agregar','solicitud_id'=>$model->id),
            'icon'=>'plus white',
            'type'=>'success'),

    ),
)); ?>



<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered',
    'dataProvider' => $model_items,
    'enableSorting'=>false,
    'template'=> '{summary}{pager}{items}{pager}',
    'columns' => array(
		array(
				'name'=>'cuenta_contable_id',
				'value'=>'GxHtml::valueEx($data->cuentaContable)',
				'filter'=>GxHtml::listDataEx(CuentaContable::model()->findAllAttributes(null, true)),
				),
                array(
                        'name' => 'valor_unitario',
                        'value' => 'Yii::app()->format->formatNumber($data->valor_unitario)',
                    ),
		'cantidad',
                array(
                                'name' => 'Total',
                                'value' => 'Yii::app()->format->formatNumber($data->total)',
                            ),
                'descripcion',
                array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'htmlOptions'=>array('style'=>'width: 50px'),
			'template'=>'{view} {update}',
			'buttons'=>array(
				'view' => array(
					'label'=>'Ver Item Solicitud',
					'url'=>'Yii::app()->createUrl("itemSolicitud/ver", array("itemsolicitud_id"=>$data->id))',
				),
				'update' => array(
					'label'=>'Actualizar Item Solicitud',
					'url'=>'Yii::app()->createUrl("itemSolicitud/actualizar", array("itemsolicitud_id"=>$data->id))',
				),
			),
				
                ),
    ),
));?>
    <div class="row">
        <div class="well pull-right" style="width:300px;">
            <h4>Total Solicitado</h4>
            <h3>
                <?php
                //echo Yii::app()->numberFormatter->formatCurrency($model->saldo, 'CLP');
                $items = $model_items;
                $items->setPagination(false);
                echo "$" . Yii::app()->format->formatNumber(Funciones::Suma($items->getData(), 'total'));
                ?>
            </h3>
        </div>
    </div>

<br><br><br>

 <div class="row">
        <div class="well pull-right" style="width:300px;">
            <h3>
                <?php
                echo "<br>";
                echo "<hr> ";
                ?>
            </h3>            
             <center><h4>FIRMA</h4></center>
        </div>
    </div>


<?php /* FUERA DE LO VISIBLE */ ?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array('id'=>'modalFinalizar')); ?>
 
<div class="modal-header">
    <a class="close" data-dismiss="modal">&times;</a>
    <h4>Finalizar</h4>
</div>
 
<div class="modal-body">
    <p>¿Estás seguro que desea finalizar?</p>
</div>
 
<div class="modal-footer">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'type'=>'primary',
        'label'=>'Finalizar',
        'icon'=>'ok white',
        'type'=>'primary',
        'url'=>array('solicitud/finalizar','id'=>$model->id),
        //'htmlOptions'=>array('data-dismiss'=>'modal'),
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label'=>'Cancelar',
        'url'=>'#',
        'htmlOptions'=>array('data-dismiss'=>'modal'),
    )); ?>
</div>
 
<?php $this->endWidget(); ?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array('id'=>'modalAnular')); ?>
 
<div class="modal-header">
    <a class="close" data-dismiss="modal">&times;</a>
    <h4>Anular</h4>
</div>
 
<div class="modal-body">
    <p>¿Estás seguro que desea anular?</p>
</div>
 
<div class="modal-footer">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'type'=>'primary',
        'label'=>'Anular',
        'icon'=>'remove white',
        'type'=>'danger',
        'url'=>array('solicitud/anular','id'=>$model->id),
        //'htmlOptions'=>array('data-dismiss'=>'modal'),
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label'=>'Cancelar',
        'url'=>'#',
        'htmlOptions'=>array('data-dismiss'=>'modal'),
    )); ?>
</div>
 
<?php $this->endWidget(); ?>
