<?php

$this->breadcrumbs = array(
	$model->label(2) => array('finalizadas'),
	'Ver Solicitud',
);

$this->menu=array(
		array('label'=>Yii::t('app', 'Back'), 'url'=>array('finalizadas')),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
'id',
'proyecto',
'proveedor',
'fecha',
'tipo_solicitud',
array(
    'name' => 'total',
    'value' => Yii::app()->format->formatNumber($model->total),
),
'estado',
	),
)); ?>
