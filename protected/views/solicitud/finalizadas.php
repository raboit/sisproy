<?php

$this->breadcrumbs = array(
	'Solicitudes Finalizadas',
);

$this->menu = array(
		array('label'=>Yii::t('app', 'Admin') . ' ' . 'Emisiones', 'url'=>array('emision/verTodos')),
	);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').slideToggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('solicitud-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Create') . ' ' . 'Emision'; ?></h1></br>

<p>


<h3><?php echo Yii::t('app', 'View') . ' ' . 'Solicitudes Finalizadas'; ?></h3>
<?php echo Yii::t('app', 'Text Option Search'); ?></p>
<?php 
$this->widget('bootstrap.widgets.TbMenu', array(
	'type'=>'pills',
	'items'=>array(
		//array('label'=>Yii::t('app', 'Create'), 'icon'=>'icon-plus', 'url'=>Yii::app()->controller->createUrl('create'), 'linkOptions'=>array()),
                //array('label'=>Yii::t('app', 'List'), 'icon'=>'icon-th-list', 'url'=>Yii::app()->controller->createUrl('admin'),'active'=>true, 'linkOptions'=>array()),
		array('label'=>Yii::t('app', 'Search'), 'icon'=>'icon-search', 'url'=>'#', 'linkOptions'=>array('class'=>'search-button')),
		//array('label'=>Yii::t('app', 'Export to PDF'), 'icon'=>'icon-download', 'url'=>Yii::app()->controller->createUrl('GeneratePdf'), 'linkOptions'=>array('target'=>'_blank'), 'visible'=>true),
		array('label'=>Yii::t('app', 'Export to Excel'), 'icon'=>'icon-download', 'url'=>Yii::app()->controller->createUrl('GenerateExcel'), 'linkOptions'=>array('target'=>'_blank'), 'visible'=>true),
	),
));
?>

<div class="search-form">
<?php $this->renderPartial('_search', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'solicitud-grid',
	'dataProvider' => $model->finalizadas(),
	'filter' => $model,
        'type'=>'striped bordered condensed',
        'template'=>"{summary}{items}{pager}",
	'columns' => array(
            'id',
                
		array(
				'name'=>'proyecto_id',
				'value'=>'GxHtml::valueEx($data->proyecto)',
				'filter'=>GxHtml::listDataEx(Proyecto::model()->findAllAttributes(null, true)),
		),
		array(
				'name'=>'proveedor_id',
				'value'=>'GxHtml::valueEx($data->proveedor)',
				'filter'=>GxHtml::listDataEx(Proveedor::model()->findAllAttributes(null, true)),
		),           
		'fecha',
                'tipo_solicitud',
                array(
                    'name' => 'total',
                    'value' => 'Yii::app()->format->formatNumber($data->total)',
                ),
                'estado',
                 //'monto_solicitado',

                array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'htmlOptions'=>array('style'=>'width: 50px'),
			'template'=>'{view} {crear}',
			'buttons'=>array(
				'view' => array(
					'label'=>'Ver Solicitud',
					'url'=>'Yii::app()->createUrl("solicitud/verSolicitudEmision", array("id"=>$data->id))',
				),
                            
                                'crear' => array(
					'label'=>'Crear Emision',
					'url'=>'Yii::app()->createUrl("emision/crear", array("solicitud_id"=>$data->id))',
                                        'icon' => 'icon-file',
                                 ),
				
			),
				
            ),               
	),
)); ?>