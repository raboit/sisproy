<?php

$this->breadcrumbs = array(
	$model->label(2) => array('verTodos'),
	GxHtml::valueEx($model) => array('ver', 'donante_id' => GxActiveRecord::extractPkValue($model, true)),
	Yii::t('app', 'Update'),
);

$this->menu = array(
	array('label' => Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('crear')),
	array('label' => Yii::t('app', 'View') . ' ' . $model->label(), 'url'=>array('ver', 'donante_id' => GxActiveRecord::extractPkValue($model, true))),
	array('label' => Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('verTodos')),
);
?>

<h1><?php echo Yii::t('app', 'Update') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php
$this->renderPartial('_crear', array(
		'model' => $model));
?>