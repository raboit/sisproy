<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('proyecto_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->proyecto)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('tipo_ingreso_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->tipoIngreso)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('financiamiento_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->financiamiento)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('fecha')); ?>:
	<?php echo GxHtml::encode($data->fecha); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('monto')); ?>:
	<?php echo GxHtml::encode($data->monto); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('observaciones')); ?>:
	<?php echo GxHtml::encode($data->observaciones); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('neto')); ?>:
	<?php echo GxHtml::encode($data->neto); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('iva')); ?>:
	<?php echo GxHtml::encode($data->iva); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('total')); ?>:
	<?php echo GxHtml::encode($data->total); ?>
	<br />
	*/ ?>

</div>