<?php 
Yii::app()->clientScript->registerScript('tipo_ingreso','
    function tipo_ingreso(value){
        if(value=="3"){
            $(".venta_servicio").show();
            $(".no_venta_servicio").hide();
        }else{
            $(".no_venta_servicio").show();            
            $(".venta_servicio").hide();
        }
    }
',CClientScript::POS_END); 
?>

<script type="text/javascript">
function addFinanciamiento()
{
    <?php echo CHtml::ajax(array(
            'url'=>Yii::app()->createUrl("financiamiento/agregar"),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'failure')
                {
                    $('#financiamiento_div').html(data.div);
                    $('#financiamiento_div').show();
                    $('#financiamiento_div form').submit(addFinanciamiento);
                }
                else
                {
                    $('#financiamiento_div').html(data.div);
                    actualizarFinanciamiento();
                    setTimeout(\"$('#mydialog').dialog('close') \",2500);
                    
		 }		
            }",
            ))?>;
    return false; 
 
} 

function actualizarFinanciamiento()
{
    <?php echo CHtml::ajax(array(
            'url'=>CController::createUrl('financiamiento/getTodos'),      
            'type'=>'post',
            'update'=>'#'.CHtml::activeId($model,'financiamiento_id'),
            ))?>;
    return false; 
 
} 
</script>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
'id'=>'mydialog',
'options'=>array(
    'title'=>'Crear nuevo financiamiento',
    'autoOpen'=>false,
    'buttons' => array(
        array('text'=>'Cerrar','click'=> 'js:function(){$(this).dialog("close");}'),
    ),
),
));
?>

<div id="financiamiento_div"></div>
<?php
$this->endWidget('zii.widgets.jui.CJuiDialog');
?>


<div class="form">
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'ingreso-form',
	'enableAjaxValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'proyecto_id'); ?>
		<?php echo $form->dropDownList($model, 'proyecto_id', GxHtml::listDataEx(Proyecto::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'proyecto_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'tipo_ingreso_id'); ?>
		<?php echo $form->dropDownList($model, 'tipo_ingreso_id', GxHtml::listDataEx(TipoIngreso::model()->findAllAttributes(null, true)),array('onchange'=>'js:tipo_ingreso(this.value)')); ?>
		<?php echo $form->error($model,'tipo_ingreso_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'financiamiento_id'); ?>
		<?php echo $form->dropDownList($model, 'financiamiento_id', GxHtml::listDataEx(Financiamiento::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'financiamiento_id'); ?>
                    <?php
                $this->widget('bootstrap.widgets.TbButton', array(
                    'label'=>'',
                    'size'=>'mini',
                    'icon'=>'plus white',
                    'type'=>'primary',
                    'htmlOptions'=>array(
                            'onclick'=>'js:addFinanciamiento();$("#mydialog").dialog("open");',
                        ),
                ));
                ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'fecha'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'fecha',
			'value' => $model->fecha,
                        'language' => Yii::app()->language,    
			'options' => array(
				'showButtonPanel' => true,
                                'changeMonth' => true,
				'changeYear' => true,
				
				),
			));
; ?>
		<?php echo $form->error($model,'fecha'); ?>
		</div><!-- row -->
                
		<div class="row no_venta_servicio">
		<?php echo $form->labelEx($model,'monto'); ?>
		<?php echo $form->textField($model, 'monto'); ?>
		<?php echo $form->error($model,'monto'); ?>
		</div><!-- row -->
                
                
                <div class="row venta_servicio" style="display:none;">
		<?php echo $form->labelEx($model,'neto'); ?>
		<?php echo $form->textField($model, 'neto'); ?>
		<?php echo $form->error($model,'neto'); ?>
		</div><!-- row -->
                
                <div class="row venta_servicio" style="display:none;">
		<?php echo $form->labelEx($model,'iva'); ?>
		<?php echo $form->textField($model, 'iva'); ?>
		<?php echo $form->error($model,'iva'); ?>
		</div><!-- row -->
                
                <div class="row venta_servicio" style="display:none;">
		<?php echo $form->labelEx($model,'total'); ?>
		<?php echo $form->textField($model, 'total'); ?>
		<?php echo $form->error($model,'total'); ?>
		</div><!-- row -->
                
		<div class="row">
		<?php echo $form->labelEx($model,'observaciones'); ?>
		<?php echo $form->textField($model, 'observaciones'); ?>
		<?php echo $form->error($model,'observaciones'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'), array('onClick' => "this.disabled=true;this.value='".Yii::t('app', 'Enviando')."';this.form.submit();"));
$this->endWidget();
?>
</div><!-- form -->