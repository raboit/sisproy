<div class="form">

    
<?php $this->widget('ext.EChosen.EChosen', array('target' => 'select')); ?>
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'proyecto-form',
	'enableAjaxValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'rrhh_id'); ?>
		<?php echo $form->dropDownList($model, 'rrhh_id', GxHtml::listDataEx(Rrhh::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'rrhh_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'centro_costo_id'); ?>
		<?php echo $form->dropDownList($model, 'centro_costo_id', GxHtml::listDataEx(CentroCosto::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'centro_costo_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model, 'nombre', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'nombre'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'descripcion'); ?>
		<?php echo $form->textField($model, 'descripcion'); ?>
		<?php echo $form->error($model,'descripcion'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'archivo'); ?>
		<?php echo $form->textField($model, 'archivo', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'archivo'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'contrato'); ?>
		<?php echo $form->textField($model, 'contrato', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'contrato'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'comuna'); ?>
		<?php echo $form->textField($model, 'comuna', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'comuna'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'region'); ?>
		<?php echo $form->textField($model, 'region', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'region'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'total'); ?>
		<?php echo $form->textField($model, 'total'); ?>
		<?php echo $form->error($model,'total'); ?>
		</div><!-- row -->

		<label><?php echo GxHtml::encode($model->getRelationLabel('contratos')); ?></label>
		<?php echo $form->checkBoxList($model, 'contratos', GxHtml::encodeEx(GxHtml::listDataEx(Contrato::model()->findAllAttributes(null, true)), false, true)); ?>
		<label><?php echo GxHtml::encode($model->getRelationLabel('emisions')); ?></label>
		<?php echo $form->checkBoxList($model, 'emisions', GxHtml::encodeEx(GxHtml::listDataEx(Emision::model()->findAllAttributes(null, true)), false, true)); ?>
		<label><?php echo GxHtml::encode($model->getRelationLabel('ingresoProyectos')); ?></label>
		<?php echo $form->checkBoxList($model, 'ingresoProyectos', GxHtml::encodeEx(GxHtml::listDataEx(IngresoProyecto::model()->findAllAttributes(null, true)), false, true)); ?>
		<label><?php echo GxHtml::encode($model->getRelationLabel('itemRendicions')); ?></label>
		<?php echo $form->checkBoxList($model, 'itemRendicions', GxHtml::encodeEx(GxHtml::listDataEx(ItemRendicion::model()->findAllAttributes(null, true)), false, true)); ?>
		<label><?php echo GxHtml::encode($model->getRelationLabel('presupuestos')); ?></label>
		<?php echo $form->checkBoxList($model, 'presupuestos', GxHtml::encodeEx(GxHtml::listDataEx(Presupuesto::model()->findAllAttributes(null, true)), false, true)); ?>
		<label><?php echo GxHtml::encode($model->getRelationLabel('solicituds')); ?></label>
		<?php echo $form->checkBoxList($model, 'solicituds', GxHtml::encodeEx(GxHtml::listDataEx(Solicitud::model()->findAllAttributes(null, true)), false, true)); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'), array('onClick' => "this.disabled=true;this.value='".Yii::t('app', 'Enviando')."';this.form.submit();"));
$this->endWidget();
?>
</div><!-- form -->