<?php

$this->breadcrumbs = array(
	$model_proyecto->label(1) => array('ver', 'id' => $model_proyecto->id),
	GxHtml::valueEx($model_proyecto),
);

$this->menu=array(
	//array('label'=>Yii::t('app', 'Create') . ' ' . $model_proyecto->label(), 'url'=>array('crear')),
	//array('label'=>Yii::t('app', 'Update') . ' ' . $model_proyecto->label(), 'url'=>array('actualizar', 'id' => $model_proyecto->id)),
        //array('label'=>Yii::t('app', 'Manage') . ' ' . $model_proyecto->label(2), 'url'=>array('verTodos')),
        //array('label'=>Yii::t('app', 'Create') . ' Solicitud', 'url'=>array('solicitud/crear','proyecto_id'=>$model_proyecto->id)),
        array('label'=>Yii::t('app', 'Create') . ' ' . 'Presupuesto', 'url'=>array('presupuesto/crear','proyecto_id'=>$model_proyecto->id)),
    );
?>
<?php $this->widget('application.extensions.print.printWidget', array(
                   'cssFile'=>'print.css',
                   'printedElement'=>'#content')); 
    ?>
<h1><?php echo Yii::t('app', 'Name') . ':  ' . GxHtml::encode($model_proyecto->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model_proyecto)); ?></h1>
 
<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model_proyecto,
	'attributes' => array(
array(
			'name' => 'Jefe de Proyecto',
			'type' => 'raw',
			'value' => $model_proyecto->rrhh,
			),
array(
			'name' => 'centroCosto',
			'type' => 'raw',
			'value' => $model_proyecto->centroCosto,
			),
'nombre',
'descripcion',
array(
			'name' => 'archivo',
			'type' => 'raw',
			'value' => $model_proyecto->archivo !== null ? CHtml::link($model_proyecto->archivo,array('uploads/'.$model_proyecto->id.'/'.$model_proyecto->archivo), array('target'=>'_blank')) : null,
			),
array(
			'name' => 'contrato',
			'type' => 'raw',
			'value' => $model_proyecto->contrato !== null ? CHtml::link($model_proyecto->contrato,array('uploads/'.$model_proyecto->id.'/'.$model_proyecto->contrato), array('target'=>'_blank')) : null,
    ),
array(
			'name' => 'Comuna',
			'type' => 'raw',
			'value' => $model_proyecto->comuna,
			),
array(
			'name' => 'Region',
			'type' => 'raw',
			'value' => $model_proyecto->region,
			),

	),
)); ?>

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered',
    'dataProvider' => $model_presupuesto,
    'enableSorting'=>false,
    'template' => "{items}\n{extendedSummary}",
    'columns' => array(
                   array(
				'name'=>'tipo_ingreso_id',
				'value'=>'GxHtml::valueEx($data->tipoIngreso)',
				'filter'=>GxHtml::listDataEx(TipoIngreso::model()->findAllAttributes(null, true)),
				),
                    'fecha',
                    array(
                                'name' => 'monto',
                                'value' => 'Yii::app()->format->formatNumber($data->monto)',
                                ),
                    'observaciones',
                    array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'htmlOptions'=>array('style'=>'width: 50px'),
			'template'=>'{view} {update}',
			'buttons'=>array(
				'view' => array(
					'label'=>'Ver Presupuesto',
					'url'=>'Yii::app()->createUrl("Presupuesto/ver", array("id"=>$data->id))',
				),
				'update' => array(
					'label'=>'Actualizar Presupuesto',
					'url'=>'Yii::app()->createUrl("Presupuesto/actualizar", array("id"=>$data->id))',
				),
			),
				
                ),
    ),
   
    
));?>

