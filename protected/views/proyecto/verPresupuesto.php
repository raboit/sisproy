<?php
$this->breadcrumbs = array(
    $model->label(2) => array('verTodos'),
    GxHtml::valueEx($model),
);

$this->menu=array(
	//array('label'=>Yii::t('app', 'Create') . ' ' . $model_proyecto->label(), 'url'=>array('crear')),
	//array('label'=>Yii::t('app', 'Update') . ' ' . $model_proyecto->label(), 'url'=>array('actualizar', 'id' => $model_proyecto->id)),
        //array('label'=>Yii::t('app', 'Manage') . ' ' . $model_proyecto->label(2), 'url'=>array('verTodos')),
        //array('label'=>Yii::t('app', 'Create') . ' Solicitud', 'url'=>array('solicitud/crear','proyecto_id'=>$model_proyecto->id)),
        array('label'=>Yii::t('app', 'Create') . ' ' . 'Presupuesto', 'url'=>array('presupuesto/crear','proyecto_id'=>$model->id)),
    );
?>
<?php
$this->widget('application.extensions.print.printWidget', array(
    'cssFile' => 'print.css',
    'printedElement' => '#content'));
?>
<h1><?php echo GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php
$this->widget('bootstrap.widgets.TbDetailView', array(
    'data' => $model,
    'attributes' => array(
        array(
            'name' => 'Jefe de Proyecto',
            'type' => 'raw',
            'value' => $model->rrhh,
        ),
        array(
            'name' => 'centroCosto',
            'type' => 'raw',
            'value' => $model->centroCosto,
        ),
        'nombre',
        'sigla',
        'descripcion',
        array(
            'name' => 'archivo',
            'type' => 'raw',
            'value' => $model->archivo !== null ? CHtml::link($model->archivo, array('uploads/' . $model->id . '/' . $model->archivo), array('target' => '_blank')) : null,
        ),
        array(
            'name' => 'contrato',
            'type' => 'raw',
            'value' => $model->contrato !== null ? CHtml::link($model->contrato, array('uploads/' . $model->id . '/' . $model->contrato), array('target' => '_blank')) : null,
        ),
        array(
            'name' => 'Comuna',
            'type' => 'raw',
            'value' => $model->comuna,
        ),
        array(
            'name' => 'Region',
            'type' => 'raw',
            'value' => $model->region,
        ),
    ),
));
?>

<?php if (isset($model->presupuestos[0])): ?>
    <h1><?php echo GxHtml::encode(Presupuesto::label(2)); ?></h1>
    <?php
    $this->widget('bootstrap.widgets.TbGridView', array(
        'type' => 'striped bordered',
        'dataProvider' => new CArrayDataProvider($model->presupuestos),
        'enableSorting' => false,
        'template' => "{summary}{items}{pager}",
        'columns' => array(
            array(
                'name' => 'Tipo Ingreso',
                'value' => '$data->tipoIngreso->nombre',
            ),
            array(
                'name' => 'Fecha',
                'value' => '$data->fecha',
            ),
            array(
                'name' => 'Monto',
                'value' => 'Yii::app()->format->formatNumber($data->monto)',
            ),
//            array(
//                'name' => 'Observaciones',
//                'value' => '$data->observaciones',
//            ),
                array(
                    'class'=>'bootstrap.widgets.TbButtonColumn',
                    'htmlOptions'=>array('style'=>'width: 50px'),
                    'template'=>'{view} {update}',
                    'buttons'=>array(
                            'view' => array(
                                    'label'=>'Ver Presupuesto',
                                    'url'=>'Yii::app()->createUrl("Presupuesto/ver", array("id"=>$data->id))',
                            ),
                            'update' => array(
                                    'label'=>'Actualizar Presupuesto',
                                    'url'=>'Yii::app()->createUrl("Presupuesto/actualizar", array("id"=>$data->id))',
                            ),
                    ),

                ),
        ),
    ));
    ?>
    <div class="row">
        <div class="well pull-right" style="width:300px;">
            <h4>Presupuesto Total</h4>
            <h3>
                <?php
                //echo Yii::app()->numberFormatter->formatCurrency($model->saldo, 'CLP');
                echo "$ " . Yii::app()->format->formatNumber($model->totalPresupuestos);
                ?>
            </h3>
        </div>
    </div>
    <?php // ------------------------------------- // ?>    
    <?php if (isset($model->ingresos[0])): ?>
        <h1><?php echo GxHtml::encode(Ingreso::label(2)); ?></h1>
        <?php
        $this->widget('bootstrap.widgets.TbGridView', array(
            'type' => 'striped bordered',
            'dataProvider' => new CArrayDataProvider($model->ingresos),
            'enableSorting' => false,
            'template' => "{summary}{items}{pager}",
            'columns' => array(
                array(
                    'name' => 'Tipo Ingreso',
                    'value' => '$data->tipoIngreso->nombre',
                ),
                array(
                    'name' => 'Financiamiento',
                    'value' => '$data->financiamiento->nombre',
                ),
                array(
                    'name' => 'Fecha',
                    'value' => '$data->fecha',
                ),
                array(
                    'name' => 'Monto',
                    'value' => 'Yii::app()->format->formatNumber($data->monto)',
                ),
//            array(
//                'name' => 'Observaciones',
//                'value' => '$data->observaciones',
//            ),
                array(
                    'class'=>'bootstrap.widgets.TbButtonColumn',
                    'htmlOptions'=>array('style'=>'width: 50px'),
                    'template'=>'{view} {update}',
                    'buttons'=>array(
                            'view' => array(
                                    'label'=>'Ver Presupuesto',
                                    'url'=>'Yii::app()->createUrl("Ingreso/ver", array("id"=>$data->id))',
                            ),
                            'update' => array(
                                    'label'=>'Actualizar Presupuesto',
                                    'url'=>'Yii::app()->createUrl("Ingreso/actualizar", array("id"=>$data->id))',
                            ),
                    ),

                ),
            ),
        ));
        ?>
        <div class="row">
            <div class="well pull-right" style="width:300px">
                <h4>Ingreso Total</h4>
                <h3>
                    <?php
                    //echo Yii::app()->numberFormatter->formatCurrency($model->saldo, 'CLP');
                    echo "$ " . Yii::app()->format->formatNumber($model->totalIngresos);
                    ?>
                </h3>
            </div>    
        </div>
        <?php if($model->totalPresupuestos != 0): ?>
        <div class="row">
            <div class="well pull-right" style="width:350px">
                <h4>Ingreso Real</h4>
                <h3>
                <?php
                //echo Yii::app()->numberFormatter->formatCurrency($model->saldo, 'CLP');
                echo "$ " . Yii::app()->format->formatNumber($model->totalIngresos) .
                " de " .
                "$ " . Yii::app()->format->formatNumber($model->totalPresupuestos);
                ?>
                </h3>
                <?php
                $valor = $model->totalIngresos * 100 / $model->totalPresupuestos;
                $this->widget('bootstrap.widgets.TbProgress2', array(
                    'percentColor'=>true,
                    'percent' => $valor
                    ));
                echo "<h5>$ " . Yii::app()->format->formatNumber($model->totalPresupuestos - $model->totalIngresos) . " saldo faltante</h5>";
                ?>
                
            </div>    
        </div>
        <?php else: ?>
        <div class="row">
            <div class="well pull-right" style="width:350px">
                <h4>No hay comparación porque el presupuesto es 0</h4>
            </div>    
        </div>  
        <?php endif; ?>
    <?php else: ?>
        <h1><?php echo "No posee " . GxHtml::encode(Ingreso::label(2)); ?></h1>
    <?php endif; ?>    
<?php else: ?>
    <h1><?php echo "No posee " . GxHtml::encode(Presupuesto::label(2)); ?></h1>
<?php endif; ?>
