<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'proyecto-form',
	'enableAjaxValidation' => false,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'rrhh_id'); ?>
		<?php echo $form->dropDownList($model, 'rrhh_id', GxHtml::listDataEx(Rrhh::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'rrhh_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'centro_costo_id'); ?>
		<?php echo $form->dropDownList($model, 'centro_costo_id', GxHtml::listDataEx(CentroCosto::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'centro_costo_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model, 'nombre', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'nombre'); ?>
		</div><!-- row -->
                <div class="row">
		<?php echo $form->labelEx($model,'sigla'); ?>
		<?php echo $form->textField($model, 'sigla', array('maxlength' => 100)); ?>
		<?php echo $form->error($model,'sigla'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'descripcion'); ?>
		<?php echo $form->textField($model, 'descripcion'); ?>
		<?php echo $form->error($model,'descripcion'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'archivo'); ?>
		<?php echo $form->fileField($model, 'archivo'); ?>
		<?php echo $form->error($model,'archivo'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'contrato'); ?>
		<?php echo $form->fileField($model, 'contrato'); ?>
		<?php echo $form->error($model,'contrato'); ?>
		</div><!-- row -->
		
		<div class="row">
		<?php echo $form->labelEx($model,'region'); ?>
		<?php  echo $form->dropDownList($model,'region',array(
                15=>'Región de Arica y Parinacota',
                14=>'Región de Los Ríos',
                13=>'Región Metropolitana',
                12=>'Región de Magallanes y la Antártica Chilena',
                11=>'Región de Aysén del General Carlos Ibáñez del Campo',
                10=>'Región de Los Lagos',
                9=>'Región de la Araucanía',
                8=>'Región del Bío-Bío',
                7=>'Región del Maule',
                6=>'Región del Libertador General Bernardo O Higgins',
                5=>'Región de Valparaiso',
                4=>'Región de Coquimbo',
                3=>'Región de Atacama',
                2=>'Región de Antofagasta',
                1=>'Región de Tarapacá'
                ),
                        array(
                    'ajax' => array(
                    'type'=>'POST', //request type
                    'url'=>CController::createUrl('proyecto/getComuna'), //url to call.
                    //Style: CController::createUrl('currentController/methodToCall')
                    'update'=>'#'.CHtml::activeId($model,'comuna'), //selector to update
                    //'data'=>'js:javascript statement' 
                    //leave out the data key to pass all form values through
                    ))
                        );  ?>
		<?php echo $form->error($model,'region'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'comuna'); ?>
		<?php echo $form->dropDownList($model,'comuna',$arica=array(
                'ARICA'=>'ARICA',
                'CAMARONES'=>'CAMARONES',
                'GENERAL LAGOS'=>'GENERAL LAGOS',
                'PUTRE'=>'PUTRE')); ?>
		<?php echo $form->error($model,'comuna'); ?>
		</div><!-- row -->

		
<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'), array('onClick' => "this.disabled=true;this.value='".Yii::t('app', 'Enviando')."';this.form.submit();"));
$this->endWidget();
?>
</div><!-- form -->
