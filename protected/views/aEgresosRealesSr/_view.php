<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('rendicion')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->rendicion), array('view', 'id' => $data->rendicion)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('item_rendicion_id')); ?>:
	<?php echo GxHtml::encode($data->item_rendicion_id); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('rendicion_directa_id')); ?>:
	<?php echo GxHtml::encode($data->rendicion_directa_id); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('item_rendicion_directa_id')); ?>:
	<?php echo GxHtml::encode($data->item_rendicion_directa_id); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('proveedor_id')); ?>:
	<?php echo GxHtml::encode($data->proveedor_id); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('cuenta_contable_id')); ?>:
	<?php echo GxHtml::encode($data->cuenta_contable_id); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('cuenta_especifica_id')); ?>:
	<?php echo GxHtml::encode($data->cuenta_especifica_id); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('proyecto_id')); ?>:
	<?php echo GxHtml::encode($data->proyecto_id); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('cuenta_id')); ?>:
	<?php echo GxHtml::encode($data->cuenta_id); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('tipo_documento')); ?>:
	<?php echo GxHtml::encode($data->tipo_documento); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('numero_documento')); ?>:
	<?php echo GxHtml::encode($data->numero_documento); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('tipo_solicitud')); ?>:
	<?php echo GxHtml::encode($data->tipo_solicitud); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('tipo_cheque')); ?>:
	<?php echo GxHtml::encode($data->tipo_cheque); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('numero_cheque')); ?>:
	<?php echo GxHtml::encode($data->numero_cheque); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('fecha')); ?>:
	<?php echo GxHtml::encode($data->fecha); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('mes')); ?>:
	<?php echo GxHtml::encode($data->mes); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('agno')); ?>:
	<?php echo GxHtml::encode($data->agno); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('monto')); ?>:
	<?php echo GxHtml::encode($data->monto); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('rendicion_directa_estado')); ?>:
	<?php echo GxHtml::encode($data->rendicion_directa_estado); ?>
	<br />
	*/ ?>

</div>