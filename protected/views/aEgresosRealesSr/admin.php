<?php

$this->breadcrumbs = array(
	Yii::t('app', 'Manage'),
);

$this->menu = array(
	
	);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('aegresos-reales-sr-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Manage') . ' ' . GxHtml::encode($model->label(2)); ?></h1>

<p>
You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo GxHtml::link(Yii::t('app', 'Advanced Search'), '#', array('class' => 'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'aegresos-reales-sr-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'rendicion',
		'item_rendicion_id',
		'rendicion_directa_id',
		'item_rendicion_directa_id',
		'proveedor_id',
		'cuenta_contable_id',
		'cuenta_especifica_id',
		'proyecto_id',
		'cuenta_id',
                /*
		'tipo_documento',
		'numero_documento',
		'tipo_solicitud',
		'tipo_cheque',
		'numero_cheque',
		'fecha',
		'mes',
		'agno',
		'monto',
		'rendicion_directa_estado',
		*/
		array(
			'class' => 'CButtonColumn',
		),
	),
)); ?>