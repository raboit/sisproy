<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'rendicion'); ?>
		<?php echo $form->textField($model, 'rendicion', array('maxlength' => 10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'item_rendicion_id'); ?>
		<?php echo $form->textField($model, 'item_rendicion_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'rendicion_directa_id'); ?>
		<?php echo $form->textField($model, 'rendicion_directa_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'item_rendicion_directa_id'); ?>
		<?php echo $form->textField($model, 'item_rendicion_directa_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'proveedor_id'); ?>
		<?php echo $form->textField($model, 'proveedor_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'cuenta_contable_id'); ?>
		<?php echo $form->textField($model, 'cuenta_contable_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'cuenta_especifica_id'); ?>
		<?php echo $form->textField($model, 'cuenta_especifica_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'proyecto_id'); ?>
		<?php echo $form->textField($model, 'proyecto_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'cuenta_id'); ?>
		<?php echo $form->textField($model, 'cuenta_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'tipo_documento'); ?>
		<?php echo $form->textField($model, 'tipo_documento', array('maxlength' => 100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'numero_documento'); ?>
		<?php echo $form->textField($model, 'numero_documento', array('maxlength' => 100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'tipo_solicitud'); ?>
		<?php echo $form->textField($model, 'tipo_solicitud', array('maxlength' => 45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'tipo_cheque'); ?>
		<?php echo $form->textField($model, 'tipo_cheque', array('maxlength' => 45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'numero_cheque'); ?>
		<?php echo $form->textField($model, 'numero_cheque', array('maxlength' => 45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'fecha'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'fecha',
			'value' => $model->fecha,
                        'language' => Yii::app()->language,    
			'options' => array(
				'showButtonPanel' => true,
                                'changeMonth' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'mes'); ?>
		<?php echo $form->textField($model, 'mes', array('maxlength' => 45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'agno'); ?>
		<?php echo $form->textField($model, 'agno', array('maxlength' => 45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'monto'); ?>
		<?php echo $form->textField($model, 'monto'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'rendicion_directa_estado'); ?>
		<?php echo $form->textField($model, 'rendicion_directa_estado', array('maxlength' => 45)); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Search')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
