<div class="form">

    
<?php $this->widget('ext.EChosen.EChosen', array('target' => 'select')); ?>
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'egresos-reales-form',
	'enableAjaxValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'rendicion'); ?>
		<?php echo $form->textField($model, 'rendicion', array('maxlength' => 10)); ?>
		<?php echo $form->error($model,'rendicion'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'item_rendicion_id'); ?>
		<?php echo $form->textField($model, 'item_rendicion_id'); ?>
		<?php echo $form->error($model,'item_rendicion_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'rendicion_directa_id'); ?>
		<?php echo $form->textField($model, 'rendicion_directa_id'); ?>
		<?php echo $form->error($model,'rendicion_directa_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'item_rendicion_directa_id'); ?>
		<?php echo $form->textField($model, 'item_rendicion_directa_id'); ?>
		<?php echo $form->error($model,'item_rendicion_directa_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'proveedor_id'); ?>
		<?php echo $form->textField($model, 'proveedor_id'); ?>
		<?php echo $form->error($model,'proveedor_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'rut'); ?>
		<?php echo $form->textField($model, 'rut', array('maxlength' => 13)); ?>
		<?php echo $form->error($model,'rut'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'proveedor'); ?>
		<?php echo $form->textField($model, 'proveedor', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'proveedor'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'cuenta_contable_id'); ?>
		<?php echo $form->textField($model, 'cuenta_contable_id'); ?>
		<?php echo $form->error($model,'cuenta_contable_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'cuenta_contable'); ?>
		<?php echo $form->textField($model, 'cuenta_contable', array('maxlength' => 100)); ?>
		<?php echo $form->error($model,'cuenta_contable'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'cuenta_especifica_id'); ?>
		<?php echo $form->textField($model, 'cuenta_especifica_id'); ?>
		<?php echo $form->error($model,'cuenta_especifica_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'cuenta_especifica'); ?>
		<?php echo $form->textField($model, 'cuenta_especifica', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'cuenta_especifica'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'proyecto_id'); ?>
		<?php echo $form->textField($model, 'proyecto_id'); ?>
		<?php echo $form->error($model,'proyecto_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'sigla'); ?>
		<?php echo $form->textField($model, 'sigla', array('maxlength' => 100)); ?>
		<?php echo $form->error($model,'sigla'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'proyecto'); ?>
		<?php echo $form->textField($model, 'proyecto', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'proyecto'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'cuenta_id'); ?>
		<?php echo $form->textField($model, 'cuenta_id'); ?>
		<?php echo $form->error($model,'cuenta_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'numero_cuenta'); ?>
		<?php echo $form->textField($model, 'numero_cuenta', array('maxlength' => 100)); ?>
		<?php echo $form->error($model,'numero_cuenta'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'banco'); ?>
		<?php echo $form->textField($model, 'banco', array('maxlength' => 100)); ?>
		<?php echo $form->error($model,'banco'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'tipo_documento'); ?>
		<?php echo $form->textField($model, 'tipo_documento', array('maxlength' => 100)); ?>
		<?php echo $form->error($model,'tipo_documento'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'numero_documento'); ?>
		<?php echo $form->textField($model, 'numero_documento', array('maxlength' => 100)); ?>
		<?php echo $form->error($model,'numero_documento'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'tipo_solicitud'); ?>
		<?php echo $form->textField($model, 'tipo_solicitud', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'tipo_solicitud'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'tipo_cheque'); ?>
		<?php echo $form->textField($model, 'tipo_cheque', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'tipo_cheque'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'numero_cheque'); ?>
		<?php echo $form->textField($model, 'numero_cheque', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'numero_cheque'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'fecha'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'fecha',
			'value' => $model->fecha,
                        'language' => Yii::app()->language,    
			'options' => array(
				'showButtonPanel' => true,
                                'changeMonth' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
		<?php echo $form->error($model,'fecha'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'mes'); ?>
		<?php echo $form->textField($model, 'mes', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'mes'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'agno'); ?>
		<?php echo $form->textField($model, 'agno', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'agno'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'monto'); ?>
		<?php echo $form->textField($model, 'monto'); ?>
		<?php echo $form->error($model,'monto'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'estado'); ?>
		<?php echo $form->textField($model, 'estado', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'estado'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'), array('onClick' => "this.disabled=true;this.value='".Yii::t('app', 'Enviando')."';this.form.submit();"));
$this->endWidget();
?>
</div><!-- form -->