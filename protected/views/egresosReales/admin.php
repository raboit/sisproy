<?php

$this->breadcrumbs = array(
	Yii::t('app', 'Manage'),
);

$this->menu = array(
		array('label'=>Yii::t('app', 'Ver Libro de Compra'), 'url'=>array('/libroCompra/verTodos')),
	);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').slideToggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('egresos-reales-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Manage') . ' ' . GxHtml::encode($model->label(2)); ?></h1>

<p>
<?php echo Yii::t('app', 'Text Option Search'); ?></p>

<?php 
$this->widget('bootstrap.widgets.TbMenu', array(
	'type'=>'pills',
	'items'=>array(
		//array('label'=>Yii::t('app', 'Create'), 'icon'=>'icon-plus', 'url'=>Yii::app()->controller->createUrl('create'), 'linkOptions'=>array()),
                //array('label'=>Yii::t('app', 'List'), 'icon'=>'icon-th-list', 'url'=>Yii::app()->controller->createUrl('admin'),'active'=>true, 'linkOptions'=>array()),
		//array('label'=>Yii::t('app', 'Search'), 'icon'=>'icon-search', 'url'=>'#', 'linkOptions'=>array('class'=>'search-button')),
		//array('label'=>Yii::t('app', 'Export to PDF'), 'icon'=>'icon-download', 'url'=>Yii::app()->controller->createUrl('GeneratePdf'), 'linkOptions'=>array('target'=>'_blank'), 'visible'=>true),
		array('label'=>Yii::t('app', 'Export to Excel'), 'icon'=>'icon-download', 'url'=>Yii::app()->controller->createUrl('GenerateExcel'), 'linkOptions'=>array('target'=>'_blank'), 'visible'=>true),
                array('label'=>Yii::t('app', 'Ver Libro de Compra'), 'icon'=>'icon-book', 'url'=>array('/libroCompra/verTodos')),
            ),
));
/*?>

<div class="search-form">
<?php $this->renderPartial('_search', array(
	'model' => $model,
)); */?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'egresos-reales-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
        'type'=>'striped bordered condensed',
        'template'=>"{summary}{items}{pager}",
        //'htmlOptions' => array(
        //                        'style' => 'overflow-y:auto;'
        //                                   .'table-layout:fixed;'
        //                                   .'white-space:nowrap;'
        //                                   ),       
	'columns' => array(
            
		'rendicion',
                'fecha',
                'tipo_documento',
                'numero_documento',
                'proveedor',
                'monto',
                'numero_cheque',
                'banco',
                'numero_cuenta',
                'sigla',
                'cuenta_contable',
                'cuenta_especifica',
                                
                /*
                'tipo_documento',
		'tipo_solicitud',
                'item_rendicion_id',
		'rendicion_directa_id',
		'item_rendicion_directa_id',
		'proveedor_id',		
		'cuenta_contable_id',		
		'cuenta_especifica_id',		
		'proyecto_id',		
		'proyecto',
		'cuenta_id',
		'tipo_cheque',
		'mes',
		'agno',		
		'estado',
                'rut',
		
                array(
                      'class'=>'bootstrap.widgets.TbButtonColumn',
                      'htmlOptions'=>array('style'=>'min-width: 50px'),
                  ), */               
	),
)); ?>