<?php $this->widget('bootstrap.widgets.TbAlert', array(
        'block'=>true, // display a larger alert block?
        'fade'=>true, // use transitions?
        'closeText'=>'&times;', // close link text - if set to false, no close link is displayed
        
    )); ?>

<?php

$this->breadcrumbs = array(
	$model->label(2) => array('verTodos'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	//array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('crear')),
	array('label'=>Yii::t('app', 'Update').' R.D.', 'url'=>array('actualizar', 'id' => $model->id)),
	//array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?')),
	//array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('verTodos')),
        array('label'=>Yii::t('app', 'Agregar Item'), 'url'=>array('itemRendicionDirecta/crear', 'id' => $model->id)),
);
?>
<?php 
$this->widget('application.extensions.print.printWidget', array(
                   'cssFile'=>'print.css',
                   'printedElement'=>'#content')); 
    ?>
<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
'id',
'cuenta',
'tipo_solicitud',
'tipo_cheque',
'numero_cheque',
            'estado',
	),
)); ?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
    'buttons'=>array(
        array(
                'label'=>'Finalizar',
            'htmlOptions'=>array(
                'data-toggle'=>'modal',
                'data-target'=>'#modalFinalizar',
            ),
                'url'=>array('rendicionDirecta/finalizar','id'=>$model->id),
                'icon'=>'ok white',
                'type'=>'primary'
            ),
        array('label'=>'Anular',
            'htmlOptions'=>array(
                'data-toggle'=>'modal',
                'data-target'=>'#modalAnular',
            ),
            'url'=>array('rendicionDirecta/anular','id'=>$model->id),
            'icon'=>'remove white',
            'type'=>'danger',
            ),
        array('label'=>'Agregar Item',
            
            'url'=>array('itemRendicionDirecta/crear', 'id' => $model->id),
            'icon'=>'plus white',
            'type'=>'success'),

    ),
)); ?>


ITEMS:
<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered',
    'dataProvider' => $model_items,
    'enableSorting'=>false,
    'template'=> '{summary}{pager}{items}{pager}',
    'columns' => array(
                array(
				'name'=>'proveedor_id',
				'value'=>'GxHtml::valueEx($data->proveedor)',
				'filter'=>GxHtml::listDataEx(Proveedor::model()->findAllAttributes(null, true)),
				),
		array(
				'name'=>'cuenta_contable_id',
				'value'=>'GxHtml::valueEx($data->cuentaContable)',
				'filter'=>GxHtml::listDataEx(CuentaContable::model()->findAllAttributes(null, true)),
				),
                array(
				'name'=>'cuenta_especifica_id',
				'value'=>'GxHtml::valueEx($data->cuentaEspecifica)',
				'filter'=>GxHtml::listDataEx(CuentaEspecifica::model()->findAllAttributes(null, true)),
				),
                array(
				'name'=>'proyecto_id',
				'value'=>'GxHtml::valueEx($data->proyecto)',
				'filter'=>GxHtml::listDataEx(Proyecto::model()->findAllAttributes(null, true)),
				),
                'fecha',
                array(
                'name' => 'monto',
                'value' => 'Yii::app()->format->formatNumber($data->monto)',
                ),
        
 
        
                array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'htmlOptions'=>array('style'=>'width: 50px'),
			'template'=>'{view} {update}',
			'buttons'=>array(
				'view' => array(
					'label'=>'Ver Item Rendición Directa',
					'url'=>'Yii::app()->createUrl("itemRendicionDirecta/ver", array("id"=>$data->id))',
				),
				'update' => array(
					'label'=>'Actualizar Item Rendición Directa',
					'url'=>'Yii::app()->createUrl("itemRendicionDirecta/actualizar", array("id"=>$data->id))',
				),
			),
				
                ),
    ),
));?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array('id'=>'modalFinalizar')); ?>
 
<div class="modal-header">
    <a class="close" data-dismiss="modal">&times;</a>
    <h4>Finalizar</h4>
</div>
 
<div class="modal-body">
    <p>¿Estás seguro que desea finalizar?</p>
</div>
 
<div class="modal-footer">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'type'=>'primary',
        'label'=>'Finalizar',
        'icon'=>'ok white',
        'type'=>'primary',
        'url'=>array('rendicionDirecta/finalizar','id'=>$model->id),
        //'htmlOptions'=>array('data-dismiss'=>'modal'),
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label'=>'Cancelar',
        'url'=>'#',
        'htmlOptions'=>array('data-dismiss'=>'modal'),
    )); ?>
</div>
 
<?php $this->endWidget(); ?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array('id'=>'modalAnular')); ?>
 
<div class="modal-header">
    <a class="close" data-dismiss="modal">&times;</a>
    <h4>Anular</h4>
</div>
 
<div class="modal-body">
    <p>Todos los Item de la rendicion se <b>eliminaran permenentemente</b>. ¿Estás seguro que desea anular?</p>
</div>
 
<div class="modal-footer">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'type'=>'primary',
        'label'=>'Anular',
        'icon'=>'remove white',
        'type'=>'danger',
        'url'=>array('rendicionDirecta/anular','id'=>$model->id),
    )); ?>
    
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label'=>'Cancelar',
        'url'=>'#',
        'htmlOptions'=>array('data-dismiss'=>'modal'),
    )); ?>
</div>
 
<?php $this->endWidget(); ?>