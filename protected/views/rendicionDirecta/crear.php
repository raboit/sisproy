<?php

$this->breadcrumbs = array(
	'Emisiones Finalizadas' => array('emision/finalizadas'),
	Yii::t('app', 'Create'),
);

$this->menu = array(
	//array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url' => array('verTodos')),
        array('label'=>Yii::t('app', 'Cancel'), 'url'=>Yii::app()->request->urlReferrer),
);
?>

<h1><?php echo Yii::t('app', 'Create') . ' ' . GxHtml::encode($model->label()); ?></h1>

<?php
$this->renderPartial('_crear', array(
		'model' => $model,
		'buttons' => 'create'));
?>