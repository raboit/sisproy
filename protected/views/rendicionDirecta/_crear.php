<?php 
Yii::app()->clientScript->registerScript('tipo_solicitud','
    function tipo_solicitud(value){
        if(value=="Cheque"){
            $(".tipo_cheque").show();
        }else{
            $(".tipo_cheque").hide();
        }
    }
',CClientScript::POS_END); 
?>


<div class="form">
    
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'rendicion-directa-form',
	'enableAjaxValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'cuenta_id'); ?>
		<?php echo $form->dropDownList($model, 'cuenta_id', GxHtml::listDataEx(Cuenta::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'cuenta_id'); ?>
		</div><!-- row -->
                
		<div class="row">
                <?php echo $form->labelEx($model, 'tipo_solicitud'); ?>
                <?php echo $form->dropDownList($model, 'tipo_solicitud', array('Cheque' => 'Cheque', 'Transferencia' => 'Transferencia'),array('onchange'=>'js:tipo_solicitud(this.value)')); ?>
                <?php echo $form->error($model, 'tipo_solicitud'); ?>
                </div><!-- row -->
                
                <div class="row tipo_cheque">
                <?php echo $form->labelEx($model, 'tipo_cheque'); ?>
                <?php echo $form->dropDownList($model, 'tipo_cheque', array('Transferencia' => 'Seleccione Opcion', 'Abierto' => 'Abierto', 'Cruzado' => 'Cruzado', 'Nominativo' => 'Nominativo', 'Portador' => 'Portador')); ?>
                <?php echo $form->error($model, 'tipo_cheque'); ?>
                </div><!-- row -->
		
                <div class="row tipo_cheque">
		<?php echo $form->labelEx($model,'numero_cheque'); ?>
		<?php echo $form->textField($model, 'numero_cheque', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'numero_cheque'); ?>
		</div><!-- row -->

		
<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'), array('onClick' => "this.disabled=true;this.value='".Yii::t('app', 'Enviando')."';this.form.submit();"));
$this->endWidget();
?>
</div><!-- form -->