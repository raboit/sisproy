<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'rendicion-directa-form',
	'enableAjaxValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'cuenta_id'); ?>
		<?php echo $form->dropDownList($model, 'cuenta_id', GxHtml::listDataEx(Cuenta::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'cuenta_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'tipo_solicitud'); ?>
		<?php echo $form->textField($model, 'tipo_solicitud', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'tipo_solicitud'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'tipo_cheque'); ?>
		<?php echo $form->textField($model, 'tipo_cheque', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'tipo_cheque'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'numero_cheque'); ?>
		<?php echo $form->textField($model, 'numero_cheque', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'numero_cheque'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'estado'); ?>
		<?php echo $form->textField($model, 'estado', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'estado'); ?>
		</div><!-- row -->

		<label><?php echo GxHtml::encode($model->getRelationLabel('itemRendicionDirectas')); ?></label>
		<?php echo $form->checkBoxList($model, 'itemRendicionDirectas', GxHtml::encodeEx(GxHtml::listDataEx(ItemRendicionDirecta::model()->findAllAttributes(null, true)), false, true)); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->