<div class="form">
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'proveedor-form',
	'enableAjaxValidation' => false,
));
?>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'rut'); ?>
		<?php echo $form->textField($model, 'rut', array('maxlength' => 13)); ?>
		<?php echo $form->error($model,'rut'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model, 'nombre', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'nombre'); ?>
		</div><!-- row -->

<?php //echo GxHtml::submitButton(Yii::t('app', 'Guardar'));?>
<?php $this->widget('bootstrap.widgets.TbButton',
                        array(
                            'type'=>'primary',
                            'buttonType'=>'submit',
                            'label'=>'Guardar',
                            )
        ); ?>

<?php
$this->endWidget();
?>
</div><!-- form -->