<script type="text/javascript">
function addProveedor()
{
    <?php echo CHtml::ajax(array(
            'url'=>Yii::app()->createUrl("proveedor/agregar"),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'failure')
                {
                    $('#proveedor_div').html(data.div);
                    $('#proveedor_div').show();
                    $('#proveedor_div form').submit(addProveedor);
                }
                else
                {
                    $('#proveedor_div').html(data.div);
                    actualizarProveedor();
                    setTimeout(\"$('#mydialog').dialog('close') \",2500);
                    
		 }		
            }",
            ))?>;
    return false; 
 
} 

function actualizarProveedor()
{
    <?php echo CHtml::ajax(array(
            'url'=>CController::createUrl('proveedor/getTodos'),      
            'type'=>'post',
            'update'=>'#'.CHtml::activeId($model,'proveedor_id'),
            ))?>;
    return false; 
 
} 
</script>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
'id'=>'mydialog',
'options'=>array(
    'title'=>'Crear nuevo Proveedor',
    'autoOpen'=>false,
    'buttons' => array(
        array('text'=>'Cerrar','click'=> 'js:function(){$(this).dialog("close");}'),
        //array('text'=>'Cancelar','click'=> 'js:function(){$(this).dialog("close");}'),
    ),
),
));
?>

<div id="proveedor_div"></div>
 
<?php
$this->endWidget('zii.widgets.jui.CJuiDialog');
?>

<div class="form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'item-rendicion-directa-form',
	'enableAjaxValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'proveedor_id'); ?>
		<?php echo $form->dropDownList($model, 'proveedor_id', GxHtml::listDataEx(Proveedor::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'proveedor_id'); ?>
                <?php
                $this->widget('bootstrap.widgets.TbButton', array(
                    'label'=>'',
                    'size'=>'mini',
                    'icon'=>'plus white',
                    'type'=>'primary',
                    'htmlOptions'=>array(
                            'onclick'=>'js:addProveedor();$("#mydialog").dialog("open");',
                        ),
                ));
                ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'cuenta_contable_id'); ?>
		<?php echo $form->dropDownList($model, 'cuenta_contable_id', GxHtml::listDataEx(CuentaContable::model()->findAllAttributes(null, true)),array(
                    'prompt' => 'Elija un Cuenta contable',
                    'ajax' => array(
                        'type'=>'POST', //request type
                        'url'=>CController::createUrl('itemRendicionDirecta/getCuenta'),
                        'update'=>'#'.CHtml::activeId($model,'cuenta_especifica_id'),
                        //'data'=>'js:javascript statement' 
                        //leave out the data key to pass all form values through
                    ))); ?>
		<?php echo $form->error($model,'cuenta_contable_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'cuenta_especifica_id'); ?>
		<?php echo $form->dropDownList($model, 'cuenta_especifica_id', GxHtml::listDataEx(CuentaEspecifica::model()->findAllAttributes(null, true, 'cuenta_contable_id=:cuenta_contable_id', array(':cuenta_contable_id'=>$model->cuenta_contable_id))),array('prompt' => 'Elija un Cuenta especifica',)); ?>
		<?php echo $form->error($model,'cuenta_especifica_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'proyecto_id'); ?>
		<?php echo $form->dropDownList($model, 'proyecto_id', GxHtml::listDataEx(Proyecto::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'proyecto_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'tipo_documento'); ?>
		<?php echo $form->dropDownList($model,'tipo_documento',array('Boleta' => 'Boleta', 'Boleta de Honorario' => 'Boleta de Honorario', 'Boleta de Pres. de 3ros' => 'Boleta de Pres. de 3ros', 'Factura' => 'Factura', 'Guia de Despacho' => 'Guia de Despacho', 'Nota de Credito' => 'Nota de Credito', 'Nota de Debito' => 'Nota de Debito', 'Recibo de Dinero' => 'Recibo de Dinero', 'S/D' => 'S/D')); ?>
		<?php echo $form->error($model,'tipo_documento'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'numero_documento'); ?>
		<?php echo $form->textField($model, 'numero_documento', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'numero_documento'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'fecha'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'fecha',
			'value' => $model->fecha,
                        'language' => Yii::app()->language,    
			'options' => array(
				'showButtonPanel' => true,
                                'changeMonth' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
		<?php echo $form->error($model,'fecha'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'monto'); ?>
		<?php echo $form->textField($model, 'monto'); ?>
		<?php echo $form->error($model,'monto'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'), array('onClick' => "this.disabled=true;this.value='".Yii::t('app', 'Enviando')."';this.form.submit();"));
$this->endWidget();
?>
</div><!-- form -->