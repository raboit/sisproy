<?php

$this->breadcrumbs = array(
	'Rendicion Directa' => array('rendicionDirecta/ver', 'id' => $model->rendicion_directa_id),
	Yii::t('app', 'Update'),
);

$this->menu = array(
    array('label'=>Yii::t('app', 'Cancel'), 'url'=>Yii::app()->request->urlReferrer),
);
?>

<h1><?php echo Yii::t('app', 'Update') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php
$this->renderPartial('_crear', array(
		'model' => $model));
?>