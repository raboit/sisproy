<?php

$this->breadcrumbs = array(
	'Rendicion Directa' => array('rendicionDirecta/ver', 'id' => $model->rendicion_directa_id),
	GxHtml::valueEx($model),
);

$this->menu=array(
        array('label'=>Yii::t('app', 'Back'), 'url'=>Yii::app()->request->urlReferrer),
	//array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('crear')),
	//array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('actualizar', 'id' => $model->id)),
	//array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('verTodos')),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>
<?php $this->widget('application.extensions.print.printWidget', array(
                   'cssFile'=>'print.css',
                   'printedElement'=>'#content')); 
    ?>
<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
'id',
array(
			'name' => 'rendicionDirecta',
			'type' => 'raw',
			'value' => $model->rendicionDirecta !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->rendicionDirecta)), array('rendicionDirecta/view', 'id' => GxActiveRecord::extractPkValue($model->rendicionDirecta, true))) : null,
			),
array(
			'name' => 'proveedor',
			'type' => 'raw',
			'value' => $model->proveedor !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->proveedor)), array('proveedor/view', 'id' => GxActiveRecord::extractPkValue($model->proveedor, true))) : null,
			),
array(
			'name' => 'cuentaContable',
			'type' => 'raw',
			'value' => $model->cuentaContable !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->cuentaContable)), array('cuentaContable/view', 'id' => GxActiveRecord::extractPkValue($model->cuentaContable, true))) : null,
			),
array(
			'name' => 'cuentaEspecifica',
			'type' => 'raw',
			'value' => $model->cuentaEspecifica !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->cuentaEspecifica)), array('cuentaEspecifica/view', 'id' => GxActiveRecord::extractPkValue($model->cuentaEspecifica, true))) : null,
			),
array(
			'name' => 'proyecto',
			'type' => 'raw',
			'value' => $model->proyecto !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->proyecto)), array('proyecto/view', 'id' => GxActiveRecord::extractPkValue($model->proyecto, true))) : null,
			),
'tipo_documento',
'numero_documento',
'fecha',
'mes',
'agno',
array(
                'name' => 'monto',
                'value' => Yii::app()->format->formatNumber($model->monto),
            ),
	),
)); ?>

