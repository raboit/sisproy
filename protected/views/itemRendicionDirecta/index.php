<?php

$this->breadcrumbs = array(
	ItemRendicionDirecta::label(2),
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Create') . ' ' . ItemRendicionDirecta::label(), 'url' => array('create')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . ItemRendicionDirecta::label(2), 'url' => array('admin')),
);
?>

<h1><?php echo GxHtml::encode(ItemRendicionDirecta::label(2)); ?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); 