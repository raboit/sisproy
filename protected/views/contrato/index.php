<?php

$this->breadcrumbs = array(
	Contrato::label(2),
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Create') . ' ' . Contrato::label(), 'url' => array('create')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . Contrato::label(2), 'url' => array('admin')),
);
?>

<h1><?php echo GxHtml::encode(Contrato::label(2)); ?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); 