<?php

$this->breadcrumbs = array(
	$model->label(2) => array('verTodos'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('crear')),
	array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('actualizar', 'id' => $model->id)),
	array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('verTodos')),
        array('label'=>Yii::t('app', 'Finish') . ' ' . $model->label(), 'url'=>array('finalizar', 'id' => $model->id)),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
'id',
array(
			'name' => 'rrhh',
			'type' => 'raw',
			'value' => $model->rrhh !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->rrhh)), array('rrhh/ver', 'id' => GxActiveRecord::extractPkValue($model->rrhh, true))) : null,
			),
array(
			'name' => 'proyecto',
			'type' => 'raw',
			'value' => $model->proyecto !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->proyecto)), array('proyecto/ver', 'id' => GxActiveRecord::extractPkValue($model->proyecto, true))) : null,
			),
array(
			'name' => 'tipoContrato',
			'type' => 'raw',
			'value' => $model->tipoContrato !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->tipoContrato)), array('tipoContrato/ver', 'id' => GxActiveRecord::extractPkValue($model->tipoContrato, true))) : null,
			),
'fecha_inicio',
'fecha_termino',
array(
                'name' => 'remuneracion',
                'value' => Yii::app()->format->formatNumber($model->remuneracion),
            ),
'estado',
	),
)); ?>

