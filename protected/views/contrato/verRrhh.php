<?php

$this->breadcrumbs = array(
	'Ver RRHH' => array('rrhh/verContrato', 'id' => $model->rrhh_id),
	'Ver Contrato',
);

$this->menu=array(
	array('label'=>Yii::t('app', 'Back'), 'url'=>array('rrhh/verContrato', 'id' => $model->rrhh_id)),
        //array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('crear')),
	//array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('actualizar', 'id' => $model->id)),
	//array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('verTodos')),
        //array('label'=>Yii::t('app', 'Finish') . ' ' . $model->label(), 'url'=>array('finalizar', 'id' => $model->id)),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
'id',
'rrhh',
'proyecto',
'tipoContrato',
'fecha_inicio',
'fecha_termino',
'remuneracion',
'estado',
	),
)); ?>

