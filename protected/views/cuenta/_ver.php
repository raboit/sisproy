<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('ver', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('numero_cuenta')); ?>:
	<?php echo GxHtml::encode($data->numero_cuenta); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('banco')); ?>:
	<?php echo GxHtml::encode($data->banco); ?>
	<br />

</div>