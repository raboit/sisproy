<div class="form">

    
<?php $this->widget('ext.EChosen.EChosen', array('target' => 'select')); ?>
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'cuenta-form',
	'enableAjaxValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'numero_cuenta'); ?>
		<?php echo $form->textField($model, 'numero_cuenta', array('maxlength' => 100)); ?>
		<?php echo $form->error($model,'numero_cuenta'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'banco'); ?>
		<?php echo $form->textField($model, 'banco', array('maxlength' => 100)); ?>
		<?php echo $form->error($model,'banco'); ?>
		</div><!-- row -->

		<label><?php echo GxHtml::encode($model->getRelationLabel('emisions')); ?></label>
		<?php echo $form->checkBoxList($model, 'emisions', GxHtml::encodeEx(GxHtml::listDataEx(Emision::model()->findAllAttributes(null, true)), false, true)); ?>
		<label><?php echo GxHtml::encode($model->getRelationLabel('itemRendicions')); ?></label>
		<?php echo $form->checkBoxList($model, 'itemRendicions', GxHtml::encodeEx(GxHtml::listDataEx(ItemRendicion::model()->findAllAttributes(null, true)), false, true)); ?>
		<label><?php echo GxHtml::encode($model->getRelationLabel('rendicions')); ?></label>
		<?php echo $form->checkBoxList($model, 'rendicions', GxHtml::encodeEx(GxHtml::listDataEx(Rendicion::model()->findAllAttributes(null, true)), false, true)); ?>
		<label><?php echo GxHtml::encode($model->getRelationLabel('rendicionDirectas')); ?></label>
		<?php echo $form->checkBoxList($model, 'rendicionDirectas', GxHtml::encodeEx(GxHtml::listDataEx(RendicionDirecta::model()->findAllAttributes(null, true)), false, true)); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'), array('onClick' => "this.disabled=true;this.value='".Yii::t('app', 'Enviando')."';this.form.submit();"));
$this->endWidget();
?>
</div><!-- form -->