<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('proveedor_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->proveedor)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('cuenta_contable_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->cuentaContable)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('cuenta_especifica_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->cuentaEspecifica)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('rendicion_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->rendicion)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('proyecto_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->proyecto)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('cuenta_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->cuenta)); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('tipo_documento')); ?>:
	<?php echo GxHtml::encode($data->tipo_documento); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('numero_documento')); ?>:
	<?php echo GxHtml::encode($data->numero_documento); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('tipo_solicitud')); ?>:
	<?php echo GxHtml::encode($data->tipo_solicitud); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('tipo_cheque')); ?>:
	<?php echo GxHtml::encode($data->tipo_cheque); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('numero_cheque')); ?>:
	<?php echo GxHtml::encode($data->numero_cheque); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('fecha')); ?>:
	<?php echo GxHtml::encode($data->fecha); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('mes')); ?>:
	<?php echo GxHtml::encode($data->mes); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('agno')); ?>:
	<?php echo GxHtml::encode($data->agno); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('monto')); ?>:
	<?php echo GxHtml::encode($data->monto); ?>
	<br />
	*/ ?>

</div>