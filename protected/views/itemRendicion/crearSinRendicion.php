<?php

$this->breadcrumbs = array(
	'Ver Rendicion' => array('rendicion/ver', 'id'=> $model->rendicion_id),
	Yii::t('app', 'create'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Back'), 'url'=>Yii::app()->request->urlReferrer),

);
?>

<h1><?php echo Yii::t('app', 'Create') . ' ' . GxHtml::encode($model->label()); ?></h1>

<?php
$this->renderPartial('_crearSinRendicion', array(
		'model' => $model,
		'buttons' => 'create'));
?>