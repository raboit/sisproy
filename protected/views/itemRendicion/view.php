<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
	array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->id)),
	array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
'id',
array(
			'name' => 'proveedor',
			'type' => 'raw',
			'value' => $model->proveedor !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->proveedor)), array('proveedor/view', 'id' => GxActiveRecord::extractPkValue($model->proveedor, true))) : null,
			),
array(
			'name' => 'cuentaContable',
			'type' => 'raw',
			'value' => $model->cuentaContable !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->cuentaContable)), array('cuentaContable/view', 'id' => GxActiveRecord::extractPkValue($model->cuentaContable, true))) : null,
			),
array(
			'name' => 'cuentaEspecifica',
			'type' => 'raw',
			'value' => $model->cuentaEspecifica !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->cuentaEspecifica)), array('cuentaEspecifica/view', 'id' => GxActiveRecord::extractPkValue($model->cuentaEspecifica, true))) : null,
			),
array(
			'name' => 'rendicion',
			'type' => 'raw',
			'value' => $model->rendicion !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->rendicion)), array('rendicion/view', 'id' => GxActiveRecord::extractPkValue($model->rendicion, true))) : null,
			),
array(
			'name' => 'proyecto',
			'type' => 'raw',
			'value' => $model->proyecto !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->proyecto)), array('proyecto/view', 'id' => GxActiveRecord::extractPkValue($model->proyecto, true))) : null,
			),
array(
			'name' => 'cuenta',
			'type' => 'raw',
			'value' => $model->cuenta !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->cuenta)), array('cuenta/view', 'id' => GxActiveRecord::extractPkValue($model->cuenta, true))) : null,
			),
'tipo_documento',
'numero_documento',
'tipo_solicitud',
'tipo_cheque',
'numero_cheque',
'fecha',
'mes',
'agno',
'monto',
	),
)); ?>

