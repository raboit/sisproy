<?php

$this->breadcrumbs = array(
	'Ver Rendicion' => array('rendicion/ver', 'id' => $model->rendicion_id),
	'Item Rendicion',
);


$this->menu=array(
        array('label'=>Yii::t('app', 'Back'), 'url'=>Yii::app()->request->urlReferrer),
        //array('label'=>Yii::t('app', 'Back'), 'url'=>array('rendicion/ver', 'id' => $model->rendicion_id)),
	//array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
	//array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	//array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->id)),
	//array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?')),
	//array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('application.extensions.print.printWidget', array(
                   'cssFile'=>'print.css',
                   'printedElement'=>'#content')); 
    ?>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
'id',
'proveedor',
'cuentaEspecifica',
'rendicion',
'proyecto',
'tipo_documento',
'numero_documento',
'tipo_solicitud',
'numero_cheque',
'cuenta',
            array(
                'name' => 'monto',
                'value' => Yii::app()->format->formatNumber($model->monto),
            ),
'fecha',
	),
)); ?>

