<div class="form">
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'rendicion-form',
	'enableAjaxValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'asignacion'); ?>
		<?php echo $form->dropDownList($model,'asignacion',array('Directa' => 'Directa', 'Multiple' => 'Multiple')); ?>
		<?php echo $form->error($model,'asignacion'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'fecha'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'fecha',
			'value' => $model->fecha,
                        'language' => Yii::app()->language,    
			'options' => array(
				'showButtonPanel' => true,
                                'changeMonth' => true,
				'changeYear' => true,
				
				),
			));
; ?>
		<?php echo $form->error($model,'fecha'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'region'); ?>
		<?php  echo $form->dropDownList($model,'region',array(
                'Región de Arica y Parinacota'=>'Región de Arica y Parinacota',
                'Región de Los Ríos'=>'Región de Los Ríos',
                'Región Metropolitana'=>'Región Metropolitana',
                'Región de Magallanes y la Antártica Chilena'=>'Región de Magallanes y la Antártica Chilena',
                'Región de Aysén del General Carlos Ibáñez del Campo'=>'Región de Aysén del General Carlos Ibáñez del Campo',
                'Región de Los Lagos'=>'Región de Los Lagos',
                'Región de la Araucanía'=>'Región de la Araucanía',
                'Región del Bío-Bío'=>'Región del Bío-Bío',
                'Región del Maule'=>'Región del Maule',
                'Región del Libertador General Bernardo O Higgins'=>'Región del Libertador General Bernardo O Higgins',
                'Región de Valparaiso'=>'Región de Valparaiso',
                'Región de Coquimbo'=>'Región de Coquimbo',
                'Región de Atacama'=>'Región de Atacama',
                'Región de Antofagasta'=>'Región de Antofagasta',
                'Región de Tarapacá'=>'Región de Tarapacá'
                ));  ?>
		<?php echo $form->error($model,'region'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'comuna'); ?>
		<?php echo $form->dropDownList($model,'comuna',$arica=array(
                'ARICA'=>'ARICA',
                'CAMARONES'=>'CAMARONES',
                'GENERAL LAGOS'=>'GENERAL LAGOS',
                'PUTRE'=>'PUTRE')); ?>
		<?php echo $form->error($model,'comuna'); ?>
		</div><!-- row -->
		<?php /* <div class="row">
		<?php echo $form->labelEx($model,'tipo_pago'); ?>
		<?php echo $form->textField($model, 'tipo_pago', array('maxlength' => 100)); ?>
		<?php echo $form->error($model,'tipo_pago'); ?>
		</div><!-- row -->
                 * */?>
                 

		<div class="row">
		<?php echo $form->labelEx($model,'observacion'); ?>
		<?php echo $form->textField($model, 'observacion'); ?>
		<?php echo $form->error($model,'observacion'); ?>
		</div><!-- row -->
	
		
<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'), array('onClick' => "this.disabled=true;this.value='".Yii::t('app', 'Enviando')."';this.form.submit();"));
$this->endWidget();
?>
</div><!-- form -->