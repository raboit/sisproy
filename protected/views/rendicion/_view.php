<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('user_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->user)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('emision_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->emision)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('cuenta_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->cuenta)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('asignacion')); ?>:
	<?php echo GxHtml::encode($data->asignacion); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('fecha')); ?>:
	<?php echo GxHtml::encode($data->fecha); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('comuna')); ?>:
	<?php echo GxHtml::encode($data->comuna); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('region')); ?>:
	<?php echo GxHtml::encode($data->region); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('tipo_solicitud')); ?>:
	<?php echo GxHtml::encode($data->tipo_solicitud); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('numero_cheque')); ?>:
	<?php echo GxHtml::encode($data->numero_cheque); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('estado')); ?>:
	<?php echo GxHtml::encode($data->estado); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('total_entrada')); ?>:
	<?php echo GxHtml::encode($data->total_entrada); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('total_salida')); ?>:
	<?php echo GxHtml::encode($data->total_salida); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('total_saldo')); ?>:
	<?php echo GxHtml::encode($data->total_saldo); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('observacion')); ?>:
	<?php echo GxHtml::encode($data->observacion); ?>
	<br />
	*/ ?>

</div>