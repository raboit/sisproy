<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('item_rendicion_id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->item_rendicion_id), array('view', 'id' => $data->item_rendicion_id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('fecha_emision')); ?>:
	<?php echo GxHtml::encode($data->fecha_emision); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('fecha_item_rendicion')); ?>:
	<?php echo GxHtml::encode($data->fecha_item_rendicion); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('dia_emision')); ?>:
	<?php echo GxHtml::encode($data->dia_emision); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('mes_emision')); ?>:
	<?php echo GxHtml::encode($data->mes_emision); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('ano_emision')); ?>:
	<?php echo GxHtml::encode($data->ano_emision); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('dia_item_rendicion')); ?>:
	<?php echo GxHtml::encode($data->dia_item_rendicion); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('mes_item_rendicion')); ?>:
	<?php echo GxHtml::encode($data->mes_item_rendicion); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('ano_item_rendicion')); ?>:
	<?php echo GxHtml::encode($data->ano_item_rendicion); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('tipo_documento')); ?>:
	<?php echo GxHtml::encode($data->tipo_documento); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('numero_documento')); ?>:
	<?php echo GxHtml::encode($data->numero_documento); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('proveedor')); ?>:
	<?php echo GxHtml::encode($data->proveedor); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('monto')); ?>:
	<?php echo GxHtml::encode($data->monto); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('tipo_pago')); ?>:
	<?php echo GxHtml::encode($data->tipo_pago); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('numero_cheque')); ?>:
	<?php echo GxHtml::encode($data->numero_cheque); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('asignacion')); ?>:
	<?php echo GxHtml::encode($data->asignacion); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('centro_costo')); ?>:
	<?php echo GxHtml::encode($data->centro_costo); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('proyecto')); ?>:
	<?php echo GxHtml::encode($data->proyecto); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('cuenta_contable')); ?>:
	<?php echo GxHtml::encode($data->cuenta_contable); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('cuenta_especifica')); ?>:
	<?php echo GxHtml::encode($data->cuenta_especifica); ?>
	<br />
	*/ ?>

</div>