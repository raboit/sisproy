<div class="form">

    
<?php $this->widget('ext.EChosen.EChosen', array('target' => 'select')); ?>
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'libro-compra-form',
	'enableAjaxValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'item_rendicion_id'); ?>
		<?php echo $form->textField($model, 'item_rendicion_id'); ?>
		<?php echo $form->error($model,'item_rendicion_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'fecha_emision'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'fecha_emision',
			'value' => $model->fecha_emision,
                        'language' => Yii::app()->language,    
			'options' => array(
				'showButtonPanel' => true,
                                'changeMonth' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
		<?php echo $form->error($model,'fecha_emision'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'fecha_item_rendicion'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'fecha_item_rendicion',
			'value' => $model->fecha_item_rendicion,
                        'language' => Yii::app()->language,    
			'options' => array(
				'showButtonPanel' => true,
                                'changeMonth' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
		<?php echo $form->error($model,'fecha_item_rendicion'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'dia_emision'); ?>
		<?php echo $form->textField($model, 'dia_emision'); ?>
		<?php echo $form->error($model,'dia_emision'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'mes_emision'); ?>
		<?php echo $form->textField($model, 'mes_emision', array('maxlength' => 10)); ?>
		<?php echo $form->error($model,'mes_emision'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'ano_emision'); ?>
		<?php echo $form->textField($model, 'ano_emision'); ?>
		<?php echo $form->error($model,'ano_emision'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'dia_item_rendicion'); ?>
		<?php echo $form->textField($model, 'dia_item_rendicion'); ?>
		<?php echo $form->error($model,'dia_item_rendicion'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'mes_item_rendicion'); ?>
		<?php echo $form->textField($model, 'mes_item_rendicion', array('maxlength' => 10)); ?>
		<?php echo $form->error($model,'mes_item_rendicion'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'ano_item_rendicion'); ?>
		<?php echo $form->textField($model, 'ano_item_rendicion'); ?>
		<?php echo $form->error($model,'ano_item_rendicion'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'tipo_documento'); ?>
		<?php echo $form->textField($model, 'tipo_documento', array('maxlength' => 100)); ?>
		<?php echo $form->error($model,'tipo_documento'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'numero_documento'); ?>
		<?php echo $form->textField($model, 'numero_documento', array('maxlength' => 100)); ?>
		<?php echo $form->error($model,'numero_documento'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'proveedor'); ?>
		<?php echo $form->textField($model, 'proveedor', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'proveedor'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'monto'); ?>
		<?php echo $form->textField($model, 'monto'); ?>
		<?php echo $form->error($model,'monto'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'tipo_pago'); ?>
		<?php echo $form->textField($model, 'tipo_pago', array('maxlength' => 100)); ?>
		<?php echo $form->error($model,'tipo_pago'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'numero_cheque'); ?>
		<?php echo $form->textField($model, 'numero_cheque', array('maxlength' => 100)); ?>
		<?php echo $form->error($model,'numero_cheque'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'asignacion'); ?>
		<?php echo $form->textField($model, 'asignacion', array('maxlength' => 100)); ?>
		<?php echo $form->error($model,'asignacion'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'centro_costo'); ?>
		<?php echo $form->textField($model, 'centro_costo', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'centro_costo'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'proyecto'); ?>
		<?php echo $form->textField($model, 'proyecto', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'proyecto'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'cuenta_contable'); ?>
		<?php echo $form->textField($model, 'cuenta_contable', array('maxlength' => 100)); ?>
		<?php echo $form->error($model,'cuenta_contable'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'cuenta_especifica'); ?>
		<?php echo $form->textField($model, 'cuenta_especifica', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'cuenta_especifica'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'), array('onClick' => "this.disabled=true;this.value='".Yii::t('app', 'Enviando')."';this.form.submit();"));
$this->endWidget();
?>
</div><!-- form -->