<?php $this->widget('ext.EChosen.EChosen' ); ?><div class="wide form">

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'search-libro-compra-form',
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'item_rendicion_id'); ?>
		<?php echo $form->textField($model, 'item_rendicion_id'); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'fecha_emision'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'fecha_emision',
			'value' => $model->fecha_emision,
                        'language' => Yii::app()->language,    
			'options' => array(
				'showButtonPanel' => true,
                                'changeMonth' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
	</div>
                	<div class="row">
                <?php echo $form->label($model, 'fecha_emision_inicio'); ?>
                <?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'fecha_emision_inicio',
			'value' => $model->fecha_emision_inicio,
                        'language' => Yii::app()->language,    
			'options' => array(
				'showButtonPanel' => true,
                                'changeMonth' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
	</div>
	<div class="row">
                <?php echo $form->label($model, 'fecha_emision_termino'); ?>
    
                <?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'fecha_emision_termino',
			'value' => $model->fecha_emision_termino,
                        'language' => Yii::app()->language,    
			'options' => array(
				'showButtonPanel' => true,
                                'changeMonth' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'fecha_item_rendicion'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'fecha_item_rendicion',
			'value' => $model->fecha_item_rendicion,
                        'language' => Yii::app()->language,    
			'options' => array(
				'showButtonPanel' => true,
                                'changeMonth' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
	</div>
                	<div class="row">
                <?php echo $form->label($model, 'fecha_item_rendicion_inicio'); ?>
                <?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'fecha_item_rendicion_inicio',
			'value' => $model->fecha_item_rendicion_inicio,
                        'language' => Yii::app()->language,    
			'options' => array(
				'showButtonPanel' => true,
                                'changeMonth' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
	</div>
	<div class="row">
                <?php echo $form->label($model, 'fecha_item_rendicion_termino'); ?>
    
                <?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'fecha_item_rendicion_termino',
			'value' => $model->fecha_item_rendicion_termino,
                        'language' => Yii::app()->language,    
			'options' => array(
				'showButtonPanel' => true,
                                'changeMonth' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'dia_emision'); ?>
		<?php echo $form->textField($model, 'dia_emision'); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'mes_emision'); ?>
		<?php echo $form->dropDownList($model, 'mes_emision', Variables::$nombreMeses, array('prompt' => Yii::t('app', 'All'))); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'ano_emision'); ?>
		<?php echo $form->textField($model, 'ano_emision'); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'dia_item_rendicion'); ?>
		<?php echo $form->textField($model, 'dia_item_rendicion'); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'mes_item_rendicion'); ?>
		<?php echo $form->dropDownList($model, 'mes_item_rendicion', Variables::$nombreMeses, array('prompt' => Yii::t('app', 'All'))); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'ano_item_rendicion'); ?>
		<?php echo $form->textField($model, 'ano_item_rendicion'); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'tipo_documento'); ?>
		<?php echo $form->textField($model, 'tipo_documento', array('maxlength' => 100)); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'numero_documento'); ?>
		<?php echo $form->textField($model, 'numero_documento', array('maxlength' => 100)); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'proveedor'); ?>
		<?php echo $form->dropDownList($model, 'proveedor', GxHtml::listDataEx(Proveedor::model()->findAllAttributes(null, true), 'nombre'), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'monto'); ?>
		<?php echo $form->textField($model, 'monto'); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'tipo_solicitud'); ?>
		<?php echo $form->textField($model, 'tipo_solicitud', array('maxlength' => 100)); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'numero_cheque'); ?>
		<?php echo $form->textField($model, 'numero_cheque', array('maxlength' => 100)); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'asignacion'); ?>
		<?php echo $form->textField($model, 'asignacion', array('maxlength' => 100)); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'centro_costo'); ?>
		<?php echo $form->dropDownList($model, 'centro_costo', GxHtml::listDataEx(CentroCosto::model()->findAllAttributes(null, true),'nombre', 'nombre'), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'proyecto'); ?>
		<?php echo $form->dropDownList($model, 'proyecto', GxHtml::listDataEx(Proyecto::model()->findAll(), 'nombre', 'nombre'), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'cuenta_contable'); ?>
		<?php echo $form->dropDownList($model, 'cuenta_contable', GxHtml::listDataEx(CuentaContable::model()->findAllAttributes(null, true),'nombre', 'nombre'), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'cuenta_especifica'); ?>
		<?php echo $form->dropDownList($model, 'cuenta_especifica', GxHtml::listDataEx(CuentaEspecifica::model()->findAllAttributes(null, true),'nombre', 'nombre'), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>
                
	<div class="row buttons">
		<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'label'=>Yii::t('app', 'Search'), 'icon'=>'search'));?>
		<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'button', 'label'=>Yii::t('app', 'Reset'), 'icon'=>'icon-remove-sign', 'htmlOptions'=>array('class'=>'btnreset btn-small')));?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
<script>
     $(".btnreset").click(function(){
             $(":input","#search-libro-compra-form").each(function() {
             var type = this.type;
             var tag = this.tagName.toLowerCase(); // normalize case
             if (type == "text" || type == "password" || tag == "textarea") this.value = "";
             else if (type == "checkbox" || type == "radio") this.checked = false;
             else if (tag == "select") this.selectedIndex = "";
       });
     });
</script>

