<?php

$this->breadcrumbs = array(
	'Presupuesto' => array('presupuesto/ver', 'id' => $model->presupuesto_id),
	'Actualizar Item',
);

$this->menu=array(
	//array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('crear')),
	array('label'=>Yii::t('app', 'Back'), 'url'=>array('presupuesto/ver', 'id' => $model->presupuesto_id)),
	//array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('verTodos')),
);
?>

<h1><?php echo Yii::t('app', 'Update') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php
$this->renderPartial('_crear', array(
		'model' => $model));
?>