<?php

$this->breadcrumbs = array(
	'Presupuesto' => array('presupuesto/ver', 'id' => $model->presupuesto_id),
	'Ver Item',
);

$this->menu=array(
	//array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('crear')),
	array('label'=>Yii::t('app', 'Back'), 'url'=>array('presupuesto/ver', 'id' => $model->presupuesto_id)),
	//array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('verTodos')),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
            'id',
            'presupuesto',
            'cuentaContable',
            array(
                'name' => 'motno',
                'value' => Yii::app()->format->formatNumber($model->monto),
            ),
            'fecha',
	),
)); ?>