<?php

$this->breadcrumbs = array(
	'Presupuesto' => array('presupuesto/ver', 'id' => $model->presupuesto_id),
	'Ver Item',
);

$this->menu=array(
	//array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('crear')),
	array('label'=>Yii::t('app', 'Back'), 'url'=>array('presupuesto/ver', 'id' => $model->presupuesto_id)),
	//array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('verTodos')),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
            'id',
            'presupuesto',
            'cuentaContable',
            array(
                'name' => 'Monto',
                'value' => Yii::app()->format->formatNumber($model->monto),
            ),
            'fecha',
	),
)); ?>

<?php if(isset($model->cuentaContable->itemRendicions) && !empty($model->cuentaContable->itemRendicions)): ?>

<h1><?php echo "Egresos reales"; ?></h1>
<?php $this->widget('bootstrap.widgets.TbGridView', array(
        'dataProvider' =>  new CArrayDataProvider($model->cuentaContable->itemRendicions),
        'type'=>'striped bordered condensed',
        'template'=>"{summary}{items}{pager}",
        'columns' => array(
                            array(
                                'name' => 'ID',
                                'value' => '$data->id',
                            ),
                            array(
                                'name' => 'Fecha',
                                'value' => '$data->fecha',
                            ),
                            array(
                                'name' => 'Cuenta Especifica',
                                'value' => '$data->cuentaEspecifica',
                            ),
//                            array(
//                                'name' => 'Cuenta Contable',
//                                'value' => '$data->cuentaContable',
//                            ),
                            array(
                                'name' => 'Monto',
                                'value' => 'Yii::app()->format->formatNumber($data->monto)',
                            ),
                            array(
                                    'class'=>'bootstrap.widgets.TbButtonColumn',
                                    'template'=>'{view}',
                                    'buttons'=>array(
                                            'view' => array(
                                                    'label'=>'Ver Egreso',
                                                    'url'=>'Yii::app()->createUrl("itemRendicion/ver", array("id"=>$data->id))',
                                            ),

                                    ),

                            ),  
                    ),
)); ?>
<?php
$totalItemRendicions = Funciones::Suma($model->cuentaContable->itemRendicions, 'monto');
$saldo = $model->monto - $totalItemRendicions;
?>
    <div class="row">
        <div class="well pull-right" style="width:300px;">
            <h4>Egreso Total</h4>
            <h3>
                <?php
                //echo Yii::app()->numberFormatter->formatCurrency($model->saldo, 'CLP');
                echo "$" . Yii::app()->format->formatNumber($totalItemRendicions);
                ?>
            </h3>
        </div>
    </div>

    <?php if($model->monto != 0): ?>
    <div class="row">
        <div class="well pull-right" style="width:350px">
            <h4>Saldo disponible</h4>
            <h3>
            <?php
            //echo Yii::app()->numberFormatter->formatCurrency($model->saldo, 'CLP');
            echo "$ " . Yii::app()->format->formatNumber($saldo) .
            " de " .
            "$ " . Yii::app()->format->formatNumber($model->monto);
            ?>
            </h3>
            <?php
            $valor = $saldo * 100 / $model->monto;
            $this->widget('bootstrap.widgets.TbProgress2', array(
                'percentColor'=>true,
                'percent' => $valor
                ));
            ?>

        </div>    
    </div>
    <?php endif; ?>

<?php else: ?>
<h1><?php echo "No posee Egresos reales"; ?></h1>
<?php endif; ?>