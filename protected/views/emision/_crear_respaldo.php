<script type="text/javascript">
function addProveedor()
{
    <?php echo CHtml::ajax(array(
            'url'=>Yii::app()->createUrl("proveedor/agregar"),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'failure')
                {
                    $('#proveedor_div').html(data.div);
                    $('#proveedor_div').show();
                    $('#proveedor_div form').submit(addProveedor);
                }
                else
                {
                    $('#proveedor_div').html(data.div);
                    actualizarProveedor();
                    setTimeout(\"$('#mydialog').dialog('close') \",2500);
                    
		 }		
            }",
            ))?>;
    return false; 
 
} 

function actualizarProveedor()
{
    <?php echo CHtml::ajax(array(
            'url'=>CController::createUrl('proveedor/getTodos'),      
            'type'=>'post',
            'update'=>'#'.CHtml::activeId($model,'proveedor_id'),
            ))?>;
    return false; 
 
} 
</script>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
'id'=>'mydialog',
'options'=>array(
    'title'=>'Crear nuevo Proveedor',
    'autoOpen'=>false,
    'buttons' => array(
        array('text'=>'Cerrar','click'=> 'js:function(){$(this).dialog("close");}'),
        //array('text'=>'Cancelar','click'=> 'js:function(){$(this).dialog("close");}'),
    ),
),
));
?>

<div id="proveedor_div"></div>
 
<?php
$this->endWidget('zii.widgets.jui.CJuiDialog');
?>


<div class="form">

    

<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'emision-form',
	'enableAjaxValidation' => true,
));
?>

   
	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'Cheque/Transferencia dirigido a:'); ?>
		<?php echo $form->dropDownList($model, 'proveedor_id', GxHtml::listDataEx(Proveedor::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'proveedor_id'); ?>
                    <?php
                $this->widget('bootstrap.widgets.TbButton', array(
                    'label'=>'',
                    'size'=>'mini',
                    'icon'=>'plus white',
                    'type'=>'primary',
                    'htmlOptions'=>array(
                            'onclick'=>'js:addProveedor();$("#mydialog").dialog("open");',
                        ),
                ));
                ?>
		</div><!-- row -->
				
<!--		<div class="row">
		<?php echo $form->labelEx($model,'ciudad'); ?>
		<?php echo $form->textField($model, 'ciudad', array('maxlength' => 100)); ?>
		<?php echo $form->error($model,'ciudad'); ?>
		</div> row -->
		<div class="row">
		<?php echo $form->labelEx($model,'fecha'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'fecha',
			'value' => $model->fecha,
                        'language' => Yii::app()->language,    
			'options' => array(
				'showButtonPanel' => true,
                                'changeMonth' => true,
				'changeYear' => true,
				
				),
			));
; ?>
		<?php echo $form->error($model,'fecha'); ?>
		</div><!-- row -->
		
		
		<div class="row">
		<?php echo $form->labelEx($model,'cuenta'); ?>
		<?php echo $form->dropDownList($model,'cuenta',array('01.Banco de Chile' => '01.Banco de Chile', '02.Banco de Chile' => '02.Banco de Chile')); ?>
		<?php echo $form->error($model,'cuenta'); ?>
		</div><!-- row -->
                
                <div class="row">
		<?php echo $form->labelEx($model,'tipo_solicitud'); ?>
		<?php echo $form->dropDownList($model,'tipo_solicitud',array('Cheque' => 'Cheque', 'Transferencia' => 'Transferencia')); ?>
		<?php echo $form->error($model,'tipo_solicitud'); ?>
		</div><!-- row -->
                
                <div class="row">
		<?php echo $form->labelEx($model,'tipo_cheque'); ?>
		<?php echo $form->dropDownList($model,'tipo_cheque',array('Transferencia' => 'Seleccione Opcion', 'Abierto' => 'Abierto', 'Cruzado' => 'Cruzado', 'Nominativo' => 'Nominativo', 'Portador' => 'Portador')); ?>
		<?php echo $form->error($model,'tipo_cheque'); ?>
		</div><!-- row -->

		<div class="row">
		<?php echo $form->labelEx($model,'numero_cheque'); ?>
		<?php echo $form->textField($model, 'numero_cheque', array('maxlength' => 100)); ?>
		<?php echo $form->error($model,'numero_cheque'); ?>		                
                </div><!-- row -->
                
		<div class="row">
		<?php echo $form->labelEx($model,'observaciones'); ?>
		<?php echo $form->textField($model, 'observaciones', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'observaciones'); ?>
		</div><!-- row -->

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'), array('onClick' => "this.disabled=true;this.value='".Yii::t('app', 'Enviando')."';this.form.submit();"));
$this->endWidget();
?>
</div><!-- form -->
