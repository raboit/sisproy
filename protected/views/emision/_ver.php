<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('proveedor_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->proveedor)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('proyecto_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->proyecto)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('solicitud_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->solicitud)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('ciudad')); ?>:
	<?php echo GxHtml::encode($data->ciudad); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('fecha')); ?>:
	<?php echo GxHtml::encode($data->fecha); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('concepto')); ?>:
	<?php echo GxHtml::encode($data->concepto); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('total_debe')); ?>:
	<?php echo GxHtml::encode($data->total_debe); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('total_haber')); ?>:
	<?php echo GxHtml::encode($data->total_haber); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('cuenta')); ?>:
	<?php echo GxHtml::encode($data->cuenta); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('numero_cheque')); ?>:
	<?php echo GxHtml::encode($data->numero_cheque); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('sub_cuenta')); ?>:
	<?php echo GxHtml::encode($data->sub_cuenta); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('banco')); ?>:
	<?php echo GxHtml::encode($data->banco); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('cuenta_corriente')); ?>:
	<?php echo GxHtml::encode($data->cuenta_corriente); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('observaciones')); ?>:
	<?php echo GxHtml::encode($data->observaciones); ?>
	<br />
	*/ ?>

</div>