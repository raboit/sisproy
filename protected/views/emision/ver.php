<?php $this->widget('bootstrap.widgets.TbAlert', array(
        'block'=>true, // display a larger alert block?
        'fade'=>true, // use transitions?
        'closeText'=>'&times;', // close link text - if set to false, no close link is displayed
        
    )); ?>

<?php

$this->breadcrumbs = array(
	$model->label(2) => array('verTodos'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	
	
	array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('actualizar', 'id' => $model->id)),
	//array('label'=>Yii::t('app', 'Add') . ' Item', 'url'=>array('itemEmision/agregar','emision_id'=>$model->id)),
        array('label'=>Yii::t('app', 'Finish'). ' '. $model->label(), 'url'=>array('emision/finalizar','id'=>$model->id)),
	
);
?>

<?php 
$this->widget('application.extensions.print.printWidget', array(
                   'cssFile'=>'print.css',
                   'printedElement'=>'#content')); 
    ?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
        array(
            'name' => $model->user->rrhh !== null ? $model->user->rrhh->label() : $model->user->label(),
            'label' => $model->user->rrhh !== null ? $model->user->rrhh->label() : $model->user->label(),
            'type' => 'raw',
            'value' => $model->user->rrhh !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->user->rrhh)), array('rrhh/view', 'id' => GxActiveRecord::extractPkValue($model->user->rrhh, true))) : $model->user,
            ),
            'proveedor',
            'proyecto',
            'solicitud',
            //'ciudad',
            'fecha',
            //'concepto',
            array(
                'name' => 'total_debe',
                'value' => Yii::app()->format->formatNumber($model->total_debe),
            ),
            /*array(
                'name' => 'total_haber',
                'value' => Yii::app()->format->formatNumber($model->total_haber),
            ),*/
            'cuenta',
            'tipo_solicitud',
            array(
                'name'=>'tipo_cheque',
                'visible'=>'($data->tipo_solicitud==="Transferencia")?true:false;',
            ),
            array(
                'name'=>'numero_cheque',
                'visible'=>'$data->cuenta=="Transferencia"',
            ),
            'observaciones',
            'estado',
	),
)); ?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
    'buttons'=>array(
        array(
                'label'=>'Finalizar',
            'htmlOptions'=>array(
                'data-toggle'=>'modal',
                'data-target'=>'#modalFinalizar',
            ),
                'url'=>array('emision/finalizar','id'=>$model->id),
                'icon'=>'ok white',
                'type'=>'primary'
            ),
        array('label'=>'Anular',
            'htmlOptions'=>array(
                'data-toggle'=>'modal',
                'data-target'=>'#modalAnular',
            ),
            'url'=>array('emision/anular','id'=>$model->id),
            'icon'=>'remove white',
            'type'=>'danger',
            ),
        ),
)); ?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array('id'=>'modalFinalizar')); ?>
 
<div class="modal-header">
    <a class="close" data-dismiss="modal">&times;</a>
    <h4>Finalizar</h4>
</div>
 
<div class="modal-body">
    <p>¿Estás seguro que desea finalizar?</p>
</div>
 
<div class="modal-footer">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'type'=>'primary',
        'label'=>'Finalizar',
        'icon'=>'ok white',
        'type'=>'primary',
        'url'=>array('emision/finalizar','id'=>$model->id),
        //'htmlOptions'=>array('data-dismiss'=>'modal'),
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label'=>'Cancelar',
        'url'=>'#',
        'htmlOptions'=>array('data-dismiss'=>'modal'),
    )); ?>
</div>
 
<?php $this->endWidget(); ?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array('id'=>'modalAnular')); ?>
 
<div class="modal-header">
    <a class="close" data-dismiss="modal">&times;</a>
    <h4>Anular</h4>
</div>
 
<div class="modal-body">
    <p>¿Estás seguro que desea anular?</p>
</div>
 
<div class="modal-footer">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'type'=>'primary',
        'label'=>'Anular',
        'icon'=>'remove white',
        'type'=>'danger',
        'url'=>array('emision/anular','id'=>$model->id),
        //'htmlOptions'=>array('data-dismiss'=>'modal'),
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label'=>'Cancelar',
        'url'=>'#',
        'htmlOptions'=>array('data-dismiss'=>'modal'),
    )); ?>
</div>
 
<?php $this->endWidget(); ?>
