

<?php 
Yii::app()->clientScript->registerScript('tipo_solicitud','
    function tipo_solicitud(value){
    if(value=="Cheque"){
    $(".tipo_cheque").show();
    }else{
    $(".tipo_cheque").hide();

    }
    }
',CClientScript::POS_END); 
?>
<div class="form">
    
    <?php
    $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'emision-form',
        'enableAjaxValidation' => true,
    ));
    ?>
    <p class="note">
<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>

<?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'Cheque/Transferencia dirigido a:'); ?>
        <?php echo $form->dropDownList($model, 'proveedor_id', GxHtml::listDataEx(Proveedor::model()->findAllAttributes(null, true))); ?>
<?php echo $form->error($model, 'proveedor_id'); ?>
    </div><!-- row -->

    <!--		<div class="row">
    <?php echo $form->labelEx($model, 'ciudad'); ?>
    <?php echo $form->textField($model, 'ciudad', array('maxlength' => 100)); ?>
<?php echo $form->error($model, 'ciudad'); ?>
                    </div> row -->
    <div class="row">
        <?php echo $form->labelEx($model, 'fecha'); ?>
        <?php
        $form->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute' => 'fecha',
            'value' => $model->fecha,
            'language' => Yii::app()->language,
            'options' => array(
                'showButtonPanel' => true,
                'changeMonth' => true,
                'changeYear' => true,
            ),
        ));
        ;
        ?>
<?php echo $form->error($model, 'fecha'); ?>
    </div><!-- row -->


    <div class="row">
	<?php echo $form->labelEx($model,'cuenta_id'); ?>
	<?php echo $form->dropDownList($model, 'cuenta_id', GxHtml::listDataEx(Cuenta::model()->findAllAttributes(null, true))); ?>
	<?php echo $form->error($model,'cuenta_id'); ?>
    </div><!-- row -->

    <div class="row">
        <?php echo $form->labelEx($model, 'tipo_solicitud'); ?>
        <?php echo $form->dropDownList($model, 'tipo_solicitud', array('Cheque' => 'Cheque', 'Transferencia' => 'Transferencia'),array('onchange'=>'js:tipo_solicitud(this.value)')); ?>
        <?php echo $form->error($model, 'tipo_solicitud'); ?>
    </div><!-- row -->

    <div class="row tipo_cheque">
        <?php echo $form->labelEx($model, 'tipo_cheque'); ?>
        <?php echo $form->dropDownList($model, 'tipo_cheque', array('Transferencia' => 'Seleccione Opcion', 'Abierto' => 'Abierto', 'Cruzado' => 'Cruzado', 'Nominativo' => 'Nominativo', 'Portador' => 'Portador')); ?>
        <?php echo $form->error($model, 'tipo_cheque'); ?>
    </div><!-- row -->

    <div class="row tipo_cheque">
        <?php echo $form->labelEx($model, 'numero_cheque'); ?>
        <?php echo $form->textField($model, 'numero_cheque', array('maxlength' => 100)); ?>
        <?php echo $form->error($model, 'numero_cheque'); ?>
        <?php
//                $this->widget('bootstrap.widgets.TbButton', array(
//                    'label'=>'',
//                    'size'=>'mini',
//                    'icon'=>'search white',
//                    'type'=>'primary',
//                    'htmlOptions'=>array(
//                            'onclick'=>'js:addProveedor();',                        
//                            'data-toggle'=>'modal',
//                            'data-target'=>'#myModal',                            
//                        ),
//                ));
        ?>

    </div><!-- row -->


    <?php /* Eliminados por el Cliente

      <div class="row">
      <?php echo $form->labelEx($model,'sub_cuenta'); ?>
      <?php echo $form->textField($model, 'sub_cuenta', array('maxlength' => 100)); ?>
      <?php echo $form->error($model,'sub_cuenta'); ?>
      </div><!-- row -->
      <div class="row">
      <?php echo $form->labelEx($model,'banco'); ?>
      <?php echo $form->textField($model, 'banco', array('maxlength' => 100)); ?>
      <?php echo $form->error($model,'banco'); ?>
      </div><!-- row -->
      <div class="row">
      <?php echo $form->labelEx($model,'cuenta_corriente'); ?>
      <?php echo $form->textField($model, 'cuenta_corriente', array('maxlength' => 100)); ?>
      <?php echo $form->error($model,'cuenta_corriente'); ?>
      </div><!-- row -->

     */ ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'observaciones'); ?>
<?php echo $form->textField($model, 'observaciones', array('maxlength' => 255)); ?>
    <?php echo $form->error($model, 'observaciones'); ?>
    </div><!-- row -->

    <?php
    echo GxHtml::submitButton(Yii::t('app', 'Save'), array('onClick' => "this.disabled=true;this.value='" . Yii::t('app', 'Enviando') . "';this.form.submit();"));
    $this->endWidget();
    ?>
</div><!-- form -->
