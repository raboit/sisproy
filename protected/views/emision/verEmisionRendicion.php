
<?php

$this->breadcrumbs = array(
	$model->label(2) => array('finalizadas'),
	'Emisiones Finalizadas',
);

$this->menu=array(
	
	
	array('label'=>Yii::t('app', 'Back'), 'url'=>array('finalizadas')),
	
            );
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
            'proveedor',
            'proyecto',
            'solicitud',         
            'ciudad',
            'fecha',
            'concepto',
            'total_debe',
            'total_haber',
            'cuenta',
            'tipo_solicitud',
            'numero_cheque',
            /*'sub_cuenta',
            'banco',
            'cuenta_corriente',*/
            'observaciones',
            'estado',
	),
)); ?>

<?php /*

ITEMS:




$this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered',
    'dataProvider' => $model_items,
    'template' => "{items}\n{extendedSummary}",
    'columns' => array(
		'tipo_documento',
		'numero_documento',
		'debe',
		'haber',                
	),
    'extendedSummary' => array(
        'title' => 'Deuda total',
        'columns' => array(
            'debe' => array('label'=>'total deuda: ', 'class'=>'TbSumOperation')
        )
    ),
    'extendedSummaryOptions' => array(
        'class' => 'well pull-right',
        'style' => 'width:300px'
    ),
    'chartOptions' => array(
        'data' => array(
            'series' => array(
                array(
                        'name' => 'Precio',
	                'attribute' => 'debe'
                )
            )
        ),
        'config' => array(
            'credits' => array('enabled' => false),
            'chart'=>array(
                'width'=>800,
                //'type'=>'column',
                 
            ),
            'tooltip'=>array(
                'headerFormat'=> '<small>    Item :{point.key}</small><br><table>',
                
            ),
            'yAxis'=>array(
                'title'=>array(
                    'text'=>'deuda Producto',
                    ),
                ),
            'title'=>array(
                'text'=>'Distribucion Items',
            ),
           
        )
    ),
));

*/ ?>