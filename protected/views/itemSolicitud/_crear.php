<div class="form">

    
<?php $this->widget('ext.EChosen.EChosen', array('target' => 'select')); ?>
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'item-solicitud-form',
	'enableAjaxValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>
                <div class="row">
		<?php echo $form->labelEx($model,'cuenta_contable_id'); ?>
		<?php echo $form->dropDownList($model, 'cuenta_contable_id', GxHtml::listDataEx(CuentaContable::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'cuenta_contable_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'valor_unitario'); ?>
		<?php echo $form->textField($model, 'valor_unitario'); ?>
		<?php echo $form->error($model,'valor_unitario'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'cantidad'); ?>
		<?php echo $form->textField($model, 'cantidad'); ?>
		<?php echo $form->error($model,'cantidad'); ?>
		</div><!-- row -->
                <div class="row">
		<?php echo $form->labelEx($model,'descripcion'); ?>
		<?php echo $form->textField($model, 'descripcion'); ?>
		<?php echo $form->error($model,'descripcion'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'), array('onClick' => "this.disabled=true;this.value='".Yii::t('app', 'Enviando')."';this.form.submit();"));
$this->endWidget();
?>
</div><!-- form -->