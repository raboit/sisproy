<?php

$this->breadcrumbs = array(
	'Ver Solicitud' => array('solicitud/ver', 'id' => $model->solicitud_id),
	'Item Solicitud',
);

$this->menu=array(
        array('label'=>Yii::t('app', 'Back'), 'url'=>array('solicitud/ver', 'id' => $model->solicitud_id)),
	//array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
	//array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	//array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->id)),
	//array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?')),
	//array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
            'id',
            'solicitud',
            'cuentaContable',
            array(
                'name' => 'valor_unitario',
                'value' => Yii::app()->format->formatNumber($model->valor_unitario),
            ),
            'cantidad',
            array(
                'name' => 'total',
                'value' => Yii::app()->format->formatNumber($model->total),
            ),
            'descripcion',
	),
)); ?>

