<?php

$this->breadcrumbs = array(
	'Ver Solicitud' => array('solicitud/ver', 'id' => $model->solicitud_id),
	Yii::t('app', 'Update'),
);

$this->menu = array(
        array('label'=>Yii::t('app', 'Back'), 'url'=>array('solicitud/ver', 'id' => $model->solicitud_id)),
	//array('label' => Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
	//array('label' => Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	//array('label' => Yii::t('app', 'View') . ' ' . $model->label(), 'url'=>array('view', 'id' => GxActiveRecord::extractPkValue($model, true))),
	//array('label' => Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'Update') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php
$this->renderPartial('_crear', array(
		'model' => $model));
?>