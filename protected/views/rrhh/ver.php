<?php

$this->breadcrumbs = array(
	$model->label(2) => array('verTodos'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('crear')),
	array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('actualizar', 'id' => $model->id)),
	array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('verTodos')),
        array('label'=>Yii::t('app', 'Ver Contratos'), 'url'=>array('rrhh/verContrato', 'id' => $model->id)),
    
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
'id',
'afp',
'nombres',
'apellidos',
'documento',
'rut',
'nacionalidad',
'sexo',
'fecha_nacimiento',
'estado_civil',
'direccion',
'telefono',
'celular',
'email',
'ciudad',
'salud',
	),
)); ?>
