<?php

$this->breadcrumbs = array(
	'Ver RRHH' => array('rrhh/ver', 'id' => $model_rrhh->id),
	GxHtml::valueEx($model_rrhh),
);

$this->menu=array(
        array('label'=>Yii::t('app', 'Back'), 'url'=>array('rrhh/ver', 'id' => $model_rrhh->id)),
	//array('label'=>Yii::t('app', 'Create') . ' ' . $model_rrhh->label(), 'url'=>array('crear')),
	//array('label'=>Yii::t('app', 'Update') . ' ' . $model_rrhh->label(), 'url'=>array('actualizar', 'id' => $model_proyecto->id)),
	//array('label'=>Yii::t('app', 'Manage') . ' ' . $model_rrhh->label(2), 'url'=>array('verTodos')),
        
);
?>

<h1><?php echo Yii::t('app', 'Name') . ':  ' . GxHtml::encode($model_rrhh->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model_rrhh)); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model_rrhh,
	'attributes' => array(
'id',
'afp',
'nombres',
'apellidos',
'documento',
'rut',
'nacionalidad',
'sexo',
'fecha_nacimiento',
'estado_civil',
'direccion',
'telefono',
'celular',
'email',
'ciudad',
'salud',
	),
)); ?>

CONTRATOS:

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered',
    'dataProvider' => $model_contrato,
    'template' => "{items}\n{extendedSummary}",
    'columns' => array(
        'id',
        'rrhh',
        'proyecto',
        'tipoContrato',
        'fecha_inicio',
        'fecha_termino',
        'remuneracion',
        'estado',
        array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'htmlOptions'=>array('style'=>'width: 50px'),
			'template'=>'{view}',
			'buttons'=>array(
				'view' => array(
					'label'=>'Ver Contrato',
					'url'=>'Yii::app()->createUrl("contrato/verRrhh", array("contrato_id"=>$data->id))',
				),
				
			),
				
	) 
	),
    
));?>

