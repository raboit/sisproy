<div class="form">

    
<?php $this->widget('ext.EChosen.EChosen', array('target' => 'select')); ?>
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'rrhh-form',
	'enableAjaxValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'afp_id'); ?>
		<?php echo $form->dropDownList($model, 'afp_id', GxHtml::listDataEx(Afp::model()->findAllAttributes(null, true)), array('empty'=>'Seleccione AFP')); ?>
		<?php echo $form->error($model,'afp_id'); ?>
		</div><!-- row -->
		
		<div class="row">
		<?php echo $form->labelEx($model,'nombres'); ?>
		<?php echo $form->textField($model, 'nombres', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'nombres'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'apellidos'); ?>
		<?php echo $form->textField($model, 'apellidos', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'apellidos'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'documento'); ?>
		<?php echo $form->dropDownList($model, 'documento', array('Carnet' => 'Carnet', 'Pasaporte' => 'Pasaporte')); ?>
		<?php echo $form->error($model,'documento'); ?>
		</div><!-- row -->
                <div class="row">
		<?php echo $form->labelEx($model,'rut'); ?>
		<?php echo $form->textField($model, 'rut', array('maxlength' => 13)); ?>
		<?php echo $form->error($model,'rut'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'nacionalidad'); ?>
		<?php echo $form->textField($model, 'nacionalidad', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'nacionalidad'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'sexo'); ?>
		<?php echo $form->dropDownList($model, 'sexo', array('Masculino' => 'Masculino', 'Femenino' => 'Femenino')); ?>
		<?php echo $form->error($model,'sexo'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'fecha_nacimiento'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'fecha_nacimiento',
			'value' => $model->fecha_nacimiento,
                        'language' => Yii::app()->language,    
			'options' => array(
				'showButtonPanel' => true,
                                'changeMonth' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
		<?php echo $form->error($model,'fecha_nacimiento'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'estado_civil'); ?>
		<?php echo $form->dropDownList($model, 'estado_civil', array('Soltero(a)' => 'Soltero(a)', 'Casado(a)' => 'Casado(a)', 'Viudo(a)' => 'Viudo(a)')); ?>
		<?php echo $form->error($model,'estado_civil'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'direccion'); ?>
		<?php echo $form->textField($model, 'direccion', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'direccion'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'telefono'); ?>
		<?php echo $form->textField($model, 'telefono', array('maxlength' => 50)); ?>
		<?php echo $form->error($model,'telefono'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'celular'); ?>
		<?php echo $form->textField($model, 'celular', array('maxlength' => 50)); ?>
		<?php echo $form->error($model,'celular'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model, 'email', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'email'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'ciudad'); ?>
		<?php echo $form->textField($model, 'ciudad', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'ciudad'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'salud'); ?>
		<?php echo $form->dropDownList($model, 'salud', array('Fonasa' => 'Fonasa', 'Isapre' => 'Isapre'), array('empty'=>'Seleccione Salud')); ?>
		<?php echo $form->error($model,'salud'); ?>
		</div><!-- row -->
                <div class="row">
		<?php echo $form->labelEx($model,'tipo_usuario'); ?>
		<?php echo $form->dropDownList($model, 'tipo_usuario', array('Director Ejecutivo' => 'Director Ejecutivo', 'Jefe de Taller' => 'Jefe de Taller', 'Usuario Comun' => 'Usuario Comun')); ?>
		<?php echo $form->error($model,'tipo_usuario'); ?>
		</div><!-- row -->

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'), array('onClick' => "this.disabled=true;this.value='".Yii::t('app', 'Enviando')."';this.form.submit();"));
$this->endWidget();
?>
</div><!-- form -->