<?php

$this->breadcrumbs = array(
	'Proyecto' => array('proyecto/verPresupuesto', 'id' => $model->proyecto_id),
	'Ver Presupuesto',
);

$this->menu=array(
	//array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('crear')),
	array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('actualizar', 'id' => $model->id)),
	//array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('verTodos')),
        array('label'=>Yii::t('app', 'Create') . ' ' . 'Item Presupuesto', 'url'=>array('itemPresupuesto/crear', 'presupuesto_id' => $model->id)),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
            'id',
            'proyecto',
            'fecha',
            array(
                'name' => 'monto',
                'value' => Yii::app()->format->formatNumber($model->monto),
            ),
            'observaciones',
	),
)); ?>

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered',
    'dataProvider' => $model_items,
    'enableSorting'=>false,
    'template' => "{items}",
    'columns' => array(
		
		   array(
				'name'=>'cuenta_contable_id',
				'value'=>'GxHtml::valueEx($data->cuentaContable)',
				'filter'=>GxHtml::listDataEx(CuentaContable::model()->findAllAttributes(null, true)),
				),	
                  
                array(
                                'name' => 'monto',
                                'value' => 'Yii::app()->format->formatNumber($data->monto)',
                            ),
                array(
                    'name' => 'Solicitado',
                    'value' => 'Yii::app()->format->formatNumber($data->getComprometido())',
                ),
                array(
                    'name' => 'Emitido',
                    'value' => 'Yii::app()->format->formatNumber($data->getEmitido())',
                ),
                array(
                    'name' => 'Resta',
                    'value' => 'Yii::app()->format->formatNumber(($data->monto-$data->getEmitido()))',
                ),
        
        
                    'fecha',
                    array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'htmlOptions'=>array('style'=>'width: 50px'),
			'template'=>'{view} {update}',
			'buttons'=>array(
				'view' => array(
					'label'=>'Ver Item Presupuesto',
					'url'=>'Yii::app()->createUrl("itemPresupuesto/ver", array("id"=>$data->id))',
				),
				'update' => array(
					'label'=>'Actualizar Item Presupuesto',
					'url'=>'Yii::app()->createUrl("itemPresupuesto/actualizar", array("id"=>$data->id))',
				),
			),
				
                ),
    ),
   
    
));?>