<?php

$this->breadcrumbs = array(
	'Proyecto' => array('proyecto/verPresupuesto', 'id' => $model->proyecto_id),
	Yii::t('app', 'Create'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Back'), 'url' => array('proyecto/verPresupuesto', 'id' => $model->proyecto_id)),
);
?>

<h1><?php echo Yii::t('app', 'Create') . ' ' . GxHtml::encode($model->label()); ?></h1>

<?php
$this->renderPartial('_crear', array(
		'model' => $model,
		'buttons' => 'create'));
?>