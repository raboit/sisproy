<?php

class ContratoController extends GxController {

        public function filters() {
                return array('rights');
        }

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Contrato'),
		));
	}
        public function actionEditable()
        {
            Yii::import('ext.x-editable.EditableSaver'); //or you can add import 'ext.editable.*' to config
            $es = new EditableSaver('Contrato');  // 'User' is classname of model to be updated
            $es->update();
        }

	public function actionCreate() {
		$model = new Contrato;

		$this->performAjaxValidation($model, 'contrato-form');

		if (isset($_POST['Contrato'])) {
			$model->setAttributes($_POST['Contrato']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Contrato');

		$this->performAjaxValidation($model, 'contrato-form');

		if (isset($_POST['Contrato'])) {
			$model->setAttributes($_POST['Contrato']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Contrato')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Contrato');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
                $session = new CHttpSession;
                $session->open();
		$model = new Contrato('search');
		$model->unsetAttributes();

		if (isset($_GET['Contrato'])){
			$model->setAttributes($_GET['Contrato']);
                }

                $session['Contrato_model_search'] = $model;
                
		$this->render('admin', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
        public function actionGenerateExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Contrato_model_search']))
               {
                $model = $session['Contrato_model_search'];
                $model = Contrato::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Contrato::model()->findAll();
             
             $this->toExcel($model, array('id', 'rrhh', 'proyecto', 'tipoContrato', 'fecha_inicio', 'fecha_termino', 'remuneracion', 'estado'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGeneratePdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Contrato_model_search']))
               {
                $model = $session['Contrato_model_search'];
                $model = Contrato::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Contrato::model()->findAll();
             
             $this->toExcel($model, array('id', 'rrhh', 'proyecto', 'tipoContrato', 'fecha_inicio', 'fecha_termino', 'remuneracion', 'estado'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}
}