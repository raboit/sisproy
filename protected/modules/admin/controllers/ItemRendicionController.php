<?php

class ItemRendicionController extends GxController {

        public function filters() {
                return array('rights');
        }

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'ItemRendicion'),
		));
	}
        public function actionEditable()
        {
            Yii::import('ext.x-editable.EditableSaver'); //or you can add import 'ext.editable.*' to config
            $es = new EditableSaver('ItemRendicion');  // 'User' is classname of model to be updated
            $es->update();
        }

	public function actionCreate() {
		$model = new ItemRendicion;

		$this->performAjaxValidation($model, 'item-rendicion-form');

		if (isset($_POST['ItemRendicion'])) {
			$model->setAttributes($_POST['ItemRendicion']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'ItemRendicion');

		$this->performAjaxValidation($model, 'item-rendicion-form');

		if (isset($_POST['ItemRendicion'])) {
			$model->setAttributes($_POST['ItemRendicion']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'ItemRendicion')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('ItemRendicion');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
                $session = new CHttpSession;
                $session->open();
		$model = new ItemRendicion('search');
		$model->unsetAttributes();

		if (isset($_GET['ItemRendicion'])){
			$model->setAttributes($_GET['ItemRendicion']);
                }

                $session['ItemRendicion_model_search'] = $model;
                
		$this->render('admin', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
        public function actionGenerateExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['ItemRendicion_model_search']))
               {
                $model = $session['ItemRendicion_model_search'];
                $model = ItemRendicion::model()->findAll($model->search()->criteria);
               }
               else
                 $model = ItemRendicion::model()->findAll();
             
             $this->toExcel($model, array('id', 'proveedor', 'cuentaContable', 'cuentaEspecifica', 'rendicion', 'proyecto', 'tipo_documento', 'numero_documento', 'fecha', 'mes', 'agno', 'monto'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGeneratePdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['ItemRendicion_model_search']))
               {
                $model = $session['ItemRendicion_model_search'];
                $model = ItemRendicion::model()->findAll($model->search()->criteria);
               }
               else
                 $model = ItemRendicion::model()->findAll();
             
             $this->toExcel($model, array('id', 'proveedor', 'cuentaContable', 'cuentaEspecifica', 'rendicion', 'proyecto', 'tipo_documento', 'numero_documento', 'fecha', 'mes', 'agno', 'monto'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}
}