<?php

class ItemSolicitudController extends GxController {

        public function filters() {
                return array('rights');
        }

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'ItemSolicitud'),
		));
	}
        
        public function actionEditable()
        {
            Yii::import('ext.x-editable.EditableSaver'); //or you can add import 'ext.editable.*' to config
            $es = new EditableSaver('ItemSolicitud');  // 'User' is classname of model to be updated
            $es->update();
        }

	public function actionCreate() {
		$model = new ItemSolicitud;

		$this->performAjaxValidation($model, 'item-solicitud-form');

		if (isset($_POST['ItemSolicitud'])) {
			$model->setAttributes($_POST['ItemSolicitud']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'ItemSolicitud');

		$this->performAjaxValidation($model, 'item-solicitud-form');

		if (isset($_POST['ItemSolicitud'])) {
			$model->setAttributes($_POST['ItemSolicitud']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'ItemSolicitud')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('ItemSolicitud');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
                $session = new CHttpSession;
                $session->open();
		$model = new ItemSolicitud('search');
		$model->unsetAttributes();

		if (isset($_GET['ItemSolicitud'])){
			$model->setAttributes($_GET['ItemSolicitud']);
                }

                $session['ItemSolicitud_model_search'] = $model;
                
		$this->render('admin', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
        public function actionGenerateExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['ItemSolicitud_model_search']))
               {
                $model = $session['ItemSolicitud_model_search'];
                $model = ItemSolicitud::model()->findAll($model->search()->criteria);
               }
               else
                 $model = ItemSolicitud::model()->findAll();
             
             $this->toExcel($model, array('id', 'solicitud', 'descripcion', 'valor_unitario', 'cantidad', 'total'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGeneratePdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['ItemSolicitud_model_search']))
               {
                $model = $session['ItemSolicitud_model_search'];
                $model = ItemSolicitud::model()->findAll($model->search()->criteria);
               }
               else
                 $model = ItemSolicitud::model()->findAll();
             
             $this->toExcel($model, array('id', 'solicitud', 'descripcion', 'valor_unitario', 'cantidad', 'total'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}
}