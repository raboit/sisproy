<?php

class ItemPresupuestoController extends GxController {

        public function filters() {
                return array('rights');
        }

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'ItemPresupuesto'),
		));
	}
        
        public function actionEditable()
        {
            Yii::import('ext.x-editable.EditableSaver'); //or you can add import 'ext.editable.*' to config
            $es = new EditableSaver('ItemPresupuesto');  // 'User' is classname of model to be updated
            $es->update();
        }

	public function actionCreate() {
		$model = new ItemPresupuesto;

		$this->performAjaxValidation($model, 'item-presupuesto-form');

		if (isset($_POST['ItemPresupuesto'])) {
			$model->setAttributes($_POST['ItemPresupuesto']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'ItemPresupuesto');

		$this->performAjaxValidation($model, 'item-presupuesto-form');

		if (isset($_POST['ItemPresupuesto'])) {
			$model->setAttributes($_POST['ItemPresupuesto']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'ItemPresupuesto')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('ItemPresupuesto');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
                $session = new CHttpSession;
                $session->open();
		$model = new ItemPresupuesto('search');
		$model->unsetAttributes();

		if (isset($_GET['ItemPresupuesto'])){
			$model->setAttributes($_GET['ItemPresupuesto']);
                }

                $session['ItemPresupuesto_model_search'] = $model;
                
		$this->render('admin', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
        public function actionGenerateExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['ItemPresupuesto_model_search']))
               {
                $model = $session['ItemPresupuesto_model_search'];
                $model = ItemPresupuesto::model()->findAll($model->search()->criteria);
               }
               else
                 $model = ItemPresupuesto::model()->findAll();
             
             $this->toExcel($model, array('id', 'presupuesto', 'cuentaContable', 'fecha', 'mes', 'agno', 'monto', 'proyecto'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGeneratePdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['ItemPresupuesto_model_search']))
               {
                $model = $session['ItemPresupuesto_model_search'];
                $model = ItemPresupuesto::model()->findAll($model->search()->criteria);
               }
               else
                 $model = ItemPresupuesto::model()->findAll();
             
             $this->toExcel($model, array('id', 'presupuesto', 'cuentaContable', 'fecha', 'mes', 'agno', 'monto', 'proyecto'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}
}