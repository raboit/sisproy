<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('rrhh_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->rrhh)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('centro_costo_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->centroCosto)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('nombre')); ?>:
	<?php echo GxHtml::encode($data->nombre); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('descripcion')); ?>:
	<?php echo GxHtml::encode($data->descripcion); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('archivo')); ?>:
	<?php echo GxHtml::encode($data->archivo); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('contrato')); ?>:
	<?php echo GxHtml::encode($data->contrato); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('comuna')); ?>:
	<?php echo GxHtml::encode($data->comuna); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('region')); ?>:
	<?php echo GxHtml::encode($data->region); ?>
	<br />
	*/ ?>

</div>