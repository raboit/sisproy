<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('presupuesto_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->presupuesto)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('cuenta_contable_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->cuentaContable)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('fecha')); ?>:
	<?php echo GxHtml::encode($data->fecha); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('mes')); ?>:
	<?php echo GxHtml::encode($data->mes); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('agno')); ?>:
	<?php echo GxHtml::encode($data->agno); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('monto')); ?>:
	<?php echo GxHtml::encode(Yii::app()->format->formatNumber($data->monto)); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('proyecto_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->proyecto)); ?>
	<br />
	*/ ?>

</div>