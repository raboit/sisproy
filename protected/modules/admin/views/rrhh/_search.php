<?php $this->widget('ext.EChosen.EChosen' ); ?><div class="wide form">

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'search-rrhh-form',
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'id'); ?>
		<?php echo $form->textField($model, 'id'); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'rut'); ?>
		<?php echo $form->textField($model, 'rut', array('maxlength' => 13)); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'nombres'); ?>
		<?php echo $form->textField($model, 'nombres', array('maxlength' => 255)); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'apellidos'); ?>
		<?php echo $form->textField($model, 'apellidos', array('maxlength' => 255)); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'sexo'); ?>
		<?php echo $form->textField($model, 'sexo', array('maxlength' => 15)); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'fecha_nacimiento'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'fecha_nacimiento',
			'value' => $model->fecha_nacimiento,
                        'language' => Yii::app()->language,    
			'options' => array(
				'showButtonPanel' => true,
                                'changeMonth' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
	</div>
                	<div class="row">
                <?php echo $form->label($model, 'fecha_nacimiento_inicio'); ?>
                <?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'fecha_nacimiento_inicio',
			'value' => $model->fecha_nacimiento_inicio,
                        'language' => Yii::app()->language,    
			'options' => array(
				'showButtonPanel' => true,
                                'changeMonth' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
	</div>
	<div class="row">
                <?php echo $form->label($model, 'fecha_nacimiento_termino'); ?>
    
                <?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'fecha_nacimiento_termino',
			'value' => $model->fecha_nacimiento_termino,
                        'language' => Yii::app()->language,    
			'options' => array(
				'showButtonPanel' => true,
                                'changeMonth' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'direccion'); ?>
		<?php echo $form->textField($model, 'direccion', array('maxlength' => 255)); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'telefono'); ?>
		<?php echo $form->textField($model, 'telefono', array('maxlength' => 50)); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'celular'); ?>
		<?php echo $form->textField($model, 'celular', array('maxlength' => 50)); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'email'); ?>
		<?php echo $form->textField($model, 'email', array('maxlength' => 255)); ?>
	</div>
                
	<div class="row buttons">
		<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'label'=>Yii::t('app', 'Search'), 'icon'=>'search'));?>
		<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'button', 'label'=>Yii::t('app', 'Reset'), 'icon'=>'icon-remove-sign', 'htmlOptions'=>array('class'=>'btnreset btn-small')));?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
<script>
     $(".btnreset").click(function(){
             $(":input","#search-rrhh-form").each(function() {
             var type = this.type;
             var tag = this.tagName.toLowerCase(); // normalize case
             if (type == "text" || type == "password" || tag == "textarea") this.value = "";
             else if (type == "checkbox" || type == "radio") this.checked = false;
             else if (tag == "select") this.selectedIndex = "";
       });
     });
</script>

