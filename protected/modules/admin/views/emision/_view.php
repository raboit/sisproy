<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('user_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->user)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('proveedor_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->proveedor)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('proyecto_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->proyecto)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('solicitud_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->solicitud)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('ciudad')); ?>:
	<?php echo GxHtml::encode($data->ciudad); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('fecha')); ?>:
	<?php echo GxHtml::encode($data->fecha); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('concepto')); ?>:
	<?php echo GxHtml::encode($data->concepto); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('total_debe')); ?>:
	<?php echo GxHtml::encode($data->total_debe); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('total_haber')); ?>:
	<?php echo GxHtml::encode($data->total_haber); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('cuenta')); ?>:
	<?php echo GxHtml::encode($data->cuenta); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('tipo_solicitud')); ?>:
	<?php echo GxHtml::encode($data->tipo_solicitud); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('numero_cheque')); ?>:
	<?php echo GxHtml::encode($data->numero_cheque); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('observaciones')); ?>:
	<?php echo GxHtml::encode($data->observaciones); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('estado')); ?>:
	<?php echo GxHtml::encode($data->estado); ?>
	<br />
	*/ ?>

</div>