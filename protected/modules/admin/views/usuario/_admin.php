<?php

$this->widget('bootstrap.widgets.TbGridView', array(
	'type' => 'striped bordered',
	'dataProvider' => $model->all(),
	'template' => "{items}{pager}",
	'columns' => array(
                'id',
		array(
			'class' => 'editable.EditableColumn',
			'name' => 'username',
			'sortable'=>true,
			'editable' => array(
				'url' => $this->createUrl('user/editable'),
				'placement' => 'right',
				'inputclass' => 'span3',
                            'type'=>'text',
                            
			)
		),
		'password',
		array(
			'class' => 'editable.EditableColumn',
			'name' => 'email',
			'sortable'=>true,
			'editable' => array(
				'url' => $this->createUrl('user/editable'),
				'placement' => 'right',
				'inputclass' => 'span3',
                            'type'=>'text',
                            
			)
		),
                array(
			'class' => 'editable.EditableColumn',
			'name' => 'tipo',
			'sortable'=>true,
			'editable' => array(
				'url' => $this->createUrl('user/editable'),
                                'type'=>'select',
                                'source'=>array('Administrador'=>'Administrador','Jefe de Proyecto'=>'Jefe de Proyecto','Director Ejecutivo'=>'Director Ejecutivo','Jefe de Taller'=>'Jefe de Taller'),
                            
			),
		),
            
		
            
            ),
));

?>