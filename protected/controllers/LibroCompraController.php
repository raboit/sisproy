<?php

class LibroCompraController extends GxController {

        public function filters() {
                return array('rights');
        }

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'LibroCompra'),
		));
	}

	public function actionCreate() {
		$model = new LibroCompra;

		$this->performAjaxValidation($model, 'libro-compra-form');

		if (isset($_POST['LibroCompra'])) {
			$model->setAttributes($_POST['LibroCompra']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->item_rendicion_id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'LibroCompra');

		$this->performAjaxValidation($model, 'libro-compra-form');

		if (isset($_POST['LibroCompra'])) {
			$model->setAttributes($_POST['LibroCompra']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->item_rendicion_id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'LibroCompra')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('LibroCompra');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
                $session = new CHttpSession;
                $session->open();
		$model = new LibroCompra('search');
		$model->unsetAttributes();

		if (isset($_GET['LibroCompra'])){
			$model->setAttributes($_GET['LibroCompra']);
                }

                $session['LibroCompra_model_search'] = $model;
                
		$this->render('admin', array(
			'model' => $model,
		));
	}
        
	public function actionVerTodos() {
                $this->layout='column1';
                $session = new CHttpSession;
                $session->open();
		$model = new LibroCompra('search');
		$model->unsetAttributes();

		if (isset($_GET['LibroCompra'])){
			$model->setAttributes($_GET['LibroCompra']);
                }

                $session['LibroCompra_model_search'] = $model;
                
		$this->render('verTodos', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
        public function actionGenerateExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['LibroCompra_model_search']))
               {
                $model = $session['LibroCompra_model_search'];
                $model = LibroCompra::model()->findAll($model->search()->criteria);
               }
               else
                 $model = LibroCompra::model()->findAll();
             
             //$this->toExcel($model, array('item_rendicion_id', 'fecha_emision', 'fecha_item_rendicion', 'dia_emision', 'mes_emision', 'ano_emision', 'dia_item_rendicion', 'mes_item_rendicion', 'ano_item_rendicion', 'tipo_documento', 'numero_documento', 'proveedor', 'monto', 'tipo_pago', 'numero_cheque', 'asignacion', 'centro_costo', 'proyecto', 'cuenta_contable', 'cuenta_especifica'), date('Y-m-d-H-i-s'), array(), 'Excel5');
               $this->toExcel($model, array('dia_item_rendicion', 'mes_item_rendicion', 'ano_item_rendicion', 'tipo_documento', 'numero_documento', 'item_rendicion_id', 'proveedor', 'monto', 'tipo_solicitud', 'numero_cheque', 'asignacion', 'centro_costo', 'proyecto', 'cuenta_contable', 'cuenta_especifica'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGeneratePdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['LibroCompra_model_search']))
               {
                $model = $session['LibroCompra_model_search'];
                $model = LibroCompra::model()->findAll($model->search()->criteria);
               }
               else
                 $model = LibroCompra::model()->findAll();
             
             $this->toExcel($model, array('item_rendicion_id', 'fecha_emision', 'fecha_item_rendicion', 'dia_emision', 'mes_emision', 'ano_emision', 'dia_item_rendicion', 'mes_item_rendicion', 'ano_item_rendicion', 'tipo_documento', 'numero_documento', 'proveedor', 'monto', 'tipo_solicitud', 'numero_cheque', 'asignacion', 'centro_costo', 'proyecto', 'cuenta_contable', 'cuenta_especifica'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}
}