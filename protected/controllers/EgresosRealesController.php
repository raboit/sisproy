<?php

class EgresosRealesController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'EgresosReales'),
		));
	}

	public function actionCreate() {
		$model = new EgresosReales;

		$this->performAjaxValidation($model, 'egresos-reales-form');

		if (isset($_POST['EgresosReales'])) {
			$model->setAttributes($_POST['EgresosReales']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->rendicion));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'EgresosReales');

		$this->performAjaxValidation($model, 'egresos-reales-form');

		if (isset($_POST['EgresosReales'])) {
			$model->setAttributes($_POST['EgresosReales']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->rendicion));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'EgresosReales')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('EgresosReales');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
                $this->layout='column1';
                $session = new CHttpSession;
                $session->open();
		$model = new EgresosReales('search');
		$model->unsetAttributes();

		if (isset($_GET['EgresosReales'])){
			$model->setAttributes($_GET['EgresosReales']);
                }

                $session['EgresosReales_model_search'] = $model;
                
		$this->render('admin', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
        public function actionGenerateExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['EgresosReales_model_search']))
               {
                $model = $session['EgresosReales_model_search'];
                $model = EgresosReales::model()->findAll($model->search()->criteria);
               }
               else
                 $model = EgresosReales::model()->findAll();
             
             $this->toExcel($model, array('rendicion',
                'fecha',
                'tipo_documento',
                'numero_documento',
                'proveedor',
                'monto',
                'numero_cheque',
                'banco',
                'numero_cuenta',
                'sigla',
                'cuenta_contable',
                'cuenta_especifica',), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGeneratePdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['EgresosReales_model_search']))
               {
                $model = $session['EgresosReales_model_search'];
                $model = EgresosReales::model()->findAll($model->search()->criteria);
               }
               else
                 $model = EgresosReales::model()->findAll();
             
             $this->toExcel($model, array('rendicion',
                'fecha',
                'tipo_documento',
                'numero_documento',
                'proveedor',
                'monto',
                'numero_cheque',
                'banco',
                'numero_cuenta',
                'sigla',
                'cuenta_contable',
                'cuenta_especifica',), date('Y-m-d-H-i-s'), array(), 'PDF');
	}
}