<?php

class TipoIngresoController extends GxController {

        public function filters() {
                return array('rights');
        }

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'TipoIngreso'),
		));
	}
        
        public function actionVer($id) {
		$this->render('ver', array(
			'model' => $this->loadModel($id, 'TipoIngreso'),
		));
	}

	public function actionCreate() {
		$model = new TipoIngreso;

		$this->performAjaxValidation($model, 'tipo-ingreso-form');

		if (isset($_POST['TipoIngreso'])) {
			$model->setAttributes($_POST['TipoIngreso']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}
        
        public function actionCrear() {
		$model = new TipoIngreso;

		$this->performAjaxValidation($model, 'tipo-ingreso-form');

		if (isset($_POST['TipoIngreso'])) {
			$model->setAttributes($_POST['TipoIngreso']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('crear', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'TipoIngreso');

		$this->performAjaxValidation($model, 'tipo-ingreso-form');

		if (isset($_POST['TipoIngreso'])) {
			$model->setAttributes($_POST['TipoIngreso']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}
        
        public function actionActualizar($id) {
		$model = $this->loadModel($id, 'TipoIngreso');

		$this->performAjaxValidation($model, 'tipo-ingreso-form');

		if (isset($_POST['TipoIngreso'])) {
			$model->setAttributes($_POST['TipoIngreso']);

			if ($model->save()) {
				$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('actualizar', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'TipoIngreso')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('TipoIngreso');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
                $session = new CHttpSession;
                $session->open();
		$model = new TipoIngreso('search');
		$model->unsetAttributes();

		if (isset($_GET['TipoIngreso'])){
			$model->setAttributes($_GET['TipoIngreso']);
                }

                $session['TipoIngreso_model_search'] = $model;
                
		$this->render('admin', array(
			'model' => $model,
		));
	}
        
        public function actionVerTodos() {
                $session = new CHttpSession;
                $session->open();
		$model = new TipoIngreso('search');
		$model->unsetAttributes();

		if (isset($_GET['TipoIngreso'])){
			$model->setAttributes($_GET['TipoIngreso']);
                }

                $session['TipoIngreso_model_search'] = $model;
                
		$this->render('verTodos', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
        public function actionGenerateExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['TipoIngreso_model_search']))
               {
                $model = $session['TipoIngreso_model_search'];
                $model = TipoIngreso::model()->findAll($model->search()->criteria);
               }
               else
                 $model = TipoIngreso::model()->findAll();
             
             $this->toExcel($model, array('id', 'nombre'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGeneratePdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['TipoIngreso_model_search']))
               {
                $model = $session['TipoIngreso_model_search'];
                $model = TipoIngreso::model()->findAll($model->search()->criteria);
               }
               else
                 $model = TipoIngreso::model()->findAll();
             
             $this->toExcel($model, array('id', 'nombre'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}
}