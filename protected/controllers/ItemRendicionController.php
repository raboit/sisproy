<?php

class ItemRendicionController extends GxController {

        public function filters() {
                return array('rights');
        }
        
        
        public function actionGetCuenta(){
            
            $data= CuentaEspecifica::model()->findAllByAttributes(array(),"cuenta_contable_id=:cuenta_contable_id",array(':cuenta_contable_id'=>$_POST['ItemRendicion']['cuenta_contable_id']));
            $data = CHtml::listData($data,'id','nombre');
             foreach($data as $value=>$name)
                {
                    echo CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);                 
                }

        }
        
        
        public function actionAgregar($rendicion_id) {
		$model_item = new ItemRendicion("crear");
                $model_item->rendicion_id=$rendicion_id;
                $model_rendicion = $this->loadModel($rendicion_id, 'Rendicion');
                $model_item->proyecto_id=$model_rendicion->emision->proyecto_id;
                $model_item->tipo_solicitud=$model_rendicion->tipo_solicitud;
                $model_item->numero_cheque=$model_rendicion->numero_cheque;
                $model_item->cuenta_id=$model_rendicion->cuenta_id;

		$this->performAjaxValidation($model_item, 'item-rendicion-form');

		if(isset($_POST['ItemRendicion'])) {
			$model_item->setAttributes($_POST['ItemRendicion']);
                                                
                        if ($model_item->save()) {
                            $model_rendicion->total_salida = $model_rendicion->total_salida + $model_item->monto;
                            $model_rendicion->total_saldo = $model_rendicion->total_entrada - $model_rendicion->total_salida;
                            $model_rendicion->save(true);
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('rendicion/ver', 'id' => $rendicion_id));
			}
		}

		$this->render('crear', array( 'model' => $model_item));
	}

        public function actionCrear($proyecto_id=null) {
		$model = new ItemRendicion('sinRendicion');
                if($proyecto_id!==null) $model->proyecto_id=$proyecto_id;
                $this->performAjaxValidation($model, 'item-rendicion-form');
                
                
		if(isset($_POST['ItemRendicion'])) {
			$model->setAttributes($_POST['ItemRendicion']);
                        if($model->tipo_solicitud=="Transferencia"):
                            $model->numero_cheque="Transferencia";
                            $model->tipo_cheque="Transferencia";                            
                        endif;
                        
                                                
                        if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('itemRendicion/ver', 'id' => $model->id));
			}
		}

		$this->render('crearSinRendicion', array( 'model' => $model));
	}
        
	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'ItemRendicion'),
		));
	}
        
        public function actionVer($id) {
		$this->render('ver', array(
			'model' => $this->loadModel($id, 'ItemRendicion'),
		));
	}

	public function actionCreate() {
		$model = new ItemRendicion;

		$this->performAjaxValidation($model, 'item-rendicion-form');

		if (isset($_POST['ItemRendicion'])) {
			$model->setAttributes($_POST['ItemRendicion']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'ItemRendicion');

		$this->performAjaxValidation($model, 'item-rendicion-form');

		if (isset($_POST['ItemRendicion'])) {
			$model->setAttributes($_POST['ItemRendicion']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}
        
        public function actionActualizar($id) {
		$model = $this->loadModel($id, 'ItemRendicion');
                $model_rendicion = $this->loadModel($model->rendicion_id, 'Rendicion');
                $monto_anterior = $model->monto;
		$this->performAjaxValidation($model, 'item-rendicion-form');
                
		if (isset($_POST['ItemRendicion'])) {
			$model->setAttributes($_POST['ItemRendicion']);
                        
                        $total = $model_rendicion->total_salida - $monto_anterior;
                        $model_rendicion->total_salida = $total + $model->monto;
                        $model_rendicion->total_saldo = $model_rendicion->total_entrada - $model_rendicion->total_salida;
                        $model_rendicion->save(true);

			if ($model->save()) {
				$this->redirect(array('rendicion/ver', 'id' => $model->rendicion_id));
                        }
		}

		$this->render('actualizar', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'ItemRendicion')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('ItemRendicion');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
                $session = new CHttpSession;
                $session->open();
		$model = new ItemRendicion('search');
		$model->unsetAttributes();

		if (isset($_GET['ItemRendicion'])){
			$model->setAttributes($_GET['ItemRendicion']);
                }

                $session['ItemRendicion_model_search'] = $model;
                
		$this->render('admin', array(
			'model' => $model,
		));
	}
        
        public function actionVerTodos() {
                $session = new CHttpSession;
                $session->open();
		$model = new ItemRendicion('search');
		$model->unsetAttributes();

		if (isset($_GET['ItemRendicion'])){
			$model->setAttributes($_GET['ItemRendicion']);
                }

                $session['ItemRendicion_model_search'] = $model;
                
		$this->render('verTodos', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
        public function actionGenerateExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['ItemRendicion_model_search']))
               {
                $model = $session['ItemRendicion_model_search'];
                $model = ItemRendicion::model()->findAll($model->search()->criteria);
               }
               else
                 $model = ItemRendicion::model()->findAll();
             
             $this->toExcel($model, array('fecha','tipo_documento','numero_documento','proveedor','tipo_solicitud','cuenta','numero_cheque','monto','proyecto','cuentaContable','cuentaEspecifica'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGeneratePdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['ItemRendicion_model_search']))
               {
                $model = $session['ItemRendicion_model_search'];
                $model = ItemRendicion::model()->findAll($model->search()->criteria);
               }
               else
                 $model = ItemRendicion::model()->findAll();
             
             $this->toExcel($model, array('id', 'proyecto', 'proveedor', 'cuentaContable', 'cuentaEspecifica', 'rendicion', 'proyecto', 'tipo_documento', 'numero_documento', 'fecha', 'monto'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}
}
