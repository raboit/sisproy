<?php

class PresupuestoController extends GxController {

        public function filters() {
                return array('rights');
        }

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Presupuesto'),
		));
	}
        
        public function actionVer($id) {
            $model_presupuesto = $this->loadModel($id, 'Presupuesto');
            $model_items = new ItemPresupuesto;
            $model_items = $model_items->misItems($id);
		$this->render('ver', array(
			'model' => $model_presupuesto,
                        'model_items' => $model_items,
		));
	}
        
        public function actionVerProyecto($id) {
            $model_presupuesto = $this->loadModel($id, 'Presupuesto');
            $model_items = new ItemPresupuesto;
            $model_items = $model_items->misItems($id);
		$this->render('verProyecto', array(
			'model' => $model_presupuesto,
                        'model_items' => $model_items,
		));
	}

	public function actionCreate() {
		$model = new Presupuesto;

		$this->performAjaxValidation($model, 'presupuesto-form');

		if (isset($_POST['Presupuesto'])) {
			$model->setAttributes($_POST['Presupuesto']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}
        
        public function actionCrear($proyecto_id) {
		$model = new Presupuesto;
                $model->proyecto_id = $proyecto_id;

		$this->performAjaxValidation($model, 'presupuesto-form');

		if (isset($_POST['Presupuesto'])) {
			$model->setAttributes($_POST['Presupuesto']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('proyecto/verPresupuesto', 'id' => $proyecto_id));
			}
		}

		$this->render('crear', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Presupuesto');

		$this->performAjaxValidation($model, 'presupuesto-form');

		if (isset($_POST['Presupuesto'])) {
			$model->setAttributes($_POST['Presupuesto']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}
        
        public function actionActualizar($id) {
		$model = $this->loadModel($id, 'Presupuesto');

		$this->performAjaxValidation($model, 'presupuesto-form');

		if (isset($_POST['Presupuesto'])) {
			$model->setAttributes($_POST['Presupuesto']);

			if ($model->save()) {
				$this->redirect(array('proyecto/verPresupuesto', 'id' => $model->proyecto_id));
			}
		}

		$this->render('actualizar', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Presupuesto')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Presupuesto');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
                $session = new CHttpSession;
                $session->open();
		$model = new Presupuesto('search');
		$model->unsetAttributes();

		if (isset($_GET['Presupuesto'])){
			$model->setAttributes($_GET['Presupuesto']);
                }

                $session['Presupuesto_model_search'] = $model;
                
		$this->render('admin', array(
			'model' => $model,
		));
	}
        
        public function actionVerTodos() {
                $session = new CHttpSession;
                $session->open();
		$model = new Presupuesto('search');
		$model->unsetAttributes();

		if (isset($_GET['Presupuesto'])){
			$model->setAttributes($_GET['Presupuesto']);
                }

                $session['Presupuesto_model_search'] = $model;
                
		$this->render('verTodos', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
        public function actionGenerateExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Presupuesto_model_search']))
               {
                $model = $session['Presupuesto_model_search'];
                $model = Presupuesto::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Presupuesto::model()->findAll();
             
             $this->toExcel($model, array('id', 'proyecto', 'tipoIngreso', 'fecha', 'monto', 'observaciones'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGeneratePdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Presupuesto_model_search']))
               {
                $model = $session['Presupuesto_model_search'];
                $model = Presupuesto::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Presupuesto::model()->findAll();
             
             $this->toExcel($model, array('id', 'proyecto', 'tipoIngreso', 'fecha', 'monto', 'observaciones'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}
}