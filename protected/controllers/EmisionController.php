<?php

class EmisionController extends GxController {

        public function filters() {
                return array('rights');
        }

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Emision'),
		));
	}
        
        public function actionVerEmisionRendicion($emision_id){
		$this->render('verEmisionRendicion', array(
			'model' => $this->loadModel($emision_id, 'Emision'),
		));
	}
        
        public function __call($name, $parameters) {
            parent::__call($name, $parameters);
        }

        public function actionVer($id) {
                $model_emision = $this->loadModel($id, 'Emision');
		$this->render('ver', array(
			'model' => $model_emision,
		));
	}
        
	public function actionCreate() {
		$model = new Emision;

		$this->performAjaxValidation($model, 'emision-form');

		if (isset($_POST['Emision'])) {
			$model->setAttributes($_POST['Emision']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

         public function actionFinalizar($id) {
                $model_emision = $this->loadModel($id, 'Emision');
                $model_emision->estado='FINALIZADA';
                $model_emision->save(false);
                Yii::app()->user->setFlash('info', '<strong>¡Listo!</strong>, La emision #'.$id.' ha sido finalizada');
                $this->redirect(array('ver', 'id' => $model_emision->id));
	}
        public function actionAnular($id) {
                $model_emision = $this->loadModel($id, 'Emision');
                $model_solicitud = $this->loadModel($model_emision->solicitud_id, 'Solicitud');
                $model_emision->estado='ANULADA';
                $model_solicitud->estado='FINALIZADA';
                $model_emision->save(false);
                $model_solicitud->save(false);
                Yii::app()->user->setFlash('error', '<strong>¡Listo!</strong>, La emision #'.$id.' ha sido anulada');
                $this->redirect(array('ver', 'id' => $model_emision->id));
	}
        
        public function actionCrear($solicitud_id) {
                
		$model = new Emision;
                $model_solicitud = $this->loadModel($solicitud_id, 'Solicitud');
                $model->solicitud_id = $model_solicitud->id;
                $model_solicitud->estado = 'EMITIDA';
                $model->proyecto_id = $model_solicitud->proyecto_id; 
                $model->total_debe = $model_solicitud->total;
                #$model->tipo_solicitud = $model_solicitud->tipo_solicitud;
                $model->user_id = Yii::app()->user->id;
                
		$this->performAjaxValidation($model, 'emision-form');

		if (isset($_POST['Emision'])){
			$model->setAttributes($_POST['Emision']);
                        
                        if($model->tipo_solicitud=="Transferencia"):
                            $model->numero_cheque="Transferencia";
                            $model->tipo_cheque="Transferencia";                            
                        endif;
                        
                        $model->estado='PENDIENTE';
			if ($model->save() && $model_solicitud->save(false)) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('crear', array( 'model' => $model));
	}
        
	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Emision');

		$this->performAjaxValidation($model, 'emision-form');

		if (isset($_POST['Emision'])) {
			$model->setAttributes($_POST['Emision']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}
        
        public function actionActualizar($id) {
		$model = $this->loadModel($id, 'Emision');

		$this->performAjaxValidation($model, 'emision-form');

		if (isset($_POST['Emision'])) {
			$model->setAttributes($_POST['Emision']);

			if ($model->save()) {
				$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('actualizar', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Emision')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}
        
        public function actionEliminar($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Emision')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('verTodos'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Emision');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
                $session = new CHttpSession;
                $session->open();
		$model = new Emision('search');
		$model->unsetAttributes();

		if (isset($_GET['Emision'])){
			$model->setAttributes($_GET['Emision']);
                }

                $session['Emision_model_search'] = $model;
                
		$this->render('admin', array(
			'model' => $model,
		));
	}
        
        public function actionVerTodos() {
                $session = new CHttpSession;
                $session->open();
		$model = new Emision('search');
		$model->unsetAttributes();

		if (isset($_GET['Emision'])){
			$model->setAttributes($_GET['Emision']);
                }

                $session['Emision_model_search'] = $model;
                
		$this->render('verTodos', array(
			'model' => $model,
		));
	}
        
        public function actionFinalizadas() {
                $session = new CHttpSession;
                $session->open();
		$model = new Emision('search');
		$model->unsetAttributes();

		if (isset($_GET['Emision'])){
			$model->setAttributes($_GET['Emision']);
                }

                $session['Emision_model_search'] = $model;
                
		$this->render('finalizadas', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
        public function actionGenerateExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Emision_model_search']))
               {
                $model = $session['Emision_model_search'];
                $model = Emision::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Emision::model()->findAll();
             
             $this->toExcel($model, array('id', 'user', 'proveedor', 'proyecto', 'solicitud', 'ciudad', 'fecha', 'concepto', 'total_debe', 'total_haber', 'cuenta', 'tipo_solicitud', 'tipo_cheque', 'numero_cheque', 'observaciones', 'estado'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGeneratePdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Emision_model_search']))
               {
                $model = $session['Emision_model_search'];
                $model = Emision::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Emision::model()->findAll();
             
             $this->toExcel($model, array('id', 'user', 'proveedor', 'proyecto', 'solicitud', 'ciudad', 'fecha', 'concepto', 'total_debe', 'total_haber', 'cuenta', 'tipo_solicitud', 'tipo_cheque', 'numero_cheque', 'observaciones', 'estado'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}
}
