<?php

class ContratoController extends GxController {

        public function filters() {
                return array('rights');
        }

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Contrato'),
		));
	}
        
        public function actionVer($id) {
		$this->render('ver', array(
			'model' => $this->loadModel($id, 'Contrato'),
		));
	}
        
        public function actionVerRrhh($contrato_id) {
		$this->render('verRrhh', array(
			'model' => $this->loadModel($contrato_id, 'Contrato'),
		));
	}
        
        public function actionFinalizar($id){
            $model=$this->loadModel($id, 'Contrato');
            $model->estado='FINIQUITADO';
            $model->save();
            $this->redirect(array('ver', 'id' => $id));
            
            
        }

	public function actionCreate() {
		$model = new Contrato;

		$this->performAjaxValidation($model, 'contrato-form');

		if (isset($_POST['Contrato'])) {
			$model->setAttributes($_POST['Contrato']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}
        
        public function actionCrear() {
		$model = new Contrato;

		$this->performAjaxValidation($model, 'contrato-form');

		if (isset($_POST['Contrato'])) {
			$model->setAttributes($_POST['Contrato']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('crear', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Contrato');

		$this->performAjaxValidation($model, 'contrato-form');

		if (isset($_POST['Contrato'])) {
			$model->setAttributes($_POST['Contrato']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}
        
        public function actionActualizar($id) {
		$model = $this->loadModel($id, 'Contrato');

		$this->performAjaxValidation($model, 'contrato-form');

		if (isset($_POST['Contrato'])) {
			$model->setAttributes($_POST['Contrato']);

			if ($model->save()) {
				$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('actualizar', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Contrato')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Contrato');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
                $session = new CHttpSession;
                $session->open();
		$model = new Contrato('search');
		$model->unsetAttributes();

		if (isset($_GET['Contrato'])){
			$model->setAttributes($_GET['Contrato']);
                }

                $session['Contrato_model_search'] = $model;
                
		$this->render('admin', array(
			'model' => $model,
		));
	}
        
        public function actionVerTodos() {
                $session = new CHttpSession;
                $session->open();
		$model = new Contrato('search');
		$model->unsetAttributes();

		if (isset($_GET['Contrato'])){
			$model->setAttributes($_GET['Contrato']);
                }

                $session['Contrato_model_search'] = $model;
                
		$this->render('verTodos', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
        public function actionGenerateExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Contrato_model_search']))
               {
                $model = $session['Contrato_model_search'];
                $model = Contrato::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Contrato::model()->findAll();
//             if(empty($model)) throw new CHttpException(400, Yii::t('app', 'No hay datos a exportar.'));
             $this->toExcel($model, array('id', 'rrhh', 'proyecto', 'tipoContrato', 'fecha_inicio', 'fecha_termino', 'remuneracion', 'estado'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGeneratePdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Contrato_model_search']))
               {
                $model = $session['Contrato_model_search'];
                $model = Contrato::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Contrato::model()->findAll();
             
             $this->toExcel($model, array('id', 'rrhh', 'proyecto', 'tipoContrato', 'fecha_inicio', 'fecha_termino', 'remuneracion', 'estado'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}
}