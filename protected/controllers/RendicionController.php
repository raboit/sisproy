<?php

class RendicionController extends GxController {

        public function filters() {
                return array('rights');
        }

        
        public function actionVer($id){
            $model_rendicion = $this->loadModel($id, 'Rendicion');
            $model_items = new ItemRendicion;
            $model_items = $model_items->misItems($id);
            
            $this->render('ver', array(
			'model' => $model_rendicion,
                        'model_items' => $model_items,
		));
        }
        
         public function actionFinalizar($id) {
             
                $model_rendicion = $this->loadModel($id, 'Rendicion');
               
                if($model_rendicion->total_saldo!=0){
                    Yii::app()->user->setFlash('error', '<strong>¡Espera!</strong>, La rendicion #'.$id.' tiene saldo pendiente por rendir.');
                }else{
                    $model_rendicion->estado='FINALIZADA';
                    $model_rendicion->save(false);
                    Yii::app()->user->setFlash('info', '<strong>¡Listo!</strong>, La rendicion #'.$id.' ha sido finalizada');                    
                }
                $this->redirect(array('ver', 'id' => $model_rendicion->id));
                
        }
        
        public function actionAnular($id) {
                $model_rendicion = $this->loadModel($id, 'Rendicion');
                $model_emision = $this->loadModel($model_rendicion->emision_id, 'Emision');
                $model_rendicion->estado='ANULADA';
                $model_emision->estado='FINALIZADA';
                $model_rendicion->save(false);
                $model_emision->save(false);
                
                $model_items = new ItemRendicion;
                $model_items->deleteAllByAttributes(array(),'rendicion_id = :id_rendicion',array(
                    ':id_rendicion'=>$id,
                ));                
                
                Yii::app()->user->setFlash('error', '<strong>¡Listo!</strong>, La rendicion #'.$id.' ha sido anulada');
                $this->redirect(array('ver', 'id' => $model_rendicion->id));
	}
        
        public function actionCrear($emision_id){
            $model_rendicion = new Rendicion;
            $model_emision = $this->loadModel($emision_id, 'Emision');
            $model_emision->estado = 'RENDIDA'; 
            $model_rendicion->emision_id=$emision_id;
            $model_rendicion->tipo_solicitud = $model_emision->tipo_solicitud;
            $model_rendicion->numero_cheque = $model_emision->numero_cheque;
            $model_rendicion->cuenta_id = $model_emision->cuenta_id;
            $model_rendicion->total_entrada = $model_emision->total_debe;
            $model_rendicion->total_saldo = $model_emision->total_debe;
            
            $model_rendicion->user_id = Yii::app()->user->id;
            $model_rendicion->estado = "ACTIVA";
            
            
		$this->performAjaxValidation($model_rendicion, 'rendicion-form');

		if (isset($_POST['Rendicion'])) {
			$model_rendicion->setAttributes($_POST['Rendicion']);

			if ($model_rendicion->save()) {
                            $model_emision->save(false);
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('ver', 'id' => $model_rendicion->id));
			}
		}

		$this->render('crear', array( 'model' => $model_rendicion));
            
        }
	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Rendicion'),
		));
	}

	public function actionCreate() {
		$model = new Rendicion;

		$this->performAjaxValidation($model, 'rendicion-form');

		if (isset($_POST['Rendicion'])) {
			$model->setAttributes($_POST['Rendicion']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Rendicion');

		$this->performAjaxValidation($model, 'rendicion-form');

		if (isset($_POST['Rendicion'])) {
			$model->setAttributes($_POST['Rendicion']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}
        
        public function actionActualizar($id) {
		$model = $this->loadModel($id, 'Rendicion');

		$this->performAjaxValidation($model, 'rendicion-form');

		if (isset($_POST['Rendicion'])) {
			$model->setAttributes($_POST['Rendicion']);

			if ($model->save()) {
				$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('actualizar', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Rendicion')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
//		$dataProvider = new CActiveDataProvider('Rendicion');
//		$this->render('index', array(
//			'dataProvider' => $dataProvider,
//		));
            $this->redirect('verTodos');
	}

	public function actionAdmin() {
                $session = new CHttpSession;
                $session->open();
		$model = new Rendicion('search');
		$model->unsetAttributes();

		if (isset($_GET['Rendicion'])){
			$model->setAttributes($_GET['Rendicion']);
                }

                $session['Rendicion_model_search'] = $model;
                
		$this->render('admin', array(
			'model' => $model,
		));
	}
        
        public function actionVerTodos() {
                $session = new CHttpSession;
                $session->open();
		$model = new Rendicion('search');
		$model->unsetAttributes();

		if (isset($_GET['Rendicion'])){
			$model->setAttributes($_GET['Rendicion']);
                }

                $session['Rendicion_model_search'] = $model;
                
		$this->render('verTodos', array(
			'model' => $model,
		));
	}
        
        
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
        public function actionGenerateExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Rendicion_model_search']))
               {
                $model = $session['Rendicion_model_search'];
                $model = Rendicion::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Rendicion::model()->findAll();
             
             $this->toExcel($model, array('id', 'user', 'emision', 'asignacion', 'fecha', 'comuna', 'region', 'total_entrada', 'total_salida', 'total_saldo', 'observacion', 'estado'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGeneratePdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Rendicion_model_search']))
               {
                $model = $session['Rendicion_model_search'];
                $model = Rendicion::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Rendicion::model()->findAll();
             
             $this->toExcel($model, array('id', 'user', 'emision', 'asignacion', 'fecha', 'comuna', 'region', 'tipo_pago', 'total_entrada', 'total_salida', 'total_saldo', 'observacion', 'estado'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}
}
