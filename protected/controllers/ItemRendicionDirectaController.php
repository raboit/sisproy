<?php

class ItemRendicionDirectaController extends GxController {

        public function filters() {
                return array('rights');
        }

        
        public function actionGetCuenta(){
            
            $data= CuentaEspecifica::model()->findAllByAttributes(array(),"cuenta_contable_id=:cuenta_contable_id",array(':cuenta_contable_id'=>$_POST['ItemRendicionDirecta']['cuenta_contable_id']));
            $data = CHtml::listData($data,'id','nombre');
             foreach($data as $value=>$name)
                {
                    echo CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);                 
                }

        }
	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'ItemRendicionDirecta'),
		));
	}
        
        public function actionVer($id) {
		$this->render('ver', array(
			'model' => $this->loadModel($id, 'ItemRendicionDirecta'),
		));
	}

	public function actionCreate() {
		$model = new ItemRendicionDirecta;

		$this->performAjaxValidation($model, 'item-rendicion-directa-form');

		if (isset($_POST['ItemRendicionDirecta'])) {
			$model->setAttributes($_POST['ItemRendicionDirecta']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}
        
        public function actionCrear($id) {
		$model = new ItemRendicionDirecta;
                
		$this->performAjaxValidation($model, 'item-rendicion-directa-form');
                $model->rendicion_directa_id=$id;
                
		if (isset($_POST['ItemRendicionDirecta'])) {
			$model->setAttributes($_POST['ItemRendicionDirecta']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('rendicionDirecta/ver', 'id' => $model->rendicion_directa_id));
			}
		}

		$this->render('crear', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'ItemRendicionDirecta');

		$this->performAjaxValidation($model, 'item-rendicion-directa-form');

		if (isset($_POST['ItemRendicionDirecta'])) {
			$model->setAttributes($_POST['ItemRendicionDirecta']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}
        
        public function actionActualizar($id) {
		$model = $this->loadModel($id, 'ItemRendicionDirecta');

		$this->performAjaxValidation($model, 'item-rendicion-directa-form');

		if (isset($_POST['ItemRendicionDirecta'])) {
			$model->setAttributes($_POST['ItemRendicionDirecta']);

			if ($model->save()) {
				$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('actualizar', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'ItemRendicionDirecta')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('ItemRendicionDirecta');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
                $session = new CHttpSession;
                $session->open();
		$model = new ItemRendicionDirecta('search');
		$model->unsetAttributes();

		if (isset($_GET['ItemRendicionDirecta'])){
			$model->setAttributes($_GET['ItemRendicionDirecta']);
                }

                $session['ItemRendicionDirecta_model_search'] = $model;
                
		$this->render('admin', array(
			'model' => $model,
		));
	}
        
        public function actionVerTodos() {
                $session = new CHttpSession;
                $session->open();
		$model = new ItemRendicionDirecta('search');
		$model->unsetAttributes();

		if (isset($_GET['ItemRendicionDirecta'])){
			$model->setAttributes($_GET['ItemRendicionDirecta']);
                }

                $session['ItemRendicionDirecta_model_search'] = $model;
                
		$this->render('verTodos', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
        public function actionGenerateExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['ItemRendicionDirecta_model_search']))
               {
                $model = $session['ItemRendicionDirecta_model_search'];
                $model = ItemRendicionDirecta::model()->findAll($model->search()->criteria);
               }
               else
                 $model = ItemRendicionDirecta::model()->findAll();
             
             $this->toExcel($model, array('id', 'rendicionDirecta', 'proveedor', 'cuentaContable', 'cuentaEspecifica', 'proyecto', 'tipo_documento', 'numero_documento', 'fecha', 'mes', 'agno', 'monto'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGeneratePdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['ItemRendicionDirecta_model_search']))
               {
                $model = $session['ItemRendicionDirecta_model_search'];
                $model = ItemRendicionDirecta::model()->findAll($model->search()->criteria);
               }
               else
                 $model = ItemRendicionDirecta::model()->findAll();
             
             $this->toExcel($model, array('id', 'rendicionDirecta', 'proveedor', 'cuentaContable', 'cuentaEspecifica', 'proyecto', 'tipo_documento', 'numero_documento', 'fecha', 'mes', 'agno', 'monto'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}
}