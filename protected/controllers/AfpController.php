<?php

class AfpController extends GxController {

        public function filters() {
                return array('rights');
        }

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Afp'),
		));
	}
        
        public function actionVer($id) {
		$this->render('ver', array(
			'model' => $this->loadModel($id, 'Afp'),
		));
	}

	public function actionCreate() {
		$model = new Afp;

		$this->performAjaxValidation($model, 'afp-form');

		if (isset($_POST['Afp'])) {
			$model->setAttributes($_POST['Afp']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}
        
        public function actionCrear() {
		$model = new Afp;

		$this->performAjaxValidation($model, 'afp-form');

		if (isset($_POST['Afp'])) {
			$model->setAttributes($_POST['Afp']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('crear', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Afp');

		$this->performAjaxValidation($model, 'afp-form');

		if (isset($_POST['Afp'])) {
			$model->setAttributes($_POST['Afp']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

        public function actionActualizar($id) {
		$model = $this->loadModel($id, 'Afp');

		$this->performAjaxValidation($model, 'afp-form');

		if (isset($_POST['Afp'])) {
			$model->setAttributes($_POST['Afp']);

			if ($model->save()) {
				$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('actualizar', array(
				'model' => $model,
				));
	}
        
	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Afp')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Afp');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
                $session = new CHttpSession;
                $session->open();
		$model = new Afp('search');
		$model->unsetAttributes();

		if (isset($_GET['Afp'])){
			$model->setAttributes($_GET['Afp']);
                }

                $session['Afp_model_search'] = $model;
                
		$this->render('admin', array(
			'model' => $model,
		));
	}
        
        public function actionVerTodos() {
                $session = new CHttpSession;
                $session->open();
		$model = new Afp('search');
		$model->unsetAttributes();

		if (isset($_GET['Afp'])){
			$model->setAttributes($_GET['Afp']);
                }

                $session['Afp_model_search'] = $model;
                
		$this->render('verTodos', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
        public function actionGenerateExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Afp_model_search']))
               {
                $model = $session['Afp_model_search'];
                $model = Afp::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Afp::model()->findAll();
             
             $this->toExcel($model, array('id', 'nombre'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGeneratePdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Afp_model_search']))
               {
                $model = $session['Afp_model_search'];
                $model = Afp::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Afp::model()->findAll();
             
             $this->toExcel($model, array('id', 'nombre'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}
}