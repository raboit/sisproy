<?php

class RendicionDirectaController extends GxController {

        public function filters() {
                return array('rights');
        }
        
        public function actionFinalizar($id) {
             
                $model_rendicion_directa = $this->loadModel($id, 'RendicionDirecta');
                $model_rendicion_directa->estado='FINALIZADA';
                $model_rendicion_directa->save(false);
                    Yii::app()->user->setFlash('info', '<strong>¡Listo!</strong>, La rendicion #'.$id.' ha sido finalizada');                    
                
                $this->redirect(array('ver', 'id' => $model_rendicion_directa->id));
        }
        
        public function actionAnular($id) {
                $model_rendicion_directa = $this->loadModel($id, 'RendicionDirecta');
                $model_rendicion_directa->estado='ANULADA';
                $model_rendicion_directa->save(false);
                
                $model_items = ItemRendicionDirecta::model()->deleteAllByAttributes(array(),'rendicion_directa_id = :id_rendicion_directa',array(
                    ':id_rendicion_directa'=>$id,
                ));                
                
                Yii::app()->user->setFlash('error', '<strong>¡Listo!</strong>, La rendicion #'.$id.' ha sido anulada');
                $this->redirect(array('ver', 'id' => $model_rendicion_directa->id));
	}

        public function actionVer($id) {
                $model_rendicion=$this->loadModel($id, 'RendicionDirecta');
                $model_items= new ItemRendicionDirecta;
                $model_items=$model_items->misItems($id);
                
		$this->render('ver', array(
			'model' => $model_rendicion,
                        'model_items'=>$model_items,
		));
	}

	public function actionCreate() {
		$model = new RendicionDirecta;

		$this->performAjaxValidation($model, 'rendicion-directa-form');

		if (isset($_POST['RendicionDirecta'])) {
			$model->setAttributes($_POST['RendicionDirecta']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}
        
        public function actionCrear() {
		$model = new RendicionDirecta;

		$this->performAjaxValidation($model, 'rendicion-directa-form');

		if (isset($_POST['RendicionDirecta'])) {
			$model->setAttributes($_POST['RendicionDirecta']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('crear', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'RendicionDirecta');

		$this->performAjaxValidation($model, 'rendicion-directa-form');

		if (isset($_POST['RendicionDirecta'])) {
			$model->setAttributes($_POST['RendicionDirecta']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}
        
        public function actionActualizar($id) {
		$model = $this->loadModel($id, 'RendicionDirecta');

		$this->performAjaxValidation($model, 'rendicion-directa-form');

		if (isset($_POST['RendicionDirecta'])) {
			$model->setAttributes($_POST['RendicionDirecta']);

			if ($model->save()) {
				$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('actualizar', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'RendicionDirecta')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('RendicionDirecta');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
                $session = new CHttpSession;
                $session->open();
		$model = new RendicionDirecta('search');
		$model->unsetAttributes();

		if (isset($_GET['RendicionDirecta'])){
			$model->setAttributes($_GET['RendicionDirecta']);
                }

                $session['RendicionDirecta_model_search'] = $model;
                
		$this->render('admin', array(
			'model' => $model,
		));
	}
        
        public function actionVerTodos() {
                $session = new CHttpSession;
                $session->open();
		$model = new RendicionDirecta('search');
		$model->unsetAttributes();

		if (isset($_GET['RendicionDirecta'])){
			$model->setAttributes($_GET['RendicionDirecta']);
                }

                $session['RendicionDirecta_model_search'] = $model;
                
		$this->render('verTodos', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
        public function actionGenerateExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['RendicionDirecta_model_search']))
               {
                $model = $session['RendicionDirecta_model_search'];
                $model = RendicionDirecta::model()->findAll($model->search()->criteria);
               }
               else
                 $model = RendicionDirecta::model()->findAll();
             
             $this->toExcel($model, array('id', 'cuenta', 'tipo_solicitud', 'tipo_cheque', 'numero_cheque'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGeneratePdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['RendicionDirecta_model_search']))
               {
                $model = $session['RendicionDirecta_model_search'];
                $model = RendicionDirecta::model()->findAll($model->search()->criteria);
               }
               else
                 $model = RendicionDirecta::model()->findAll();
             
             $this->toExcel($model, array('id', 'cuenta', 'tipo_solicitud', 'tipo_cheque', 'numero_cheque'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}
}