<?php

class ItemPresupuestoController extends GxController {

        public function filters() {
                return array('rights');
        }

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'ItemPresupuesto'),
		));
	}
        

        public function actionVer($id) {
                $model = ItemPresupuesto::model()->with(array(
                    'cuentaContable.itemRendicions'=>
                    array('on'=>'MONTH(itemRendicions.fecha) = MONTH(t.fecha) 
                        and YEAR(itemRendicions.fecha) = YEAR(t.fecha) 
                        and itemRendicions.proyecto_id = t.proyecto_id')
                        ))->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, Yii::t('giix', 'The requested page does not exist.'));
		$this->render('itemPresupuestoVsItemRendicion', array(
			'model' => $model
		));
	}        
        
//        public function actionItemPresupuestoVsItemRendicion($id){
//                //$model = $this->loadModel($id, 'ItemPresupuesto');
//                $model = ItemPresupuesto::model()->with(array(
//                    'cuentaContable.itemRendicions'=>
//                    //array('joinType'=>'INNER JOIN','condition'=>'itemRendicions.fecha = t.fecha and itemRendicions.proyecto_id = t.proyecto_id'))
//                    array('on'=>'itemRendicions.fecha = t.fecha and itemRendicions.proyecto_id = t.proyecto_id'))
//                        )->findByPk($id);
//		if ($model === null)
//			throw new CHttpException(404, Yii::t('giix', 'The requested page does not exist.'));
//		$this->render('itemPresupuestoVsItemRendicion', array(
//			'model' => $model
//		));
////                $criteriaItemRendicion=new CDbCriteria;
////                $criteriaItemRendicion->condition='fecha=:fecha and cuenta_contable_id=:cuenta_contable_id and proyecto_id=:proyecto_id';
////                $criteriaItemRendicion->params=array(':fecha'=>date('Y-m-d', CDateTimeParser::parse($model->fecha, Yii::app()->locale->getDateFormat('medium'))),
////                    ':cuenta_contable_id'=>$model->cuenta_contable_id,
////                    ':proyecto_id'=>$model->presupuesto->proyecto_id);
////                $model_itemRendicion = ItemRendicion::model()->findAll($criteriaItemRendicion);
////                        
////		$this->render('itemPresupuestoVsItemRendicion', array(
////			'model' => $model, 'model_itemRendicion' => $model_itemRendicion
////		));      
//                
////                $model_itemRendicion = new ItemRendicion('search');
////                $model_itemRendicion->cuenta_contable_id = $model->cuenta_contable_id;
////                $model_itemRendicion->proyecto_id = $model->presupuesto->proyecto_id;
////                $model_itemRendicion->fecha = date('Y-m-d', CDateTimeParser::parse($model->fecha, Yii::app()->locale->getDateFormat('medium')));
//        
////                $model_itemRendicion = new ItemRendicion();
////		$this->render('itemPresupuestoVsItemRendicion', array(
////			'model' => $model, 'model_itemRendicion' => $model_itemRendicion->misItems(null, $model->cuenta_contable_id, $model->presupuesto->proyecto_id, $model->fecha)
////		));
//        }
        
        public function actionVerProyecto($id) {
		$this->render('verProyecto', array(
			'model' => $this->loadModel($id, 'ItemPresupuesto'),
		));
	}

	public function actionCrear($presupuesto_id) {
		$model = new ItemPresupuesto;
                $model_presupuesto = $this->loadModel($presupuesto_id, 'Presupuesto');
                $model->presupuesto_id = $presupuesto_id;
                $model->proyecto_id = $model_presupuesto->proyecto_id;

		$this->performAjaxValidation($model, 'item-presupuesto-form');

		if (isset($_POST['ItemPresupuesto'])) {
			$model->setAttributes($_POST['ItemPresupuesto']);
                        $model_presupuesto->monto = $model_presupuesto->monto + $model->monto;
                        $model_presupuesto->save(false);
			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('presupuesto/ver', 'id' => $presupuesto_id));
			}
		}

		$this->render('crear', array( 'model' => $model));
	}
        
        public function actionCrearProyecto($presupuesto_id) {
		$model = new ItemPresupuesto;
                $model->presupuesto_id = $presupuesto_id;

		$this->performAjaxValidation($model, 'item-presupuesto-form');

		if (isset($_POST['ItemPresupuesto'])) {
			$model->setAttributes($_POST['ItemPresupuesto']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('presupuesto/verProyecto', 'id' => $presupuesto_id));
			}
		}

		$this->render('crearProyecto', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'ItemPresupuesto');

		$this->performAjaxValidation($model, 'item-presupuesto-form');

		if (isset($_POST['ItemPresupuesto'])) {
			$model->setAttributes($_POST['ItemPresupuesto']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}
        
        public function actionActualizar($id) {
		$model = $this->loadModel($id, 'ItemPresupuesto');
                $model_presupuesto = $this->loadModel($model->presupuesto_id, 'Presupuesto');
                $model_presupuesto->monto = $model_presupuesto->monto - $model->monto;

		$this->performAjaxValidation($model, 'item-presupuesto-form');

		if (isset($_POST['ItemPresupuesto'])) {
			$model->setAttributes($_POST['ItemPresupuesto']);
                        $model_presupuesto->monto = $model_presupuesto->monto + $model->monto;
                        $model_presupuesto->save(false);

			if ($model->save()) {
				$this->redirect(array('presupuesto/ver', 'id' => $model->presupuesto_id));
			}
		}

		$this->render('actualizar', array(
				'model' => $model,
				));
	}
        
        public function actionActualizarProyecto($id) {
		$model = $this->loadModel($id, 'ItemPresupuesto');

		$this->performAjaxValidation($model, 'item-presupuesto-form');

		if (isset($_POST['ItemPresupuesto'])) {
			$model->setAttributes($_POST['ItemPresupuesto']);

			if ($model->save()) {
				$this->redirect(array('presupuesto/verProyecto', 'id' => $model->presupuesto_id));
			}
		}

		$this->render('actualizarProyecto', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'ItemPresupuesto')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('ItemPresupuesto');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
                $session = new CHttpSession;
                $session->open();
		$model = new ItemPresupuesto('search');
		$model->unsetAttributes();

		if (isset($_GET['ItemPresupuesto'])){
			$model->setAttributes($_GET['ItemPresupuesto']);
                }

                $session['ItemPresupuesto_model_search'] = $model;
                
		$this->render('admin', array(
			'model' => $model,
		));
	}
        
        public function actionVerTodos() {
                $session = new CHttpSession;
                $session->open();
		$model = new ItemPresupuesto('search');
		$model->unsetAttributes();

		if (isset($_GET['ItemPresupuesto'])){
			$model->setAttributes($_GET['ItemPresupuesto']);
                }

                $session['ItemPresupuesto_model_search'] = $model;
                
		$this->render('verTodos', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
        public function actionGenerateExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['ItemPresupuesto_model_search']))
               {
                $model = $session['ItemPresupuesto_model_search'];
                $model = ItemPresupuesto::model()->findAll($model->search()->criteria);
               }
               else
                 $model = ItemPresupuesto::model()->findAll();
             
             $this->toExcel($model, array('id', 'presupuesto', 'cuentaContable', 'cuentaEspecifica', 'monto'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGeneratePdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['ItemPresupuesto_model_search']))
               {
                $model = $session['ItemPresupuesto_model_search'];
                $model = ItemPresupuesto::model()->findAll($model->search()->criteria);
               }
               else
                 $model = ItemPresupuesto::model()->findAll();
             
             $this->toExcel($model, array('id', 'presupuesto', 'cuentaContable', 'cuentaEspecifica', 'monto'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}
}