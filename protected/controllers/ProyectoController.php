<?php

class ProyectoController extends GxController {

        public function filters() {
                return array('rights');
        }

        public function actionGetComuna(){
            $data=Comuna::model()->findAllByAttributes(
                                array(), 
                                "region_id=:region_id", 
                                array(':region_id'=>$_POST["Proyecto"]["region"]));
            $data=CHtml::listData($data,'id','nombre');
             foreach($data as $value=>$name)
                {
                    echo CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
                 
                }
             
 

        }
	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Proyecto'),
		));
	}
        
        public function actionVerPresupuesto($id) {
//            $model_proyecto = $this->loadModel($id, 'Proyecto');
//            $model_presupuesto = new Presupuesto;
//            $model_presupuesto= $model_presupuesto->misPresupuestos($id);
//		$this->render('verPresupuesto', array(
//			'model_proyecto' => $model_proyecto,
//                        'model_presupuesto'=>$model_presupuesto,
//		));
            $model = Proyecto::model()->with('presupuestos', 'ingresos')->findByPk($id);
            if($model === null) throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
            $this->render('verPresupuesto', array(
                    'model' => $model,
                    ));
	}
        
              
        public function actionVer($id) {
            $model_proyecto = $this->loadModel($id, 'Proyecto');        
		$this->render('ver', array(
			'model_proyecto' => $model_proyecto,
		));
	}

	public function actionCreate() {
		$model = new Proyecto;

		$this->performAjaxValidation($model, 'proyecto-form');

		if (isset($_POST['Proyecto'])) {
			$model->setAttributes($_POST['Proyecto']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

        public function actionCrear() {
		$model = new Proyecto;
                $model->scenario = 'crear';
		if (isset($_POST['Proyecto'])) {
                        
			$model->setAttributes($_POST['Proyecto']);
//                        
//                        $model_comuna=$this->loadModel($model->comuna, 'Comuna');
//                        $model->comuna=$model_comuna->nombre;
//                        $model->region=$model_comuna->region->nombre;
//                        
                        $model->archivo=CUploadedFile::getInstance($model,'archivo');
                        $model->contrato=CUploadedFile::getInstance($model,'contrato');
                        
			if ($model->save()) {
                            //if(!empty($model->archivo) && !empty($model->contrato)){
                                $directorio = Yii::getPathOfAlias('archivos') . DIRECTORY_SEPARATOR . $model->id;
                                if(is_dir($directorio)) Funciones::rmdir($directorio);
                                if(mkdir($directorio, 0755)){
                                    if(!empty($model->archivo)) $model->archivo->saveAs($directorio . DIRECTORY_SEPARATOR . $model->archivo);
                                     if(!empty($model->contrato)) $model->contrato->saveAs($directorio. DIRECTORY_SEPARATOR. $model->contrato);
                                }
                           // }
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('presupuesto/crear', 'proyecto_id' => $model->id));
			}
		}

		$this->render('crear', array( 'model' => $model));
	}
        
	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Proyecto');
                
		$this->performAjaxValidation($model, 'proyecto-form');

		if (isset($_POST['Proyecto'])) {
			$model->setAttributes($_POST['Proyecto']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}
        
        public function actionActualizar($id) {
		$model = $this->loadModel($id, 'Proyecto');
                $model->scenario = 'actualizar';
                $archivo = $model->archivo;
                $contrato = $model->contrato;

		$this->performAjaxValidation($model, 'proyecto-form');

		if (isset($_POST['Proyecto'])) {
			$model->setAttributes($_POST['Proyecto']);
                        
                        $model->archivo=CUploadedFile::getInstance($model,'archivo')?:$model->archivo;
                        $model->contrato=CUploadedFile::getInstance($model,'contrato')?:$model->contrato;
			if ($model->save()) {
                                $directorio = Yii::getPathOfAlias('archivos') . DIRECTORY_SEPARATOR . $model->id;
                                if(!is_dir($directorio)) mkdir($directorio, 0755);
                                if($archivo !== $model->archivo) $model->archivo->saveAs($directorio . DIRECTORY_SEPARATOR . $model->archivo);
                                if($contrato !== $model->contrato) $model->contrato->saveAs($directorio. DIRECTORY_SEPARATOR . $model->contrato);
                                
				$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('actualizar', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Proyecto')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Proyecto');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
                $session = new CHttpSession;
                $session->open();
		$model = new Proyecto('search');
		$model->unsetAttributes();

		if (isset($_GET['Proyecto'])){
			$model->setAttributes($_GET['Proyecto']);
                }

                $session['Proyecto_model_search'] = $model;
                
		$this->render('admin', array(
			'model' => $model,
		));
	}
        
        public function actionVerTodos() {
                $session = new CHttpSession;
                $session->open();
                
		$model = new Proyecto('search');
		$model->unsetAttributes();

		if (isset($_GET['Proyecto'])){
			$model->setAttributes($_GET['Proyecto']);
                }

                $session['Proyecto_model_search'] = $model;
                
		$this->render('verTodos', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
        public function actionGenerateExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Proyecto_model_search']))
               {
                $model = $session['Proyecto_model_search'];
                $model = Proyecto::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Proyecto::model()->findAll();
             
             $this->toExcel($model, array('id', 'rrhh', 'centroCosto', 'nombre', 'descripcion', 'archivo', 'contrato', 'comuna', 'region'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGeneratePdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Proyecto_model_search']))
               {
                $model = $session['Proyecto_model_search'];
                $model = Proyecto::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Proyecto::model()->findAll();
             
             $this->toExcel($model, array('id', 'rrhh', 'centroCosto', 'nombre', 'descripcion', 'archivo', 'contrato', 'comuna', 'region'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}
        
//        public function actionPresupuestoVsIngreso($id){
//            //$model_proyecto = $this->loadModel($id, 'Proyecto');
//            $model = Proyecto::model()->with('presupuestos', 'ingresos')->findByPk($id);
//            if($model === null) throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
//            $this->render('presupuestoVsIngreso', array(
//                    'model' => $model,
//                    ));
//        }
}
