<?php

class SolicitudController extends GxController {

        public function filters() {
                return array('rights');
        }

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Solicitud'),
		));
	}
        
        public function validarCreacion(){
            
            $sql = "SELECT
                    Count(*)
                    FROM
                    `user`
                    LEFT JOIN solicitud ON `user`.id = solicitud.user_id
                    LEFT JOIN emision ON emision.solicitud_id = solicitud.id
                    WHERE
                    (user.id = ".Yii::app()->user->id.") AND (
                    (
                    solicitud.estado = 'PENDIENTE' OR
                    solicitud.estado = 'FINALIZADA'
                    )
                    OR
                    (
                    emision.estado = 'FINALIZADA' OR
                    emision.estado = 'PENDIENTE'
                    )
                    )

            ";
            $numSolicitudes = Yii::app()->db->createCommand($sql)->queryScalar();
            if($numSolicitudes>=3){
                return true;
            }else{
            return false;
            }
        }
        
        /***
         * Ver una solicitud desde el modulo de emision (Solo consulta)
         */
        public function actionVerSolicitudEmision($id) {
		$this->render('verSolicitudEmision', array(
			'model' => $this->loadModel($id, 'Solicitud'),
		));
	}
        public function actionFinalizar($id) {
                $model_solicitud = $this->loadModel($id, 'Solicitud');
                $model_solicitud->estado='FINALIZADA';
                $model_solicitud->save(false);
                Yii::app()->user->setFlash('info', '<strong>¡Listo!</strong>, La solicitud #'.$id.' ha sido finalizada');
                $this->redirect(array('ver', 'id' => $model_solicitud->id));
	}
        public function actionAnular($id) {
                $model_solicitud = $this->loadModel($id, 'Solicitud');
                $model_solicitud->estado='ANULADA';
                $model_solicitud->save(false);
                Yii::app()->user->setFlash('error', '<strong>¡Listo!</strong>, La solicitud #'.$id.' ha sido anulada');
                $this->redirect(array('ver', 'id' => $model_solicitud->id));
	}
        
        public function actionVer($id) {
            $model_solicitud = $this->loadModel($id, 'Solicitud');
            $model_items= new ItemSolicitud;
            $model_items=$model_items->misItems($id);
		$this->render('ver', array(
			'model' => $model_solicitud,
                        'model_items'=>$model_items,
		));
	}

        /***************************************+
         * Funcion para crear una solicitud, desde un proyecto.
         */
        public function actionCrear($proyecto_id) {
            
            
                $model_solicitud = new Solicitud;
                $model_solicitud->proyecto_id=$proyecto_id;
                $model_solicitud->user_id = Yii::app()->user->id;
                $model_solicitud->estado='PENDIENTE';

                if($this->validarCreacion()):
                    Yii::app()->user->setFlash('error', '<strong>¡Lo sentimos!</strong>, Usted solo puede tener hasta 3 solicitudes sin rendir.');
                    $this->render('crear', array( 'model' => $model_solicitud));
                    return;
                endif;
                
		$this->performAjaxValidation($model_solicitud, 'solicitud-form');

		if (isset($_POST['Solicitud'])) {
			$model_solicitud->setAttributes($_POST['Solicitud']);

			if ($model_solicitud->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('ver', 'id' => $model_solicitud->id));
			}
		}

		$this->render('crear', array( 'model' => $model_solicitud));
	}
        
        /***************************************+
         * Funcion para crear una solicitud, desde el modulo de solicitud.
         */
        public function actionCrearSolicitud() {
		$model = new Solicitud;
                
                $model->user_id=Yii::app()->user->id;
                $model->estado='PENDIENTE';

                if($this->validarCreacion()):
                    Yii::app()->user->setFlash('error', '<strong>¡Lo sentimos!</strong>, Usted solo puede tener hasta 3 solicitudes sin rendir.');
                    $this->render('crear', array( 'model' => $model));
                    return;
                endif;
		$this->performAjaxValidation($model, 'solicitud-form');

		if (isset($_POST['Solicitud'])) {
			$model->setAttributes($_POST['Solicitud']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('crearSolicitud', array( 'model' => $model));
	}
        
        
        public function actionFinalizadas() {
                $session = new CHttpSession;
                $session->open();
		$model_solicitud = new Solicitud('search');
		$model_solicitud->unsetAttributes();

		if (isset($_GET['Solicitud'])){
			$model_solicitud->setAttributes($_GET['Solicitud']);
                }

                $session['Solicitud_model_search'] = $model_solicitud;
                
		$this->render('finalizadas', array(
			'model' => $model_solicitud,
		));
	}
        
        
	public function actionCreate() {
		$model = new Solicitud;

		$this->performAjaxValidation($model, 'solicitud-form');
                $model_solicitud->user_id=Yii::app()->user->id;
                $model_solicitud->estado='PENDIENTE';
		if (isset($_POST['Solicitud'])) {
			$model->setAttributes($_POST['Solicitud']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Solicitud');

		$this->performAjaxValidation($model, 'solicitud-form');

		if (isset($_POST['Solicitud'])) {
			$model->setAttributes($_POST['Solicitud']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}
        
        public function actionActualizar($id) {
		$model = $this->loadModel($id, 'Solicitud');

		$this->performAjaxValidation($model, 'solicitud-form');

		if (isset($_POST['Solicitud'])) {
			$model->setAttributes($_POST['Solicitud']);

			if ($model->save()) {
				$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('actualizar', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Solicitud')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Solicitud');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
                $session = new CHttpSession;
                $session->open();
		$model = new Solicitud('search');
		$model->unsetAttributes();

		if (isset($_GET['Solicitud'])){
			$model->setAttributes($_GET['Solicitud']);
                }

                $session['Solicitud_model_search'] = $model;
                
		$this->render('admin', array(
			'model' => $model,
		));
	}
        
        public function actionVerTodos() {
                $session = new CHttpSession;
                $session->open();
		$model = new Solicitud('search');
		$model->unsetAttributes();

		if (isset($_GET['Solicitud'])){
			$model->setAttributes($_GET['Solicitud']);
                }

                $session['model_export']=$model->search()->getData();
                
		$this->render('verTodos', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
        public function actionGenerateExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Solicitud_model_search']))
               {
                $model = $session['Solicitud_model_search'];
                $model = Solicitud::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Solicitud::model()->findAll();
             
             $this->toExcel($model, array('id', 'proyecto', 'proveedor', 'user', 'fecha', 'tipo_solicitud', 'total', 'estado'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGeneratePdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Solicitud_model_search']))
               {
                $model = $session['Solicitud_model_search'];
                $model = Solicitud::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Solicitud::model()->findAll();
             
             $this->toExcel($model, array('id', 'proyecto', 'proveedor', 'user', 'fecha', 'tipo_solicitud', 'total', 'estado'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}
}
