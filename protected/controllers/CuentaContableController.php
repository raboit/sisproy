<?php

class CuentaContableController extends GxController {

        public function filters() {
                return array('rights');
        }

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'CuentaContable'),
		));
	}

        public function actionVer($id) {
		$this->render('ver', array(
			'model' => $this->loadModel($id, 'CuentaContable'),
		));
	}
        
	public function actionCreate() {
		$model = new CuentaContable;

		$this->performAjaxValidation($model, 'cuenta-contable-form');

		if (isset($_POST['CuentaContable'])) {
			$model->setAttributes($_POST['CuentaContable']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

        public function actionCrear() {
		$model = new CuentaContable;

		$this->performAjaxValidation($model, 'cuenta-contable-form');

		if (isset($_POST['CuentaContable'])) {
			$model->setAttributes($_POST['CuentaContable']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('crear', array( 'model' => $model));
	}
        
	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'CuentaContable');

		$this->performAjaxValidation($model, 'cuenta-contable-form');

		if (isset($_POST['CuentaContable'])) {
			$model->setAttributes($_POST['CuentaContable']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}
        
        public function actionActualizar($id) {
		$model = $this->loadModel($id, 'CuentaContable');

		$this->performAjaxValidation($model, 'cuenta-contable-form');

		if (isset($_POST['CuentaContable'])) {
			$model->setAttributes($_POST['CuentaContable']);

			if ($model->save()) {
				$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('actualizar', array(
				'model' => $model,
				));
	}

	/*public function actionActualizar($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'CuentaContable')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}*/

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('CuentaContable');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
                $session = new CHttpSession;
                $session->open();
		$model = new CuentaContable('search');
		$model->unsetAttributes();

		if (isset($_GET['CuentaContable'])){
			$model->setAttributes($_GET['CuentaContable']);
                }

                $session['CuentaContable_model_search'] = $model;
                
		$this->render('admin', array(
			'model' => $model,
		));
	}
        
        public function actionVerTodos() {
                $session = new CHttpSession;
                $session->open();
		$model = new CuentaContable('search');
		$model->unsetAttributes();

		if (isset($_GET['CuentaContable'])){
			$model->setAttributes($_GET['CuentaContable']);
                }

                $session['CuentaContable_model_search'] = $model;
                
		$this->render('verTodos', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
        public function actionGenerateExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['CuentaContable_model_search']))
               {
                $model = $session['CuentaContable_model_search'];
                $model = CuentaContable::model()->findAll($model->search()->criteria);
               }
               else
                 $model = CuentaContable::model()->findAll();
             
             $this->toExcel($model, array('id', 'nombre'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGeneratePdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['CuentaContable_model_search']))
               {
                $model = $session['CuentaContable_model_search'];
                $model = CuentaContable::model()->findAll($model->search()->criteria);
               }
               else
                 $model = CuentaContable::model()->findAll();
             
             $this->toExcel($model, array('id', 'nombre'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}
}